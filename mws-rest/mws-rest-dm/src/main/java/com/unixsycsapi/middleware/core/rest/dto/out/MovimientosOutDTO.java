/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import java.util.ArrayList;
import java.util.List;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class MovimientosOutDTO extends BaseOutRestDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3798725525510753661L;
	private List<MovimientoDTO> movimientos = new ArrayList<MovimientoDTO>();
	/**
	 * @return the movimientos
	 */
	public List<MovimientoDTO> getMovimientos() {
		return movimientos;
	}
	/**
	 * @param movimientos the movimientos to set
	 */
	public void setMovimientos(List<MovimientoDTO> movimientos) {
		this.movimientos = movimientos;
	}
}
