/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.DetalleTarjetaVirtualDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class DetalleTarjetaVirtualOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2427791063483091932L;
	private DetalleTarjetaVirtualDTO tarjeta;

	/**
	 * @return the tarjeta
	 */
	public DetalleTarjetaVirtualDTO getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta
	 *            the tarjeta to set
	 */
	public void setTarjeta(DetalleTarjetaVirtualDTO tarjeta) {
		this.tarjeta = tarjeta;
	}

}
