/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1235302814289202890L;
	private EnrolamientoDTO enrolamiento;

	/**
	 * @return the enrolamiento
	 */
	public EnrolamientoDTO getEnrolamiento() {
		return enrolamiento;
	}

	/**
	 * @param enrolamiento
	 *            the enrolamiento to set
	 */
	public void setEnrolamiento(EnrolamientoDTO enrolamiento) {
		this.enrolamiento = enrolamiento;
	}

}
