/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out.seguridad;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class SesionOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4514911249594671069L;
	private String idSession;
	/**
	 * @return the idSession
	 */
	public String getIdSession() {
		return idSession;
	}
	/**
	 * @param idSession the idSession to set
	 */
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}

}
