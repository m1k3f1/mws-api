/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in.seguridad;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class SesionInDTO extends BaseInRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -110312824021039901L;
	private String equipoID;

	/**
	 * @return the equipoID
	 */
	public String getEquipoID() {
		return equipoID;
	}

	/**
	 * @param equipoID the equipoID to set
	 */
	public void setEquipoID(String equipoID) {
		this.equipoID = equipoID;
	}

	
}
