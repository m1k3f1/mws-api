/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.RespuestaOperacionDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class ConfirmacionInDTO extends BaseInRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3634743914081975311L;
	private boolean estadoCorrecto;
	private MontoDTO monto;
	private TarjetaLeanDTO tarjeta;
	private RespuestaOperacionDTO operacion;

	/**
	 * @return the monto
	 */
	public MontoDTO getMonto() {
		return monto;
	}

	/**
	 * @param monto
	 *            the monto to set
	 */
	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}

	/**
	 * @return the tarjeta
	 */
	public TarjetaLeanDTO getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta
	 *            the tarjeta to set
	 */
	public void setTarjeta(TarjetaLeanDTO tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the operacion
	 */
	public RespuestaOperacionDTO getOperacion() {
		return operacion;
	}

	/**
	 * @param operacion the operacion to set
	 */
	public void setOperacion(RespuestaOperacionDTO operacion) {
		this.operacion = operacion;
	}

	/**
	 * @return the estadoCorrecto
	 */
	public boolean isEstadoCorrecto() {
		return estadoCorrecto;
	}

	/**
	 * @param estadoCorrecto the estadoCorrecto to set
	 */
	public void setEstadoCorrecto(boolean estadoCorrecto) {
		this.estadoCorrecto = estadoCorrecto;
	}


}
