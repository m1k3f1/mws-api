/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import java.util.ArrayList;
import java.util.List;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.ReferenciaBancariaDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class ReferenciasOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5638817360662254863L;
	private List<ReferenciaBancariaDTO> referencias = new ArrayList<ReferenciaBancariaDTO>();

	/**
	 * @return the referencias
	 */
	public List<ReferenciaBancariaDTO> getReferencias() {
		return referencias;
	}

	/**
	 * @param referencias
	 *            the referencias to set
	 */
	public void setReferencias(List<ReferenciaBancariaDTO> referencias) {
		this.referencias = referencias;
	}
}
