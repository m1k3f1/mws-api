/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.RespuestaOperacionDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class RespuestaOperacionOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8344425709150777408L;
	private RespuestaOperacionDTO respuestaOperacion;

	/**
	 * @return the respuestaOperacion
	 */
	public RespuestaOperacionDTO getRespuestaOperacion() {
		return respuestaOperacion;
	}

	/**
	 * @param respuestaOperacion the respuestaOperacion to set
	 */
	public void setRespuestaOperacion(RespuestaOperacionDTO respuestaOperacion) {
		this.respuestaOperacion = respuestaOperacion;
	}
}
