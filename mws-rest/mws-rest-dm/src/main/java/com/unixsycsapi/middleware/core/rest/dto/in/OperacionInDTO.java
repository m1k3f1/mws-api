/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.ReferenciaBancariaDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class OperacionInDTO extends BaseInRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3634743914081975311L;
	private MontoDTO monto;
	private TarjetaLeanDTO tarjeta;


	/**
	 * @return the monto
	 */
	public MontoDTO getMonto() {
		return monto;
	}

	/**
	 * @param monto the monto to set
	 */
	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}

	/**
	 * @return the tarjeta
	 */
	public TarjetaLeanDTO getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(TarjetaLeanDTO tarjeta) {
		this.tarjeta = tarjeta;
	}

}
