/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.DetalleMovimientoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class DetalleMovimientoOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -564846042734919591L;
	private DetalleMovimientoDTO movimiento;

	/**
	 * @return the movimiento
	 */
	public DetalleMovimientoDTO getMovimiento() {
		return movimiento;
	}

	/**
	 * @param movimiento the movimiento to set
	 */
	public void setMovimiento(DetalleMovimientoDTO movimiento) {
		this.movimiento = movimiento;
	}
}
