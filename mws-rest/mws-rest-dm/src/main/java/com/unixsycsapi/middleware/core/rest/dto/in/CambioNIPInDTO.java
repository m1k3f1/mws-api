/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class CambioNIPInDTO extends BaseInRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7888721132658959984L;
	private TarjetaLeanDTO tarjeta;
	private String nuevoNIP;
	/**
	 * @return the tarjeta
	 */
	public TarjetaLeanDTO getTarjeta() {
		return tarjeta;
	}
	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(TarjetaLeanDTO tarjeta) {
		this.tarjeta = tarjeta;
	}
	/**
	 * @return the nuevoNIP
	 */
	public String getNuevoNIP() {
		return nuevoNIP;
	}
	/**
	 * @param nuevoNIP the nuevoNIP to set
	 */
	public void setNuevoNIP(String nuevoNIP) {
		this.nuevoNIP = nuevoNIP;
	}
}
