/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class TarjetaVirtualOutDTO  extends BaseOutRestDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7928674536311446969L;
	private TarjetaVirtualDTO tarjeta;
	/**
	 * @return the tarjeta
	 */
	public TarjetaVirtualDTO getTarjeta() {
		return tarjeta;
	}
	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(TarjetaVirtualDTO tarjeta) {
		this.tarjeta = tarjeta;
	}
	

}
