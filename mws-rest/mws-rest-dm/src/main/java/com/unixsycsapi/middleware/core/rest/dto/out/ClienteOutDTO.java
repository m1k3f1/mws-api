/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class ClienteOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3825726492157417619L;
	private ClienteDTO cliente;
	private EnrolamientoDTO enrolamiento;
	/**
	 * @return the cliente
	 */
	public ClienteDTO getCliente() {
		return cliente;
	}
	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	/**
	 * @return the enrolamiento
	 */
	public EnrolamientoDTO getEnrolamiento() {
		return enrolamiento;
	}
	/**
	 * @param enrolamiento the enrolamiento to set
	 */
	public void setEnrolamiento(EnrolamientoDTO enrolamiento) {
		this.enrolamiento = enrolamiento;
	}

}
