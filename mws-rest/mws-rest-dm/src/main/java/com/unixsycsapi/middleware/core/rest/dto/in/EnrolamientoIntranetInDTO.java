/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoIntranetInDTO extends BaseInRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6774617389732830499L;
	private EnrolamientoValidadoDTO enrolamiento;

	/**
	 * 
	 */
	public EnrolamientoIntranetInDTO() {
	}

	/**
	 * @return the enrolamiento
	 */
	public EnrolamientoValidadoDTO getEnrolamiento() {
		return enrolamiento;
	}

	/**
	 * @param enrolamiento the enrolamiento to set
	 */
	public void setEnrolamiento(EnrolamientoValidadoDTO enrolamiento) {
		this.enrolamiento = enrolamiento;
	}

	

}
