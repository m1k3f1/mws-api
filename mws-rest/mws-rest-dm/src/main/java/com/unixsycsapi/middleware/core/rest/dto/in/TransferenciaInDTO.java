package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.ClienteLeanDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;

/**
 * @author angel
 *
 */
public class TransferenciaInDTO extends BaseInRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MontoDTO monto;
	private TarjetaLeanDTO tarjetaOrigen;
	private TarjetaLeanDTO tarjetaDestino;

	public MontoDTO getMonto() {
		return monto;
	}

	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}

	public TarjetaLeanDTO getTarjetaOrigen() {
		return tarjetaOrigen;
	}

	public void setTarjetaOrigen(TarjetaLeanDTO tarjetaOrigen) {
		this.tarjetaOrigen = tarjetaOrigen;
	}

	public TarjetaLeanDTO getTarjetaDestino() {
		return tarjetaDestino;
	}

	public void setTarjetaDestino(TarjetaLeanDTO tarjetaDestino) {
		this.tarjetaDestino = tarjetaDestino;
	}

}
