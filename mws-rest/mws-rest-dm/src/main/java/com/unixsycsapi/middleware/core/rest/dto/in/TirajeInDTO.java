/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class TirajeInDTO extends BaseInRestDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5745503857148373598L;
	private ArchivoDTO data;
	/**
	 * @return the data
	 */
	public ArchivoDTO getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(ArchivoDTO data) {
		this.data = data;
	}

}
