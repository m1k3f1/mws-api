/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;

/**
 * @author TATTVA
 *
 */
public class EnrolamientoInDTO extends BaseInRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6774617389732830499L;
	private EnrolamientoDTO enrolamiento;


	/**
	 * 
	 */
	public EnrolamientoInDTO() {
	}

	/**
	 * @return the enrolamiento
	 */
	public EnrolamientoDTO getEnrolamiento() {
		return enrolamiento;
	}

	/**
	 * @param enrolamiento the enrolamiento to set
	 */
	public void setEnrolamiento(EnrolamientoDTO enrolamiento) {
		this.enrolamiento = enrolamiento;
	}

}
