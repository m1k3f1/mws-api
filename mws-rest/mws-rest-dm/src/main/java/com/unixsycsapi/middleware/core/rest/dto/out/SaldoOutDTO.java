/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class SaldoOutDTO extends BaseOutRestDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1683793848825719096L;
	private MontoDTO monto;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss", timezone = "America/Mexico_City")
	private Date fecha;

	/**
	 * @return the monto
	 */
	public MontoDTO getMonto() {
		return monto;
	}

	/**
	 * @param monto
	 *            the monto to set
	 */
	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


}
