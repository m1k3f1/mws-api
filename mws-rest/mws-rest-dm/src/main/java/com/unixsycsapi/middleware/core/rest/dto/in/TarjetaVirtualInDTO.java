/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class TarjetaVirtualInDTO extends BaseInRestDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5211808917054183276L;
	
	private TarjetaVirtualDTO tarjeta;

	/**
	 * @return the tarjeta
	 */
	public TarjetaVirtualDTO getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(TarjetaVirtualDTO tarjeta) {
		this.tarjeta = tarjeta;
	}
}
