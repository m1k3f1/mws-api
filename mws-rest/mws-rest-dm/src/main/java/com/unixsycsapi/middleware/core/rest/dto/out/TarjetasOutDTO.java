/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.out;

import java.util.ArrayList;
import java.util.List;

import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class TarjetasOutDTO extends BaseOutRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4761066163428344872L;
	List<TarjetaLeanDTO> tarjetas = new ArrayList<TarjetaLeanDTO>();

	/**
	 * @return the tarjetas
	 */
	public List<TarjetaLeanDTO> getTarjetas() {
		return tarjetas;
	}

	/**
	 * @param tarjetas
	 *            the tarjetas to set
	 */
	public void setTarjetas(List<TarjetaLeanDTO> tarjetas) {
		this.tarjetas = tarjetas;
	}
}
