/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class ClienteInDTO extends BaseInRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6380314090995058479L;
	private ClienteDTO cliente;

	/**
	 * @return the cliente
	 */
	public ClienteDTO getCliente() {
		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
}
