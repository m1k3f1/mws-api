/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.dto.in;

import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoMixDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoMixInDTO extends BaseInRestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6774617389732830499L;
	private EnrolamientoMixDTO enrolamiento;

	/**
	 * 
	 */
	public EnrolamientoMixInDTO() {
	}

	/**
	 * @return the enrolamiento
	 */
	public EnrolamientoMixDTO getEnrolamiento() {
		return enrolamiento;
	}

	/**
	 * @param enrolamiento the enrolamiento to set
	 */
	public void setEnrolamiento(EnrolamientoMixDTO enrolamiento) {
		this.enrolamiento = enrolamiento;
	}


}
