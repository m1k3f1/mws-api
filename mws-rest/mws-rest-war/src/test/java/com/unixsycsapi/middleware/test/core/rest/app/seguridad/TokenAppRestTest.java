/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app.seguridad;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.middleware.core.rest.dto.in.seguridad.SesionInDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

/**
 * @author Vladimir Aguirre
 *
 */
public class TokenAppRestTest extends AbstractBaseAppRestTest {
//	private final static String SERVER = "localhost";
	private final static String SERVER = "192.168.238.50";

	public static void main(String[] args) {
		iniciar();
	}

	public static void iniciar() {
		final long timeIni = System.currentTimeMillis();

		Client client = Client.create();

		WebResource webResource = client.resource("http://" + SERVER + ":8080/mws/api/seguridad/tokens/iniciar");
		LOGGER.debug("" + webResource.toString());
		final Gson gson = getPrettyGson();
		SesionInDTO obj = new SesionInDTO();
		obj.setCabecero(getCabeceroOnlyApiKey());
		obj.setEquipoID("qwerty09874");

		String jsonInString = gson.toJson(obj);

		LOGGER.debug("jsonInString :: " + jsonInString);
		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
		if (response.getStatus() != 200) {
			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}

		LOGGER.debug("Output from Server .... \n");
		String output = response.getEntity(String.class);
		LOGGER.debug(output);
		final long timeFin = System.currentTimeMillis();
		LOGGER.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");

	}
}
