/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.CambioNIPInDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

/**
 * @author Vladimir Aguirre
 *
 */
public class NIPAppRestTest  extends AbstractBaseAppRestTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		cambiar();

	}
	private static void cambiar() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/NIP/cambiar");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final CambioNIPInDTO obj = new CambioNIPInDTO();

			TarjetaLeanDTO tar = new TarjetaLeanDTO();
			tar.setId(2L);
			obj.setTarjeta(tar);
			obj.setCabecero(getCabeceroCustomer());
			
			obj.setNuevoNIP("qcfhwufitofk74dh");
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
