/**
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

/**
 * @author Vladimir Aguirre
 *
 */
public class VersionAppRestTest extends AbstractBaseAppRestTest {
	private final static String SERVER = "172.19.223.10";
//	 private final static String SERVER = "192.168.238.50";
//	 private final static String SERVER = "127.0.0.1";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		getVersion();
		LOGGER.debug("================================================");
//		getEstado();

	}

	private static void getVersion() {
		try {

			Client client = Client.create();

			WebResource webRq = client.resource("http://" + SERVER + ":8096/mws/api/version/versionActual");
			
			LOGGER.debug(webRq.getURI());

			ClientResponse webRsp = webRq.type("application/xhtml+xml").get(ClientResponse.class);
			LOGGER.debug("webRsp.getStatus() :: " + webRsp.getStatus());
			if (webRsp.getStatus() != 200) {
				LOGGER.debug("responseFaltantes.Output from Server .... \n");
				String output = webRsp.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + webRsp.getStatus());
			}
			LOGGER.debug("webResourceFaltantes.Output from Server .... \n");
			String output = webRsp.getEntity(String.class);
			// LOGGER.debug("(" + output.length() + ")" + output);
			LOGGER.debug(output);

		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	private static void getEstado() {
		try {

			Client client = Client.create();

			WebResource webRq = client.resource("http://" + SERVER + ":8080/mws/api/version/estado");
			LOGGER.debug(webRq.getURI());

			ClientResponse webRsp = webRq.type("application/xhtml+xml").get(ClientResponse.class);
			LOGGER.debug("webRsp.getStatus() :: " + webRsp.getStatus());
			if (webRsp.getStatus() != 200) {
				LOGGER.debug("responseFaltantes.Output from Server .... \n");
				String output = webRsp.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + webRsp.getStatus());
			}
			LOGGER.debug("webResourceFaltantes.Output from Server .... \n");
			String output = webRsp.getEntity(String.class);
			// LOGGER.debug("(" + output.length() + ")" + output);
			LOGGER.debug(output);

		} catch (NullPointerException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
}
