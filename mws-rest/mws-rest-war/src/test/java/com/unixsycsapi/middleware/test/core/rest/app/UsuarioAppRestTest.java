/**
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.ClienteInDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

/**
 * @author Vladimir Aguirre
 *
 */
public class UsuarioAppRestTest extends AbstractBaseAppRestTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		alta();
		baja();

	}

	private static void alta() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/usuario/alta");

			final Gson gson = getGson();
			ClienteInDTO obj = new ClienteInDTO();
			ClienteDTO cli = new ClienteDTO();
			cli.setNombre("Vladimir");
			cli.setPrimerApellido("Pax");
			cli.setEmail("vlad.pax@gmail.com");
			cli.setNegocio("60321975");

			cli.setCelular(getCel());

			obj.setCabecero(getCabeceroSeguro());
			obj.setCliente(cli);

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: " + jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	private static void baja() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/usuario/baja");

			Gson gson = new Gson();
			ClienteInDTO obj = new ClienteInDTO();
			ClienteDTO cli = new ClienteDTO();
			cli.setIdCustomer("60321975");


			obj.setCabecero(getCabeceroCustomer());
			obj.setCliente(cli);

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: " + jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
