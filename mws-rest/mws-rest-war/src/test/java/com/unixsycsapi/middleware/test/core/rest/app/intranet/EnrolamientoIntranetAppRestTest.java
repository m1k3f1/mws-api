/**
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app.intranet;

import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.framework.commons.enums.Multimedias;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.TipoMultimediaDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.ClienteInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.EnrolamientoIntranetInDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoIntranetAppRestTest extends AbstractBaseAppRestTest {

	private final static String IMG_CREDENCIAL_PATH = "/home/angel/backup/Pictures/jesus.jpg";
	private final static String IMG_SELFIE_PATH = "/home/angel/backup/Pictures/selfie1.jpg";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		alta();
		// baja();

	}

	private static void alta() {
		try {
			final long timeIni = System.currentTimeMillis();

			Client client = Client.create();
//			 WebResource webResource = client.resource("http://localhost:8080/mws/api/intranet/enrolamiento/alta");
			WebResource webResource = client.resource("http://192.168.238.50:8080/mws/api/intranet/enrolamiento/alta");

			LOGGER.debug("" + webResource.toString());
			final Gson gson = getPrettyGson();
			EnrolamientoIntranetInDTO obj = new EnrolamientoIntranetInDTO();

			EnrolamientoValidadoDTO enrolamiento = new EnrolamientoValidadoDTO();
//			enrolamiento.setId(334L);
			ClienteDTO cli = new ClienteDTO();
			cli.setNombre("Vlad");
			cli.setPrimerApellido("Birth");
			cli.setSegundoApellido("Day");
			cli.setEmail("vlad.pax@gmail.com");
			cli.setNegocio("60321975");
			cli.setCurp("AUPV821003HMSGDL01");
			cli.setEmail("vlad.pax@gmail.com");
			cli.setFechaNacimiento(getDate(1981, 2, 9));
			enrolamiento.setClaveElector("APV0000");
			LOGGER.debug(getDate(1982, 3, 10));
			final ArchivoDTO archINE = new ArchivoDTO();
			archINE.setContenido(IOUtils.toByteArray(new FileInputStream(IMG_CREDENCIAL_PATH)));
			archINE.setNombre("credencial.png");
			LOGGER.debug("file :::: " + archINE.getContenido().length);
			TipoMultimediaDTO tipo = new TipoMultimediaDTO(Multimedias.PNG.getId());
			archINE.setTipo(tipo);
			final ArchivoDTO archSelfie = new ArchivoDTO();
			archSelfie.setContenido(IOUtils.toByteArray(new FileInputStream(IMG_SELFIE_PATH)));
			archSelfie.setNombre("selfie.png");
			LOGGER.debug("file :::: " + archSelfie.getContenido().length);
			archSelfie.setTipo(tipo);
//			 enrolamiento.setIdentificacion(archINE);
//			 enrolamiento.setFoto(archSelfie);
			enrolamiento.setCliente(cli);
			enrolamiento.setCanal("MovHF");
			enrolamiento.setIdProducto("50000002");
			enrolamiento.setIdInstitucion("1002");
//			enrolamiento.setNombreTitular("Vlad Pax A");
			enrolamiento.setTipoCliente("ENTTPERS");

			TarjetaLeanDTO tv = new TarjetaLeanDTO();
			tv.setNumero("0000123412340000");
			tv.setVencimiento("2909");
			enrolamiento.setTarjeta(tv);
			cli.setCelular(getCel());
			cli.setDireccion(getDireccion());

			enrolamiento.setCodigoValidacion("OK");
			enrolamiento.setSimilitud("70.99");
			obj.setCabecero(getCabeceroSeguro());
			obj.setEnrolamiento(enrolamiento);

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			final long timeFin = System.currentTimeMillis();
			LOGGER.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");
		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void baja() {
		try {

			Client client = Client.create();

//			WebResource webResource = client.resource("http://localhost:8080/mws/api/usuario/baja");
			WebResource webResource = client.resource("http://192.168.238.50:8080/mws/api/usuario/baja");

			Gson gson = new Gson();
			ClienteInDTO obj = new ClienteInDTO();
			ClienteDTO cli = new ClienteDTO();
			cli.setNegocio("60321975");

			obj.setCabecero(getCabeceroCustomer());
			obj.setCliente(cli);

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: " + jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
