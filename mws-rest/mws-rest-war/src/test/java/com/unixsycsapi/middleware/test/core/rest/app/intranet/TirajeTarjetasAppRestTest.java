package com.unixsycsapi.middleware.test.core.rest.app.intranet;

import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.framework.commons.enums.Multimedias;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.TipoMultimediaDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.TirajeInDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

public class TirajeTarjetasAppRestTest extends AbstractBaseAppRestTest {

	private final static String EMB_FILE_MANE = "UNIXSYC-01-00-20190221-100227.EMB";
	// private final static String EMB_FILE_MANE =
	// "UNIXSYC-01-00-20180912-110740.EMB";
	// private final static String EMB_FILE_PATH =
	// "C:\\VAP\\proyectos\\bpc\\UNIXSYC-01-00-20180904-092230.EMB";
	private final static String EMB_FILE_PATH = "/home/angel/Downloads/" + EMB_FILE_MANE;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		alta();
		// baja();

	}

	private static void alta() {
		try {
			final long timeIni = System.currentTimeMillis();

			Client client = Client.create();
			WebResource webResource = client.resource("http://192.168.238.50:8080/mws/api/intranet/tiraje/alta");
			// WebResource webResource =
			// client.resource("http://localhost:8080/mws/api/intranet/enrolamiento/alta");

			LOGGER.debug("" + webResource.toString());
			final Gson gson = getPrettyGson();
			TirajeInDTO obj = new TirajeInDTO();

			final ArchivoDTO fileEMB = new ArchivoDTO();
			fileEMB.setContenido(IOUtils.toByteArray(new FileInputStream(EMB_FILE_PATH)));
			fileEMB.setNombre(EMB_FILE_MANE);
			LOGGER.debug("file :::: " + fileEMB.getContenido().length);
			TipoMultimediaDTO tipo = new TipoMultimediaDTO(Multimedias.PNG.getId());
			fileEMB.setTipo(tipo);
			obj.setData(fileEMB);
			obj.setCabecero(getCabeceroSeguro());

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			final long timeFin = System.currentTimeMillis();
			LOGGER.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");
		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
