/**
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app;

import java.io.File;
import java.io.FileInputStream;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.MultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;
import com.unixsycsapi.framework.commons.enums.Multimedias;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoMixDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.TipoMultimediaDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.ClienteInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.EnrolamientoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.EnrolamientoMixInDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoMixAppRestTest extends AbstractBaseAppRestTest {

	private final static String IMG_CREDENCIAL_PATH = "/home/jesus/Pictures/prueba/ine.jpg";
	private final static String IMG_SELFIE_PATH = "/home/jesus/Pictures/prueba/selfie.jpg";

//	private final static String SERVER = "localhost";
	private final static String SERVER = "192.168.238.50";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// alta();
		altaPart();
		// baja();

	}

	
	/**
	 * https://gist.github.com/damianmcdonald/f4ccc7805305daf5691f
	 */
	private static void altaPart() {
		try {
			final long timeIni = System.currentTimeMillis();
			final String API_URI = "http://" + SERVER + ":8080/mws/api/enrolamiento/tarjeta/altaValidacionFacial/multi";

			final ClientConfig config = new DefaultClientConfig();
			final Client client = Client.create(config);

			WebResource webResource = client.resource(API_URI);
			// WebResource webResource = client
			// .resource("http://localhost:8080/mws/api/enrolamiento/altaValidacionFacial");
			LOGGER.debug("" + webResource.toString());
			final Gson gson = getPrettyGson();
			EnrolamientoMixInDTO obj = new EnrolamientoMixInDTO();

			EnrolamientoMixDTO enrolamiento = new EnrolamientoMixDTO();
			ClienteDTO cli = new ClienteDTO();
			cli.setNombre("Vladimir");
			cli.setPrimerApellido("Aguirre");
			cli.setSegundoApellido("Pax");
			cli.setEmail("vlad.pax@gmail.com");
			cli.setNegocio("60321975");
			cli.setCurp("AUPV821003HMSGDL01");
			cli.setEmail("vlad.pax@gmail.com");
			cli.setFechaNacimiento(getDate(1982, 3, 10));
			LOGGER.debug(getDate(1982, 3, 10));
			final ArchivoDTO archINE = new ArchivoDTO();
			// archINE.setContenido(IOUtils.toByteArray(new
			// FileInputStream(IMG_CREDENCIAL_PATH)));
			archINE.setNombre("ine_VAP.jpg");
			// LOGGER.debug("file :::: " + archINE.getContenido().length);
			TipoMultimediaDTO tipo = new TipoMultimediaDTO(Multimedias.JPG.getId());
			archINE.setTipo(tipo);
			final ArchivoDTO archSelfie = new ArchivoDTO();
			// archSelfie.setContenido(IOUtils.toByteArray(new
			// FileInputStream(IMG_SELFIE_PATH)));
			archSelfie.setNombre("selfie_VAP.jpg");
			// LOGGER.debug("file :::: " + archSelfie.getContenido().length);
			archSelfie.setTipo(tipo);
			enrolamiento.setIdentificacion(archINE);
			enrolamiento.setFoto(archSelfie);
			enrolamiento.setCliente(cli);
			enrolamiento.setCanal("MovHF");
			enrolamiento.setIdProducto("50000002");
			enrolamiento.setIdInstitucion("1002");
			enrolamiento.setNombreTitular("Vladimir Aguirre P");
			enrolamiento.setTipoCliente("ENTTPERS");

			cli.setCelular(getCel());
			cli.setDireccion(getDireccion());

			TarjetaLeanDTO tv = new TarjetaLeanDTO();
			tv.setNumero("0000123412340000");
			tv.setVencimiento("2909");
			enrolamiento.setTarjeta(tv);
			
			obj.setCabecero(getCabeceroSeguro());
			obj.setEnrolamiento(enrolamiento);

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);
			File fileToUploadCred = new File(IMG_CREDENCIAL_PATH);
			FileDataBodyPart fileDataBodyPartCreden = new FileDataBodyPart("archivoContenidoINE", fileToUploadCred,
					MediaType.APPLICATION_OCTET_STREAM_TYPE);
			fileDataBodyPartCreden.setContentDisposition(FormDataContentDisposition.name("archivoContenidoINE")
					.fileName(fileToUploadCred.getName()).build());

			File fileToUploadSelfie = new File(IMG_SELFIE_PATH);
			FileDataBodyPart fileDataBodyPartSelfie = new FileDataBodyPart("archivoContenidoSelfie", fileToUploadSelfie,
					MediaType.APPLICATION_OCTET_STREAM_TYPE);
			fileDataBodyPartSelfie.setContentDisposition(FormDataContentDisposition.name("archivoContenidoSelfie")
					.fileName(fileToUploadSelfie.getName()).build());

			final MultiPart multiPart = new FormDataMultiPart()
					.field("description", "Picture of Jabba the Hutt", MediaType.TEXT_PLAIN_TYPE)
					.field("body_json", jsonInString, MediaType.APPLICATION_JSON_TYPE)
					.field("filename", fileToUploadCred.getName(), MediaType.TEXT_PLAIN_TYPE)
					.bodyPart(fileDataBodyPartCreden).bodyPart(fileDataBodyPartSelfie);
			multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);

			MultipartFormDataOutput multipartFormDataOutput = new MultipartFormDataOutput();
			// multipartFormDataOutput.addFormData("archivoContenido", new
			// FileInputStream(IMG_CREDENCIAL_PATH),
			// MediaType.MULTIPART_FORM_DATA_TYPE, "ine_VAP.jpg");

			LOGGER.debug("jsonInString :: \n");
			LOGGER.debug(jsonInString);

			ClientResponse response = webResource.type("multipart/form-data").post(ClientResponse.class, multiPart);

			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			final long timeFin = System.currentTimeMillis();
			LOGGER.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");
		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void baja() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/usuario/baja");

			Gson gson = new Gson();
			ClienteInDTO obj = new ClienteInDTO();
			ClienteDTO cli = new ClienteDTO();
			cli.setNegocio("60321975");

			obj.setCabecero(getCabeceroCustomer());
			obj.setCliente(cli);

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: " + jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
