/**
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app;

import java.math.BigInteger;
import java.util.Calendar;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.middleware.core.dm.BancoDTO;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.ReferenciaBancariaDTO;
import com.unixsycsapi.middleware.core.dm.RespuestaOperacionDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.ClienteInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.ConfirmacionInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.DepositoBancarioInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.DepositoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.OperacionInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.TarjetaVirtualInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.TransferenciaInDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

/**
 * @author TATTVA
 *
 */
public class TarjetaVirtualAppRestTest extends AbstractBaseAppRestTest {

	public static void main(String[] args) {
		// crear();
//		 consultarTarjetas();
//		consultar();
		System.out.println("=====================================");
		System.out.println("=====================================");
		// bloquear();
		// activar();
		System.out.println("=====================================");
		System.out.println("=====================================");
//		 consultar();
		System.out.println("=====================================");
		System.out.println("=====================================");
//		consultarTarjetas();
		// referencias();
		// realizarDeposito();
		System.out.println("=====================================");
		System.out.println("=====================================");
//		 realizarCompra();
		System.out.println("=====================================");
		System.out.println("=====================================");
		// confirmarCompra();
//		realizarDeposito();
//		realizarTransferenciaTarjeta();
//		registrarDeposito();
		System.out.println("=====================================");
		System.out.println("=====================================");		
	}

	private static void crear() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/tarjetaVirtual/crear");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final TarjetaVirtualInDTO obj = new TarjetaVirtualInDTO();

			final TarjetaVirtualDTO tv = new TarjetaVirtualDTO();
			tv.setNombreTitular("Vladimir Aguirre P");
			ClienteDTO tit = new ClienteDTO();
			tv.setTitular(tit);
			tit.setIdCustomer(getCabeceroCustomer().getIdCustomer());
			obj.setCabecero(getCabeceroCustomer());
			obj.setTarjeta(tv);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void bloquear() {
		try {

			Client client = Client.create();
			// WebResource webResource =
			// client.resource("http://192.168.238.50:8080/mws/api/tarjetaVirtual/bloquear");
			WebResource webResource = client.resource("http://localhost:8080/mws/api/tarjetaVirtual/bloquear");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final TarjetaVirtualInDTO obj = new TarjetaVirtualInDTO();

			final TarjetaVirtualDTO tv = new TarjetaVirtualDTO();
			tv.setId(1L);
			obj.setCabecero(getCabeceroCustomer());
			obj.setTarjeta(tv);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void activar() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/tarjetaVirtual/activar");
			// WebResource webResource =
			// client.resource("http://192.168.238.50:8080/mws/api/tarjetaVirtual/activar");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final TarjetaVirtualInDTO obj = new TarjetaVirtualInDTO();

			final TarjetaVirtualDTO tv = new TarjetaVirtualDTO();
			tv.setId(1L);
			obj.setCabecero(getCabeceroCustomer());
			obj.setTarjeta(tv);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void consultar() {
		try {

			Client client = Client.create();
			WebResource webResource = client.resource("http://192.168.238.50:8080/mws/api/tarjetaVirtual/consultar");
//			 WebResource webResource =
//			 client.resource("http://172.19.223.10:8096/mws/api/tarjetaVirtual/consultar");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final TarjetaVirtualInDTO obj = new TarjetaVirtualInDTO();

			final TarjetaVirtualDTO tv = new TarjetaVirtualDTO();
			tv.setId(63L);
			obj.setCabecero(getCabeceroCustomer());
			obj.getCabecero().setIdCustomer("1");

			obj.setTarjeta(tv);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void consultarTarjetas() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://172.19.223.10:8096/mws/api/tarjetaVirtual/consultarTarjetas");
			// WebResource webResource =
			// client.resource("http://192.168.238.50:8080/mws/api/tarjetaVirtual/consultarTarjetas");

			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final ClienteInDTO obj = new ClienteInDTO();

			obj.setCabecero(getCabeceroCustomer());
			ClienteDTO cli = new ClienteDTO();
			cli.setId(1L);
			obj.getCabecero().setIdCustomer(cli.getId() + "");
			obj.setCliente(cli);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void referencias() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/tarjetaVirtual/referencias");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final TarjetaVirtualInDTO obj = new TarjetaVirtualInDTO();

			final TarjetaVirtualDTO tv = new TarjetaVirtualDTO();
			tv.setId(4567L);
			obj.setCabecero(getCabeceroCustomer());
			obj.setTarjeta(tv);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void realizarDeposito() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/tarjetaVirtual/realizarDeposito");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final DepositoInDTO obj = new DepositoInDTO();
			obj.setCabecero(getCabeceroCustomer());
			MontoDTO m = new MontoDTO();
			m.setMoneda(484);
//			m.setMonto(300.0);
			obj.setMonto(m);
			ReferenciaBancariaDTO ref = new ReferenciaBancariaDTO();
			BancoDTO b = new BancoDTO();
			b.setId(1L);
			// b.setClave("BBVA");
			ref.setBanco(b);
			ref.setReferencia("098787");
			obj.setReferencia(ref);
			final TarjetaLeanDTO tv = new TarjetaLeanDTO();
			tv.setId(4L);
			obj.setCabecero(getCabeceroCustomer());
			obj.getCabecero().setIdCustomer("4");
			obj.setTarjeta(tv);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void realizarCompra() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/tarjetaVirtual/realizarCompra");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final OperacionInDTO obj = new OperacionInDTO();
			obj.setCabecero(getCabeceroCustomer());
			MontoDTO m = new MontoDTO();
			m.setMoneda(484);
			BigInteger bi = new BigInteger("100");
			m.setMonto(bi);
			
			obj.setMonto(m);
			final TarjetaVirtualDTO tv = new TarjetaVirtualDTO();
			tv.setId(20L);
			obj.setTarjeta(tv);
			
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	private static void confirmarCompra() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://localhost:8080/mws/api/tarjetaVirtual/confirmarEstado");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final ConfirmacionInDTO obj = new ConfirmacionInDTO();
			obj.setEstadoCorrecto(true);
			obj.setCabecero(getCabeceroCustomer());
			MontoDTO m = new MontoDTO();
			m.setMoneda(484);
//			m.setMonto(300.0);
			obj.setMonto(m);
			final RespuestaOperacionDTO resp = new RespuestaOperacionDTO();
			resp.setAutorizacion("00092845672");
			resp.setFecha(UtilDates.getCurrentDate());
			resp.setNumeroAuditoria(74663);
			resp.setId(53483L);
			obj.setOperacion(resp);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	private static void realizarTransferenciaTarjeta() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://192.168.238.50:8080/mws/api/tarjetaVirtual/transferenciaTarjetas");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final TransferenciaInDTO obj = new TransferenciaInDTO();
			obj.setCabecero(getCabeceroCustomer());
			obj.getCabecero().setIdCustomer("1");
			
			MontoDTO m = new MontoDTO();
			m.setMonto(new BigInteger("1"));
			obj.setMonto(m);
			
			final TarjetaLeanDTO origen = new TarjetaLeanDTO();
			origen.setId(1L);
			obj.setTarjetaOrigen(origen);
			
			final TarjetaLeanDTO destino = new TarjetaLeanDTO();
			destino.setNumero("5064431000290923");
			obj.setTarjetaDestino(destino);
			
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	private static void registrarDeposito() {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("http://192.168.238.50:8080/mws/api/tarjetaVirtual/registrarDeposito");
			LOGGER.debug("" + webResource.toString());

			final Gson gson = getPrettyGson();
			final DepositoBancarioInDTO obj = new DepositoBancarioInDTO();
			obj.setCabecero(getCabeceroCustomer());
			MontoDTO m = new MontoDTO();
			m.setMoneda(484);
			m.setMonto(new BigInteger("1"));
			obj.setMonto(m);
			BancoDTO b = new BancoDTO();
			b.setId(1L);
			// b.setClave("BBVA");
			obj.setBanco(b);
			obj.setAutorizacion("009509");
			obj.setCuenta("*12345");
			Calendar c = Calendar.getInstance();
			c.set(Calendar.DATE, 20);
			c.set(Calendar.HOUR_OF_DAY,14);
			c.set(Calendar.MINUTE,5);
			obj.setFecha(c.getTime());
			obj.setMotivo("Recarga");
			obj.setFolio("001137576");
			final TarjetaLeanDTO tv = new TarjetaLeanDTO();
			tv.setId(4L);
			obj.setCabecero(getCabeceroCustomer());
			obj.getCabecero().setIdCustomer("4");
			obj.setTarjeta(tv);
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}	
	
}
