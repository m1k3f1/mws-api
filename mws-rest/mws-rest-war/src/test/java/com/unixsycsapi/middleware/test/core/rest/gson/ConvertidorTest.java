/**
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.gson;

import com.google.gson.Gson;
import com.unixsycsapi.middleware.core.rest.dto.in.ClienteInDTO;

/**
 * @author TATTVA
 *
 */
public class ConvertidorTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Gson gson = new Gson();
		ClienteInDTO obj = new ClienteInDTO();

		// 2. Java object to JSON, and assign to a String
		String jsonInString = gson.toJson(obj);
		System.out.println(jsonInString);
	}

}
