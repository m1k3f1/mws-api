/**
 * 
 */
package com.unixsycsapi.middleware.test.core.rest.app.base;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.framework.core.rest.dto.base.CabeceroRestDTO;
import com.unixsycsapi.middleware.core.dm.DireccionDTO;
import com.unixsycsapi.middleware.core.dm.NumeroCelularDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.seguridad.SesionInDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.seguridad.SesionOutDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public abstract class AbstractBaseAppRestTest {
	protected final static Logger LOGGER = LogManager.getLogger(AbstractBaseAppRestTest.class);
//	 private final static String SERVER = "172.19.223.10";
//	private final static String SERVER = "127.0.0.1";
	private final static String SERVER = "192.168.238.50";

	protected static CabeceroRestDTO getCabeceroSeguro() {
		final CabeceroRestDTO cab = new CabeceroRestDTO();
		cab.setApiKey("16639c9f7212d71ed1959337509fed6b1a8cff25");
		cab.setIdSession(iniciarSession());
		// cab.setIdSession("3STA3SUN4S3S10NT0T4LM3NT3F4KEDUMMYDUMMY");
		return cab;
	}

	protected static CabeceroRestDTO getCabeceroOnlyApiKey() {
		final CabeceroRestDTO cab = new CabeceroRestDTO();
		cab.setApiKey("16639c9f7212d71ed1959337509fed6b1a8cff25");
		return cab;
	}

	private static String iniciarSession() {
		final long timeIni = System.currentTimeMillis();

		Client client = Client.create();
		WebResource webResource = client.resource("http://" + SERVER + ":8080/mws/api/seguridad/tokens/iniciar");
//		WebResource webResource = client.resource("http://" + SERVER + ":8096/mws/api/seguridad/tokens/iniciar");
		LOGGER.debug("" + webResource.toString());
		final Gson gson = getPrettyGson();
		SesionInDTO obj = new SesionInDTO();
		obj.setCabecero(getCabeceroOnlyApiKey());
		obj.setEquipoID("qwerty09875");

		String jsonInString = gson.toJson(obj);

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
		if (response.getStatus() != 200) {
			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}

		String output = response.getEntity(String.class);
		LOGGER.debug(output);
		SesionOutDTO ses = gson.fromJson(output, SesionOutDTO.class);
		final long timeFin = System.currentTimeMillis();
		LOGGER.debug("idSesion [OK] en " + (timeFin - timeIni) + " ms .... \n");
		LOGGER.info("!!!                      [" + ses.getIdSession() + "]                      !!!\n");
		return ses.getIdSession();

	}

	protected static CabeceroRestDTO getCabeceroCustomer() {
		final CabeceroRestDTO cab = getCabeceroSeguro();
		cab.setIdCustomer("2");
		return cab;
	}

	protected static NumeroCelularDTO getCel() {
		NumeroCelularDTO cel = new NumeroCelularDTO();
		cel.setNumero("5560321975");
		cel.setCompania("ATT");
		return cel;
	}

	protected static DireccionDTO getDireccion() {
		DireccionDTO dir = new DireccionDTO();
		dir.setCalle("Porfirio Díaz");
		dir.setCiudad("México");
		dir.setColonia("Noche Búena");
		dir.setNoExterior("12");
		dir.setNoInterior("1D");
		dir.setCp("93000");
		dir.setPais("484");
		dir.setTipo("C");
		return dir;
	}

	protected static Date getDate(int yyyy, int mes, int dia) {
		int mesReal = mes++;
		final Calendar aux = Calendar.getInstance(TimeZone.getTimeZone("America/Mexico_City"));
		aux.set(Calendar.YEAR, yyyy);
		aux.set(Calendar.MONTH, mesReal);
		aux.set(Calendar.DATE, dia);
		aux.set(Calendar.HOUR_OF_DAY, 0);
		aux.set(Calendar.MINUTE, 0);
		aux.set(Calendar.SECOND, 1);
		aux.set(Calendar.MILLISECOND, 1);

		return aux.getTime();
	}

	protected static Gson getGson() {
		Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();
		return gson;
	}

	protected static Gson getPrettyGson() {
		LOGGER.debug("gson PrettyPrinting!");
		Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").setPrettyPrinting().create();
		return gson;
	}

}
