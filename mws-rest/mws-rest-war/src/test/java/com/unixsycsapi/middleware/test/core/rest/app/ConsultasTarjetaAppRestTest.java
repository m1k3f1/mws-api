package com.unixsycsapi.middleware.test.core.rest.app;

import java.util.Calendar;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.DetalleMovimientoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.MovimientosInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.SaldoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.MovimientosOutDTO;
import com.unixsycsapi.middleware.test.core.rest.app.base.AbstractBaseAppRestTest;

public class ConsultasTarjetaAppRestTest extends AbstractBaseAppRestTest {
	public static void main(String[] args) {
		System.out.println("=====================================");
		consultaSaldo();
		System.out.println("=====================================");
		System.out.println("=====================================");

//		consultaMovs();
//		consultaDetalleMov();
		System.out.println("=====================================");

	}

	private static void consultaSaldo() {
		try {
			final long timeIni = System.currentTimeMillis();

			Client client = Client.create();
//			WebResource webResource = client
//					.resource("http://localhost:8080/mws/api/consultas/saldo");
			WebResource webResource = client
					.resource("http://192.168.238.50:8080/mws/api/consultas/saldo");
			LOGGER.debug("" + webResource.toString());
			final Gson gson = getPrettyGson();
			SaldoInDTO obj = new SaldoInDTO();
			obj.setCabecero(getCabeceroSeguro());
			obj.getCabecero().setIdCustomer("10");
			TarjetaLeanDTO tar = new TarjetaLeanDTO();
			tar.setId(10L);
			obj.setTarjeta(tar);

			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			final long timeFin = System.currentTimeMillis();
			LOGGER.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");
		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	private static void consultaMovs() {
		try {
			final long timeIni = System.currentTimeMillis();

			Client client = Client.create();
//			WebResource webResource = client
//					.resource("http://172.19.223.10:8096/mws/api/consultas/movs");
			WebResource webResource = client
					.resource("http://192.168.238.50:8080/mws/api/consultas/movs");
//			WebResource webResource = client
//			.resource("http://localhost:8080/mws/api/consultas/movs");
			
			LOGGER.debug("" + webResource.toString());
			final Gson gson = getPrettyGson();
			MovimientosInDTO obj = new MovimientosInDTO();
			obj.setCabecero(getCabeceroSeguro());
			obj.getCabecero().setIdCustomer("10");
			TarjetaLeanDTO tar = new TarjetaLeanDTO();
			tar.setId(10L);
			obj.setTarjeta(tar);
			Calendar ini = Calendar.getInstance();
			ini.set(Calendar.DATE, 1);
			ini.set(Calendar.MONTH, Calendar.JANUARY);
			obj.setInicio(ini.getTime());

			Calendar fin = Calendar.getInstance();
			obj.setFin(fin.getTime());
			obj.setLimite(170);

			
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			MovimientosOutDTO outDto = gson.fromJson(output, MovimientosOutDTO.class);
			LOGGER.debug("" + outDto.getMovimientos());
			if (outDto.getMovimientos() != null) {
				LOGGER.debug("outDto.getMovimientos().size() :: " + outDto.getMovimientos().size());
				for (MovimientoDTO mov:outDto.getMovimientos()) {
					LOGGER.debug("mov :::: " + mov.getFecha());
					LOGGER.debug("monto :: " + mov.getMonto().getMonto());

				}
			}

			final long timeFin = System.currentTimeMillis();
			LOGGER.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");
		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	
	private static void consultaDetalleMov() {
		try {
			final long timeIni = System.currentTimeMillis();

			Client client = Client.create();
			WebResource webResource = client
					.resource("http://localhost:8080/mws/api/consultas/detalleMov");
			LOGGER.debug("" + webResource.toString());
			final Gson gson = getPrettyGson();
			DetalleMovimientoInDTO obj = new DetalleMovimientoInDTO();
			obj.setCabecero(getCabeceroCustomer());
			TarjetaLeanDTO tar = new TarjetaLeanDTO();
			tar.setId(1L);
			obj.setTarjeta(tar);
			MovimientoDTO m = new MovimientoDTO();
			m.setId(342342L);
			obj.setMovimiento(m);

			
			// 2. Java object to JSON, and assign to a String
			String jsonInString = gson.toJson(obj);

			LOGGER.debug("jsonInString :: ");
			LOGGER.debug(jsonInString);
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonInString);
			if (response.getStatus() != 200) {
				LOGGER.debug("Output from Server .... \n");
				String output = response.getEntity(String.class);
				LOGGER.debug(output);
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			LOGGER.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOGGER.debug(output);
			final long timeFin = System.currentTimeMillis();
			LOGGER.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");
		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
