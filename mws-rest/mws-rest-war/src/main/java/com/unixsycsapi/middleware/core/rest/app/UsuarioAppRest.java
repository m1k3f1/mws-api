/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.app;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.rest.dto.in.ClienteInDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/usuario")
public class UsuarioAppRest extends AbstractBaseRestApp{
	/**
	 * 
	 */
	private static final long serialVersionUID = -493042083187078916L;
	private final static Logger LOGGER = LogManager.getLogger(UsuarioAppRest.class);


	@POST
	@Path("/baja")
	@Consumes( MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response baja(ClienteInDTO cliente) {
		try {
			LOGGER.info("cliente :: " + cliente);
			return super.armarResponseOK();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}
	
	@Override
	protected void lookup() {
		// TODO Auto-generated method stub
		
	}

	
	
}
