/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.app.intranet;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.core.rest.dto.base.BaseOutRestDTO;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.api.TarjetaRemoteService;
import com.unixsycsapi.middleware.core.rest.dto.in.TirajeInDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/intranet/tiraje")
public class TirajeTarjetasAppRest extends AbstractBaseRestApp{
	private final static Logger LOGGER = LogManager.getLogger(TirajeTarjetasAppRest.class);

	private static TarjetaRemoteService service=null;
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3825909325918590405L;

	@POST
	@Path("/alta")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response guardarEnrolamiento(TirajeInDTO tirajeIn) {
		
		try {
			service.cargarTiraje(tirajeIn.getData());
			BaseOutRestDTO out = new BaseOutRestDTO();
			return super.armarResponseOK(out, tirajeIn);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e,tirajeIn);
		}
	}

	@Override
	protected void lookup() {
		if (service==null) {
			service=UtilRemoteServiceLocator.lookupGeneric(TarjetaRemoteService.class);
		}
		
	}
}
