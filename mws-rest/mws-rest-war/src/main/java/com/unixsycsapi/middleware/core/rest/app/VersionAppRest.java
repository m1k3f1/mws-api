/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.app;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.core.rest.dto.EstadoServicioOutDTO;
import com.unixsycsapi.framework.core.rest.dto.VersionOutDTO;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.api.infra.ParametroRemoteService;
import com.unixsycsapi.middleware.core.commons.enums.Params;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/version")
public class VersionAppRest extends AbstractBaseRestApp {
	/**
	 * 
	 */
	private static final long serialVersionUID = 462550585635088009L;
	private final static Logger LOGGER = LogManager.getLogger(VersionAppRest.class);
	private static ParametroRemoteService paramService;

	private final static String VERSION = "1.4.0-2018.12.27";
	private final static String NOMBRE_APP = "middleware-unixsyc";
	private static Date startup = null;

	@GET
	@Path("/versionActual")
	@Produces(MediaType.APPLICATION_JSON)
	public Response versionActual() {
		try {
			final VersionOutDTO dto = new VersionOutDTO();

			dto.setVersion(VERSION);
			final long sum = generarNumerico(dto.getVersion());
			dto.setNumero(sum);
			return super.armarResponseOK(dto);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@GET
	@Path("/estado")
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarEstadoApp() {
		try {
			final EstadoServicioOutDTO dto = new EstadoServicioOutDTO();
			dto.setNombre(NOMBRE_APP);
			dto.setVersion(VERSION);
			dto.setFechaStartup(startup);
			Map<String, Object> props = new HashMap<String, Object>();
			props.put(Params.SIS_PROV_NUBARIUM_FACIAL_ACTIVO.name(),
					paramService.consultarAsBoolean(Params.SIS_PROV_NUBARIUM_FACIAL_ACTIVO));
			dto.setPropiedades(props);
			return super.armarResponseOK(dto);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	private long generarNumerico(final String version) {
		if (version == null) {
			return 0;
		}
		final String[] dosPartes = version.split("-");
		final String[] apache = dosPartes[0].split("\\.");

		long sum = 0;
		int potenciaDecimal = 3;
		String seg = null;
		for (int i = 0; i < apache.length; i++) {
			seg = apache[i];
			sum += Long.parseLong(seg) * (Math.pow(10, potenciaDecimal));
			potenciaDecimal--;
		}
		final String[] dates = dosPartes[1].split("\\.");
		potenciaDecimal = 2;
		for (int i = 0; i < dates.length; i++) {
			seg = dates[i];
			sum += Long.parseLong(seg) * (Math.pow(10, potenciaDecimal));
			potenciaDecimal--;
		}
		return sum;
	}

	@Override
	protected void lookup() {
		try {
			if (paramService == null) {
				startup = UtilDates.getCurrentDate();
				paramService = UtilRemoteServiceLocator.lookupGeneric(ParametroRemoteService.class);
			}
		} catch (USException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
