/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.interpcetor;

import javax.ws.rs.WebApplicationException;

import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.Failure;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

/**
 * @author Vladimir Aguirre
 *
 */
public class MyPreProcessInterceptor implements PreProcessInterceptor {

	/* (non-Javadoc)
	 * @see org.jboss.resteasy.spi.interception.PreProcessInterceptor#preProcess(org.jboss.resteasy.spi.HttpRequest, org.jboss.resteasy.core.ResourceMethodInvoker)
	 */
	@Override
	public ServerResponse preProcess(HttpRequest request, ResourceMethodInvoker method)
			throws Failure, WebApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

}
