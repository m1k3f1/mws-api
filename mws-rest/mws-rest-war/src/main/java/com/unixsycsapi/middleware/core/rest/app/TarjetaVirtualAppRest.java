/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.app;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.api.TarjetaRemoteService;
import com.unixsycsapi.middleware.core.commons.enums.EstadosTransaccion;
import com.unixsycsapi.middleware.core.dm.BancoDTO;
import com.unixsycsapi.middleware.core.dm.ClienteLeanDTO;
import com.unixsycsapi.middleware.core.dm.DepositoBancarioDTO;
import com.unixsycsapi.middleware.core.dm.DetalleMovimientoDTO;
import com.unixsycsapi.middleware.core.dm.EstadoTransaccionDTO;
import com.unixsycsapi.middleware.core.dm.FiltroConsultaDTO;
import com.unixsycsapi.middleware.core.dm.ReferenciaBancariaDTO;
import com.unixsycsapi.middleware.core.dm.RespuestaOperacionDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.ClienteInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.ConfirmacionInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.DepositoBancarioInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.DepositoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.OperacionInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.TarjetaVirtualInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.TransferenciaInDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.DetalleTarjetaVirtualOutDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.ReferenciasOutDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.RespuestaOperacionOutDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.TarjetaVirtualOutDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.TarjetasOutDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/tarjetaVirtual")
public class TarjetaVirtualAppRest extends AbstractBaseRestApp {

	private static TarjetaRemoteService service = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 593740694548667211L;
	private final static Logger LOGGER = LogManager.getLogger(TarjetaVirtualAppRest.class);

	@POST
	@Path("/crear")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response alta(TarjetaVirtualInDTO tarjeta) {
		try {
			LOGGER.info("tarjeta :: " + tarjeta);
			final TarjetaVirtualOutDTO out = new TarjetaVirtualOutDTO();
			TarjetaVirtualDTO nvaTV = new TarjetaVirtualDTO();
			nvaTV.setId(4567L);
			nvaTV.setUltimoDigitos("98");

			out.setTarjeta(nvaTV);

			return super.armarResponseOK(out, tarjeta);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/bloquear")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response bloquear(TarjetaVirtualInDTO tarjeta) {
		try {

			LOGGER.info("tarjeta :: " + tarjeta);
			final TarjetaVirtualOutDTO out = new TarjetaVirtualOutDTO();

			final FiltroConsultaDTO filtro = new FiltroConsultaDTO();
			filtro.setApiKey(tarjeta.getCabecero().getApiKey());
			filtro.setTarjeta(tarjeta.getTarjeta());
			final ClienteLeanDTO cli = new ClienteLeanDTO(tarjeta.getCabecero().getIdCustomer());
			cli.setId(Long.parseLong(cli.getIdCustomer()));
			filtro.setCliente(cli);

			final TarjetaVirtualDTO tvDtoResp = service.bloquearByClient(filtro);
			out.setTarjeta(tvDtoResp);

			return super.armarResponseOK(out, tarjeta);

		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, tarjeta);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, tarjeta);
		}
	}

	@POST
	@Path("/activar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response activar(TarjetaVirtualInDTO tarjeta) {
		try {
			LOGGER.info("tarjeta :: " + tarjeta);
			final TarjetaVirtualOutDTO out = new TarjetaVirtualOutDTO();

			final FiltroConsultaDTO filtro = new FiltroConsultaDTO();
			filtro.setApiKey(tarjeta.getCabecero().getApiKey());
			filtro.setTarjeta(tarjeta.getTarjeta());
			final ClienteLeanDTO cli = new ClienteLeanDTO(tarjeta.getCabecero().getIdCustomer());
			cli.setId(Long.parseLong(cli.getIdCustomer()));
			filtro.setCliente(cli);

			final TarjetaVirtualDTO tvDtoResp = service.activarByClient(filtro);
			out.setTarjeta(tvDtoResp);

			return super.armarResponseOK(out, tarjeta);

		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, tarjeta);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, tarjeta);
		}
	}

	@POST
	@Path("/consultarTarjetas")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarTarjetas(ClienteInDTO tarjeta) {
		try {
			LOGGER.info("tarjeta :: " + tarjeta);
			final TarjetasOutDTO out = new TarjetasOutDTO();
			tarjeta.getCliente().setApiKey(tarjeta.getCabecero().getApiKey());
			List<TarjetaLeanDTO> data = service.consultarTarjetas(tarjeta.getCliente());
			out.setTarjetas(data);
			return super.armarResponseOK(out, tarjeta);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, tarjeta);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/consultar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultar(TarjetaVirtualInDTO tarjeta) {
		try {
			LOGGER.info("tarjeta :: " + tarjeta);
			final DetalleTarjetaVirtualOutDTO out = new DetalleTarjetaVirtualOutDTO();

			FiltroConsultaDTO filtro = new FiltroConsultaDTO();

			filtro.setApiKey(tarjeta.getCabecero().getApiKey());
			filtro.setCliente(tarjeta.getTarjeta().getTitular());
			filtro.setTarjeta(tarjeta.getTarjeta());

			out.setTarjeta(service.consultarTarjeta(filtro));

			return super.armarResponseOK(out, tarjeta);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, tarjeta);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/referencias")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarReferencias(TarjetaVirtualInDTO tarjeta) {
		try {
			LOGGER.info("tarjeta :: " + tarjeta);
			final ReferenciasOutDTO out = new ReferenciasOutDTO();

			List<ReferenciaBancariaDTO> refs = new ArrayList<ReferenciaBancariaDTO>();
			ReferenciaBancariaDTO unaRef = new ReferenciaBancariaDTO();
			unaRef.setId(44L);
			BancoDTO b = new BancoDTO();
			b.setId(1L);
			b.setClave("BBVA");
			b.setNombre("Bancomer");
			unaRef.setBanco(b);
			unaRef.setReferencia("098787");
			refs.add(unaRef);
			unaRef = new ReferenciaBancariaDTO();
			unaRef.setId(45L);
			b = new BancoDTO();
			b.setId(2L);
			b.setClave("BNT");
			b.setNombre("Banorte");
			unaRef.setBanco(b);
			unaRef.setReferencia("11098787");
			refs.add(unaRef);

			out.setReferencias(refs);
			return super.armarResponseOK(out, tarjeta);
//		} catch (USException e) {
//			LOGGER.error(e.getMessage());
//			return super.armarErrorResponse(e, tarjeta);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/realizarDeposito")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response realizarDeposito(DepositoInDTO inDto) {

		try {
			LOGGER.info("realizarDeposito :: " + inDto);
			RespuestaOperacionOutDTO out = new RespuestaOperacionOutDTO();
			DetalleMovimientoDTO detalle = new DetalleMovimientoDTO();
			final RespuestaOperacionDTO resp = new RespuestaOperacionDTO();

			final EstadoTransaccionDTO et = new EstadoTransaccionDTO();

			final FiltroConsultaDTO filtro = new FiltroConsultaDTO();
			filtro.setApiKey(inDto.getCabecero().getApiKey());
			filtro.setTarjeta(inDto.getTarjeta());
			final ClienteLeanDTO cliente = new ClienteLeanDTO();
			cliente.setId(Long.parseLong(inDto.getCabecero().getIdCustomer()));
			filtro.setCliente(cliente);

			detalle = service.realizarDeposito(filtro, inDto.getMonto());

			et.setId(1L);
			resp.setEstado(et);
			resp.setAutorizacion(detalle.getAutorizacion());
			resp.setNumeroAuditoria(detalle.getNumeroAuditoria());
			resp.setFecha(detalle.getFecha());
			resp.setId(detalle.getId());

			out.setRespuestaOperacion(resp);
			et.setNombre(out.getEstadoRespuesta().getDescripcion());
			et.setCodigo(out.getEstadoRespuesta().getCodigo());

			return super.armarResponseOK(out, inDto);

		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, inDto);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/realizarCompra")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response realizarCompra(OperacionInDTO inDto) {

		try {
			LOGGER.info("realizarCompraconTV :: " + inDto);
			RespuestaOperacionOutDTO out = new RespuestaOperacionOutDTO();
			DetalleMovimientoDTO detalle = new DetalleMovimientoDTO();
			final RespuestaOperacionDTO resp = new RespuestaOperacionDTO();

			final EstadoTransaccionDTO et = new EstadoTransaccionDTO();

			final FiltroConsultaDTO filtro = new FiltroConsultaDTO();
			filtro.setApiKey(inDto.getCabecero().getApiKey());
			filtro.setTarjeta(inDto.getTarjeta());
			final ClienteLeanDTO cliente = new ClienteLeanDTO();
			cliente.setId(Long.parseLong(inDto.getCabecero().getIdCustomer()));
			filtro.setCliente(cliente);

			detalle = service.realizarCompra(filtro, inDto.getMonto());

			et.setId(detalle.getId());
			resp.setEstado(et);
			resp.setAutorizacion(detalle.getAutorizacion());
			resp.setNumeroAuditoria(detalle.getNumeroAuditoria());
			resp.setFecha(detalle.getFecha());
			resp.setId(detalle.getId());

			out.setRespuestaOperacion(resp);
			et.setNombre(out.getEstadoRespuesta().getDescripcion());
			et.setCodigo(out.getEstadoRespuesta().getCodigo());

			return super.armarResponseOK(out, inDto);

		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, inDto);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/confirmarEstado")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response confirmarEstado(ConfirmacionInDTO inDto) {
		try {
			LOGGER.info("tarjeta :: " + inDto);
			RespuestaOperacionOutDTO out = new RespuestaOperacionOutDTO();
			out.setRespuestaOperacion(inDto.getOperacion());
			EstadoTransaccionDTO et = new EstadoTransaccionDTO();
			et.setCodigo("00");
			et.setNombre("APROBADA");
			et.setId(EstadosTransaccion.CONFIRMADA.getId());
			out.getRespuestaOperacion().setEstado(et);
//			FIXME VAP
			return super.armarResponseOK(out, inDto);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/transferenciaTarjetas")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response transferenciaP2P(TransferenciaInDTO inDto) {
		try {
			LOGGER.info("transferenciaP2P :: " + inDto);
			RespuestaOperacionOutDTO out = new RespuestaOperacionOutDTO();
			DetalleMovimientoDTO detalle = new DetalleMovimientoDTO();
			final RespuestaOperacionDTO resp = new RespuestaOperacionDTO();

			final EstadoTransaccionDTO et = new EstadoTransaccionDTO();

			final FiltroConsultaDTO origen = new FiltroConsultaDTO();
			origen.setApiKey(inDto.getCabecero().getApiKey());
			origen.setTarjeta(inDto.getTarjetaOrigen());
			
			final FiltroConsultaDTO destino = new FiltroConsultaDTO();
			destino.setApiKey(inDto.getCabecero().getApiKey());
			destino.setTarjeta(inDto.getTarjetaDestino());
			
			final ClienteLeanDTO cliente = new ClienteLeanDTO();
			cliente.setId(Long.parseLong(inDto.getCabecero().getIdCustomer()));
			origen.setCliente(cliente);

			detalle = service.transferenciaP2P(origen, destino, inDto.getMonto());

			et.setId(EstadosTransaccion.CORRECTO.getId());
			resp.setEstado(et);
			resp.setAutorizacion(detalle.getAutorizacion());
			resp.setNumeroAuditoria(detalle.getNumeroAuditoria());
			resp.setFecha(detalle.getFecha());
			resp.setId(detalle.getId());

			out.setRespuestaOperacion(resp);
			et.setNombre(out.getEstadoRespuesta().getDescripcion());
			et.setCodigo(out.getEstadoRespuesta().getCodigo());

			return super.armarResponseOK(out, inDto);

		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, inDto);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@POST
	@Path("/registrarDeposito")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registrarDeposito(DepositoBancarioInDTO inDto) {

		try {
			LOGGER.info("registrarDeposito :: " + inDto);
			RespuestaOperacionOutDTO out = new RespuestaOperacionOutDTO();
			DetalleMovimientoDTO detalle = new DetalleMovimientoDTO();
			final RespuestaOperacionDTO resp = new RespuestaOperacionDTO();

			final EstadoTransaccionDTO et = new EstadoTransaccionDTO();

			final FiltroConsultaDTO filtro = new FiltroConsultaDTO();
			filtro.setApiKey(inDto.getCabecero().getApiKey());
			filtro.setTarjeta(inDto.getTarjeta());
			final ClienteLeanDTO cliente = new ClienteLeanDTO();
			cliente.setId(Long.parseLong(inDto.getCabecero().getIdCustomer()));
			filtro.setCliente(cliente);
			final DepositoBancarioDTO dto = new DepositoBancarioDTO();
			dto.setBanco(inDto.getBanco());
			dto.setAutorizacion(inDto.getAutorizacion());
			dto.setCuenta(inDto.getCuenta());
			dto.setFechaDeposito(inDto.getFecha());
			dto.setFolio(inDto.getFolio());
			dto.setMonto(inDto.getMonto());
			dto.setMotivo(inDto.getMotivo());
			dto.setTarjeta(filtro.getTarjeta());
			detalle = service.registrarDeposito(filtro, dto);

			et.setId(EstadosTransaccion.CORRECTO.getId());
			resp.setEstado(et);
			resp.setAutorizacion(detalle.getAutorizacion());
			resp.setNumeroAuditoria(detalle.getNumeroAuditoria());
			resp.setFecha(detalle.getFecha());
			resp.setId(detalle.getId());

			out.setRespuestaOperacion(resp);
			et.setNombre(out.getEstadoRespuesta().getDescripcion());
			et.setCodigo(out.getEstadoRespuesta().getCodigo());

			return super.armarResponseOK(out, inDto);

		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, inDto);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}	
	
	@Override
	protected void lookup() {
		if (service == null) {
			service = UtilRemoteServiceLocator.lookupGeneric(TarjetaRemoteService.class);
		}

	}

}
