/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.TokenGenerator;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.api.EnrolamientoRemoteService;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.EnrolamientoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.EnrolamientoMixInDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/enrolamiento/tarjeta")
public class EnrolamientoMixAppRest extends AbstractBaseRestApp {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7043404345367610751L;

	private final static Logger LOGGER = LogManager.getLogger(EnrolamientoMixAppRest.class);

	private static EnrolamientoRemoteService enrolaService;


	

	@POST
	@Path("/altaValidacionFacial/multi")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response altaValidacionFacialMulti(MultipartFormDataInput multipartFormDataInput) {
		final long timeIni = System.currentTimeMillis();
		EnrolamientoMixInDTO enrolIn = null;
		String fileName = null;
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Inicia - altaValidacionFacialMulti(...)");
			}
			final Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();

			final Map<String, List<InputPart>> uploadForm = multipartFormDataInput.getFormDataMap();
			// Get file name
			final String jsonString = uploadForm.get("body_json").get(0).getBodyAsString();
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("jsonString :: \n");
				LOGGER.debug(jsonString);
			}
			enrolIn = gson.fromJson(jsonString, EnrolamientoMixInDTO.class);
			enrolIn.getCabecero().setTokenOperacion(TokenGenerator.generarToken(enrolIn.getCabecero()));

			final List<InputPart> inputPartsIne = uploadForm.get("archivoContenidoINE");
			if (inputPartsIne != null) {
				LOGGER.info("archivoContenidoINE OK");
			}
			final List<InputPart> inputPartsSelfie = uploadForm.get("archivoContenidoSelfie");
			if (inputPartsIne != null) {
				LOGGER.info("archivoContenidoSelfie OK");
			}


			final ArchivoDTO archDto = new ArchivoDTO();

			for (InputPart inputPart : inputPartsIne) {
				final MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);
				LOGGER.info("fileName :: " + fileName);
				// convert the uploaded file to inputstream
				final InputStream inputStream = inputPart.getBody(InputStream.class, null);
				byte[] archivoContenidoINE = IOUtils.toByteArray(inputStream);
				LOGGER.info("archivoContenidoINE.length :: " + archivoContenidoINE.length);
				archDto.setContenido(archivoContenidoINE);
				lookup();
				enrolIn.getEnrolamiento().getIdentificacion().setContenido(archivoContenidoINE);
			}

			for (InputPart inputPart : inputPartsSelfie) {
				final MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);
				LOGGER.info("fileName :: " + fileName);
				// convert the uploaded file to inputstream
				final InputStream inputStream = inputPart.getBody(InputStream.class, null);
				byte[] archivoContenidoSelfie = IOUtils.toByteArray(inputStream);
				LOGGER.info("archivoContenidoSelfie.length :: " + archivoContenidoSelfie.length);
				archDto.setContenido(archivoContenidoSelfie);
				lookup();
				enrolIn.getEnrolamiento().getFoto().setContenido(archivoContenidoSelfie);
			}
			enrolIn.getEnrolamiento().setApiKey(enrolIn.getCabecero().getApiKey());
			final EnrolamientoDTO data = enrolaService.guardarConValidacionFacialTarjeta(enrolIn.getEnrolamiento());

			if (LOGGER.isInfoEnabled()) {
				final long timeFin = System.currentTimeMillis();
				LOGGER.info("Fin - altaValidacionFacialMulti() en " + (timeFin - timeIni) + " ms");
			}
			// ECU-002-1.b. Hospital ha adjuntado un documento al expediente de
			// manera exitosa
			return Response.status(200).entity(data).build();

		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, enrolIn);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			return super.armarErrorResponse(e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return super.armarErrorResponse(e);
		}
	}

	private String getFileName(MultivaluedMap<String, String> multivaluedMap) {

		String[] contentDisposition = multivaluedMap.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {

			if ((filename.trim().startsWith("filename"))) {
				String[] name = filename.split("=");
				String exactFileName = name[1].trim().replaceAll("\"", "");
				return exactFileName;
			}
		}
		return "UnknownFile";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp#lookup()
	 */
	@Override
	protected void lookup() {
		if (enrolaService == null) {
			enrolaService = UtilRemoteServiceLocator.lookupGeneric(EnrolamientoRemoteService.class);
		}

	}

}
