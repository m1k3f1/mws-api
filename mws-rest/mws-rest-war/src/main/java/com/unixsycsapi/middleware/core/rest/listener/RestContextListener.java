/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author vladimir.aguirre
 *
 */
public class RestContextListener implements ServletContextListener {
	private static final Logger LOG = LogManager.getLogger(RestContextListener.class);
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.ServletContextListener#contextInitialized(javax.servlet.
	 * ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		if (LOG.isInfoEnabled()) {
			LOG.info("*******************************************");
			LOG.info("**                                       **");
			LOG.info("**  Adaptador REST/JSON inicializado...  **");
			LOG.info("**                                       **");
			LOG.info("*******************************************");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.
	 * ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if (LOG.isInfoEnabled()) {
			LOG.info("*************************************");
			LOG.info("**                                 **");
			LOG.info("**  Adaptador REST/JSON detenido!  **");
			LOG.info("**                                 **");
			LOG.info("*************************************");
		}

	}

}
