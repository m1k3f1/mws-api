/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.app;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.rest.dto.in.CambioNIPInDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/NIP")
public class NIPAppRest extends AbstractBaseRestApp{
	private final static Logger LOGGER = LogManager.getLogger(NIPAppRest.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = -7181004805882266736L;

	@POST
	@Path("/cambiar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response cambiar(CambioNIPInDTO tarjeta) {
		try {

			return super.armarResponseOK(tarjeta);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}
	
	@Override
	protected void lookup() {
		// TODO Auto-generated method stub
		
	}

}
