/**
 * 
 */
package com.unixsycsapi.middleware.core.rest.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.enums.CodigosRespuesta;
import com.unixsycsapi.framework.commons.exception.NegocioException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.core.rest.dto.base.CabeceroRestDTO;
import com.unixsycsapi.framework.core.rest.dto.base.EstadoRespuestaRestDTO;
import com.unixsycsapi.framework.util.TokenGenerator;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.api.EnrolamientoRemoteService;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.TipoMultimediaDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.EnrolamientoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.EnrolamientoOutDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/enrolamiento")
public class EnrolamientoAppRest extends AbstractBaseRestApp {
	private final static Logger LOGGER = LogManager.getLogger(EnrolamientoAppRest.class);

	private static EnrolamientoRemoteService enrolaService;

	/**
	 * 
	 */
	private static final long serialVersionUID = -6122613939765078182L;

	@POST
	@Path("/altaValidacionFacial")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response altaValidacionFacial(EnrolamientoInDTO enrolIn) {
		try {

			if (enrolIn.getEnrolamiento() != null && enrolIn.getEnrolamiento().getIdentificacion() != null
					&& enrolIn.getEnrolamiento().getIdentificacion().getContenido() != null) {
				LOGGER.info("Identificacion :::: " + enrolIn.getEnrolamiento().getIdentificacion());
				LOGGER.info(
						"Contenido.length :: " + enrolIn.getEnrolamiento().getIdentificacion().getContenido().length);
			}
			if (enrolIn.getEnrolamiento() != null && enrolIn.getEnrolamiento().getFoto() != null
					&& enrolIn.getEnrolamiento().getFoto().getContenido() != null) {
				LOGGER.info("Foto :::: " + enrolIn.getEnrolamiento().getFoto());
				LOGGER.info("Contenido.length :: " + enrolIn.getEnrolamiento().getFoto().getContenido().length);
			}
			if (enrolIn.getEnrolamiento() != null && enrolIn.getEnrolamiento().getCliente() != null
					&& enrolIn.getEnrolamiento().getCliente().getFechaNacimiento() != null) {
				LOGGER.info("cli.fechaNacimiento :: " + enrolIn.getEnrolamiento().getCliente().getFechaNacimiento());
			}
			if (enrolIn.getEnrolamiento() != null && enrolIn.getEnrolamiento().getCliente() != null
					&& enrolIn.getEnrolamiento().getCliente().getDireccion() != null) {
				LOGGER.info("cli.direcion :: " + enrolIn.getEnrolamiento().getCliente().getDireccion());
			}
			final EnrolamientoOutDTO out = new EnrolamientoOutDTO();

			enrolIn.getEnrolamiento().setApiKey(enrolIn.getCabecero().getApiKey());
			final EnrolamientoDTO data = enrolaService.guardarConValidacionFacial(enrolIn.getEnrolamiento());
			out.setEnrolamiento(data);
			return super.armarResponseOK(out, enrolIn);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, enrolIn);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, enrolIn);
		}
	}

	@POST
	@Path("/altaValidacionFacial/multi")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response altaValidacionFacialMulti(MultipartFormDataInput multipartFormDataInput) {
		final long timeIni = System.currentTimeMillis();
		EnrolamientoInDTO enrolIn = null;
		String fileName = null;
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Inicia - altaValidacionFacialMulti(...)");
			}
			lookup();

			final Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();

			final Map<String, List<InputPart>> uploadForm = multipartFormDataInput.getFormDataMap();
			// Get file name
			final String jsonString = uploadForm.get("body_json").get(0).getBodyAsString();
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("jsonString :: \n");
				LOGGER.info(jsonString);
			}
			enrolIn = gson.fromJson(jsonString, EnrolamientoInDTO.class);
			enrolIn.getCabecero().setTokenOperacion(TokenGenerator.generarToken(enrolIn.getCabecero()));

			final List<InputPart> inputPartsIne = uploadForm.get("archivoContenidoINE");
			if (inputPartsIne != null) {
				LOGGER.info("archivoContenidoINE OK");
			}
			final List<InputPart> inputPartsSelfie = uploadForm.get("archivoContenidoSelfie");
			if (inputPartsIne != null) {
				LOGGER.info("archivoContenidoSelfie OK");
			}


			final ArchivoDTO archDto = new ArchivoDTO();

			for (InputPart inputPart : inputPartsIne) {
				final MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);
				LOGGER.info("fileName :: " + fileName);
				// convert the uploaded file to inputstream
				final InputStream inputStream = inputPart.getBody(InputStream.class, null);
				final byte[] archivoContenidoINE = IOUtils.toByteArray(inputStream);
				LOGGER.info("archivoContenidoINE.length :: " + archivoContenidoINE.length);
				archDto.setContenido(archivoContenidoINE);
				enrolIn.getEnrolamiento().getIdentificacion().setContenido(archivoContenidoINE);
			}

			for (InputPart inputPart : inputPartsSelfie) {
				final MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);
				LOGGER.info("fileName :: " + fileName);
				// convert the uploaded file to inputstream
				final InputStream inputStream = inputPart.getBody(InputStream.class, null);
				final byte[] archivoContenidoSelfie = IOUtils.toByteArray(inputStream);
				LOGGER.info("archivoContenidoSelfie.length :: " + archivoContenidoSelfie.length);
				archDto.setContenido(archivoContenidoSelfie);
				enrolIn.getEnrolamiento().getFoto().setContenido(archivoContenidoSelfie);
			}
			enrolIn.getEnrolamiento().setApiKey(enrolIn.getCabecero().getApiKey());
			final EnrolamientoDTO data = enrolaService.guardarConValidacionFacial(enrolIn.getEnrolamiento());
			final EnrolamientoOutDTO out = new EnrolamientoOutDTO();
			out.setEstadoRespuesta(new EstadoRespuestaRestDTO(CodigosRespuesta.EXITO));
			out.setEnrolamiento(data);

			if (LOGGER.isInfoEnabled()) {
				final long timeFin = System.currentTimeMillis();
				LOGGER.info("Fin - altaValidacionFacialMulti() en " + (timeFin - timeIni) + " ms");
			}
			// ECU-002-1.b. Hospital ha adjuntado un documento al expediente de
			// manera exitosa
			return super.armarResponseOK(out, enrolIn);

		} catch (NegocioException e) {
			LOGGER.error(e.getMessage());
			if (e.getCodigo().equals(CodigosError.ENR_DATOS_NO_COINCIDEN)) {
				final EnrolamientoOutDTO out = new EnrolamientoOutDTO();
				out.setEstadoRespuesta(new EstadoRespuestaRestDTO(CodigosError.ENR_DATOS_NO_COINCIDEN));
				out.setEnrolamiento((EnrolamientoDTO) e.getDto());
				return super.armarResponseOK(out, enrolIn);
			}
			return super.armarErrorResponse(e, enrolIn);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, enrolIn);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			return super.armarErrorResponse(e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return super.armarErrorResponse(e);
		}
	}

	private String getFileName(MultivaluedMap<String, String> multivaluedMap) {

		String[] contentDisposition = multivaluedMap.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {

			if ((filename.trim().startsWith("filename"))) {
				String[] name = filename.split("=");
				String exactFileName = name[1].trim().replaceAll("\"", "");
				return exactFileName;
			}
		}
		return "UnknownFile";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp#lookup()
	 */
	@Override
	protected void lookup() {
		if (enrolaService == null) {
			enrolaService = UtilRemoteServiceLocator.lookupGeneric(EnrolamientoRemoteService.class);
		}

	}

}
