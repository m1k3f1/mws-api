/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.app;

import java.math.BigInteger;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.NegocioException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.api.TarjetaRemoteService;
import com.unixsycsapi.middleware.core.dm.ClienteLeanDTO;
import com.unixsycsapi.middleware.core.dm.DetalleMovimientoDTO;
import com.unixsycsapi.middleware.core.dm.FiltroConsultaDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;
import com.unixsycsapi.middleware.core.dm.SaldoDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.DetalleMovimientoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.MovimientosInDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.SaldoInDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.DetalleMovimientoOutDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.MovimientosOutDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.SaldoOutDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/consultas")
public class ConsultasTarjetaAppRest extends AbstractBaseRestApp {
	private final static Logger LOGGER = LogManager.getLogger(ConsultasTarjetaAppRest.class);

	private static TarjetaRemoteService service = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 9182472190996700666L;

	@POST
	@Path("/saldo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarSaldo(SaldoInDTO saldoIn) {
		try {
			LOGGER.info("saldoIn :: " + saldoIn);
			SaldoOutDTO out = new SaldoOutDTO();
			final FiltroConsultaDTO filtro = new FiltroConsultaDTO();
			filtro.setApiKey(saldoIn.getCabecero().getApiKey());
			filtro.setTarjeta(saldoIn.getTarjeta());
			final ClienteLeanDTO cli = new ClienteLeanDTO(saldoIn.getCabecero().getIdCustomer());
			cli.setId(Long.parseLong(cli.getIdCustomer()));
			filtro.setCliente(cli);
			final SaldoDTO saldo = service.consultarSaldo(filtro);

			out.setFecha(saldo.getFecha());
			out.setMonto(saldo.getMonto());

			return super.armarResponseOK(out, saldoIn);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, saldoIn);			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, saldoIn);
		}
	}

	@POST
	@Path("/movs")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarMovs(MovimientosInDTO movsIn) {
		try {
			final MovimientosOutDTO out = new MovimientosOutDTO();

			if (movsIn==null || movsIn.getTarjeta()==null || movsIn.getTarjeta().getId()==null) {
				throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_REQUERIDO, "La tarjeta es requerido");

			}
			final FiltroConsultaDTO filtro = new FiltroConsultaDTO();
			filtro.setApiKey(movsIn.getCabecero().getApiKey());
			filtro.setInicio(movsIn.getInicio());
			filtro.setFin(movsIn.getFin());
			filtro.setLimite(movsIn.getLimite());
			filtro.setTarjeta(movsIn.getTarjeta());
			final ClienteLeanDTO cli = new ClienteLeanDTO(movsIn.getCabecero().getIdCustomer());
			cli.setId(Long.parseLong(cli.getIdCustomer()));
			filtro.setCliente(cli);
			final List<MovimientoDTO> listaMovs = service.consultarMovimientos(filtro);
			out.setMovimientos(listaMovs);

			return super.armarResponseOK(out, movsIn);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, movsIn);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return super.armarErrorResponse(e, movsIn);
		}
	}
	
	@POST
	@Path("/detalleMov")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultarDetalleMov(DetalleMovimientoInDTO movsIn) {
		try {
			final DetalleMovimientoOutDTO out = new DetalleMovimientoOutDTO();

			DetalleMovimientoDTO mov = new DetalleMovimientoDTO();
			mov.setId(movsIn.getMovimiento().getId());
			mov.setAutorizacion("123123");
			mov.setFecha(UtilDates.getCurrentDate());
			MontoDTO mo= new MontoDTO();
			mo.setMoneda(484);
			mo.setMonto(BigInteger.valueOf(203));
			mov.setMonto(mo); 
			mov.setNumeroAuditoria(2131);
			mov.setTerminal("EP");
			out.setMovimiento(mov);
//			TODO JESUS conectar
			return super.armarResponseOK(out, movsIn);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e);
		}
	}

	@Override
	protected void lookup() {
		if (service == null) {
			service = UtilRemoteServiceLocator.lookupGeneric(TarjetaRemoteService.class);
		}

	}

}
