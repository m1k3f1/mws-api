/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.aop;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.NegocioException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.core.rest.dto.base.BaseInRestDTO;
import com.unixsycsapi.framework.core.rest.dto.base.CabeceroRestDTO;
import com.unixsycsapi.framework.core.rest.dto.base.ErrorRestDTO;
import com.unixsycsapi.framework.util.TokenGenerator;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.middleware.core.api.ApiKeyRemoteService;

/**
 * @author Vladimir Aguirre
 *
 */
@Provider
@ServerInterceptor
public class ApiKeyRESTInterceptor implements ContainerRequestFilter {
	private final static Logger LOG = LogManager.getLogger(ApiKeyRESTInterceptor.class);

	private static ApiKeyRemoteService apiKeyService = null;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		long timeIni = System.currentTimeMillis();
		lookup();
		String tknOp = null;
		if (isJson(requestContext)) {
			try {
				final String json = IOUtils.toString(requestContext.getEntityStream(), "UTF-8");
				// do whatever you need with json
				LOG.info("json :: " + json);
				final Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();
				final BaseInRestDTO bir = gson.fromJson(json, BaseInRestDTO.class);

				final CabeceroRestDTO cab = bir.getCabecero();
				LOG.info("cab :: " + cab);

				final JsonParser jp = new JsonParser();
				final JsonElement jelement = jp.parse(json);
				final JsonObject jobject = jelement.getAsJsonObject();
				final JsonObject cabeceroJS = jobject.getAsJsonObject("cabecero");
				LOG.info("cabeceroJS :: " + cabeceroJS);
				if (cabeceroJS == null) {
					requestContext.abortWith(this.armarErrorResponse(HttpStatus.SC_BAD_REQUEST,
							TokenGenerator.generarToken(), CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO));
				}
				tknOp = TokenGenerator.generarToken(cab);
				cabeceroJS.addProperty("tokenOperacion", tknOp);

				final String resultingJson = gson.toJson(jelement);
				// replace input stream for Jersey as we've already read it
				final InputStream in = IOUtils.toInputStream(resultingJson, "UTF-8");
				requestContext.setEntityStream(in);
				in.close();

				final Boolean val;
				final String urlIniciarSesion = "/seguridad/tokens/iniciar";
				final String urlrequestContext = String.valueOf(requestContext.getUriInfo().getAbsolutePath());
				LOG.info(" requestContext.getUriInfo().getAbsolutePath(): " + urlrequestContext);

				if (urlrequestContext.contains(urlIniciarSesion)) {
					val = apiKeyService.validar(cab.getApiKey());
				} else {
					LOG.info("Va a validar idSession");
					if(cab.getIdSession() == null || cab.getIdSession().isEmpty()) {
						throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_REQUERIDO, "idSession es requerido");
					}
					val = apiKeyService.validarSesion(cab.getApiKey(), cab.getIdSession());
					
				}

				if (val == null || !val.booleanValue()) {
					requestContext.abortWith(this.armarErrorResponse(HttpStatus.SC_BAD_REQUEST, tknOp,
							CodigosError.INFRA_APIKEY_NOEXISTE));
				}
			} catch (IOException ex) {
				LOG.error(ex.getMessage(), ex);
				throw new RuntimeException(ex);
			} catch (USException e) {
				LOG.error(e.getMessage(),e);
				requestContext.abortWith(this.armarErrorResponse(e, tknOp));
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new RuntimeException(e);

			}
		} else {
			LOG.info("No es JSON");
			if (isMultiPart(requestContext)) {
				LOG.info("Es multipart, la validación se hace mas adelante");
				
			}
		}
		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Interceptor ejecutado en " + (timeFin - timeIni) + " ms | Token generado :: " + tknOp);
		}
	}

	private boolean isJson(ContainerRequestContext request) {
		// define rules when to read body
		return request.getMediaType() != null && request.getMediaType().toString().contains("application/json");
	}

	private boolean isMultiPart(ContainerRequestContext request) {
		// define rules when to read body
		return request.getMediaType() != null && request.getMediaType().toString().contains("multipart/form-data");
	}

	private void lookup() {
		if (apiKeyService == null) {
			apiKeyService = UtilRemoteServiceLocator.lookupGeneric(ApiKeyRemoteService.class);
		}
	}

	protected Response armarErrorResponse(int status, String token, CodigosError code) {
		return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR)
				.entity(new ErrorRestDTO(new USException(code), false, token)).build();
	}
	
	protected Response armarErrorResponse(USException exception, String ope) {
		LOG.info(ope + " con resultado " + exception);
		return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR)
				.entity(new ErrorRestDTO(exception, true, ope)).build();
	}
}
