/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.rest.app.seguridad;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilRemoteServiceLocator;
import com.unixsycsapi.framework.web.rest.app.base.AbstractBaseRestApp;
import com.unixsycsapi.middleware.core.api.ApiKeyRemoteService;
import com.unixsycsapi.middleware.core.dm.TokenDTO;
import com.unixsycsapi.middleware.core.rest.dto.in.seguridad.SesionInDTO;
import com.unixsycsapi.middleware.core.rest.dto.out.seguridad.SesionOutDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Path("/seguridad/tokens")
public class TokenAppRest extends AbstractBaseRestApp {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8120056818738191419L;
	private final static Logger LOGGER = LogManager.getLogger(TokenAppRest.class);
	private static ApiKeyRemoteService service = null;

	@POST
	@Path("/iniciar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response iniciarSesion(SesionInDTO dataIn) {
		try {
			lookup();
			final SesionOutDTO out = new SesionOutDTO();
			final TokenDTO filtro = new TokenDTO();
			filtro.setApiKey(dataIn.getCabecero().getApiKey());
			filtro.setEquipoID(dataIn.getEquipoID());
			final TokenDTO token = service.iniciarSesion(filtro);
			out.setIdSession(token.getIdSession());
			return super.armarResponseOK(out, dataIn);
		} catch (USException e) {
			LOGGER.error(e.getMessage());
			return super.armarErrorResponse(e, dataIn);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return super.armarErrorResponse(e, dataIn);
		}

	}

	@Override
	protected void lookup() {
		if (service == null) {
			service = UtilRemoteServiceLocator.lookupGeneric(ApiKeyRemoteService.class);
		}
	}
}
