/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.provider;

import java.io.UnsupportedEncodingException;

import javax.ejb.Local;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.provider.clientws.nubarium.IneResponseDto;
import com.unixsycsapi.middleware.core.provider.clientws.nubarium.ReconocimientoFacialRespuestaDto;

/**
 * @author Vladimir Aguirre
 *
 */
@Local
public interface NubariumProviderService {

	/**
	 * 
	 * @param ine
	 * @return
	 * @throws USException
	 * @throws UnsupportedEncodingException 
	 */
	IneResponseDto validarINE(ArchivoDTO ine) throws USException, UnsupportedEncodingException;

	/**
	 * 
	 * @param ine
	 * @param selfie
	 * @return
	 * @throws USException
	 */
	ReconocimientoFacialRespuestaDto validarReconocimientoFacial(ArchivoDTO ine, ArchivoDTO selfie) throws USException;

}
