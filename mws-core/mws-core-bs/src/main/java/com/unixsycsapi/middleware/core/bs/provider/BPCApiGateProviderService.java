/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.provider;

import java.math.BigInteger;
import java.util.List;

import javax.ejb.Local;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.DetalleCompletoTarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.dm.DetalleMovimientoDTO;
import com.unixsycsapi.middleware.core.dm.EstadoTarjetaDTO;
import com.unixsycsapi.middleware.core.dm.FiltroConsultaDTO;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;
import com.unixsycsapi.middleware.core.dm.RespuestaCVV;
import com.unixsycsapi.middleware.core.dm.SaldoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.entity.TarjetaVirtual;

/**
 * Contrato para proveer conectividad con BPC
 * 
 * @author Vladimir Aguirre
 *
 */
@Local
public interface BPCApiGateProviderService {
	List<MovimientoDTO> consultarMovimientos(FiltroConsultaDTO tarjeta) throws USException;

	MovimientoDTO consultarMovimiento(MovimientoDTO tarjeta) throws USException;

	SaldoDTO consultarSaldo(TarjetaLeanDTO tarjeta) throws USException;

	EstadoTarjetaDTO activarTarjetaByClient(TarjetaVirtualDTO tarjeta) throws USException;

	EstadoTarjetaDTO bloquearTarjetaByClient(TarjetaVirtualDTO tarjeta) throws USException;

	/**
	 * Este metodo se debe invocar después del enrolamiento, para saber si la
	 * tarjeta ya se enrolo.<br>
	 * <code>cardStatusInquiry</code>
	 * 
	 * @param tarjeta
	 * @return
	 * @throws USException
	 */
	EstadoTarjetaDTO preguntarEstadoTarjeta(TarjetaVirtualDTO tarjeta) throws USException;

	/**
	 * <code>generateCVC2</code>
	 * 
	 * @param tarjeta
	 * @return
	 * @throws USException
	 */
	RespuestaCVV obtenerCVV(TarjetaVirtualDTO tarjeta) throws USException;

	/**
	 * <code>getCardData</code>
	 * 
	 * @param tarjeta
	 * @return
	 * @throws USException
	 */
	DetalleCompletoTarjetaVirtualDTO obtenerDetalle(TarjetaVirtualDTO tarjeta) throws USException;

	/**
	 * Realizar compra
	 * @param tvEnt
	 * @param monto
	 * @return
	 * @throws USException
	 */
	DetalleMovimientoDTO purchase(TarjetaVirtualDTO tvEnt, BigInteger monto) throws USException;
	
	/**
	 * Despositar efectivo
	 * @param tvEnt
	 * @param monto
	 * @return
	 * @throws USException
	 */
	DetalleMovimientoDTO cashDeposit(TarjetaVirtual tvEnt, BigInteger monto) throws USException;
	
	/**
	 * @param source
	 * @param destination
	 * @param amount
	 * @return
	 * @throws USException
	 */
	DetalleMovimientoDTO p2pTransfer(TarjetaVirtualDTO source, TarjetaVirtualDTO destination, BigInteger amount) throws USException;

}
