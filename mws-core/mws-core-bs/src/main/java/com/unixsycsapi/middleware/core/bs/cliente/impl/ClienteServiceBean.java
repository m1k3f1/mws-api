/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.cliente.impl;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.middleware.core.api.ApiKeyService;
import com.unixsycsapi.middleware.core.api.cliente.ClienteRemoteService;
import com.unixsycsapi.middleware.core.api.cliente.ClienteService;
import com.unixsycsapi.middleware.core.bs.cliente.impl.transformador.ClienteTransformer;
import com.unixsycsapi.middleware.core.commons.enums.EstadosCliente;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;
import com.unixsycsapi.middleware.core.dm.NumeroCelularDTO;
import com.unixsycsapi.middleware.core.dm.empresa.EmpresaDTO;
import com.unixsycsapi.middleware.core.ds.CelularDAO;
import com.unixsycsapi.middleware.core.ds.ClienteDAO;
import com.unixsycsapi.middleware.core.ds.DireccionDAO;
import com.unixsycsapi.middleware.core.ds.PersonaDAO;
import com.unixsycsapi.middleware.core.entity.Celular;
import com.unixsycsapi.middleware.core.entity.Cliente;
import com.unixsycsapi.middleware.core.entity.Direccion;
import com.unixsycsapi.middleware.core.entity.Empresa;
import com.unixsycsapi.middleware.core.entity.Persona;
import com.unixsycsapi.middleware.core.entity.estado.EstadoCliente;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class ClienteServiceBean implements ClienteRemoteService, ClienteService {
	private final static Logger LOG = LogManager.getLogger(ClienteServiceBean.class);

	@EJB
	private ApiKeyService apiKeyService;
	@EJB
	private ClienteDAO cliDao;

	@EJB
	private CelularDAO celDao;
	@EJB
	private PersonaDAO personDao;

	@EJB
	private DireccionDAO dirDao;

	@Override
	public ClienteDTO buscarCliente(ClienteDTO cli) throws USException {
		// TODO VAP desarrollar
		return null;

	}

	@Override
	public ClienteDTO guardarCliente(EnrolamientoDTO enrolamiento) throws USException {
		final ClienteDTO respCli = new ClienteDTO();
		try {
			long timeIni = System.currentTimeMillis();
			final Date sysDate = UtilDates.getCurrentDate();
			// TODO VAP desarrollar guardar cliente
			Persona entPer = null;
			if (enrolamiento.getCliente().getIdPersona() == null) {
				entPer = ClienteTransformer.transformaPersonaDTO(enrolamiento.getCliente());
				this.personDao.guardar(entPer);
			} else {
				entPer = this.personDao.consultar(enrolamiento.getCliente().getIdPersona());
			}

			final Cliente entCli = ClienteTransformer.transformaDTO(enrolamiento.getCliente());
			entCli.setNombreTitular(enrolamiento.getNombreTitular());

			final EmpresaDTO empDto = this.apiKeyService.recuperarEmpresa(enrolamiento.getApiKey());
			entCli.setEmpresa(new Empresa(empDto.getId()));
			entCli.setPersona(entPer);
			entCli.setEstado(new EstadoCliente(EstadosCliente.ACTIVO.getId()));

			this.cliDao.guardar(entCli);

			final NumeroCelularDTO numCel = enrolamiento.getCliente().getCelular();
			if (numCel != null) {
				final Celular celEnt = ClienteTransformer.transformarCelularDTO(numCel);
				celEnt.setCliente(entCli);
				celEnt.setActivo(Boolean.TRUE);
				celEnt.setFechaInicio(sysDate);
				celEnt.setFechaRegistro(sysDate);
				this.celDao.guardar(celEnt);
			}
			final Direccion dirEnt = ClienteTransformer
					.transformarDireccionDTO(enrolamiento.getCliente().getDireccion());
			dirEnt.setCliente(entCli);
			dirEnt.setTipoDomicilio("C");
			dirEnt.setActivo(Boolean.TRUE);
			dirEnt.setFechaInicio(sysDate);
			dirEnt.setFechaRegistro(sysDate);
			this.dirDao.guardar(dirEnt);

			respCli.setIdPersona(entPer.getId());
			respCli.setId(entCli.getId());
			if (LOG.isInfoEnabled()) {
				long timeFin = System.currentTimeMillis();
				LOG.info("Finaliza guardarCliente() en " + (timeFin - timeIni) + " ms");
			}
		} catch (USException e) {
			LOG.error(e.getMessage());
			throw e;
		}

		return respCli;
	}

}
