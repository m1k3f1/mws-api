package com.unixsycsapi.middleware.core.bs.infra.impl.transformador;

import com.unixsycsapi.middleware.core.dm.ParametroDTO;
import com.unixsycsapi.middleware.core.entity.infra.Parametro;

public class ParametroTransformador {

	public static ParametroDTO transformar(Parametro paramBD) {
		if (paramBD == null) {
			return null;
		}
		final ParametroDTO dto = new ParametroDTO();
		dto.setCache(paramBD.getCache());
		dto.setClave(paramBD.getClave());
		dto.setDescripcion(paramBD.getDescripcion());
		dto.setId(paramBD.getId());
		dto.setValor(paramBD.getValor());
		return dto;
	}
}
