/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.NegocioException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.AES256Crypto;
import com.unixsycsapi.framework.util.mail.SendEmail;
import com.unixsycsapi.middleware.core.api.TarjetaRemoteService;
import com.unixsycsapi.middleware.core.api.TarjetaService;
import com.unixsycsapi.middleware.core.api.infra.ParametroService;
import com.unixsycsapi.middleware.core.bs.TarjetasDisponiblesService;
import com.unixsycsapi.middleware.core.bs.impl.transformador.TarjetaTransformador;
import com.unixsycsapi.middleware.core.bs.provider.BPCApiGateProviderService;
import com.unixsycsapi.middleware.core.commons.enums.EstadosSVFE;
import com.unixsycsapi.middleware.core.commons.enums.EstadosTarjeta;
import com.unixsycsapi.middleware.core.commons.enums.EstadosTransaccion;
import com.unixsycsapi.middleware.core.commons.enums.Params;
import com.unixsycsapi.middleware.core.dm.ClienteLeanDTO;
import com.unixsycsapi.middleware.core.dm.DepositoBancarioDTO;
import com.unixsycsapi.middleware.core.dm.DepositoBancarioNotificacionDTO;
import com.unixsycsapi.middleware.core.dm.DetalleCompletoTarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.dm.DetalleMovimientoDTO;
import com.unixsycsapi.middleware.core.dm.DetalleTarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.dm.EstadoTarjetaDTO;
import com.unixsycsapi.middleware.core.dm.FiltroConsultaDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;
import com.unixsycsapi.middleware.core.dm.SaldoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.ds.BancoCuentaDAO;
import com.unixsycsapi.middleware.core.ds.DepositoBancarioDAO;
import com.unixsycsapi.middleware.core.ds.TarjetaVirtualDAO;
import com.unixsycsapi.middleware.core.entity.BancoCuenta;
import com.unixsycsapi.middleware.core.entity.DepositoBancario;
import com.unixsycsapi.middleware.core.entity.TarjetaDisponible;
import com.unixsycsapi.middleware.core.entity.TarjetaVirtual;
import com.unixsycsapi.middleware.core.entity.estado.EstadoTarjeta;
import com.unixsycsapi.middleware.core.entity.estado.EstadoTransaccion;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class TarjetaServiceBean implements TarjetaService, TarjetaRemoteService {
	private final static Logger LOG = LogManager.getLogger(TarjetaServiceBean.class);

	@EJB
	private TarjetasDisponiblesService tarjetasDispoService;

	@EJB
	private TarjetaVirtualDAO tarjetaVirtualDAO;

	@EJB
	private BPCApiGateProviderService apiGateProvider;

	@EJB
	private DepositoBancarioDAO depoBancarioDao;

	@EJB
	private BancoCuentaDAO bancoDao;

	@EJB
	private ParametroService paramService;

	private String MONTO_BITCLUB;
	private String PAN_BITCLUB;
	private String URL_NOTIFICACION_EBITWARE;
	private String AUTORIZACION_KEY;
	private String CRYPTO_SALT;
	
	@PostConstruct
	public void init() throws Exception {

		this.recuperaConfiguracion();

	}

	private void recuperaConfiguracion() throws USException {
		MONTO_BITCLUB = this.paramService.consultarAsString(Params.MONTO_BITCLUB);
		PAN_BITCLUB = this.paramService.consultarAsString(Params.PAN_BITCLUB);
		URL_NOTIFICACION_EBITWARE = this.paramService.consultarAsString(Params.URL_NOTIFICACION_EBITWARE);
		AUTORIZACION_KEY = this.paramService.consultarAsString(Params.AUTORIZACION_KEY);
		CRYPTO_SALT = this.paramService.consultarAsString(Params.CRYPTO_SALT);
	}

	@Override
	public void cargarTiraje(ArchivoDTO arch) throws USException {
		this.tarjetasDispoService.cargarTiraje(arch);
	}

	@Override
	public List<MovimientoDTO> consultarMovimientos(FiltroConsultaDTO filtro) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia consultarMovimientos()");
		}

		if (filtro.getInicio() == null || filtro.getFin() == null) {
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_REQUERIDO, "El periodo es requerido");
		}

		final List<MovimientoDTO> resp = new ArrayList<MovimientoDTO>();
		final TarjetaVirtual tvEnt = recuperarTarjetaVirtualValida(filtro.getTarjeta(), filtro.getCliente());

		final TarjetaVirtualDTO tv4Provider = new TarjetaVirtualDTO();
		tv4Provider.setNumero(tvEnt.getOrigen().getNumero());
		tv4Provider.setVencimiento(tvEnt.getOrigen().getVencimiento());
		tv4Provider.setId(filtro.getTarjeta().getId());
		filtro.setTarjeta(tv4Provider);
		final List<MovimientoDTO> data = apiGateProvider.consultarMovimientos(filtro);

		if (CollectionUtils.isNotEmpty(data)) {
			resp.addAll(data);
		}

		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza consultarMovimientos() en " + (timeFin - timeIni) + " ms");
		}
		return resp;
	}

	@Override
	public SaldoDTO consultarSaldo(FiltroConsultaDTO filtro) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia consultarSaldo()");
		}
		final TarjetaVirtual tvEnt = recuperarTarjetaVirtualValida(filtro.getTarjeta(), filtro.getCliente());

		final TarjetaVirtualDTO tv4Provider = new TarjetaVirtualDTO();
		tv4Provider.setNumero(tvEnt.getOrigen().getNumero());
		tv4Provider.setVencimiento(tvEnt.getOrigen().getVencimiento());
		tv4Provider.setId(filtro.getTarjeta().getId());
		final SaldoDTO saldoDto = apiGateProvider.consultarSaldo(tv4Provider);
		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza consultarSaldo() en " + (timeFin - timeIni) + " ms");
		}
		return saldoDto;
	}

	private TarjetaVirtual recuperarTarjetaVirtualValida(TarjetaLeanDTO tvLeanDto, ClienteLeanDTO cliDto)
			throws USException {
		final TarjetaVirtual tvEnt = tarjetaVirtualDAO.consultar(tvLeanDto.getId());
		if (tvEnt == null) {
			LOG.error("No existe tarjeta");
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO);

		}

		if (tvEnt.getCliente().getId().longValue() != cliDto.getId().longValue()) {
			LOG.error("Tarjeta no coincide con cliente");
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "Datos no coinciden");
		}

		sincronizarEstadoSV(tvLeanDto, tvEnt, false);
		final Boolean activa = isTarjetaActiva(tvLeanDto, tvEnt);
		if (!activa) {
			throw new NegocioException(CodigosError.NEGOCIO_TARJETA_INVALIDA);
		}

		return tvEnt;
	}

	private TarjetaVirtual recuperarTarjetaVirtual(TarjetaLeanDTO tvLeanDto, ClienteLeanDTO cliDto) throws USException {
		final TarjetaVirtual tvEnt = tarjetaVirtualDAO.consultar(tvLeanDto.getId());
		if (tvEnt == null) {
			LOG.error("No existe tarjeta " + tvLeanDto.getId());
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO);

		}

		if (tvEnt.getCliente().getId().longValue() != cliDto.getId().longValue()) {
			LOG.error("Tarjeta no coincide con cliente " + tvEnt.getCliente().getId());
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "Datos no coinciden");
		}

		sincronizarEstadoSV(tvLeanDto, tvEnt, false);

		return tvEnt;
	}

	private void sincronizarEstadoSV(TarjetaLeanDTO tvLeanDto, final TarjetaVirtual tvEnt, boolean aplicarRegla)
			throws USException, NegocioException {
		if (tvEnt.getEstado().getId().longValue() == EstadosTarjeta.ENROLADA.getId().longValue()) {
			// sigue enrolada
			final TarjetaVirtualDTO tv4Provider = new TarjetaVirtualDTO();
			tv4Provider.setNumero(tvEnt.getOrigen().getNumero());
			tv4Provider.setVencimiento(tvEnt.getOrigen().getVencimiento());
			tv4Provider.setId(tvLeanDto.getId());
			EstadoTarjetaDTO estado = this.apiGateProvider.preguntarEstadoTarjeta(tv4Provider);
			if (estado != null && estado.getIntSmartVistaStatus() != null
					&& estado.getIntSmartVistaStatus().intValue() == EstadosSVFE.VALID.getCodeIntSVFE()) {
//				DetalleCompletoTarjetaVirtualDTO detalle= this.apiGateProvider.obtenerDetalle(tv4Provider);
				// TODO VAP guardar la cuenta
				// se pasa a valida
				tvEnt.setEstado(new EstadoTarjeta(EstadosTarjeta.VALIDA.getId()));
				tarjetaVirtualDAO.modificar(tvEnt);
				LOG.warn("Tarjeta " + tvLeanDto.getId() + " ha sido marcada como " + EstadosTarjeta.VALIDA);
			} else {
				if (estado != null && estado.getIntSmartVistaStatus() != null
						&& estado.getIntSmartVistaStatus().intValue() == EstadosSVFE.NOT_PERMITTED.getCodeIntSVFE()
						&& aplicarRegla) {
					// interrupcion del flujo
					LOG.warn(CodigosError.NEGOCIO_TARJETA_ENROLADA + " " + tvEnt.getOrigen().getNumero());
					throw new NegocioException(CodigosError.NEGOCIO_TARJETA_ENROLADA);

				}
			}
		}
	}

	/**
	 * Hace la validacion de la tarjeta virtual en SV
	 * 
	 * @param tvLeanDto
	 * @param tvEnt
	 * @return <code>Boolean.TRUE</code> si en SV es <code>VALID</code>,
	 *         <code>Boolean.FALSE</code> en caso contrario
	 * @throws USException
	 */
	private Boolean isTarjetaActiva(TarjetaLeanDTO tvLeanDto, final TarjetaVirtual tvEnt) throws USException {
		try {
			final TarjetaVirtualDTO tv4Provider = new TarjetaVirtualDTO();
			tv4Provider.setNumero(tvEnt.getOrigen().getNumero());
			tv4Provider.setVencimiento(tvEnt.getOrigen().getVencimiento());
			tv4Provider.setId(tvLeanDto.getId());
			EstadoTarjetaDTO estado = this.apiGateProvider.preguntarEstadoTarjeta(tv4Provider);
			if (estado != null && estado.getIntSmartVistaStatus() != null
					&& estado.getIntSmartVistaStatus().intValue() == EstadosSVFE.VALID.getCodeIntSVFE()) {
				// se actualiza a VALIDA
				tvEnt.setEstado(new EstadoTarjeta(EstadosTarjeta.VALIDA.getId()));
				tarjetaVirtualDAO.modificar(tvEnt);
				return Boolean.TRUE;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public TarjetaVirtualDTO bloquearByClient(FiltroConsultaDTO filtro) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia bloquearByClient()");
		}
		final TarjetaVirtual tvEnt = recuperarTarjetaVirtual(filtro.getTarjeta(), filtro.getCliente());

		final TarjetaVirtualDTO tv4Provider = new TarjetaVirtualDTO();
		tv4Provider.setNumero(tvEnt.getOrigen().getNumero());
		tv4Provider.setVencimiento(tvEnt.getOrigen().getVencimiento());
		tv4Provider.setId(filtro.getTarjeta().getId());
		final EstadoTarjetaDTO response = apiGateProvider.bloquearTarjetaByClient(tv4Provider);
		EstadoTarjeta edo = new EstadoTarjeta(EstadosTarjeta.BLOQUEO_TEMPORAL_CLIENTE.getId());
		tvEnt.setEstado(edo);
		this.tarjetaVirtualDAO.modificar(tvEnt);
		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza bloquearByClient() en " + (timeFin - timeIni) + " ms");
		}
		return tv4Provider;
	}

	@Override
	public TarjetaVirtualDTO activarByClient(FiltroConsultaDTO filtro) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia activarByClient()");
		}
		final TarjetaVirtual tvEnt = recuperarTarjetaVirtual(filtro.getTarjeta(), filtro.getCliente());

		final TarjetaVirtualDTO tv4Provider = new TarjetaVirtualDTO();
		tv4Provider.setNumero(tvEnt.getOrigen().getNumero());
		tv4Provider.setVencimiento(tvEnt.getOrigen().getVencimiento());
		tv4Provider.setId(filtro.getTarjeta().getId());
		final EstadoTarjetaDTO response = apiGateProvider.activarTarjetaByClient(tv4Provider);
		EstadoTarjeta edo = new EstadoTarjeta(EstadosTarjeta.VALIDA.getId());
		tvEnt.setEstado(edo);
		this.tarjetaVirtualDAO.modificar(tvEnt);

		// TODO VAP registrar response en BD
		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza activarByClient() en " + (timeFin - timeIni) + " ms");
		}
		return tv4Provider;
	}

	@Override
	public List<TarjetaLeanDTO> consultarTarjetas(ClienteLeanDTO cliente) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia consultarTarjetas()");
		}
		final List<TarjetaLeanDTO> resp = new ArrayList<TarjetaLeanDTO>();
		List<TarjetaVirtual> data = this.tarjetaVirtualDAO.consultarTarjetasByCliente(cliente.getId());

		if (CollectionUtils.isNotEmpty(data)) {
			TarjetaLeanDTO dto = null;
			for (TarjetaVirtual row : data) {
				dto = new TarjetaLeanDTO();
				dto.setId(row.getId());
				this.sincronizarEstadoSV(dto, row, false);
				dto.setEstatus(row.getEstado().getCliente());
				resp.add(dto);
			}
		}

		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza consultarTarjetas() en " + (timeFin - timeIni) + " ms");
		}
		return resp;
	}

	@Override
	public DetalleTarjetaVirtualDTO consultarTarjeta(FiltroConsultaDTO filtro) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia consultarTarjeta()");
		}
		DetalleTarjetaVirtualDTO respuesta = null;

		final TarjetaVirtual tarjeVirtual = tarjetaVirtualDAO.consultar(filtro.getTarjeta().getId());

		if (tarjeVirtual == null) {
			LOG.error("No existe tarjeta " + filtro.getTarjeta().getId());
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO);
		}

		final TarjetaLeanDTO tarLean = new TarjetaLeanDTO();
		tarLean.setNombreTitular(tarjeVirtual.getCliente().getNombreTitular());
		tarLean.setNumero(tarjeVirtual.getOrigen().getNumero());
		tarLean.setEstatus(tarjeVirtual.getEstado().getActivo().toString());

		final Boolean activa = isTarjetaActiva(tarLean, tarjeVirtual);
		if (activa) {

			respuesta = new DetalleTarjetaVirtualDTO();
			respuesta.setId(tarjeVirtual.getId());
			respuesta.setNumero(tarjeVirtual.getOrigen().getNumero());
			respuesta.setVencimiento(tarjeVirtual.getOrigen().getVencimiento());
			TarjetaVirtualDTO tvDTO = new TarjetaVirtualDTO();
			tvDTO.setNumero(tarjeVirtual.getOrigen().getNumero());
			tvDTO.setVencimiento(tarjeVirtual.getOrigen().getVencimiento());

			respuesta.setCvc(apiGateProvider.obtenerCVV(tvDTO).getCvv());

			final DetalleCompletoTarjetaVirtualDTO detComTv = apiGateProvider.obtenerDetalle(tvDTO);
			respuesta.setNombreTitular(detComTv.getNombreTitular());
			respuesta.setNumeroEnmascarado(detComTv.getNumeroEnmascarado());
			respuesta.setCuenta(detComTv.getCuenta());

		} else {
			throw new NegocioException(CodigosError.NEGOCIO_TARJETA_INVALIDA);
		}
		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza consultarTarjeta() en " + (timeFin - timeIni) + " ms");
		}
		return respuesta;
	}

	@Override
	public DetalleMovimientoDTO realizarCompra(FiltroConsultaDTO filtro, MontoDTO monto) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia compra");
		}
		DetalleMovimientoDTO detMov = new DetalleMovimientoDTO();

		final TarjetaVirtual tvEnt = recuperarTarjetaVirtualValida(filtro.getTarjeta(), filtro.getCliente());
		final TarjetaVirtualDTO tv4Provider = new TarjetaVirtualDTO();
		tv4Provider.setNumero(tvEnt.getOrigen().getNumero());
		tv4Provider.setVencimiento(tvEnt.getOrigen().getVencimiento());
		tv4Provider.setId(filtro.getTarjeta().getId());
		filtro.setTarjeta(tv4Provider);
		detMov = apiGateProvider.purchase(tv4Provider, monto.getMonto());

		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza realizarCompra() en " + (timeFin - timeIni) + " ms");
		}
		return detMov;
	}

	@Override
	public DetalleMovimientoDTO realizarDeposito(FiltroConsultaDTO filtro, MontoDTO monto) throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia depo");
		}
		DetalleMovimientoDTO detMov = new DetalleMovimientoDTO();

		final TarjetaVirtual tvEnt = recuperarTarjetaVirtualValida(filtro.getTarjeta(), filtro.getCliente());

		detMov = apiGateProvider.cashDeposit(tvEnt, monto.getMonto());

		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza realizarCompra() en " + (timeFin - timeIni) + " ms");
		}
		return detMov;
	}

	@Override
	public DetalleMovimientoDTO transferenciaP2P(FiltroConsultaDTO origen, FiltroConsultaDTO destino, MontoDTO monto)
			throws USException {
		long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia transferenciaP2P");
		}
		DetalleMovimientoDTO detMov = new DetalleMovimientoDTO();

		TarjetaVirtualDTO source = new TarjetaVirtualDTO();
		source.setNumero(destino.getTarjeta().getNumero());
		LOG.info("Tarjeta origen " + origen.getTarjeta().getId());
		TarjetaVirtualDTO destination = new TarjetaVirtualDTO();
		destination.setNumero(destino.getTarjeta().getNumero());
		LOG.info("Tarjeta destino " + destino.getTarjeta().getId());

		detMov = apiGateProvider.p2pTransfer(source, destination, monto.getMonto());

		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza transferenciaP2P() en " + (timeFin - timeIni) + " ms");
		}
		return detMov;
	}

	@Override
	public DetalleMovimientoDTO registrarDeposito(FiltroConsultaDTO filtro, DepositoBancarioDTO depo)
			throws USException {
		final DetalleMovimientoDTO resp = new DetalleMovimientoDTO();

		if (depo.getMonto() == null || depo.getMonto().getMonto() == null
				|| depo.getMonto().getMonto().intValue() <= 0) {
			LOG.error("Monto incorrecto");
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "El monto es requerido");
		}
		if (depo.getBanco() == null || depo.getBanco().getId() == null) {
			LOG.error("Banco requerido");
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "El banco es requerido");
		}

		if (StringUtils.isBlank(depo.getAutorizacion())) {
			LOG.error("Autorizacion requerido");
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "Autorizacion es requerido");
		}

		if (depo.getFechaDeposito() == null) {
			LOG.error("Fecha requerido");
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "Fecha es requerido");
		}

		final TarjetaVirtual tarjeVirtual = tarjetaVirtualDAO.consultar(filtro.getTarjeta().getId());

		if (tarjeVirtual == null) {
			LOG.error("No existe tarjeta " + filtro.getTarjeta().getId());
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO);
		}

		final BancoCuenta banco = bancoDao.consultar(depo.getBanco().getId());

		if (banco == null) {
			LOG.error("No existe el banco " + depo.getBanco().getId());
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "El banco no existe");
		}
		final DepositoBancario ent = new DepositoBancario();
		ent.setFechaRegistro(new Date());
		ent.setFechaDeposito(depo.getFechaDeposito());
		ent.setFolio(depo.getFolio());
		ent.setBanco(new BancoCuenta(depo.getBanco().getId()));
		ent.setCuenta(depo.getCuenta());
		ent.setEstado(new EstadoTransaccion(EstadosTransaccion.REGISTRADA.getId()));
		ent.setAutorizacion(depo.getAutorizacion());
		ent.setMonto(depo.getMonto().getMonto());
		ent.setMotivo(depo.getMotivo());
		ent.setTarjeta(new TarjetaVirtual(filtro.getTarjeta().getId()));
		depoBancarioDao.guardar(ent);
		resp.setId(ent.getId());
		resp.setFecha(ent.getFechaRegistro());

		SendEmail.NotificationEmail(depo.getCuenta(), 2);

		return resp;
	}

	@Override
	public int registrarDepositoInterno(DepositoBancarioDTO depoDTO, String validation) throws USException {
		LOG.info("Iniciando deposito bancario");
		boolean bitclub = false;
		BigInteger montoDescuento = BigInteger.ZERO;
		DepositoBancario deposito = depoBancarioDao.consultar(depoDTO.getId());
		deposito.setEstado(new EstadoTransaccion(EstadosTransaccion.CONFIRMADA.getId()));

		String[] propNamesDepositos = { "tarjeta", "estado" };
		List<DepositoBancario> cantidadDepositos = depoBancarioDao.consultarByProperties(propNamesDepositos, deposito);
		LOG.info("cantidad de depositos: " + cantidadDepositos.size());
		LOG.info("cantidad" + deposito.getMonto().doubleValue());

		if (cantidadDepositos.size() < 2 && deposito.getMonto().doubleValue() >= 25000) {
			LOG.info("Reglas de Bitclub encontradas ");
			bitclub = true;
			montoDescuento = new BigInteger(MONTO_BITCLUB);
			final TarjetaVirtual tarjetaClub = new TarjetaVirtual();
			TarjetaDisponible disponibleTarjeta = new TarjetaDisponible();
			disponibleTarjeta.setNumero(PAN_BITCLUB);
			tarjetaClub.setOrigen(disponibleTarjeta);
			final TarjetaVirtualDTO tarjetaCliente = new TarjetaVirtualDTO();
			tarjetaCliente.setNumero(deposito.getTarjeta().getOrigen().getNumero());
			tarjetaCliente.setVencimiento(deposito.getTarjeta().getOrigen().getVencimiento());

			// Deposito a cuenta del cliente
			LOG.info("Depositando a cliente bitclub");
			apiGateProvider.cashDeposit(deposito.getTarjeta(), deposito.getMonto());
			// Compra de afiliacion a bitclub
			LOG.info("compra Bitclub cliente");
			apiGateProvider.purchase(tarjetaCliente, montoDescuento);
			// deposito a cuenta concentradora de bitclub
			LOG.info("Depositando a cuenta concentradora Bitclub");
			apiGateProvider.cashDeposit(tarjetaClub, montoDescuento);
		} else {
			LOG.info("Depositando a cliente");
			// Deposito a cuenta del cliente
			apiGateProvider.cashDeposit(deposito.getTarjeta(), deposito.getMonto());
		}
		depoBancarioDao.modificar(deposito);
		notificacionREST(deposito, bitclub, montoDescuento);
		return 1;
	}

	public List<DepositoBancarioDTO> getDepositoInterno() throws USException {
		DepositoBancario deposito = new DepositoBancario();
		deposito.setEstado(new EstadoTransaccion(EstadosTransaccion.REGISTRADA.getId()));
		String[] propNames = { "estado" };
		return TarjetaTransformador.transformar(depoBancarioDao.consultarByProperties(propNames, deposito));
	}

	private void notificacionREST(DepositoBancario deposito, boolean bitclub, BigInteger montoDescuento) {
		final long timeIni = System.currentTimeMillis();

		Client client = Client.create();
		WebResource webResource = client.resource(URL_NOTIFICACION_EBITWARE);
		final Gson gson = new Gson();
		DepositoBancarioNotificacionDTO notification = new DepositoBancarioNotificacionDTO();
		notification.setAmount(deposito.getMonto());
		notification.setBitclub(bitclub);
		notification.setBitclubAmount(montoDescuento);
		notification.setCardId(AES256Crypto.encrypt(deposito.getTarjeta().getId().toString(), CRYPTO_SALT));
		notification.setDate(new SimpleDateFormat("yyyy-MM-dd").format(deposito.getFechaDeposito()));
		notification.setTime(new SimpleDateFormat("HH:mm:ss").format(deposito.getFechaDeposito()));
		notification.setOperationId(AES256Crypto.encrypt(deposito.getId().toString(), CRYPTO_SALT));
		notification.setType("success");

		String jsonInString = gson.toJson(notification);

		LOG.debug("jsonInString :: " + jsonInString);
		ClientResponse response = webResource.type("application/json")
				.header("Authorization", AUTORIZACION_KEY)
				.post(ClientResponse.class, jsonInString);
		if (response.getStatus() != 200) {
			LOG.debug("Output from Server .... \n");
			String output = response.getEntity(String.class);
			LOG.debug(output);
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}

		LOG.debug("Output from Server .... \n");
		String output = response.getEntity(String.class);
		LOG.debug(output);
		final long timeFin = System.currentTimeMillis();
		LOG.debug("Fin - rest() en " + (timeFin - timeIni) + " ms");
	}
	
}
