/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs;

import javax.ejb.Local;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.entity.TarjetaDisponible;

/**
 * @author Vladimir Aguirre
 *
 */
@Local
public interface TarjetasDisponiblesService {

	TarjetaDisponible asignaTarjeta() throws USException;

	void desasignaTarjeta(TarjetaDisponible fallida) throws USException;
	
	void cargarTiraje(ArchivoDTO arch) throws USException;

}
