/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.compress.archivers.zip.PKWareExtraHeader.EncryptionAlgorithm;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.enums.CodigosRespuesta;
import com.unixsycsapi.framework.commons.exception.InfraestructuraException;
import com.unixsycsapi.framework.commons.exception.NegocioException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.commons.util.StringUtilBuilder;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.middleware.core.api.ApiKeyService;
import com.unixsycsapi.middleware.core.api.EnrolamientoRemoteService;
import com.unixsycsapi.middleware.core.api.EnrolamientoService;
import com.unixsycsapi.middleware.core.api.cliente.ClienteService;
import com.unixsycsapi.middleware.core.api.cliente.PersonaService;
import com.unixsycsapi.middleware.core.bs.TarjetasDisponiblesService;
import com.unixsycsapi.middleware.core.bs.impl.transformador.EnrolamientoTransformador;
import com.unixsycsapi.middleware.core.bs.impl.transformador.TarjetaTransformador;
import com.unixsycsapi.middleware.core.bs.provider.BPCApiGateProviderService;
import com.unixsycsapi.middleware.core.bs.provider.BPCBackOfficeProviderService;
import com.unixsycsapi.middleware.core.bs.provider.ContentManagerProviderService;
import com.unixsycsapi.middleware.core.bs.provider.NubariumProviderService;
import com.unixsycsapi.middleware.core.commons.enums.Documentos;
import com.unixsycsapi.middleware.core.commons.enums.EstadosEnrolamiento;
import com.unixsycsapi.middleware.core.commons.enums.EstadosTarjeta;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoMixDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.ds.ArchivoCMDAO;
import com.unixsycsapi.middleware.core.ds.ClienteDAO;
import com.unixsycsapi.middleware.core.ds.EnrolamientoDAO;
import com.unixsycsapi.middleware.core.ds.PersonaDAO;
import com.unixsycsapi.middleware.core.ds.TarjetaVirtualDAO;
import com.unixsycsapi.middleware.core.entity.Cliente;
import com.unixsycsapi.middleware.core.entity.Enrolamiento;
import com.unixsycsapi.middleware.core.entity.TarjetaDisponible;
import com.unixsycsapi.middleware.core.entity.TarjetaVirtual;
import com.unixsycsapi.middleware.core.entity.archivo.ArchivoCM;
import com.unixsycsapi.middleware.core.entity.archivo.TipoDocumento;
import com.unixsycsapi.middleware.core.entity.estado.EstadoEnrolamiento;
import com.unixsycsapi.middleware.core.entity.estado.EstadoTarjeta;
import com.unixsycsapi.middleware.core.provider.clientws.nubarium.IneResponseDto;
import com.unixsycsapi.middleware.core.provider.clientws.nubarium.ReconocimientoFacialRespuestaDto;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class EnrolamientoServiceBean implements EnrolamientoService, EnrolamientoRemoteService {
	private final static Logger LOG = LogManager.getLogger(EnrolamientoServiceBean.class);

	@EJB
	private ClienteService clientService;
	@EJB
	private PersonaService personaService;
	@EJB
	private BPCBackOfficeProviderService backOfficeService;

	@EJB
	private BPCApiGateProviderService apiGateService;

	@EJB
	private ApiKeyService apiKeyService;

	@EJB
	private EnrolamientoDAO enrolDao;

	@EJB
	private PersonaDAO personDao;

	@EJB
	private ClienteDAO cliDao;

	@EJB
	private TarjetasDisponiblesService tarjetasDispoService;

	@EJB
	private NubariumProviderService nubariumService;

	@EJB
	private TarjetaVirtualDAO tarjetaVirtualDao;

	@EJB
	private ContentManagerProviderService contentMan;

	@EJB
	private ArchivoCMDAO archCMDao;

	@Override
	public EnrolamientoDTO consultarEnrolamiento(EnrolamientoDTO enrolamiento) throws USException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EnrolamientoDTO guardar(EnrolamientoValidadoDTO enrolamiento) throws USException {
		final EnrolamientoDTO resp = new EnrolamientoDTO();
		validarGuardarRequest(enrolamiento);
		TarjetaDisponible disponible = null;
		try {
			long timeIni = System.currentTimeMillis();

			final boolean isNuevoEnrol = enrolamiento.getId() == null;
			Enrolamiento enrEnt = null;
			if (isNuevoEnrol) {
				enrEnt = EnrolamientoTransformador.transformarDTO(enrolamiento);
				enrEnt.setIntentos(new Integer(1));
			} else {
				enrEnt = this.enrolDao.consultar(enrolamiento.getId());
				if (enrEnt == null) {
					throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
							"No existe proceso de enrolamiento");
				}
				enrEnt.setIntentos(new Integer((enrEnt.getIntentos().intValue() + 1)));
			}
			final ClienteDTO per = this.personaService.buscarPersona(enrolamiento.getCliente());
			if (per != null) {
				enrEnt.setEstado(new EstadoEnrolamiento(EstadosEnrolamiento.ABORTADO_PERSONA_EXISTE.getId()));
				enrolamiento.getCliente().setIdPersona(per.getIdPersona());
			}
			final ClienteDTO cli = this.clientService.buscarCliente(enrolamiento.getCliente());
			if (cli == null) {
				final ClienteDTO cliSaved = this.clientService.guardarCliente(enrolamiento);
				enrEnt.setCliente(new Cliente(cliSaved.getId()));
			} else {
				// TODO VAP update cliente (domicilio-celular)
				enrEnt.setEstado(new EstadoEnrolamiento(EstadosEnrolamiento.ABORTADO_CLIENTE_EXISTE.getId()));
			}

			if (enrEnt.getTarjeta() == null) {
				disponible = this.tarjetasDispoService.asignaTarjeta();
				enrolamiento.setTarjeta(TarjetaTransformador.transformar(disponible));
				enrolamiento.setId(enrEnt.getId());
				enrEnt.setTarjeta(disponible);

				Date fechaRegistro = UtilDates.getCurrentDate();
				final EstadoTarjeta edoTarjeta = new EstadoTarjeta();
				edoTarjeta.setId(EstadosTarjeta.ENROLADA.getId());

				TarjetaVirtual tarVirt = new TarjetaVirtual();
				tarVirt.setCliente(enrEnt.getCliente());
				tarVirt.setEstado(edoTarjeta);
				tarVirt.setNumeroEnmascarado("0000");
				tarVirt.setCardId("0000");
				tarVirt.setFechaRegistro(fechaRegistro);
				tarVirt.setOrigen(enrEnt.getTarjeta());

				tarjetaVirtualDao.guardar(tarVirt);
			} else {
				// TODO VAP hacer algo con la tarjeta

			}

			ArchivoDTO iden = contentMan.guardarArchivo(enrolamiento.getIdentificacion());
			ArchivoDTO self = contentMan.guardarArchivo(enrolamiento.getFoto());

			if (iden != null || self != null) {
				ArchivoCM idenCM = new ArchivoCM();
				ArchivoCM selfCM = new ArchivoCM();
				LOG.info("enrolamiento.getIdentificacion() :: " + enrolamiento.getIdentificacion());
				TipoDocumento tipoD = new TipoDocumento(Documentos.IDENTIFICACION.getId());

				idenCM.setIdCM(String.valueOf(iden.getId()));
				idenCM.setFechaModificacion(null);
				idenCM.setFechaRegistro(UtilDates.getCurrentDate());
				idenCM.setNombre(enrolamiento.getIdentificacion().getNombre());
				idenCM.setNumeroVersion(1);
				idenCM.setTipoDocumento(tipoD);
				LOG.info("enrolamiento.getFoto() :: " + enrolamiento.getFoto());
				tipoD = new TipoDocumento(Documentos.SELFIE.getId());
				selfCM.setIdCM(String.valueOf(self.getId()));
				selfCM.setFechaModificacion(null);
				selfCM.setFechaRegistro(UtilDates.getCurrentDate());
				selfCM.setNombre(enrolamiento.getFoto().getNombre());
				selfCM.setNumeroVersion(1);
				selfCM.setTipoDocumento(tipoD);

				archCMDao.guardar(idenCM);
				archCMDao.guardar(selfCM);

				enrEnt.setArchivoINE(idenCM);
				enrEnt.setArchivoSelfie(selfCM);

			}

			if (isNuevoEnrol) {
				this.enrolDao.guardar(enrEnt);
			} else {
				this.enrolDao.modificar(enrEnt);
			}

			enrolamiento.setId(enrEnt.getId());
			this.backOfficeService.enviarApplicationXML2SmartVista(enrolamiento);

			resp.setId(enrEnt.getId());
			resp.setCliente(enrolamiento.getCliente());
			resp.getCliente().setId(enrEnt.getCliente().getId());
			resp.getCliente().setIdCustomer(String.valueOf(resp.getCliente().getId()));
			if (LOG.isInfoEnabled()) {
				long timeFin = System.currentTimeMillis();
				LOG.info("Finaliza guardar() en " + (timeFin - timeIni) + " ms");
			}
		} catch (InfraestructuraException e) {
			LOG.error(e.getMessage());
			if (disponible != null) {
				this.tarjetasDispoService.desasignaTarjeta(disponible);
			}
			// TODO Auto-generated catch block
		}
		return resp;
	}

	private void validarGuardarRequest(EnrolamientoValidadoDTO enrolamiento) throws USException {
		if (enrolamiento == null) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "No existen datos de entrada");
		}

		if (StringUtils.isBlank(enrolamiento.getCanal())) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "canal es requerido");
		}

		if (StringUtils.isBlank(enrolamiento.getSimilitud())) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "similitud es requerido");
		}
		if (StringUtils.isBlank(enrolamiento.getCodigoValidacion())) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"codigo de validacion del CURP es requerido");
		}

		if (enrolamiento.getCliente().getDireccion() == null) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "direccion es requerido");
		}

		if (StringUtils.isBlank(enrolamiento.getCliente().getDireccion().getPais())) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "pais es requerido");
		}

		if (StringUtils.isBlank(enrolamiento.getCliente().getDireccion().getColonia())
				&& StringUtils.isBlank(enrolamiento.getCliente().getDireccion().getCp())) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"Debe proporcionar colonia o cp de la direccion");
		}

	}

	@Override
	public EnrolamientoDTO guardarConValidacionFacial(EnrolamientoDTO enrolamiento) throws USException {
		final long timeIni = System.currentTimeMillis();

		this.validarConValidacionFacialRequest(enrolamiento);

		final EnrolamientoValidadoDTO enrolVal = new EnrolamientoValidadoDTO();

		enrolVal.setCanal(enrolamiento.getCanal());
		enrolVal.setApp(enrolamiento.getApp());
		enrolVal.setApiKey(enrolamiento.getApiKey());
		enrolVal.setCliente(enrolamiento.getCliente());
		enrolVal.setFechaInicio(enrolamiento.getFechaInicio());
		enrolVal.setFoto(enrolamiento.getFoto());
		enrolVal.setIdentificacion(enrolamiento.getIdentificacion());
		enrolVal.setIdInstitucion(enrolamiento.getIdInstitucion());
		enrolVal.setIdProducto(enrolamiento.getIdProducto());
		enrolVal.setNombreTitular(enrolamiento.getNombreTitular());
		enrolVal.setTipoCliente(enrolamiento.getTipoCliente());
		// invocar servicio de nubarium de validacion INE
		final IneResponseDto ineRespDTO = nubariumService.validarINE(enrolVal.getIdentificacion());
		if (LOG.isDebugEnabled()) {
			LOG.info(StringUtilBuilder.getDTOAsString(ineRespDTO));
		}
		if (ineRespDTO.getEstatus() != null && ineRespDTO.getEstatus().equalsIgnoreCase("ERROR")) {
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"Fall\u00F3 validaci\u00F3n de la identificaci\u00F3n: " + ineRespDTO.getMensaje());
		}

		if (!ineRespDTO.getNombres().equalsIgnoreCase(enrolamiento.getCliente().getNombre())
				|| !ineRespDTO.getPrimerApellido().equalsIgnoreCase(enrolamiento.getCliente().getPrimerApellido())) {
			enrolamiento.getCliente().setNombre(ineRespDTO.getNombres());
			enrolamiento.getCliente().setPrimerApellido(ineRespDTO.getPrimerApellido());
			enrolamiento.getCliente().setSegundoApellido(ineRespDTO.getSegundoApellido());
			enrolamiento.getFoto().setContenido(null);
			enrolamiento.getIdentificacion().setContenido(null);
			throw new NegocioException(CodigosError.ENR_DATOS_NO_COINCIDEN, enrolamiento);
		}

		// invocar servicio de nubarium de validacion facial
		final ReconocimientoFacialRespuestaDto resFac = nubariumService
				.validarReconocimientoFacial(enrolVal.getIdentificacion(), enrolVal.getFoto());
		if (LOG.isDebugEnabled()) {
			LOG.info("RESPUESTA_NUBARIUM_INE :: [" + resFac.getEstatus() + "]");
			LOG.info(StringUtilBuilder.getDTOAsString(resFac));
		}
		if (resFac.getEstatus() != null && resFac.getEstatus().equalsIgnoreCase("ERROR")) {
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"Fall\u00F3 validaci\u00F3n de similitud facial: " + resFac.getMensaje());
		}
		enrolVal.setClaveElector(ineRespDTO.getClaveElector());
		enrolVal.setSimilitud(resFac.getSimilitud());
		enrolVal.setCodigoValidacion(resFac.getCodigoValidacion());
		

		final EnrolamientoDTO resp = this.guardar(enrolVal);

		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza guardarConValidacionFacial() en " + (timeFin - timeIni) + " ms");
		}
		return resp;
	}

	private void validarConValidacionFacialRequest(EnrolamientoDTO enrolamiento) throws USException {
		if (enrolamiento == null) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "No existen datos de entrada");
		}

		if (enrolamiento.getCliente().getCelular() == null) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "celular es requerido");
		}

		if (enrolamiento.getIdentificacion() == null || enrolamiento.getIdentificacion().getContenido() == null) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"El archivo de identificacion (INE) es requerido");
		}

		if (enrolamiento.getFoto() == null || enrolamiento.getFoto().getContenido() == null) {
			throw new USException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"El archivo de foto (selfie) es requerido");
		}

	}

	@Override
	public EnrolamientoDTO guardarConValidacionFacialTarjeta(EnrolamientoMixDTO enrolamiento) throws USException {
		final long timeIni = System.currentTimeMillis();

		this.validarConValidacionFacialRequest(enrolamiento);

		final EnrolamientoValidadoDTO enrolVal = new EnrolamientoValidadoDTO();

		enrolVal.setCanal(enrolamiento.getCanal());
		enrolVal.setApp(enrolamiento.getApp());
		enrolVal.setApiKey(enrolamiento.getApiKey());
		enrolVal.setCliente(enrolamiento.getCliente());
		enrolVal.setFechaInicio(enrolamiento.getFechaInicio());
		enrolVal.setFoto(enrolVal.getFoto());
		enrolVal.setIdentificacion(enrolamiento.getIdentificacion());
		enrolVal.setIdInstitucion(enrolamiento.getIdInstitucion());
		enrolVal.setIdProducto(enrolamiento.getIdProducto());
		enrolVal.setNombreTitular(enrolamiento.getNombreTitular());
		enrolVal.setTipoCliente(enrolamiento.getTipoCliente());
		// invocar servicio de nubarium de validacion INE
		final IneResponseDto ineRespDTO = nubariumService.validarINE(enrolamiento.getIdentificacion());
		if (LOG.isDebugEnabled()) {
			LOG.info(StringUtilBuilder.getDTOAsString(ineRespDTO));
		}
		if (ineRespDTO.getEstatus() != null && ineRespDTO.getEstatus().equalsIgnoreCase("ERROR")) {
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"Fall\u00F3 validaci\u00F3n de la identificaci\u00F3n: " + ineRespDTO.getMensaje());
		}
		// invocar servicio de nubarium de validacion facial
		final ReconocimientoFacialRespuestaDto resFac = nubariumService
				.validarReconocimientoFacial(enrolamiento.getIdentificacion(), enrolamiento.getFoto());
		if (LOG.isDebugEnabled()) {
			LOG.info("RESPUESTA_NUBARIUM_INE :: [" + resFac.getEstatus() + "]");
			LOG.info(StringUtilBuilder.getDTOAsString(resFac));
		}
		if (resFac.getEstatus() != null && resFac.getEstatus().equalsIgnoreCase("ERROR")) {
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO,
					"Fall\u00F3 validaci\u00F3n de similitud facial: " + resFac.getMensaje());
		}
		enrolVal.setClaveElector(ineRespDTO.getClaveElector());
		enrolVal.setSimilitud(resFac.getSimilitud());
		enrolVal.setCodigoValidacion(resFac.getCodigoValidacion());
		final EnrolamientoDTO resp = this.guardar(enrolVal);

		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza guardarConValidacionFacialTarjeta() en " + (timeFin - timeIni) + " ms");
		}
		return resp;
	}

}
