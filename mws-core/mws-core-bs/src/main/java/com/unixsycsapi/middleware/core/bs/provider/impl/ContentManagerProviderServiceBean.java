/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.provider.impl;

import java.io.ByteArrayInputStream;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.MultiPart;
import com.sun.jersey.multipart.file.StreamDataBodyPart;
import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.api.infra.ParametroService;
import com.unixsycsapi.middleware.core.bs.provider.ContentManagerProviderService;
import com.unixsycsapi.middleware.core.commons.enums.Params;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.dm.archivo.TipoMultimediaDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class ContentManagerProviderServiceBean implements ContentManagerProviderService {
	private final static Logger LOG = LogManager.getLogger(ContentManagerProviderServiceBean.class);
	private static String FTP_HOST = null;

	private static WebResource webResource;

	@EJB
	private ParametroService paramService;

	@PostConstruct
	public void init() throws Exception {
		this.recuperaConfiguracion();
		if (webResource == null) {
			final ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);
			webResource = client.resource(FTP_HOST);
		}

	}

	@Override
	public ArchivoDTO guardarArchivo(ArchivoDTO archn) throws USException {
		long timeIni = System.currentTimeMillis();
		LOG.info("Inicia guardarArchivo()");

		try {
			if (archn != null && archn.getId() == null) {
				TipoMultimediaDTO tipo = archn.getTipo();
				// File f = new File("image."+tipo);
				// Path path = Paths.get(f.getAbsolutePath());
				// Files.write(path, content);
				ByteArrayInputStream bais = new ByteArrayInputStream(archn.getContenido());
				StreamDataBodyPart fileStreamDaBoPa = new StreamDataBodyPart("archivoContent", bais, archn.getNombre(),
						MediaType.APPLICATION_OCTET_STREAM_TYPE);

				(fileStreamDaBoPa).setContentDisposition(
						FormDataContentDisposition.name("archivoContenido").fileName(archn.getNombre()).build());

				@SuppressWarnings("resource")
				final MultiPart multiPart = new FormDataMultiPart()
						.field("comentario", "Enrolamiento", MediaType.TEXT_PLAIN_TYPE)
						.field("archivoTipo", String.valueOf(tipo.getId()), MediaType.TEXT_PLAIN_TYPE)
						.field("archivoNombre", archn.getNombre(), MediaType.TEXT_PLAIN_TYPE)
						.bodyPart(fileStreamDaBoPa);
				multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);
				final ClientResponse response = webResource.type("multipart/form-data").post(ClientResponse.class, multiPart);
				String output = response.getEntity(String.class);
				final Gson gson = getGson();
				
				final ArchivoDTO outDto = gson.fromJson(output, ArchivoDTO.class);

				
				archn.setId(outDto.getId());
				if (LOG.isInfoEnabled()) {
					long timeFin = System.currentTimeMillis();
					LOG.info("Finaliza guardarArchivo() en " + (timeFin - timeIni) + " ms");
				}
			} else {
				return archn;
			}
			return archn;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Error al guardar en el CM");
		}

	}

	protected Gson getGson() {
		Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy HH:mm:ss").create();
		return gson;
	}
	
	@Override
	public ArchivoDTO consultarArchivo(ArchivoDTO archn) throws USException {
		ArchivoDTO response = webResource.type("application/json").post(ArchivoDTO.class, archn.getId());
		return response;
	}

	private void recuperaConfiguracion() throws USException {

		try {
			if (FTP_HOST == null) {
				FTP_HOST = this.paramService.consultarAsString(Params.SIS_PROV_INT_URL_CM);
				if (LOG.isInfoEnabled()) {
					LOG.info("FTP_HOST :: [" + FTP_HOST + "]");
				}
			}
		} catch (USException e) {
			LOG.error(e.getMessage());
			throw e;
		}
	}

}
