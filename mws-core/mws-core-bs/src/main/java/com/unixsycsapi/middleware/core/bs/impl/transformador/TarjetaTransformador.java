/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl.transformador;

import java.util.ArrayList;
import java.util.List;

import com.unixsycsapi.middleware.core.dm.BancoDTO;
import com.unixsycsapi.middleware.core.dm.DepositoBancarioDTO;
import com.unixsycsapi.middleware.core.dm.EstadoTransaccionDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.entity.DepositoBancario;
import com.unixsycsapi.middleware.core.entity.TarjetaDisponible;

/**
 * @author Vladimir Aguirre
 *
 */
public class TarjetaTransformador {

	public static TarjetaLeanDTO transformar(TarjetaDisponible inEnt) {
		if (inEnt == null) {
			return null;
		}
		final TarjetaLeanDTO dto = new TarjetaLeanDTO();
		dto.setNumero(inEnt.getNumero());
		return dto;
	}

	public static List<DepositoBancarioDTO> transformar(List<DepositoBancario> depositos) {
		List<DepositoBancarioDTO> list = new ArrayList<DepositoBancarioDTO>();
		for (DepositoBancario dep : depositos) {
			DepositoBancarioDTO depoDTO = new DepositoBancarioDTO();
			depoDTO.setId(dep.getId());
			depoDTO.setFechaDeposito(dep.getFechaDeposito());
			EstadoTransaccionDTO estado = new EstadoTransaccionDTO();
			estado.setId(dep.getEstado().getId());
			depoDTO.setEstado(estado);
			TarjetaLeanDTO tarjeta = new TarjetaLeanDTO();
			tarjeta.setId(dep.getTarjeta().getId());
			tarjeta.setNumero(dep.getTarjeta().getOrigen().getNumero());
			depoDTO.setTarjeta(tarjeta);
			MontoDTO monto = new MontoDTO();
			monto.setMonto(dep.getMonto());
			depoDTO.setMonto(monto);
			depoDTO.setCuenta(dep.getCuenta());
			depoDTO.setMotivo(dep.getMotivo());
			depoDTO.setAutorizacion(dep.getAutorizacion());
			depoDTO.setFolio(dep.getFolio());
			BancoDTO banco = new BancoDTO();
			banco.setId(dep.getBanco().getId());
			depoDTO.setBanco(banco);
			list.add(depoDTO);
		}
		return list;
	}
}
