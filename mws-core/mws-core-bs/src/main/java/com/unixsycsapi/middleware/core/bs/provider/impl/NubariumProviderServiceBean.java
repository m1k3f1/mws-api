/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.provider.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.api.infra.ParametroService;
import com.unixsycsapi.middleware.core.bs.provider.NubariumProviderService;
import com.unixsycsapi.middleware.core.commons.enums.Params;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.provider.clientws.nubarium.IneResponseDto;
import com.unixsycsapi.middleware.core.provider.clientws.nubarium.ReconocimientoFacialRespuestaDto;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class NubariumProviderServiceBean implements NubariumProviderService {
	private final static Logger LOG = LogManager.getLogger(NubariumProviderServiceBean.class);
	static String content = "application/x-www-form-urlencoded";
	// TODO VAP meter a parametro
	static String autorizacion = "Basic " + "dW5peHN5YzpfYzFzWHgxbnU=";

	private static String URL_ENDPOINT_INE = null;
	private static String URL_ENDPOINT_FACIAL = null;
	private static Boolean FACIAL_ACTIVO = null;

	@EJB
	private ParametroService paramService;

	@PostConstruct
	public void init() throws Exception {
		this.recuperaConfiguracion();
	}

	@Override
	public IneResponseDto validarINE(ArchivoDTO ine) throws USException, UnsupportedEncodingException {
		final long timeIni = System.currentTimeMillis();
		if (LOG.isInfoEnabled()) {
			LOG.info("Inicia validarINE()");
			LOG.info("ine :: " + ine);
		}

		IneResponseDto ineRespDto = null;
		StringEntity params = new StringEntity("{\"id\":\"" + Base64.getEncoder().encodeToString(ine.getContenido()) + "\"} ");
		try {
			final String jsonResult = conexion(URL_ENDPOINT_INE, params, content, autorizacion);
			LOG.info("RESPUESTA_NUBARIUM_INE :: [" + jsonResult + "]");
			ObjectMapper mapper = new ObjectMapper();
			ineRespDto = mapper.readValue(jsonResult, IneResponseDto.class);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, e.getMessage());
		}
		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza validarINE() en " + (timeFin - timeIni) + " ms");
		}
		return ineRespDto;
	}

	@Override
	public ReconocimientoFacialRespuestaDto validarReconocimientoFacial(ArchivoDTO ine, ArchivoDTO selfie)
			throws USException {
		final long timeIni = System.currentTimeMillis();
		try {
			this.recuperaConfiguracion();

			if (BooleanUtils.isTrue(FACIAL_ACTIVO)) {
				if (LOG.isInfoEnabled()) {
					LOG.info("Inicia validarReconocimientoFacial()");
					LOG.info("ine :: " + ine);
					LOG.info("selfie :: " + selfie);
				}
				ReconocimientoFacialRespuestaDto recFacialRespDto = null;
				StringEntity params = new StringEntity( "{\"credencial\" : \"" + Base64.getEncoder().encodeToString(ine.getContenido()) + "\", "
						+ "\"captura\" : \"" + Base64.getEncoder().encodeToString(selfie.getContenido()) + "\", "
						+ "\"tipo\" : \"imagen\", " + "\"limiteInferior\" : \"10\"}");
				final String jsonResult = conexion(URL_ENDPOINT_FACIAL, params, content, autorizacion);
				LOG.info("RESPUESTA_NUBARIUM_RECONOCIMENTO_FACIAL :: [" + jsonResult + "]");
				ObjectMapper mapper = new ObjectMapper();
				recFacialRespDto = mapper.readValue(jsonResult, ReconocimientoFacialRespuestaDto.class);
				recFacialRespDto.setValidacionActiva(true);

				if (LOG.isInfoEnabled()) {
					long timeFin = System.currentTimeMillis();
					LOG.info("Finaliza validarReconocimientoFacial() en " + (timeFin - timeIni) + " ms");
				}
				return recFacialRespDto;
			} else {
				LOG.warn("");
				LOG.warn("ATENCION ATENCION ATENCION ATENCION");
				LOG.warn("La validación facial está APAGADA");
				LOG.warn("Para encender asignar [true] al parametro SIS_PROV_NUBARIUM_FACIAL_ACTIVO");
				LOG.warn("ATENCION ATENCION ATENCION ATENCION");
				LOG.warn("");
				final ReconocimientoFacialRespuestaDto respInactiva = new ReconocimientoFacialRespuestaDto();
				respInactiva.setValidacionActiva(false);
				respInactiva.setCodigoValidacion("NO_ACTIVA");
				respInactiva.setSimilitud("-1");
				return respInactiva;
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, e.getMessage());
		}

	}

	private void recuperaConfiguracion() throws USException {
		try {
			if (URL_ENDPOINT_INE == null) {
				URL_ENDPOINT_INE = this.paramService.consultarAsString(Params.SIS_PROV_URL_NUBARIUM_INE);
				URL_ENDPOINT_FACIAL = this.paramService.consultarAsString(Params.SIS_PROV_URL_NUBARIUM_FACIAL);

			}
			FACIAL_ACTIVO = this.paramService.consultarAsBoolean(Params.SIS_PROV_NUBARIUM_FACIAL_ACTIVO);
			if (LOG.isInfoEnabled()) {
				LOG.info("URL_ENDPOINT_INE :: [" + URL_ENDPOINT_INE + "]");
				LOG.info("URL_ENDPOINT_FACIAL :: [" + URL_ENDPOINT_FACIAL + "]");
				LOG.info("FACIAL_ACTIVO :: [" + FACIAL_ACTIVO + "]");
			}
		} catch (USException e) {
			LOG.error(e.getMessage());
			throw e;
		}
	}

	public String conexion(String url, StringEntity parametros, String content, String autorizacion) throws IOException {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();

		String jsonResponse = "";

		final HttpPost request = new HttpPost(url);

		request.addHeader("content-type", content);
		request.setHeader("Authorization", autorizacion);
		request.setEntity(parametros);
		final HttpResponse response = (HttpResponse) httpClient.execute(request);

		jsonResponse = EntityUtils.toString(response.getEntity());

		return jsonResponse;
	}
}
