/**
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.api.BloqueoTarjetaRemoteService;
import com.unixsycsapi.middleware.core.api.BloqueoTarjetaService;
import com.unixsycsapi.middleware.core.ds.ApiKeyDAO;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class BloqueoTarjetaServiceBean implements BloqueoTarjetaService, BloqueoTarjetaRemoteService {
	private final static Logger LOGGER = LogManager.getLogger(BloqueoTarjetaServiceBean.class);
	@EJB
	private ApiKeyDAO apiDao;

	@Override
	public void saludar(String data) throws USException {
		try {
			LOGGER.info("Hi :: " + data);
			doNothing();
			if (false) {
				throw new USException(CodigosError.SEGURIDAD_PROHIBIDO);
			}
		} catch (USException e) {
			throw e;
		} catch (Exception e) {
			throw new USException(CodigosError.GENERICO_INTERNO, e);
		}
	}

	private void doNothing() throws USException {
		LOGGER.info("Nada");
	}

}
