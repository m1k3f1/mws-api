/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.cliente.impl.transformador;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.DireccionDTO;
import com.unixsycsapi.middleware.core.dm.NumeroCelularDTO;
import com.unixsycsapi.middleware.core.entity.Celular;
import com.unixsycsapi.middleware.core.entity.Cliente;
import com.unixsycsapi.middleware.core.entity.Direccion;
import com.unixsycsapi.middleware.core.entity.Persona;

/**
 * @author Vladimir Aguirre
 *
 */
public class ClienteTransformer {

	public static Persona transformaPersonaDTO(ClienteDTO dto) throws USException {
		if (dto==null) {
			return null;
		}
		final Persona ent = new Persona();
		
		ent.setNombre(dto.getNombre());
		ent.setPrimerApellido(dto.getPrimerApellido());
		ent.setSegundoApellido(dto.getSegundoApellido());
		ent.setFechaRegistro(UtilDates.getCurrentDate());
		ent.setCurp(dto.getCurp());
		ent.setNacionalidad(dto.getNacionalidad());
		ent.setLugarNacimiento(dto.getLugarNacimiento());
		ent.setFechaNacimiento(dto.getFechaNacimiento());
//		ent.setRfc(dto.getrf); TODO VAP RFC
		ent.setActivo(Boolean.TRUE);
		
		return ent;
		
	}

	public static Cliente transformaDTO(ClienteDTO per)throws USException {
		if (per==null) {
			return null;
		}
		final Cliente cli = new Cliente();
		cli.setFechaRegistro(UtilDates.getCurrentDate());
		cli.setCustomerNumber(per.getNegocio());
		return cli;
		
	}
	
	public static Celular transformarCelularDTO(NumeroCelularDTO dto) throws USException {
		if (dto==null) {
			return null;
		}
		final Celular ent = new Celular();
		
		ent.setNumero(dto.getNumero());
		ent.setCompania(dto.getCompania());
		
		return ent;
		
	}	

	public static Direccion transformarDireccionDTO(DireccionDTO dto) throws USException {
		if (dto==null) {
			return null;
		}
		final Direccion ent = new Direccion();
		
		ent.setPais(dto.getPais());
		ent.setCiudad(dto.getCiudad());
		ent.setColonia(dto.getColonia());
		ent.setCp(dto.getCp());
		ent.setCalle(dto.getCalle());
		ent.setNoExterior(dto.getNoExterior());
		ent.setNoInterior(dto.getNoInterior());
		ent.setReferencia(dto.getReferencia());
		ent.setLatitud(dto.getLatitud());
		ent.setLongitud(dto.getLongitud());
		
		return ent;
		
	}
	
}
