/**
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.InfraestructuraException;
import com.unixsycsapi.framework.commons.exception.NegocioException;
import com.unixsycsapi.framework.commons.exception.SeguridadException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilSeguridad;
import com.unixsycsapi.middleware.core.api.ApiKeyRemoteService;
import com.unixsycsapi.middleware.core.api.ApiKeyService;
import com.unixsycsapi.middleware.core.dm.TokenDTO;
import com.unixsycsapi.middleware.core.dm.empresa.EmpresaDTO;
import com.unixsycsapi.middleware.core.ds.ApiKeyDAO;
import com.unixsycsapi.middleware.core.entity.ApiKey;

/**
 * @author Tejon01
 *
 */
@Stateless
public class ApiKeyServiceBean implements ApiKeyRemoteService, ApiKeyService {
	private final static Logger LOG = LogManager.getLogger(ApiKeyServiceBean.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.unixsycsapi.middleware.core.api.ApiKeyIService#validar(java.lang.
	 * String)
	 */
	@EJB
	private ApiKeyDAO apiDao;

	@Override
	public Boolean validar(String key) throws USException {
		final ApiKey respuesta = recuperarApiKey(key);
		if (respuesta != null) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}

	}

	private ApiKey recuperarApiKey(String key) throws USException {
		final String[] consultar = { "apikey", "activo" };
		final ApiKey example = new ApiKey();
		example.setActivo(Boolean.TRUE);
		example.setApikey(key);
		final List<ApiKey> respuesta = apiDao.consultarByProperties(consultar, example);
		if (CollectionUtils.isNotEmpty(respuesta) && respuesta.size() == 1) {
			return respuesta.get(0);
		}
		LOG.warn(key + " no existe");
		throw new InfraestructuraException(CodigosError.INFRA_CONFIGURACION,
				"Error en la configuraci\u00F3n de la apikey");
	}

	@Override
	public Boolean validarSesion(String key, String sesion) throws USException {
		final ApiKey respuesta = recuperarSesion(key, sesion);
		if (respuesta == null) {
			throw new SeguridadException(CodigosError.SEGURIDAD_USUARIO_NO_EXISTE);
		}
		return Boolean.TRUE;

	}

	private ApiKey recuperarSesion(String key, String sesion) throws USException {
		final String[] consultar = { "apikey", "activo", "idSesion" };
		final ApiKey example = new ApiKey();
		example.setActivo(Boolean.TRUE);
		example.setApikey(key);
		example.setIdSesion(sesion);
		List<ApiKey> respuesta = apiDao.consultarByProperties(consultar, example);
		if (CollectionUtils.isNotEmpty(respuesta) && respuesta.size() == 1) {
			return respuesta.get(0);
		}
		throw new InfraestructuraException(CodigosError.SEGURIDAD_USUARIO_NO_EXISTE);
	}

	@Override
	public Boolean validarDummy(String key) throws USException {
		return Boolean.TRUE;
	}

	@Override
	public EmpresaDTO recuperarEmpresa(String apiKey) throws USException {
		final Long idEmpre = this.apiDao.getIdEmpresa(apiKey);
		if (idEmpre == null) {
			throw new NegocioException(CodigosError.INFRA_APIKEY_NOEXISTE);
		}
		return new EmpresaDTO(idEmpre);
	}

	@Override
	public TokenDTO iniciarSesion(TokenDTO apikey) throws USException {
		try {
			final ApiKey apiKeyEnt = this.apiDao.loginEmpresa(apikey.getApiKey(), apikey.getEquipoID());
			if (apiKeyEnt != null) {
				final TokenDTO dto = new TokenDTO();
				final String idSesion = UtilSeguridad.getUid(apikey.getApiKey());
				dto.setIdSession(idSesion);
				apiKeyEnt.setIdSesion(idSesion);
				this.apiDao.modificar(apiKeyEnt);
				return dto;
			}
			throw new NegocioException(CodigosError.INFRA_APIKEY_NOEXISTE);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(CodigosError.GENERICO_INTERNO, "Error al iniciar la sesi\u00F3n");
		}
	}

}
