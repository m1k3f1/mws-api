/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.provider;

import javax.ejb.Local;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@Local
public interface BPCBackOfficeProviderService {

	public void enviarApplicationXML2SmartVista(EnrolamientoValidadoDTO enrol) throws USException;
	
}
