/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.InfraestructuraException;
import com.unixsycsapi.framework.commons.exception.NegocioException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.middleware.core.bs.TarjetasDisponiblesService;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;
import com.unixsycsapi.middleware.core.ds.TarjetaDisponibleDAO;
import com.unixsycsapi.middleware.core.ds.TirajeTarjetaDAO;
import com.unixsycsapi.middleware.core.entity.TarjetaDisponible;
import com.unixsycsapi.middleware.core.entity.TirajeTarjeta;

/**
 * @author Vladimir Aguirre
 *
 */
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Singleton
public class TarjetasDisponiblesServiceBean implements TarjetasDisponiblesService {
	private final static Logger LOG = LogManager.getLogger(TarjetasDisponiblesServiceBean.class);

	@EJB
	private TarjetaDisponibleDAO tdDao;

	@EJB
	private TirajeTarjetaDAO tirajeDao;

	@Override
	@Lock(LockType.WRITE)
	public TarjetaDisponible asignaTarjeta() throws USException {
		final TarjetaDisponible entity = this.tdDao.getNextTarjeta();

		if (entity == null) {
			LOG.error(CodigosError.NEGOCIO_TARJETAS_NO_DISPONIBLES);
			throw new NegocioException(CodigosError.NEGOCIO_TARJETAS_NO_DISPONIBLES);
		}
		entity.setFechaAsignacion(UtilDates.getCurrentDate());
		entity.setAsignada(Boolean.TRUE);
		this.tdDao.modificar(entity);
		return entity;
	}

	@Override
	@Lock(LockType.WRITE)
	public void desasignaTarjeta(TarjetaDisponible fallida) throws USException {
		this.tdDao.desvincular(fallida);
		final TarjetaDisponible entity = this.tdDao.consultar(fallida.getId());
		entity.setFechaAsignacion(UtilDates.getCurrentDate());
		entity.setAsignada(Boolean.TRUE);
		this.tdDao.modificar(entity);
	}

	@Override
	public void cargarTiraje(ArchivoDTO arch) throws USException {
		byte[] content = arch.getContenido();
		InputStream is = null;
		BufferedReader bfReader = null;
		String noTarjeta = null;
		String vencimiento = null;
		final SimpleDateFormat sdfVenc = new SimpleDateFormat();
		final String md5Hash = DigestUtils.md5Hex(arch.getContenido());
		final Long idExiste = this.tirajeDao.consultarIdByHash(md5Hash);
		if (idExiste != null) {
			throw new NegocioException(CodigosError.NEGOCIO_ARGUMENTO_INCORRECTO, "El archivo ya eixste en la BD");
		}
		final TirajeTarjeta tiraje = new TirajeTarjeta();
		tiraje.setHashMd5(md5Hash);
		TarjetaDisponible unaTarD = null;
		try {
			
			tiraje.setFechaRegistro(UtilDates.getCurrentDate());
			is = new ByteArrayInputStream(content);
			bfReader = new BufferedReader(new InputStreamReader(is));
			String linea = null;
			String nombreLote = null;
			int row = 1;
			while ((linea = bfReader.readLine()) != null) {
				if (row == 1) {
					nombreLote = linea.substring(1, 35);
					tiraje.setNombre(nombreLote);
					LOG.info("nombreLote :: " + nombreLote);
					this.tirajeDao.guardar(tiraje);
				} else {
					if (linea.length() > 14) {
						noTarjeta = linea.substring(1, 17);
						vencimiento = linea.substring(47, 51);
						unaTarD = new TarjetaDisponible();
						unaTarD.setAsignada(Boolean.FALSE);
						unaTarD.setFechaRegistro(UtilDates.getCurrentDate());
						unaTarD.setNumero(noTarjeta);
						unaTarD.setVencimiento(vencimiento);
						unaTarD.setTiraje(tiraje);
						this.tdDao.guardar(unaTarD);
					}
				}
				row++;
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new InfraestructuraException(CodigosError.GENERICO_INTERNO, e.getMessage());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, e.getMessage(), e);
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (Exception ex) {
				LOG.warn(ex.getMessage());
			}
		}

	}
}
