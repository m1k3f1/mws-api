/**
 * 
 */
package com.unixsycsapi.middleware.core.bs.infra.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.InfraestructuraException;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.api.infra.ParametroRemoteService;
import com.unixsycsapi.middleware.core.api.infra.ParametroService;
import com.unixsycsapi.middleware.core.bs.infra.impl.transformador.ParametroTransformador;
import com.unixsycsapi.middleware.core.commons.enums.Params;
import com.unixsycsapi.middleware.core.dm.ParametroDTO;
import com.unixsycsapi.middleware.core.ds.infra.ParametroDAO;
import com.unixsycsapi.middleware.core.entity.infra.Parametro;

/**
 * @author vladimir.aguirre
 *
 */
@Stateless
public class ParametroServiceBean implements ParametroService, ParametroRemoteService {
	private final static Logger LOGGER = LogManager.getLogger(ParametroServiceBean.class);

	@EJB
	private ParametroDAO paramDao;

	@Override
	public ParametroDTO consultar(Params claveParam) throws USException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Buscando " + claveParam);
		}
		final Parametro leido = this.paramDao.consultarPorClave(claveParam.name());
		final ParametroDTO paramDTO = ParametroTransformador.transformar(leido);
		if (paramDTO == null) {
			throw new InfraestructuraException(CodigosError.INFRA_CONFIGURACION, claveParam.name());
		}
		if (LOGGER.isDebugEnabled()) {
			if (paramDTO.getClave().contains("PWD")) {
				LOGGER.debug(paramDTO.getClave() + " -> [" + paramDTO.getValor().substring(0, 4) + "...]");
			} else {
				LOGGER.debug(paramDTO.getClave() + " -> [" + paramDTO.getValor() + "]");
			}
		}
		return paramDTO;
	}

	@Override
	public Integer consultarAsInteger(Params claveParam) throws USException {
		final ParametroDTO param = this.consultar(claveParam);
		if (param == null) {
			return null;
		}
		try {
			return Integer.parseInt(param.getValor());
		} catch (NumberFormatException e) {
			LOGGER.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_CONFIGURACION, "Se esperaba un valor num\u00E9rico");
		}
	}

	@Override
	public Boolean consultarAsBoolean(Params claveParam) throws USException {
		final ParametroDTO param = this.consultar(claveParam);
		if (param == null) {
			return null;
		}
		return Boolean.valueOf(param.getValor());
	}

	@Override
	public String consultarAsString(Params claveParam) throws USException {
		final ParametroDTO param = this.consultar(claveParam);
		if (param == null) {
			return null;
		}
		return param.getValor();
	}

	@Override
	public List<ParametroDTO> consultarParametros() throws USException {
		List<ParametroDTO> parametros = new ArrayList<ParametroDTO>();
		List<Parametro> data = this.paramDao.consultarTodos();
		for (Parametro row : data) {
			parametros.add(ParametroTransformador.transformar(row));
		}
		return parametros;
	}

	@Override
	public Long consultarAsLong(Params claveParam) throws USException {
		final ParametroDTO param = this.consultar(claveParam);
		if (param == null) {
			return null;
		}
		try {
			return Long.valueOf(param.getValor());
		} catch (NumberFormatException e) {
			LOGGER.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_CONFIGURACION, "Se esperaba un valor num\u00E9rico");
		}
	}

	@Override
	public ParametroDTO modificar(ParametroDTO dto) throws USException {
		Parametro par;
		par = new Parametro();
		par.setId(dto.getId());
		par.setCache(dto.getCache());
		par.setClave(dto.getClave());
		par.setDescripcion(dto.getDescripcion());
		par.setValor(dto.getValor());
		this.paramDao.modificar(par);
		return consultar(Params.valueOf(par.getClave()));
	}

}
