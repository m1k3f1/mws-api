/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl.transformador;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.middleware.core.commons.enums.EstadosEnrolamiento;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;
import com.unixsycsapi.middleware.core.entity.Enrolamiento;
import com.unixsycsapi.middleware.core.entity.estado.EstadoEnrolamiento;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoTransformador {

	public static Enrolamiento transformarDTO(EnrolamientoValidadoDTO dto) throws USException {
		if (dto == null) {
			return null;
		}
		Enrolamiento en = new Enrolamiento();
		en.setCodigoValidacion(dto.getCodigoValidacion());
		en.setSimilitud(dto.getSimilitud());
		en.setFechaRegistro(UtilDates.getCurrentDate());
		en.setCanal(dto.getCanal());
		en.setAplicacion(dto.getApp());
		en.setClaveElector(dto.getClaveElector());
		en.setEstado(new EstadoEnrolamiento(EstadosEnrolamiento.GUARDADO_INTRANET.getId()));
		return en;
	}

}
