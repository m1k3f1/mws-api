/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.cliente.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.api.cliente.PersonaRemoteService;
import com.unixsycsapi.middleware.core.api.cliente.PersonaService;
import com.unixsycsapi.middleware.core.bs.cliente.impl.transformador.ClienteTransformer;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;
import com.unixsycsapi.middleware.core.dm.NumeroCelularDTO;
import com.unixsycsapi.middleware.core.ds.CelularDAO;
import com.unixsycsapi.middleware.core.ds.PersonaDAO;
import com.unixsycsapi.middleware.core.entity.Celular;
import com.unixsycsapi.middleware.core.entity.Persona;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class PersonaServiceBean implements PersonaService, PersonaRemoteService {

	@EJB
	private PersonaDAO personDao;

	@Override
	public ClienteDTO buscarPersona(ClienteDTO cli) throws USException {
		// TODO VAP desarrollar
		return null;
	}

}
