/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.provider.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.mail.SendEmail;
import com.unixsycsapi.middleware.core.api.infra.ParametroService;
import com.unixsycsapi.middleware.core.bs.provider.BPCBackOfficeProviderService;
import com.unixsycsapi.middleware.core.commons.enums.Params;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;
import com.unixsycsapi.middleware.core.ds.TarjetaVirtualDAO;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Account;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.AccountObject;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Address;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.AddressName;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.ApplicationType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Applications;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Card;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Cardholder;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Contract;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Customer;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Person;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.PersonName;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.SecWord;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Service;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.ServiceObject;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class BPCBackOfficeProviderServiceBean implements BPCBackOfficeProviderService {
	private final static Logger LOG = LogManager.getLogger(BPCBackOfficeProviderServiceBean.class);
	private static String FTP_HOST = null;
	private static Integer FTP_PORT = 0;
	private static String FTP_USER = null;
	private static String FTP_FOLDER_DESTINO = null;

	private static String FTP_PWD = null;

	private static String FTP_PRIVATE_KEY_PATH = null;
	private static String FTP_KNOW_HOSTS_PATH = null;
	private static String FOLDER_BACKUP_APP_FILE = null;
	private static JAXBContext jaxbContext = null;

	@EJB
	private TarjetaVirtualDAO tarjetaVirtualDAO;
	
	@EJB
	private ParametroService paramService;
	
	private final String sequenceName = "seq_enrollment";

	@PostConstruct
	public void init() throws Exception {
		this.recuperaConfiguracion();
	}

	@Override
	public void enviarApplicationXML2SmartVista(EnrolamientoValidadoDTO enrol) throws USException {
		long timeIni = System.currentTimeMillis();
		InputStream isFromFirstData = null;
		ByteArrayOutputStream baos = null;
		try {
			if (jaxbContext == null) {
				jaxbContext = JAXBContext.newInstance(Applications.class);
			}
			final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			// jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
			// "http://www.mysite.com/abc.xsd");

			final Applications appObj = this.poblarApplicationDocumento(enrol);
			baos = new ByteArrayOutputStream();
			jaxbMarshaller.marshal(appObj, baos);
			if (LOG.isDebugEnabled()) {
				LOG.debug("baos.size() :: " + baos.size());
			}
			isFromFirstData = new ByteArrayInputStream(baos.toByteArray());
			final String nombreArchivoEnviado = this.sendApplication(isFromFirstData);

			backupAppFile(enrol, baos, nombreArchivoEnviado);
			
			try {
			SendEmail.NotificationEmail(nombreArchivoEnviado, 1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (LOG.isInfoEnabled()) {
				long timeFin = System.currentTimeMillis();
				LOG.info("Finaliza enviarApplicationXML2SmartVista() en " + (timeFin - timeIni) + " ms");
			}
		} catch (JAXBException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, e.getMessage());
		} finally {
			try {
				if (baos != null) {
					baos.close();
				}
			} catch (IOException e) {
				LOG.warn("Error al liberar recurso " + e.getMessage());
			}
			try {
				if (isFromFirstData != null) {
					isFromFirstData.close();
				}
			} catch (IOException e) {
				LOG.warn("Error al liberar recurso " + e.getMessage());
			}
		}

	}

	/**
	 * 
	 * @param enrol
	 * @param baos
	 * @param nombreArchivoEnviado
	 */
	private void backupAppFile(EnrolamientoValidadoDTO enrol, ByteArrayOutputStream baos,
			final String nombreArchivoEnviado) {
		try (final OutputStream outputStream = new FileOutputStream(
				FOLDER_BACKUP_APP_FILE + "/" + enrol.getId() + "-" + nombreArchivoEnviado)) {
			baos.writeTo(outputStream);
		} catch (FileNotFoundException e) {
			LOG.warn("Error al generar el respaldo " + e.getMessage());
		} catch (IOException e) {
			LOG.warn("Error al generar el respaldo " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param enrol
	 * @return
	 * @throws USException
	 */
	private Applications poblarApplicationDocumento(EnrolamientoValidadoDTO enrol) throws USException {
		final Applications objApp = new Applications();
		final ApplicationType at = new ApplicationType();
		at.setApplicationType("APTPISSA");
		at.setApplicationFlowId(1001);
		at.setApplicationStatus("APST0006");
		at.setOperatorId("ADMIN");
		at.setInstitutionId(1001);
		at.setAgentId(10000001);
		at.setAgentNumber("70000001");
		at.setCustomerType("ENTTPERS");
		final Customer cus = new Customer();
		cus.setCommand("CMMDCREX");
		// cus.setCustomerCategory("CCTGORDN");
		cus.setCustomerRelation("RSCBEXTR");
		cus.setResident(1);
		cus.setNationality("484");// TODO VAP catalogo
		final Contract con = new Contract();
		con.setCommand("CMMDCREX");
		con.setContractType("CNTPINIC");
		con.setProductId(Integer.decode(enrol.getIdProducto()));
		con.setStartDate(getFechaXML(new Date()));
		final Card card = new Card();
		card.setId("card_1");
		card.setCommand("CMMDEXUP");
		card.setCardNumber(enrol.getTarjeta().getNumero());// TODO VAP enviar
															// req
		card.setCardStatus("CSTE0200");
		final Cardholder ch = new Cardholder();
		ch.setCommand("CMMDCRUP");
//		 Notification not = new Notification();
//		 not.setCommand("CMMDCREX");
//		 not.setDeliveryChannel(3);
//		 not.setDeliveryAddress(enrol.getCliente().getCelular().getNumero());
//		 not.setIsActive(1);
//		 ch.getNotification().add(not);
		final Person per = new Person();
		per.setBirthday(getFechaXML(enrol.getCliente().getFechaNacimiento()));
		per.setCommand("CMMDCRUP");
		final PersonName pn = new PersonName();
		pn.setFirstName(removerTildes(enrol.getCliente().getNombre()));
		if (enrol.getCliente().getSegundoApellido() != null) {
			pn.setSurname(removerTildes(
					enrol.getCliente().getPrimerApellido() + " " + enrol.getCliente().getSegundoApellido()));
		} else {
			pn.setSurname(removerTildes(enrol.getCliente().getPrimerApellido()));
		}
		if (enrol.getNombreTitular() != null) {
			ch.setCardholderName(removerTildes(enrol.getNombreTitular()));
		} else {
			ch.setCardholderName(removerTildes(enrol.getCliente().getNombre() + " " + pn.getSurname()));
		}
		pn.setLanguage("LANGENG");
		per.addPersonName(pn);
//		final IdentityCard ic = new IdentityCard();
//		ic.setCommand("CMMDEXPR");
//		ic.setIdType("IDTP0001");
//		ic.setIdSeries("0");
//		ic.setIdNumber("13243546");
//		per.getIdentityCard().add(ic);
		ch.setPerson(per);
		final Address adr = new Address();
		adr.setCommand("CMMDCRUP");
		adr.setAddressType("ADTPHOME");
		adr.setCountry("484");// TODO VAP catalogo
		ch.addAddress(adr);
		AddressName an = new AddressName();
		an.setLanguage("LANGENG");
		an.setCity(removerTildes(enrol.getCliente().getDireccion().getCiudad()));
		an.setRegion(removerTildes(enrol.getCliente().getDireccion().getCiudad()));
		if (StringUtils.isNotBlank(enrol.getCliente().getDireccion().getNoExterior())) {
			an.setStreet(removerTildes(enrol.getCliente().getDireccion().getCalle() + " "
					+ enrol.getCliente().getDireccion().getNoExterior()));
		} else {
			an.setStreet(removerTildes(enrol.getCliente().getDireccion().getCalle()));
		}
		adr.setHouse(removerTildes(enrol.getCliente().getDireccion().getNoExterior()));
		adr.setApartment(removerTildes(enrol.getCliente().getDireccion().getNoInterior()));
		adr.getAddressName().add(an);
		final SecWord sw = new SecWord();
		sw.setSecretQuestion("SEQUWORD");
		sw.setSecretAnswer("TITIN");
		ch.getSecWord().add(sw);
		card.setCardholder(ch);

		final Service ser1 = new Service();
		ser1.setValue(70000002);
		final ServiceObject so1 = new ServiceObject();
		so1.setStartDate(getFechaXML(new Date()));
		so1.setRefId("account_1");
		ser1.getServiceObject().add(so1);
		con.getService().add(ser1);

		final Service ser2 = new Service();
		ser2.setValue(70000001);
		final ServiceObject so2 = new ServiceObject();
		so2.setStartDate(getFechaXML(new Date()));
		so2.setRefId("card_1");
		ser2.getServiceObject().add(so2);
		con.getService().add(ser2);

		Account ac = new Account();
		ac.setId("account_1");
		ac.setCommand("CMMDCRPR");
		ac.setCurrency("484");// TODO VAP catalogo
		ac.setAccountType("ACTP0100");
		ac.setAccountStatus("ACSTACTV");
		final AccountObject ao = new AccountObject();
		ao.setRefId("card_1");
		ao.setAccountLinkFlag(1);
		ao.setIsPosDefault(0);
		ao.setIsAtmDefault(0);
		ac.getAccountObject().add(ao);

		con.getAccount().add(ac);

		con.addCard(card);
		cus.setContract(con);
		Person perp = new Person();
		perp.setCommand("CMMDCRUP");
		perp.addPersonName(pn);
//		final IdentityCard icp = new IdentityCard();
//		icp.setCommand("CMMDCRUP");
//		icp.setIdType("IDTP0001");
//		icp.setIdSeries("0");
//		icp.setIdNumber("13243546");

//		perp.getIdentityCard().add(icp);
		cus.setPerson(perp);
		at.setCustomer(cus);

		objApp.addApplication(at);

		return objApp;
	}

	private String removerTildes(String cadena) {
		return cleanString(cadena);
	}

	public String cleanString(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return texto;
	}

	/**
	 * 
	 * @param fec
	 * @return
	 */
	public static XMLGregorianCalendar getFechaXML(Date fec) {
		final GregorianCalendar c = new GregorianCalendar();
		c.setTime(fec);
		XMLGregorianCalendar date2;
		try {
			date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c.get(Calendar.YEAR),
					c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.HOUR_OF_DAY),
					c.get(Calendar.MINUTE), c.get(Calendar.SECOND), DatatypeConstants.FIELD_UNDEFINED,
					DatatypeConstants.FIELD_UNDEFINED);
			return date2;
		} catch (DatatypeConfigurationException e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * 
	 * @param appIS
	 * @throws USException
	 */
	private String sendApplication(InputStream appIS) throws USException {
		JSch jsch = null;
		try {

			jsch = new JSch();
			final String privateKey = FTP_PRIVATE_KEY_PATH;
//			final File file = new File(privateKey);
			// String privateKey =
			// "c://Users//pablocs//Desktop//MarioPrivate.ppk";
			jsch.setKnownHosts(new FileInputStream(FTP_KNOW_HOSTS_PATH));

			// jsch.addIdentity(privateKey,"wld72000");
			jsch.addIdentity(privateKey, FTP_PWD);

			LOG.debug("identity added ");
			Session session = jsch.getSession(FTP_USER, FTP_HOST, FTP_PORT);
			LOG.debug("session created.");

			session.connect();
			LOG.debug("session connected.....");

			final ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();
			sftp.cd(FTP_FOLDER_DESTINO);
			final SimpleDateFormat SDF = new SimpleDateFormat("yyMMdd");
			final String nombreArchivo = "03000109000" + SDF.format(new Date()) + getNextSeqVal() + ".xml";
			sftp.put(appIS, nombreArchivo);

			LOG.debug("Archivo subido :: " + nombreArchivo);

			sftp.exit();
			sftp.disconnect();
			session.disconnect();
			return nombreArchivo;
		} catch (FileNotFoundException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, e.getMessage());
		} catch (JSchException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, e.getMessage());
		} catch (SftpException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, e.getMessage());
		}
	}

	private void recuperaConfiguracion() throws USException {

		try {
			if (FTP_HOST == null) {
				FTP_HOST = this.paramService.consultarAsString(Params.SIS_PBC_SFTP_HOST);
				FTP_PORT = this.paramService.consultarAsInteger(Params.SIS_PBC_SFTP_PORT);
				FTP_USER = this.paramService.consultarAsString(Params.SIS_PBC_SFTP_USER);
				FTP_PWD = this.paramService.consultarAsString(Params.SIS_PBC_SFTP_PWD);// TODO
																						// VAP
																						// cifrar
				FTP_PRIVATE_KEY_PATH = this.paramService.consultarAsString(Params.SIS_PBC_SFTP_PATH_FILE_PRIVATE_KEY);
				FTP_KNOW_HOSTS_PATH = this.paramService.consultarAsString(Params.SIS_PBC_SFTP_PATH_FILE_KNOW_HOSTS);
				FTP_FOLDER_DESTINO = this.paramService.consultarAsString(Params.SIS_PBC_SFTP_FOLDER_DESTINO);
				FOLDER_BACKUP_APP_FILE = this.paramService.consultarAsString(Params.SIS_PBC_FOLDER_BACKUP_APP_FILE);
				if (LOG.isInfoEnabled()) {
					LOG.info("FTP_HOST :: [" + FTP_HOST + "]");
					LOG.info("FTP_PRIVATE_KEY_PATH :: [" + FTP_PRIVATE_KEY_PATH + "]");
					LOG.info("FTP_FOLDER_DESTINO :: [" + FTP_FOLDER_DESTINO + "]");
				}
			}
		} catch (USException e) {
			LOG.error(e.getMessage());
			throw e;
		}
	}
	
	private String getNextSeqVal() {
		try {
			Long sequence = tarjetaVirtualDAO.getSequenceNextVal(sequenceName);
			if (sequence > 9999) {
				tarjetaVirtualDAO.resetSequence(sequenceName);
				sequence = tarjetaVirtualDAO.getSequenceNextVal(sequenceName);
			}
			return StringUtils.leftPad(sequence.toString(), 4, "0");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
