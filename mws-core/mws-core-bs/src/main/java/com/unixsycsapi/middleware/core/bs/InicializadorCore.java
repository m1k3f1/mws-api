/**
 * 
 */
package com.unixsycsapi.middleware.core.bs;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author VLadimir Aguirre
 *
 */

@Singleton
@Startup
public class InicializadorCore {
	private final static Logger LOG = LogManager.getLogger(InicializadorCore.class);

	@PostConstruct
	public void inicializar() {
		if (LOG.isInfoEnabled()) {
			LOG.info("**************************************");
			LOG.info("**                                  **");
			LOG.info("**      Core MWS inicializado!      **");
			LOG.info("**                                  **");
			LOG.info("**************************************");
		}
	}
}
