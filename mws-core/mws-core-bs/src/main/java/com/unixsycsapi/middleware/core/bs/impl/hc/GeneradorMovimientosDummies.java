/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.impl.hc;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.unixsycsapi.middleware.core.dm.FiltroConsultaDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class GeneradorMovimientosDummies {
	private final static Map<String, List<MovimientoDTO>> ESTADOS_CUENTA = new HashMap<String, List<MovimientoDTO>>();
	private static List<BigInteger> MONTOS = null;

	public static List<MovimientoDTO> generarMovimientos(FiltroConsultaDTO filtro) {

		if (ESTADOS_CUENTA.containsKey(filtro.getTarjeta().getNumero())) {
			return ESTADOS_CUENTA.get(filtro.getTarjeta().getNumero());
		} else {
			final List<MovimientoDTO> dummies = new ArrayList<MovimientoDTO>();

			dummies.addAll(generarMovimientosDummies());

			ESTADOS_CUENTA.put(filtro.getTarjeta().getNumero(), dummies);
			return dummies;
		}

	}

	private static List<MovimientoDTO> generarMovimientosDummies() {
		final List<MovimientoDTO> dummies = new ArrayList<MovimientoDTO>();
		for (int i = 1; i <= 30; i++) {
			dummies.add(crear(i, 9, 2018));
			dummies.add(crear(i, 9, 2018));
		}
		return dummies;

	}

	public static void main(String[] args) {
		for (int i = 1; i <= 30; i++) {
			System.out.println(i);
			System.out.println(crear(i, 9, 2018));
			System.out.println(crear(i, 9, 2018));
		}
	}

	private static MovimientoDTO crear(int date, int mes, int anio) {
		final MovimientoDTO dto = new MovimientoDTO();
		final MontoDTO monto = new MontoDTO();
		final MontoDTO comision = new MontoDTO();
		dto.setDescripcion(getDesc());
		Calendar uax = Calendar.getInstance();
		uax.set(Calendar.DATE, date);
		mes--;
		uax.set(Calendar.MONTH, mes);
		uax.set(Calendar.YEAR, anio);
		uax.add(Calendar.MINUTE, (date + mes));
		uax.add(Calendar.SECOND, (date * 12));


		dto.setFecha(uax.getTime());
		Random r = new Random();
		monto.setMonto(getMonto());
		monto.setMoneda(484);
		if (dto.getDescripcion().contains("RECIBIDO") || dto.getDescripcion().contains("DEPOSITO")) {
			dto.setDireccionOperacion("Abono");
		} else {
			dto.setDireccionOperacion("Cargo");
		}
		dto.setMonto(monto);
		comision.setMonto(BigInteger.ZERO);
		comision.setMoneda(monto.getMoneda());
		dto.setDireccionComision("noop");
		dto.setComision(comision);
		dto.setId(new Long(dto.hashCode()));
		return dto;
	}

	private static String getDesc() {
		List<String> arrayList = new ArrayList<String>();
		arrayList.add("TRASPASO CUENTAS PROPIAS");
		arrayList.add("RETIRO CAJERO AUTOMATICO");
		arrayList.add("PAGO CUENTA DE TERCERO");
		arrayList.add("COBRO AUTOMATICO RECIBO");
		arrayList.add("LIB ROSARIO CASTELLANO");
		arrayList.add("EFECTIVO SEGURO");
		arrayList.add("SPEI RECIBIDO XYZ");
		arrayList.add("PAGO TARJETA DE CREDITO");
		arrayList.add("UBER TRIP Y6TKP HELP.UBER");
		arrayList.add("UBER TRIP Q4YPN HELP.UBER");
		arrayList.add("UBER TRIP FDK3B HELP.UBER");
		arrayList.add("NETFLIX COM");
		arrayList.add("STARBUCKS NY");
		arrayList.add("PURCHASE ELECTRONIC");
		arrayList.add("SUPERMERCADO XYYZ");
		arrayList.add("CUALQUIERSITIO.COM COMPRA");
		arrayList.add("DEPOSITO EN EFECTIVO");
		arrayList.add("AMAZON MEXICO");


		Collections.shuffle(arrayList);
		return arrayList.get(0);
	}

	private static BigInteger getMonto() {
		if (MONTOS == null) {
			MONTOS = new ArrayList<BigInteger>();
			int min = 45;
			int max = 9999;
			Random r = new Random();
			for (int i = 0; i < 100; i++) {
				long x = r.nextInt((max - min) + 1) + min;
				if (i%2==0){
					x=x*2;
				} else {
					x=x/2;
				}
				MONTOS.add(BigInteger.valueOf(x));
			}
		}

		Collections.shuffle(MONTOS);
		return MONTOS.get(0).multiply(BigInteger.valueOf(100)) ;
	}

}
