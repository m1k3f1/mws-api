/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.aop;

import java.security.Principal;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Vladimir Aguirre
 *
 */
public class ApiKeyInterceptor {
	private final static Logger LOG = LogManager.getLogger(ApiKeyInterceptor.class);

    @AroundInvoke
    protected Object myInterceptor(InvocationContext ctx) throws Exception {
    	LOG.info("Interceptando");
    	return ctx.proceed();
    }

}
