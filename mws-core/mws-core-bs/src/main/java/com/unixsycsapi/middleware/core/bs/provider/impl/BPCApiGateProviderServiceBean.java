/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.bs.provider.impl;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.framework.util.UtilDates;
import com.unixsycsapi.middleware.core.api.infra.ParametroService;
import com.unixsycsapi.middleware.core.bs.provider.BPCApiGateProviderService;
import com.unixsycsapi.middleware.core.commons.enums.Params;
import com.unixsycsapi.middleware.core.dm.DetalleCompletoTarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.dm.DetalleMovimientoDTO;
import com.unixsycsapi.middleware.core.dm.EstadoTarjetaDTO;
import com.unixsycsapi.middleware.core.dm.FiltroConsultaDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;
import com.unixsycsapi.middleware.core.dm.RespuestaCVV;
import com.unixsycsapi.middleware.core.dm.SaldoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.entity.TarjetaVirtual;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.AccountDataType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.AccountsType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.Apigate;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.Apigate_Service;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CardIdentificationType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CardStatusInquiryRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CardStatusInquiryResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CashDepositRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.ChangeCardStatusRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.FinancialTransactionResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GenerateCVC2RequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GenerateCVC2ResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardBalanceRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardBalanceResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardDataRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardDataResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetTransactionsRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetTransactionsResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.P2PTransferRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.PurchaseRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.ServiceLevelException;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.SimpleResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.SvfeProcessingException;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.TransactionDataType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.TransactionDateStrictPeriodType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.TransactionsType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.ValidateCardRequestType;

/**
 * Implementacion para proveer conectividad con BPC
 * 
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class BPCApiGateProviderServiceBean implements BPCApiGateProviderService {
	private final static Logger LOG = LogManager.getLogger(BPCApiGateProviderServiceBean.class);
	private static String URL_ENDPOINT = null;
	private static Map<String, String> MAP_CAT_DIRECCIONES = null;
	private static Apigate endPoint;

	private final static int HOT_STATUS_TEMPORAL_BLOCK_BY_CLIENT = 20;

	@EJB
	private ParametroService paramService;

	@PostConstruct
	public void init() throws Exception {

		this.recuperaConfiguracion();

	}

	@Override
	public List<MovimientoDTO> consultarMovimientos(FiltroConsultaDTO filtro) throws USException {
		try {
			if (filtro.getLimite() != null) {
				LOG.info("filtro.getLimite() :: " + filtro.getLimite());
			}
			final List<MovimientoDTO> resp = new ArrayList<MovimientoDTO>();

			final Apigate endPoint = getPortApiGate();
			final GetTransactionsRequestType req = new GetTransactionsRequestType();

			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(filtro.getTarjeta().getNumero());
			// ci.setExpDate(filtro.);
			req.setCardIdentification(ci);
			final TransactionDateStrictPeriodType per = new TransactionDateStrictPeriodType();
			per.setStart(getFechaXML(filtro.getInicio()));
			per.setEnd(getFechaXML(filtro.getFin()));
			req.setPeriod(per);

			final GetTransactionsResponseType data = endPoint.getTransactions(req);
			final TransactionsType tt = data.getTransactions();
			final List<TransactionDataType> listaTDs = tt.getTransaction();
			if (CollectionUtils.isNotEmpty(listaTDs)) {
				LOG.info("movs from SV :: " + listaTDs.size());
				MovimientoDTO dto = null;
				MontoDTO monto = null;
				MontoDTO comision = null;
				int registros = 0;
				int pos = listaTDs.size() - 1;
				TransactionDataType row = null;
				while (filtro.getLimite() != null
						&& (filtro.getLimite().intValue() > registros || filtro.getLimite().intValue() < 0)
						&& pos >= 0) {
					row = listaTDs.get(pos);
					dto = new MovimientoDTO();
					dto.setId(row.getUtrnno());
					dto.setDescripcion(row.getTransactionDescription());
					dto.setFecha(row.getAuthorizationDate().toGregorianCalendar().getTime());
					monto = new MontoDTO();
					monto.setMonto(row.getAmount());
					monto.setMoneda(row.getCurrency());
					dto.setDireccionOperacion(MAP_CAT_DIRECCIONES.get(String.valueOf(row.getOperationDirection())));
					dto.setMonto(monto);
					comision = new MontoDTO();
					comision.setMonto(row.getFeIssuerFeeAmount());
					comision.setMoneda(row.getCurrency());
					dto.setDireccionComision(MAP_CAT_DIRECCIONES.get(String.valueOf(row.getFeeDirection())));
					dto.setComision(comision);
					resp.add(dto);
					registros++;
					pos--;
				}
			}

			return resp;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	/**
	 * 
	 * @return
	 * @throws MalformedURLException
	 */
	private Apigate getPortApiGate() throws MalformedURLException {

		final long timeIni = System.currentTimeMillis();
		if (endPoint == null) {
			if (LOG.isDebugEnabled()) {
				System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
				System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
				System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
				System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
				LOG.debug(" - properties dump [OK]");
				LOG.debug(" - Revisar la salida estandar para ver los mensajes SOAP");
			}
			final URL urlEndPoint = new URL(URL_ENDPOINT);
			final Apigate_Service apiGate = new Apigate_Service(urlEndPoint, Apigate_Service.APIGATE_QNAME);
			endPoint = apiGate.getApigate();
		}
		if (LOG.isInfoEnabled()) {
			long timeFin = System.currentTimeMillis();
			LOG.info("Finaliza getPortApiGate() en " + (timeFin - timeIni) + " ms");
		}

		return endPoint;
	}

	private XMLGregorianCalendar getFechaXML(Date fec) {
		final GregorianCalendar c = new GregorianCalendar();
		c.setTime(fec);
		XMLGregorianCalendar date2;
		try {
			date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c.get(Calendar.YEAR),
					c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.HOUR_OF_DAY),
					c.get(Calendar.MINUTE), c.get(Calendar.SECOND), DatatypeConstants.FIELD_UNDEFINED,
					DatatypeConstants.FIELD_UNDEFINED);
			return date2;
		} catch (DatatypeConfigurationException e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	@Override
	public SaldoDTO consultarSaldo(TarjetaLeanDTO tarjeta) throws USException {
		try {
			final Apigate port = getPortApiGate();

			final GetCardBalanceRequestType req = new GetCardBalanceRequestType();

			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tarjeta.getNumero());
			req.setCardIdentification(ci);

			final GetCardBalanceResponseType respTye = port.getCardBalance(req);
			final SaldoDTO dto = new SaldoDTO();
			final MontoDTO monto = new MontoDTO();
			monto.setMonto(respTye.getBalance());
			monto.setMoneda(respTye.getCurrency());
			dto.setMonto(monto);
			dto.setFecha(UtilDates.getCurrentDate());
			return dto;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	@Override
	public EstadoTarjetaDTO bloquearTarjetaByClient(TarjetaVirtualDTO tarjeta) throws USException {
		try {
			final EstadoTarjetaDTO dto = new EstadoTarjetaDTO();
			final Apigate port = getPortApiGate();
			final ChangeCardStatusRequestType req = new ChangeCardStatusRequestType();

			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tarjeta.getNumero());
			ci.setExpDate("20" + tarjeta.getVencimiento());
			req.setCardIdentification(ci);
			req.setHotCardStatus(HOT_STATUS_TEMPORAL_BLOCK_BY_CLIENT);
			final SimpleResponseType resp = port.blockCard(req);
			final String code = resp.getResponseCode();
			LOG.debug("code :: [" + code + "]");
			dto.setSmartVistaStatus(code);
			return dto;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	private void recuperaConfiguracion() throws USException {

		try {
			if (URL_ENDPOINT == null) {
				URL_ENDPOINT = this.paramService.consultarAsString(Params.SIS_PROV_URL_BPC_APIGATE);
				if (LOG.isInfoEnabled()) {
					LOG.info("URL_ENDPOINT :: [" + URL_ENDPOINT + "]");
				}
			}
			if (MAP_CAT_DIRECCIONES == null) {
				MAP_CAT_DIRECCIONES = new HashMap<String, String>();
				MAP_CAT_DIRECCIONES.put("DEBIT", "Cargo");
				MAP_CAT_DIRECCIONES.put("CREDIT", "Abono");
				MAP_CAT_DIRECCIONES.put("NOOP", "NOOP");
				MAP_CAT_DIRECCIONES.put("debit", "Cargo");
				MAP_CAT_DIRECCIONES.put("credit", "Abono");
				MAP_CAT_DIRECCIONES.put("noop", "noop");

			}
		} catch (USException e) {
			LOG.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public EstadoTarjetaDTO preguntarEstadoTarjeta(TarjetaVirtualDTO tarjeta) throws USException {
		final EstadoTarjetaDTO dto = new EstadoTarjetaDTO();
		try {
			final Apigate port = getPortApiGate();
			final CardStatusInquiryRequestType req = new CardStatusInquiryRequestType();

			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tarjeta.getNumero());
			ci.setExpDate("20" + tarjeta.getVencimiento());
			req.setCardIdentification(ci);
			final CardStatusInquiryResponseType resp = port.cardStatusInquiry(req);
			final int code = resp.getHotCardStatus();
			LOG.debug("code :: [" + code + "]");
			dto.setSmartVistaStatus(String.valueOf(code));
			return dto;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			LOG.error(e.getMessage());
			final String code = e.getFaultInfo().getResponseCode();
			LOG.debug("code :: [" + code + "]");
			dto.setSmartVistaStatus(code);
			return dto;
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	@Override
	public EstadoTarjetaDTO activarTarjetaByClient(TarjetaVirtualDTO tarjeta) throws USException {
		try {
			final EstadoTarjetaDTO dto = new EstadoTarjetaDTO();
			final Apigate port = getPortApiGate();
			final ValidateCardRequestType req = new ValidateCardRequestType();

			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tarjeta.getNumero());
			ci.setExpDate("20" + tarjeta.getVencimiento());
			req.setCardIdentification(ci);
			final SimpleResponseType resp = port.validateCard(req);
			final String code = resp.getResponseCode();
			LOG.debug("code :: [" + code + "]");
			dto.setSmartVistaStatus(String.valueOf(code));
			return dto;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	@Override
	public RespuestaCVV obtenerCVV(TarjetaVirtualDTO tarjeta) throws USException {
		RespuestaCVV respDto = new RespuestaCVV();
		final EstadoTarjetaDTO dto = new EstadoTarjetaDTO();
		try {
			final Apigate port = getPortApiGate();
			final GenerateCVC2RequestType req = new GenerateCVC2RequestType();

			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tarjeta.getNumero());
			ci.setExpDate("20" + tarjeta.getVencimiento());
			req.setCardIdentification(ci);
			final GenerateCVC2ResponseType resp = port.generateCVC2(req);
			respDto.setCvv(resp.getCvc2());
			final String code = resp.getResponseCode();
			LOG.debug("code :: [" + code + "]");
			dto.setSmartVistaStatus(String.valueOf(code));
			respDto.setEstadoTarjeta(dto);
			return respDto;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	@Override
	public MovimientoDTO consultarMovimiento(MovimientoDTO tarjeta) throws USException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DetalleCompletoTarjetaVirtualDTO obtenerDetalle(TarjetaVirtualDTO tarjeta) throws USException {
		try {
			final DetalleCompletoTarjetaVirtualDTO respDto = new DetalleCompletoTarjetaVirtualDTO();

			final Apigate port = getPortApiGate();

			final GetCardDataRequestType req = new GetCardDataRequestType();

			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tarjeta.getNumero());
			ci.setExpDate("20" + tarjeta.getVencimiento());
			req.setCardIdentification(ci);

			final GetCardDataResponseType resp = port.getCardData(req);

			respDto.setNumeroEnmascarado(resp.getCardData().getCardNumberMask());
			final AccountsType accTpe = resp.getCardData().getAccounts();
			respDto.setNombreTitular(resp.getCardData().getEmbossedName());
			if (accTpe != null) {
				final List<AccountDataType> cuentas = accTpe.getAccountData();
				if (CollectionUtils.isNotEmpty(cuentas)) {
					for (AccountDataType adt : cuentas) {
						respDto.setCuenta(adt.getNumber());
						final MontoDTO monto = new MontoDTO();
						monto.setMonto(adt.getBalance());
						monto.setMoneda(adt.getCurrency());
						respDto.setSaldo(monto);
						break;
					}
				}
			}
			return respDto;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	@Override
	public DetalleMovimientoDTO purchase(TarjetaVirtualDTO tvEnt, BigInteger monto) throws USException {
		DetalleMovimientoDTO transaction = new DetalleMovimientoDTO();
		try {
			final Apigate port = getPortApiGate();

			final PurchaseRequestType req = new PurchaseRequestType();
			req.setCurrency(484);// TODO VAP poner la moneda en la cuenta
			req.setAmount(monto);
			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tvEnt.getNumero());
			ci.setExpDate("20" + tvEnt.getVencimiento());
			req.setCardIdentification(ci);

			final FinancialTransactionResponseType resp = port.purchase(req);

			transaction.setAutorizacion(resp.getAuthorizationIdResponse());
			final Date fecha = resp.getLocalTransactionDate().toGregorianCalendar().getTime();
			transaction.setFecha(fecha);
			transaction.setNumeroAuditoria(resp.getSystemTraceAuditNumber());
			transaction.setId(Long.parseLong(resp.getRrn()));

			// TODO VAP poner timeput, si no reversa le damos reversal
			return transaction;

		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}
	
	@Override
	public DetalleMovimientoDTO cashDeposit(TarjetaVirtual tvEnt, BigInteger monto) throws USException {
		DetalleMovimientoDTO transaction = new DetalleMovimientoDTO();
		try {
			final Apigate port = getPortApiGate();

			final CashDepositRequestType req = new CashDepositRequestType();
			req.setCurrency(484);// TODO VAP poner la moneda en la cuenta
			req.setAmount(monto);
			final CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber(tvEnt.getOrigen().getNumero());
			req.setCardIdentification(ci);

			final FinancialTransactionResponseType resp = port.cashDeposit(req);

			transaction.setAutorizacion(resp.getAuthorizationIdResponse());
			final Date fecha = resp.getLocalTransactionDate().toGregorianCalendar().getTime();
			transaction.setFecha(fecha);
			transaction.setNumeroAuditoria(resp.getSystemTraceAuditNumber());
			transaction.setId(Long.parseLong(resp.getRrn()));

			// TODO VAP poner timeput, si no reversa le damos reversal
			return transaction;

		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}

	@Override
	public DetalleMovimientoDTO p2pTransfer(TarjetaVirtualDTO source, TarjetaVirtualDTO destination, BigInteger amount) throws USException {
		DetalleMovimientoDTO transaction = new DetalleMovimientoDTO();
		try {
			final Apigate port = getPortApiGate();

			P2PTransferRequestType req = new P2PTransferRequestType();

			CardIdentificationType sourceCardIdentification = new CardIdentificationType();
			sourceCardIdentification.setCardNumber(source.getNumero());

			CardIdentificationType destinationCardIdentification = new CardIdentificationType();
			destinationCardIdentification.setCardNumber(destination.getNumero());

			req.setAmount(amount);
			req.setCurrency(484);// TODO VAP poner la moneda en la cuenta
			req.setSourceCardIdentification(sourceCardIdentification);
			req.setDestinationCardIdentification(destinationCardIdentification);

			FinancialTransactionResponseType resp = port.p2PTransfer(req);
			
			transaction.setAutorizacion(resp.getAuthorizationIdResponse());
			final Date fecha = resp.getLocalTransactionDate().toGregorianCalendar().getTime();
			transaction.setFecha(fecha);
			transaction.setNumeroAuditoria(resp.getSystemTraceAuditNumber());
			transaction.setId(Long.parseLong(resp.getRrn()));
			
			return transaction;
		} catch (ServiceLevelException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas al acceder a SV");
		} catch (SvfeProcessingException e) {
			throw new USException(CodigosError.NEGOCIO_ESTADO_INCORRECTO, "Hay una inconsistencia de negocio con SV");
		} catch (MalformedURLException e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO, "Problemas de conexión con SV");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new USException(CodigosError.INFRA_EXTERNO_NO_DISPONIBLE, "Problemas al acceder a SV");
		}
	}
}
