/**
 * 
 */
package com.unixsycsapi.middleware.test.core.bs.impl;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.api.ApiKeyService;
import com.unixsycsapi.middleware.core.api.BloqueoTarjetaRemoteService;
import com.unixsycsapi.middleware.test.core.base.AbstractRemoteClienteBase;

/**
 * @author TATTVA
 *
 */
public class BloqueoTarjetaServiceClient extends AbstractRemoteClienteBase<BloqueoTarjetaRemoteService> {
	public static void main(String[] args) {
		
		BloqueoTarjetaServiceClient clazz = new BloqueoTarjetaServiceClient();
		clazz.saludar();
		
	}

	private void saludar() {
		try {
			service.saludar("cliente remoto clase main mws-core");
		} catch (USException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
