/**
 * 
 */
package com.unixsycsapi.middleware.test.core.bs.impl;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.api.ApiKeyRemoteService;
import com.unixsycsapi.middleware.test.core.base.AbstractRemoteClienteBase;

/**
 * @author TATTVA
 *
 */
public class ApiKeyServiceClient extends AbstractRemoteClienteBase<ApiKeyRemoteService> {
	public static void main(String[] args) {
		
		ApiKeyServiceClient clazz = new ApiKeyServiceClient();
		clazz.saludar();
		
	}

	private void saludar() {
		try {
			service.validar("1234567890");
		} catch (USException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
