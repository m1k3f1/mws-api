/**
 * 
 */
package com.unixsycsapi.middleware.test.core.base;

import java.lang.reflect.ParameterizedType;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.exception.InfraestructuraException;

/**
 * @author vladimir.aguirre
 *
 */
public class AbstractRemoteClienteBase<T> {
	protected final static Logger LOGGER = LogManager.getLogger(AbstractRemoteClienteBase.class);
	/**
	 * Para obtener la posicio <b>cero</b> del arreglo
	 */
	protected final static int PRIMERA_POSICION = 0;

	/**
	 * Para comparar cuando el arreglo tiene un solo registro.
	 */
	protected final static int SIZE_UNA_POSICION = 1;
	/**
	 * Para trabajar con la clase parametrizada.
	 */
	private Class<T> type = null;

	protected T service = null;

	// nombre ear
	private final static String APP_NAME = "mws-core-ear";
	// nombre del modulo EJB
	private final static String MODULE_NAME = "mws-core-bs-1.0.0";

	protected final static String PATH_FILES = "files/";

	public AbstractRemoteClienteBase() {
		super();
		try {
			service = getRemote();
		} catch (InfraestructuraException e) {
			LOGGER.error(e.getMessage());
		}
	}

	/**
	 * Localiza el EJB de manera remota
	 * 
	 * @return
	 * @throws InfraestructuraException
	 */
	public T getRemote() throws InfraestructuraException {
		// TODO VAP recuperar del pom.xml
		long iniMilis = System.currentTimeMillis();
		final String jndi = "" + APP_NAME + "/" + MODULE_NAME + "//" + getType().getSimpleName().replace("Remote", "")
				+ "Bean!" + getType().getCanonicalName();
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("RemoteService jndi = " + jndi);
		}
		Context context = null;
		final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
		// username
		jndiProperties.put(Context.SECURITY_PRINCIPAL, "admin");
		// password
		jndiProperties.put(Context.SECURITY_CREDENTIALS, "admin123");
		try {
			context = new InitialContext();

			final T remoteProxy = (T) context.lookup(jndi);
			long finMilis = System.currentTimeMillis();
			LOGGER.debug("Proxy recuperado en " + (finMilis - iniMilis) + " ms");
			return remoteProxy;
		} catch (final NamingException ne) {
			LOGGER.error(ne.getMessage(), ne);
			throw new InfraestructuraException("Error al buscar el EJB remoto");
		}
	}

	@SuppressWarnings("rawtypes")
	public Class<T> getType() {
		if (type == null) {
			Class clazz = getClass();

			while (!(clazz.getGenericSuperclass() instanceof ParameterizedType)) {
				clazz = clazz.getSuperclass();
			}

			type = (Class<T>) ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
		}

		return type;
	}

}
