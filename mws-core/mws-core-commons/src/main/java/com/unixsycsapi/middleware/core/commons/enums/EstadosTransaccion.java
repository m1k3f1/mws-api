/**
 * 
 */
package com.unixsycsapi.middleware.core.commons.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vladimir Aguirre Piedragil
 *
 */
public enum EstadosTransaccion implements Serializable {
	/**
	 * SE mando a SV en el application-flow.xml
	 */
	CORRECTO(1L), REGISTRADA(2L), CONFIRMADA(3L);
	final static Map<Long, EstadosTransaccion> map = new HashMap<Long, EstadosTransaccion>();

	private Long id;
	static {
		final EstadosTransaccion[] values = EstadosTransaccion.values();
		for (int i = 0; i < values.length; i++) {
			map.put(values[i].getId(), values[i]);
		}
	}

	private EstadosTransaccion(Long idBD) {
		this.id = idBD;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	public static EstadosTransaccion getById(Long key) {
		return map.get(key);
	}

}
