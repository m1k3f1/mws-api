/**
 * 
 */
package com.unixsycsapi.middleware.core.commons.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vladimir Aguirre Piedragil
 *
 */
public enum EstadosCliente implements Serializable {
	/**
	 * Activo
	 */
	ACTIVO(1L),
	/**
	 * Inactivo
	 */
	INACTIVO(2L);
	final static Map<Long, EstadosCliente> map = new HashMap<Long, EstadosCliente>();

	private Long id;
	static {
		final EstadosCliente[] values = EstadosCliente.values();
		for (int i = 0; i < values.length; i++) {
			map.put(values[i].getId(), values[i]);
		}
	}

	private EstadosCliente(Long idBD) {
		this.id = idBD;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	public static EstadosCliente getById(Long key) {
		return map.get(key);
	}

}
