/**
 * 
 */
package com.unixsycsapi.middleware.core.commons.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vladimir Aguirre Piedragil
 *
 */
public enum EstadosTarjeta implements Serializable {
	/**
	 * SE mando a SV en el application-flow.xml
	 */
	ENROLADA(1L),
	/**
	 * Se ejecuto cardStatusInquiry y reponde hotCardStatus = 0
	 */
	VALIDA(2L), BLOQUEO_TEMPORAL_CLIENTE(3L);
	final static Map<Long, EstadosTarjeta> map = new HashMap<Long, EstadosTarjeta>();

	private Long id;
	static {
		final EstadosTarjeta[] values = EstadosTarjeta.values();
		for (int i = 0; i < values.length; i++) {
			map.put(values[i].getId(), values[i]);
		}
	}

	private EstadosTarjeta(Long idBD) {
		this.id = idBD;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	public static EstadosTarjeta getById(Long key) {
		return map.get(key);
	}

}
