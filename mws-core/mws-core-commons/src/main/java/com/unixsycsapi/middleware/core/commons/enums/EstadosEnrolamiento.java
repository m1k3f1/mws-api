/**
 * 
 */
package com.unixsycsapi.middleware.core.commons.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vladimir Aguirre Piedragil
 *
 */
public enum EstadosEnrolamiento implements Serializable {
	/**
	 * Inicio del proceso de enrolamiento
	 */
	CREADO(1L),
	/**
	 * Se guardo confiando en las validaciones del ATM o aplicacion de
	 * escritorio
	 */
	GUARDADO_INTRANET(2L),
	/**
	 * El proceso se llevo a cabo de manera correcta
	 */
	EXITOSO(3L),
	/**
	 * La <code>Persona</code> ya existia de un registro previo
	 */
	ABORTADO_PERSONA_EXISTE(4L),
	/**
	 * El <code>Cliente</code> ya existia de un registro previo
	 */
	ABORTADO_CLIENTE_EXISTE(5L);
	final static Map<Long, EstadosEnrolamiento> map = new HashMap<Long, EstadosEnrolamiento>();

	private Long id;
	static {
		final EstadosEnrolamiento[] values = EstadosEnrolamiento.values();
		for (int i = 0; i < values.length; i++) {
			map.put(values[i].getId(), values[i]);
		}
	}

	private EstadosEnrolamiento(Long idBD) {
		this.id = idBD;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	public static EstadosEnrolamiento getById(Long key) {
		return map.get(key);
	}

}
