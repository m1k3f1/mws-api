/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.commons.enums;

/**
 * Enumeration para las claves de los parametros
 * 
 * @author Vladimir Aguirre
 *
 */
public enum Params {
	/**
	 * PBC back office
	 */
	SIS_PBC_SFTP_HOST, SIS_PBC_SFTP_PORT, SIS_PBC_SFTP_USER, SIS_PBC_SFTP_PWD, SIS_PBC_SFTP_FOLDER_DESTINO,
	SIS_PBC_SFTP_PATH_FILE_PRIVATE_KEY, SIS_PBC_SFTP_PATH_FILE_KNOW_HOSTS,

	/**
	 * Ruta donde se almacena el respaldo de app_flow generados
	 */
	SIS_PBC_FOLDER_BACKUP_APP_FILE,
	/**
	 * <code>URL</code> de proveedores.
	 */
	SIS_PROV_URL_BPC_APIGATE, SIS_PROV_URL_NUBARIUM_INE, SIS_PROV_URL_NUBARIUM_FACIAL, SIS_PROV_NUBARIUM_FACIAL_ACTIVO,

	/**
	 * <code>URL</code> de servicios internos.
	 */
	SIS_PROV_INT_URL_CM,
	/*
	 * Servicios de bitclub
	 */
	MONTO_BITCLUB, PAN_BITCLUB, URL_NOTIFICACION_EBITWARE, AUTORIZACION_KEY, CRYPTO_SALT;
}