/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.commons.enums;

/**
 * @author Vladimir Aguirre
 *
 */
public enum Documentos {
	IDENTIFICACION(1L), SELFIE(2L);
	private Long id;

	private Documentos(Long idBD) {
		this.id = idBD;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

}
