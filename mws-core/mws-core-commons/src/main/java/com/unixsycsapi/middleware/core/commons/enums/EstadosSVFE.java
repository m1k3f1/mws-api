/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.commons.enums;

/**
 * @author Vladimir Aguirre
 *
 */
public enum EstadosSVFE {
	VALID("0"), NOT_PERMITTED("05"), TEMPORAL_BLOCK_BY_CLIENT("20");

	private String codeSVFE;

	private EstadosSVFE(String code) {
		this.codeSVFE = code;
	}

	/**
	 * @return the codeSVFE
	 */
	public String getCodeSVFE() {
		return codeSVFE;
	}
	
	public int getCodeIntSVFE() {
		return Integer.parseInt(codeSVFE);
	}
}
