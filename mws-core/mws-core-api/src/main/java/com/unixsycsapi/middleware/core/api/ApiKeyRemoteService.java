/**
 * 
 */
package com.unixsycsapi.middleware.core.api;

import javax.ejb.Remote;

/**
 * @author Tejon01
 *
 */
@Remote
public interface ApiKeyRemoteService extends ApiKeyIService {

}
