/**
 * 
 */
package com.unixsycsapi.middleware.core.api;

import javax.ejb.Local;

/**
 * @author Vladimir Aguirre
 *
 */
@Local
public interface BloqueoTarjetaService extends BloqueoTarjetaIService {

}
