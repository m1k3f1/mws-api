/**
 * 
 */
package com.unixsycsapi.middleware.core.api.infra;

import javax.ejb.Remote;

/**
 * @author  Vladimir Aguirre
 *
 */
@Remote
public interface ParametroRemoteService extends ParametroIService {

}
