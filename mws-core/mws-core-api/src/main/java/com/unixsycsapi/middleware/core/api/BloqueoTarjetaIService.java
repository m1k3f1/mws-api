/**
 * 
 */
package com.unixsycsapi.middleware.core.api;

import com.unixsycsapi.framework.commons.exception.USException;

/**
 * @author Vladimir Aguirre
 *
 */
public interface BloqueoTarjetaIService {
	
	void saludar(String data) throws USException;

}
