/**
 * 
 */
package com.unixsycsapi.middleware.core.api;

import javax.ejb.Local;

/**
 * @author Tejon01
 *
 */
@Local
public interface ApiKeyService extends ApiKeyIService {

}
