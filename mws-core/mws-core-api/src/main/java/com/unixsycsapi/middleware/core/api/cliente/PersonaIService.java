/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.api.cliente;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.ClienteDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public interface PersonaIService {
	/**
	 * Busca una persona
	 * @param cli
	 * @return
	 * @throws USException
	 */
	ClienteDTO buscarPersona(ClienteDTO cli) throws USException;


}
