/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.api;

import java.util.List;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.ClienteLeanDTO;
import com.unixsycsapi.middleware.core.dm.DepositoBancarioDTO;
import com.unixsycsapi.middleware.core.dm.DetalleMovimientoDTO;
import com.unixsycsapi.middleware.core.dm.DetalleTarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.dm.FiltroConsultaDTO;
import com.unixsycsapi.middleware.core.dm.MontoDTO;
import com.unixsycsapi.middleware.core.dm.MovimientoDTO;
import com.unixsycsapi.middleware.core.dm.SaldoDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaLeanDTO;
import com.unixsycsapi.middleware.core.dm.TarjetaVirtualDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public interface TarjetaIService {
	/**
	 * @param arch
	 * @throws USException
	 */
	void cargarTiraje(ArchivoDTO arch) throws USException;

	/**
	 * @param filtro
	 * @return
	 * @throws USException
	 */
	List<MovimientoDTO> consultarMovimientos(FiltroConsultaDTO filtro) throws USException;

	/**
	 * @param filtro
	 * @return
	 * @throws USException
	 */
	SaldoDTO consultarSaldo(FiltroConsultaDTO filtro) throws USException;

	/**
	 * @param filtro
	 * @return
	 * @throws USException
	 */
	TarjetaVirtualDTO bloquearByClient(FiltroConsultaDTO filtro) throws USException;

	/**
	 * @param filtro
	 * @return
	 * @throws USException
	 */
	TarjetaVirtualDTO activarByClient(FiltroConsultaDTO filtro) throws USException;

	/**
	 * @param cliente
	 * @return
	 * @throws USException
	 */
	List<TarjetaLeanDTO> consultarTarjetas(ClienteLeanDTO cliente) throws USException;

	/**
	 * @param filtro
	 * @return
	 * @throws USException
	 */
	DetalleTarjetaVirtualDTO consultarTarjeta(FiltroConsultaDTO filtro) throws USException;

	/**
	 * @param filtro
	 * @param monto
	 * @return
	 * @throws USException
	 */
	DetalleMovimientoDTO realizarCompra(FiltroConsultaDTO filtro, MontoDTO monto) throws USException;

	/**
	 * @param filtro
	 * @param monto
	 * @return
	 * @throws USException
	 */
	DetalleMovimientoDTO realizarDeposito(FiltroConsultaDTO filtro, MontoDTO monto) throws USException;

	/**
	 * @param filtro
	 * @param depo
	 * @return
	 * @throws USException
	 */
	DetalleMovimientoDTO registrarDeposito(FiltroConsultaDTO filtro, DepositoBancarioDTO depo) throws USException;

	/**
	 * @param origen
	 * @param destino
	 * @param monto
	 * @return
	 * @throws USException
	 */
	DetalleMovimientoDTO transferenciaP2P(FiltroConsultaDTO origen, FiltroConsultaDTO destino, MontoDTO monto)
			throws USException;

	/**
	 * @param depo
	 * @return
	 * @throws USException
	 */
	int registrarDepositoInterno(DepositoBancarioDTO depo, String validation) throws USException;
	
	/**
	 * @param depo
	 * @return
	 * @throws USException
	 */
	public List<DepositoBancarioDTO> getDepositoInterno() throws USException;
}
