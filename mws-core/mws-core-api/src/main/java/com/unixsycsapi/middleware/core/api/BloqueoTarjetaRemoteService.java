/**
 * 
 */
package com.unixsycsapi.middleware.core.api;

import javax.ejb.Remote;

/**
 * @author Vladimir Aguirre
 *
 */
@Remote
public interface BloqueoTarjetaRemoteService extends BloqueoTarjetaIService {

}
