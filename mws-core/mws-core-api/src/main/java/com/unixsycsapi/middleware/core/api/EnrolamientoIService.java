/**
 * 
 */
package com.unixsycsapi.middleware.core.api;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.EnrolamientoDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoMixDTO;
import com.unixsycsapi.middleware.core.dm.EnrolamientoValidadoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public interface EnrolamientoIService {
	/**
	 * Realiza el guardado de la informacion sin ejecutr ninguna validacion.
	 * 
	 * @param enrolamiento
	 * @return
	 * @throws USException
	 */
	EnrolamientoDTO guardar(EnrolamientoValidadoDTO enrolamiento) throws USException;
	
	/**
	 * Realiza el guardado de la informacion ejecutando las validaciones pertinentes
	 * 
	 * @param enrolamiento
	 * @return
	 * @throws USException
	 */
	EnrolamientoDTO guardarConValidacionFacial(EnrolamientoDTO enrolamiento) throws USException;
	
	/**
	 * Realiza el guardado de la informacion ejecutando las validaciones pertinentes
	 * 
	 * @param enrolamiento
	 * @return
	 * @throws USException
	 */
	EnrolamientoDTO guardarConValidacionFacialTarjeta(EnrolamientoMixDTO enrolamiento) throws USException;	

	/**
	 * Consulta el enrolamiento
	 * 
	 * @param enrolamiento
	 * @return
	 * @throws USException
	 */

	EnrolamientoDTO consultarEnrolamiento(EnrolamientoDTO enrolamiento) throws USException;

}
