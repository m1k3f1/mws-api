/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.api;

import javax.ejb.Remote;

/**
 * @author Vladimir Aguirre
 *
 */
@Remote
public interface TarjetaRemoteService extends TarjetaIService {
	

}
