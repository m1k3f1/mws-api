/**
 * 
 */
package com.unixsycsapi.middleware.core.api.infra;

import javax.ejb.Local;

/**
 * @author  Vladimir Aguirre
 *
 */
@Local
public interface ParametroService extends ParametroIService {

}
