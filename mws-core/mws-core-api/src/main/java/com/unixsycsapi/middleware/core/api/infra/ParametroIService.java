/**
 * 
 */
package com.unixsycsapi.middleware.core.api.infra;

import java.util.List;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.commons.enums.Params;
import com.unixsycsapi.middleware.core.dm.ParametroDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public interface ParametroIService {

	ParametroDTO consultar(Params claveParam) throws USException;

	Integer consultarAsInteger(Params claveParam) throws USException;

	Long consultarAsLong(Params claveParam) throws USException;

	Boolean consultarAsBoolean(Params claveParam) throws USException;

	String consultarAsString(Params claveParam) throws USException;

	List<ParametroDTO> consultarParametros() throws USException;

	ParametroDTO modificar(ParametroDTO dto) throws USException;

}
