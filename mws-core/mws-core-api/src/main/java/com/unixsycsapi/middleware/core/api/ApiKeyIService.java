/**
 * 
 */
package com.unixsycsapi.middleware.core.api;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.dm.TokenDTO;
import com.unixsycsapi.middleware.core.dm.empresa.EmpresaDTO;

/**
 * @author Tejon01
 *
 */
public interface ApiKeyIService {
	Boolean validar(String key) throws USException;

	Boolean validarDummy(String key) throws USException;

	EmpresaDTO recuperarEmpresa(String apiKey) throws USException;

	Boolean validarSesion(String key, String sesion) throws USException;

	TokenDTO iniciarSesion(TokenDTO apikey) throws USException;
}
