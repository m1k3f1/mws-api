package com.unixsycsapi.middleware.core.dm;

import java.io.Serializable;

/**
 * @author angel
 *
 */
public class DepositoBancarioNotificacionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String operationId;
	private String cardId;
	private String type;
	private boolean bitclub;
	private Number amount;
	private Number bitclubAmount;
	private String date;
	private String time;

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isBitclub() {
		return bitclub;
	}

	public void setBitclub(boolean bitclub) {
		this.bitclub = bitclub;
	}

	public Number getAmount() {
		return amount;
	}

	public void setAmount(Number amount) {
		this.amount = amount;
	}

	public Number getBitclubAmount() {
		return bitclubAmount;
	}

	public void setBitclubAmount(Number bitclubAmount) {
		this.bitclubAmount = bitclubAmount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
