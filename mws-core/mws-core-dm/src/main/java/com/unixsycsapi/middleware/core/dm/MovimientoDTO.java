/**
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class MovimientoDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2700367159653781533L;

	private MontoDTO monto;
	private MontoDTO comision;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "America/Mexico_City")
	private Date fecha;
	private String descripcion;
	/**
	 * retiro, abono, noop [no-operation]
	 */
	private String direccionOperacion ;
	/**
	 * retiro, abono, noop [no-operation]
	 */
	private String direccionComision ;

	/**
	 * 
	 */
	public MovimientoDTO() {
	}

	/**
	 * @param id
	 */
	public MovimientoDTO(Long id) {
		super(id);
	}

	/**
	 * @return the monto
	 */
	public MontoDTO getMonto() {
		return monto;
	}

	/**
	 * @param monto
	 *            the monto to set
	 */
	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}

	/**
	 * @return the comision
	 */
	public MontoDTO getComision() {
		return comision;
	}

	/**
	 * @param comision
	 *            the comision to set
	 */
	public void setComision(MontoDTO comision) {
		this.comision = comision;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the direccionOperacion
	 */
	public String getDireccionOperacion() {
		return direccionOperacion;
	}

	/**
	 * @param direccionOperacion the direccionOperacion to set
	 */
	public void setDireccionOperacion(String direccionOperacion) {
		this.direccionOperacion = direccionOperacion;
	}

	/**
	 * @return the direccionComision
	 */
	public String getDireccionComision() {
		return direccionComision;
	}

	/**
	 * @param direccionComision the direccionComision to set
	 */
	public void setDireccionComision(String direccionComision) {
		this.direccionComision = direccionComision;
	}

}
