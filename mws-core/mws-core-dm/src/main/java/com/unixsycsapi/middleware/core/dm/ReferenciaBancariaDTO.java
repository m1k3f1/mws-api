/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class ReferenciaBancariaDTO extends BaseCoreDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 796880518139156284L;
	private String referencia;
	private BancoDTO banco;

	/**
	 * @return the banco
	 */
	public BancoDTO getBanco() {
		return banco;
	}

	/**
	 * @param banco
	 *            the banco to set
	 */
	public void setBanco(BancoDTO banco) {
		this.banco = banco;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia
	 *            the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
}
