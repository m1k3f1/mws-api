/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class RespuestaOperacionDTO extends BaseCoreDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3698893101997297326L;
	private String autorizacion;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "America/Mexico_City")
	private Date fecha;
	private Integer numeroAuditoria;
	private EstadoTransaccionDTO estado;
	/**
	 * @return the autorizacion
	 */
	public String getAutorizacion() {
		return autorizacion;
	}
	/**
	 * @param autorizacion the autorizacion to set
	 */
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the numeroAuditoria
	 */
	public Integer getNumeroAuditoria() {
		return numeroAuditoria;
	}
	/**
	 * @param numeroAuditoria the numeroAuditoria to set
	 */
	public void setNumeroAuditoria(Integer numeroAuditoria) {
		this.numeroAuditoria = numeroAuditoria;
	}
	/**
	 * @return the estado
	 */
	public EstadoTransaccionDTO getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoTransaccionDTO estado) {
		this.estado = estado;
	}
	
}
