/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class DetalleMovimientoDTO extends BaseCoreDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6476925430636565999L;
	private MontoDTO monto;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss", timezone = "America/Mexico_City")
	private Date fecha;
	private String autorizacion;
	private String terminal;
	private Integer numeroAuditoria;


	/**
	 * @return the autorizacion
	 */
	public String getAutorizacion() {
		return autorizacion;
	}

	/**
	 * @param autorizacion the autorizacion to set
	 */
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	/**
	 * @return the terminal
	 */
	public String getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	/**
	 * @return the monto
	 */
	public MontoDTO getMonto() {
		return monto;
	}

	/**
	 * @param monto the monto to set
	 */
	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the numeroAuditoria
	 */
	public Integer getNumeroAuditoria() {
		return numeroAuditoria;
	}

	/**
	 * @param numeroAuditoria the numeroAuditoria to set
	 */
	public void setNumeroAuditoria(Integer numeroAuditoria) {
		this.numeroAuditoria = numeroAuditoria;
	}

}
