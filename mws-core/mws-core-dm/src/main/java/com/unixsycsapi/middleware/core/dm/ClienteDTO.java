/**
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Representa un cliente
 * 
 * @author Vladimir Aguirre
 *
 */
public class ClienteDTO extends ClienteLeanDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9088559092109214017L;
	private String curp;
	private NumeroCelularDTO celular;
	private String nombre;
	private String primerApellido;
	private String segundoApellido;
	private String email;
	private String nacionalidad;
	private String lugarNacimiento;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss", timezone = "America/Mexico_City")
	private Date fechaNacimiento;
	/**
	 * Campo de negocio definido  y conocido por la <code>Empresa</code> que nos consume
	 */
	private String negocio;
	private DireccionDTO direccion;
	
	private Long idPersona;

	
	
	public ClienteDTO() {
		super();
	}

	public ClienteDTO(Long id) {
		super(id);
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the primerApellido
	 */
	public String getPrimerApellido() {
		return primerApellido;
	}

	/**
	 * @param primerApellido
	 *            the primerApellido to set
	 */
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	/**
	 * @return the segundoApellido
	 */
	public String getSegundoApellido() {
		return segundoApellido;
	}

	/**
	 * @param segundoApellido
	 *            the segundoApellido to set
	 */
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the negocio
	 */
	public String getNegocio() {
		return negocio;
	}

	/**
	 * @param negocio
	 *            the negocio to set
	 */
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	/**
	 * @return the celular
	 */
	public NumeroCelularDTO getCelular() {
		return celular;
	}

	/**
	 * @param celular
	 *            the celular to set
	 */
	public void setCelular(NumeroCelularDTO celular) {
		this.celular = celular;
	}

	/**
	 * @return the nacionalidad
	 */
	public String getNacionalidad() {
		return nacionalidad;
	}

	/**
	 * @param nacionalidad
	 *            the nacionalidad to set
	 */
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	/**
	 * @return the curp
	 */
	public String getCurp() {
		return curp;
	}

	/**
	 * @param curp
	 *            the curp to set
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}

	/**
	 * @return the lugarNacimiento
	 */
	public String getLugarNacimiento() {
		return lugarNacimiento;
	}

	/**
	 * @param lugarNacimiento
	 *            the lugarNacimiento to set
	 */
	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	/**
	 * @return the fechaNacimiento
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * @param fechaNacimiento
	 *            the fechaNacimiento to set
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	/**
	 * @return the direccion
	 */
	public DireccionDTO getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion
	 *            the direccion to set
	 */
	public void setDireccion(DireccionDTO direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the idPersona
	 */
	public Long getIdPersona() {
		return idPersona;
	}

	/**
	 * @param idPersona the idPersona to set
	 */
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}



}
