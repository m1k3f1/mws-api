/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoValidadoDTO extends EnrolamientoDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3425459469245396276L;
	/**
	 * Similitud obtenida en la validacion facial
	 */
	private String similitud;
	/**
	 * Codigo obtenido en la validacion del curp
	 */
	private String codigoValidacion;
	/**
	 * Leida del INE
	 */
	private String claveElector;
	private TarjetaLeanDTO tarjeta;

	/**
	 * @return the similitud
	 */
	public String getSimilitud() {
		return similitud;
	}

	/**
	 * @param similitud
	 *            the similitud to set
	 */
	public void setSimilitud(String similitud) {
		this.similitud = similitud;
	}

	/**
	 * @return the codigoValidacion
	 */
	public String getCodigoValidacion() {
		return codigoValidacion;
	}

	/**
	 * @param codigoValidacion
	 *            the codigoValidacion to set
	 */
	public void setCodigoValidacion(String codigoValidacion) {
		this.codigoValidacion = codigoValidacion;
	}

	/**
	 * @return the tarjeta
	 */
	public TarjetaLeanDTO getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta
	 *            the tarjeta to set
	 */
	public void setTarjeta(TarjetaLeanDTO tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the claveElector
	 */
	public String getClaveElector() {
		return claveElector;
	}

	/**
	 * @param claveElector
	 *            the claveElector to set
	 */
	public void setClaveElector(String claveElector) {
		this.claveElector = claveElector;
	}

}
