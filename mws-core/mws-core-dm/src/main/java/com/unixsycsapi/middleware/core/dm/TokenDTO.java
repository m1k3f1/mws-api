/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class TokenDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8038877540442741227L;
	private String idSession;
	private String equipoID;

	/**
	 * @return the idSession
	 */
	public String getIdSession() {
		return idSession;
	}
	/**
	 * @param idSession the idSession to set
	 */
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}
	/**
	 * @return the equipoID
	 */
	public String getEquipoID() {
		return equipoID;
	}
	/**
	 * @param equipoID the equipoID to set
	 */
	public void setEquipoID(String equipoID) {
		this.equipoID = equipoID;
	}

}
