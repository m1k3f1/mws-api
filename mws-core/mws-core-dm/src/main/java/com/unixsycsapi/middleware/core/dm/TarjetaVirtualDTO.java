/**
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Vladimir Aguirre
 *
 */
@JsonIgnoreProperties(value = { "apiKey", "idEmpresa", "vencimiento" })
public class TarjetaVirtualDTO extends TarjetaLeanDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5357472814085875769L;
	private MontoDTO monto;
	private String cuentaEnmascarada;
	private String ultimoDigitos;
	private ClienteDTO titular;

	/**
	 * 
	 */
	public TarjetaVirtualDTO() {
	}

	/**
	 * @return the monto
	 */
	public MontoDTO getMonto() {
		return monto;
	}

	/**
	 * @param monto
	 *            the monto to set
	 */
	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}

	/**
	 * @return the cuentaEnmascarada
	 */
	public String getCuentaEnmascarada() {
		return cuentaEnmascarada;
	}

	/**
	 * @param cuentaEnmascarada
	 *            the cuentaEnmascarada to set
	 */
	public void setCuentaEnmascarada(String cuentaEnmascarada) {
		this.cuentaEnmascarada = cuentaEnmascarada;
	}

	/**
	 * @return the ultimoDigitos
	 */
	public String getUltimoDigitos() {
		return ultimoDigitos;
	}

	/**
	 * @param ultimoDigitos
	 *            the ultimoDigitos to set
	 */
	public void setUltimoDigitos(String ultimoDigitos) {
		this.ultimoDigitos = ultimoDigitos;
	}

	/**
	 * @return the titular
	 */
	public ClienteDTO getTitular() {
		return titular;
	}

	/**
	 * @param titular
	 *            the titular to set
	 */
	public void setTitular(ClienteDTO titular) {
		this.titular = titular;
	}

	

}
