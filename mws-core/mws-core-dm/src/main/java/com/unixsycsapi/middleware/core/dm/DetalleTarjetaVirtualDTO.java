/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

/**
 * @author Vladimir Aguirre
 *
 */
public class DetalleTarjetaVirtualDTO extends TarjetaLeanDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8938021484884178364L;
	private ClienteLeanDTO cliente;
	private String numeroEnmascarado;
	private String cvc;
	private String cuenta;


	/**
	 * @return the cliente
	 */
	public ClienteLeanDTO getCliente() {
		return cliente;
	}
	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(ClienteLeanDTO cliente) {
		this.cliente = cliente;
	}


	/**
	 * @return the numeroEnmascarado
	 */
	public String getNumeroEnmascarado() {
		return numeroEnmascarado;
	}
	/**
	 * @param numeroEnmascarado the numeroEnmascarado to set
	 */
	public void setNumeroEnmascarado(String numeroEnmascarado) {
		this.numeroEnmascarado = numeroEnmascarado;
	}
	/**
	 * @return the cvc
	 */
	public String getCvc() {
		return cvc;
	}
	/**
	 * @param cvc the cvc to set
	 */
	public void setCvc(String cvc) {
		this.cvc = cvc;
	}
	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
}
