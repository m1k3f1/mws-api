/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class EstadoTarjetaDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 616166741558852511L;
	private String negocio;
	/**
	 * Estado de negocio para la entidad, descripción para el cliente (cuenta
	 * habiente) .
	 */
	private String cliente;
	/**
	 * Estado de negocio para la entidad segun SV
	 */
	private String smartVistaStatusDesc;
	private String smartVistaStatus;

	/**
	 * @return the negocio
	 */
	public String getNegocio() {
		return negocio;
	}

	/**
	 * @param negocio
	 *            the negocio to set
	 */
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	/**
	 * @return the cliente
	 */
	public String getCliente() {
		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the smartVistaStatusDesc
	 */
	public String getSmartVistaStatusDesc() {
		return smartVistaStatusDesc;
	}

	/**
	 * @param smartVistaStatusDesc
	 *            the smartVistaStatusDesc to set
	 */
	public void setSmartVistaStatusDesc(String smartVistaStatusDesc) {
		this.smartVistaStatusDesc = smartVistaStatusDesc;
	}

	/**
	 * @return the smartVistaStatus
	 */
	public String getSmartVistaStatus() {
		return smartVistaStatus;
	}

	public Integer getIntSmartVistaStatus() {
		if (smartVistaStatus==null){
			return null;
		}
		return Integer.parseInt(smartVistaStatus);
	}

	
	/**
	 * @param smartVistaStatus
	 *            the smartVistaStatus to set
	 */
	public void setSmartVistaStatus(String smartVistaStatus) {
		this.smartVistaStatus = smartVistaStatus;
	}
}
