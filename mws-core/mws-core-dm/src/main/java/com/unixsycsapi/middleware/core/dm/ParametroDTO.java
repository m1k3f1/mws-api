/**
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import org.apache.commons.lang3.BooleanUtils;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;


/**
 * @author vladimir.aguirre
 *
 */
public class ParametroDTO extends BaseCoreDTO {
	private final static int LENGTH_USER_FRIENDLY = 25;
	/**
	 * 
	 */
	private static final long serialVersionUID = 5684278388641165242L;

	private String clave;
	private String valor;
	private String valorRecortado;
	private String descripcion;

	private Boolean cache;

	/**
	 * 
	 */
	public ParametroDTO() {
	}

	/**
	 * @param id
	 */
	public ParametroDTO(Long id) {
		super(id);
	}

	public boolean isRecortado() {
		if (valor != null && valor.length() > LENGTH_USER_FRIENDLY) {
			this.valorRecortado = valor.substring(0, LENGTH_USER_FRIENDLY);
			return true;
		}

		return false;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave
	 *            the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the cache
	 */
	public Boolean getCache() {
		return cache;
	}

	/**
	 * @param cache
	 *            the cache to set
	 */
	public void setCache(Boolean cache) {
		this.cache = cache;
	}

	/**
	 * @return the valorRecortado
	 */
	public String getValorRecortado() {
		return valorRecortado;
	}

	/**
	 * @param valorRecortado
	 *            the valorRecortado to set
	 */
	public void setValorRecortado(String valorRecortado) {
		this.valorRecortado = valorRecortado;
	}

	public boolean isActivo(){
		return BooleanUtils.toBoolean(valor);
	}
	
}
