/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@JsonIgnoreProperties(value = { "apiKey", "idEmpresa", "smartVistaStatusDesc" })
public class EstadoTransaccionDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2328583982885120951L;
	private String codigo;
	private String nombre;
	private String smartVistaStatusDesc;

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the smartVistaStatusDesc
	 */
	public String getSmartVistaStatusDesc() {
		return smartVistaStatusDesc;
	}

	/**
	 * @param smartVistaStatusDesc
	 *            the smartVistaStatusDesc to set
	 */
	public void setSmartVistaStatusDesc(String smartVistaStatusDesc) {
		this.smartVistaStatusDesc = smartVistaStatusDesc;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
