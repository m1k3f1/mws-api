/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class TirajeDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8016858019749947353L;
	private ArchivoDTO archivo;
}
