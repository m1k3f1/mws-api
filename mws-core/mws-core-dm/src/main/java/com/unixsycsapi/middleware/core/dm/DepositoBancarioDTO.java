/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import java.math.BigInteger;
import java.util.Date;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class DepositoBancarioDTO extends BaseCoreDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5191915211135116064L;

	/**
	 * Punto en el tiempo en que se realiza el deposito en el cajero.
	 */
	private Date fechaDeposito;

	private EstadoTransaccionDTO estado;

	private TarjetaLeanDTO tarjeta;

	private MontoDTO monto;

	/**
	 * Cuenta o tarjeta
	 */

	private String cuenta;
	/**
	 * Motivo o concepto
	 */
	private String motivo;
	/**
	 * Autorizacion/referencia
	 */
	private String autorizacion;
	/**
	 * Si existe el folio/movimiento se guarda
	 */
	private String folio;
	private BancoDTO banco;
	/**
	 * @return the fechaDeposito
	 */
	public Date getFechaDeposito() {
		return fechaDeposito;
	}
	/**
	 * @param fechaDeposito the fechaDeposito to set
	 */
	public void setFechaDeposito(Date fechaDeposito) {
		this.fechaDeposito = fechaDeposito;
	}
	/**
	 * @return the estado
	 */
	public EstadoTransaccionDTO getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoTransaccionDTO estado) {
		this.estado = estado;
	}
	/**
	 * @return the tarjeta
	 */
	public TarjetaLeanDTO getTarjeta() {
		return tarjeta;
	}
	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(TarjetaLeanDTO tarjeta) {
		this.tarjeta = tarjeta;
	}
	/**
	 * @return the monto
	 */
	public MontoDTO getMonto() {
		return monto;
	}
	/**
	 * @param monto the monto to set
	 */
	public void setMonto(MontoDTO monto) {
		this.monto = monto;
	}
	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	/**
	 * @return the autorizacion
	 */
	public String getAutorizacion() {
		return autorizacion;
	}
	/**
	 * @param autorizacion the autorizacion to set
	 */
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}
	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}
	/**
	 * @return the banco
	 */
	public BancoDTO getBanco() {
		return banco;
	}
	/**
	 * @param banco the banco to set
	 */
	public void setBanco(BancoDTO banco) {
		this.banco = banco;
	}
}
