/**
 * 
 */
package com.unixsycsapi.middleware.core.dm.archivo;

import com.unixsycsapi.framework.commons.enums.Multimedias;
import com.unixsycsapi.framework.core.dto.base.CatalogoDTO;

/**
 * @see Multimedias
 * @author vladimir.aguirre
 *
 */
public class TipoMultimediaDTO extends CatalogoDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3677002022556531141L;
	public String mimeType;
	public String extension;

	/**
	 * 
	 */
	public TipoMultimediaDTO() {
		super();
	}

	/**
	 * @param id
	 */
	public TipoMultimediaDTO(Long id) {
		super(id);
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType
	 *            the mimeType to set
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}
}
