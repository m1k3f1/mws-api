/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class ClienteLeanDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6147730154852707171L;
	protected String idCustomer;

	public ClienteLeanDTO() {
		super();
	}

	public ClienteLeanDTO(Long id) {
		super(id);
	}

	public ClienteLeanDTO(String idCustomer) {
		super();
		this.idCustomer = idCustomer;
	}

	/**
	 * @return the idCustomer
	 */
	public String getIdCustomer() {
		return idCustomer;
	}

	/**
	 * @param idCustomer
	 *            the idCustomer to set
	 */
	public void setIdCustomer(String idCustomer) {
		this.idCustomer = idCustomer;
	}
}
