/**
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import java.io.Serializable;
import java.math.BigInteger;

import com.unixsycsapi.framework.commons.util.StringUtilBuilder;

/**
 * @author Vladimir Aguirre
 *
 */
public class MontoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -952120633087107302L;
	
	private int moneda = 484;
	private BigInteger monto = BigInteger.ZERO;
	/**
	 * @return the moneda
	 */
	public int getMoneda() {
		return moneda;
	}
	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(int moneda) {
		this.moneda = moneda;
	}
	/**
	 * @return the monto
	 */
	public BigInteger getMonto() {
		return monto;
	}
	/**
	 * @param monto the monto to set
	 */
	public void setMonto(BigInteger monto) {
		this.monto = monto;
	}
	@Override
	public String toString() {
		return StringUtilBuilder.getDTOAsString(this);
	}
}
