/**
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class UsuarioDTO extends BaseCoreDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4914576093288921440L;
	private String nombre;
	private String display;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}
}
