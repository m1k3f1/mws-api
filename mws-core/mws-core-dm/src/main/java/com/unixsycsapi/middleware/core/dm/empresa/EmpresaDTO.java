/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm.empresa;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class EmpresaDTO extends BaseCoreDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5990439505002025409L;
	private String nombre;
	/**
	 * 
	 */
	public EmpresaDTO() {
	}

	/**
	 * @param id
	 */
	public EmpresaDTO(Long id) {
		super(id);
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
