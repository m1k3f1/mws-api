/**
 * 
 */
package com.unixsycsapi.middleware.core.dm.archivo;

import com.unixsycsapi.framework.commons.enums.Multimedias;
import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author vladimir.aguirre
 * @see Multimedias
 * @see TipoMultimediaDTO
 */
public class ArchivoDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9154434311409943381L;

	/**
	 * Arreglo de <code>bytes</code> que represetan el contenido del archivo
	 */
	private byte[] contenido;

	protected String nombre;
	/**
	 * @see Multimedias
	 */
	private TipoMultimediaDTO tipo;

	/**
	 * 
	 */
	public ArchivoDTO() {
	}

	/**
	 * @param id
	 */
	public ArchivoDTO(Long id) {
		super(id);
	}

	/**
	 * @param nombre
	 */
	public ArchivoDTO(Long id, String nombre) {
		super(id);
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		final StringBuilder cad = new StringBuilder(getClass().getSimpleName());
		cad.append("=[");
		if (id != null) {
			cad.append("id=").append(id);
		}
		if (nombre != null) {
			cad.append(",nombre=").append(nombre);
		}
		if (contenido != null) {
			cad.append(", archivo.length=").append(contenido.length);
		}
		if (tipo != null) {
			cad.append(", tipo.id=").append(tipo.getId());
			cad.append(", tipo.mimeType=").append(tipo.getMimeType());
		}

		cad.append("]");
		return cad.toString();
	}

	/**
	 * @return the contenido
	 */
	public byte[] getContenido() {
		return contenido;
	}

	/**
	 * @param contenido
	 *            the contenido to set
	 */
	public void setContenido(byte[] contenido) {
		this.contenido = contenido;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the tipo
	 */
	public TipoMultimediaDTO getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(TipoMultimediaDTO tipo) {
		this.tipo = tipo;
	}
}
