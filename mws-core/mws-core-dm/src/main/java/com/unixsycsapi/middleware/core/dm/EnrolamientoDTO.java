/**
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;
import com.unixsycsapi.middleware.core.dm.archivo.ArchivoDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class EnrolamientoDTO extends BaseCoreDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7153243216741213651L;
	/**
	 * Cliente a enrolar
	 */
	private ClienteDTO cliente;
	/**
	 * Nombre del titular de la tarjeta
	 */
	private String nombreTitular;
	/**
	 * Tipo de producto que origina el enrolamiento: <br>
	 * identificador de producto (50000002-BitCard, 70000002-MetroCard,
	 * 50000001-MetroCardSocialProgram)
	 */
	private String idProducto;

	/**
	 * tipo de institución bancaria (1001-Transport Card, 1002-Bit Card)
	 */
	private String idInstitucion;
	/**
	 * tipo de cliente (ENTTPERS-personal, ENTTCOMP-company)
	 */
	private String tipoCliente;
	/**
	 * Canal donde se origina el enrolamiento:ATM, handy, etc...
	 */

	private String canal;
	/**
	 * Aplicacion que genera el enrolamiento
	 */
	private String app;

	/**
	 * Fecha de cuando inicio como cliente, si es null se toma el sysdate
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss", timezone = "America/Mexico_City")
	private Date fechaInicio;
	/**
	 * INE
	 */
	private ArchivoDTO identificacion;
	/**
	 * <i>selfie</i>
	 */
	private ArchivoDTO foto;

	public EnrolamientoDTO() {
	}

	/**
	 * @return the cliente
	 */
	public ClienteDTO getCliente() {
		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}

	/**
	 * @param nombreTitular
	 *            the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto
	 *            the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	/**
	 * @return the tipoCliente
	 */
	public String getTipoCliente() {
		return tipoCliente;
	}

	/**
	 * @param tipoCliente
	 *            the tipoCliente to set
	 */
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio
	 *            the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return the identificacion
	 */
	public ArchivoDTO getIdentificacion() {
		return identificacion;
	}

	/**
	 * @param identificacion
	 *            the identificacion to set
	 */
	public void setIdentificacion(ArchivoDTO identificacion) {
		this.identificacion = identificacion;
	}

	/**
	 * @return the foto
	 */
	public ArchivoDTO getFoto() {
		return foto;
	}

	/**
	 * @param foto
	 *            the foto to set
	 */
	public void setFoto(ArchivoDTO foto) {
		this.foto = foto;
	}

	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * @param canal
	 *            the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * @return the idInstitucion
	 */
	public String getIdInstitucion() {
		return idInstitucion;
	}

	/**
	 * @param idInstitucion
	 *            the idInstitucion to set
	 */
	public void setIdInstitucion(String idInstitucion) {
		this.idInstitucion = idInstitucion;
	}

	/**
	 * @return the app
	 */
	public String getApp() {
		return app;
	}

	/**
	 * @param app the app to set
	 */
	public void setApp(String app) {
		this.app = app;
	}

}
