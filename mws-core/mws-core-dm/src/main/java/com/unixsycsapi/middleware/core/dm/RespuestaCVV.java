/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.dm;

import com.unixsycsapi.framework.core.dto.base.BaseCoreDTO;

/**
 * @author Vladimir Aguirre
 *
 */
public class RespuestaCVV extends BaseCoreDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3791864487475515028L;
	private EstadoTarjetaDTO estadoTarjeta;
	private String cvv;

	/**
	 * @return the estadoTarjeta
	 */
	public EstadoTarjetaDTO getEstadoTarjeta() {
		return estadoTarjeta;
	}

	/**
	 * @param estadoTarjeta the estadoTarjeta to set
	 */
	public void setEstadoTarjeta(EstadoTarjetaDTO estadoTarjeta) {
		this.estadoTarjeta = estadoTarjeta;
	}

	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}

	/**
	 * @param cvv the cvv to set
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
}
