/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.test.core.provider.clientws.bpc.svapissuing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

/**
 * @author Vladimir Aguirre
 *
 */
public class SenderFTPTests {
	private final static String FTP_USER = "weblogic";
	private final static String FTP_HOST = "192.168.238.47";
	private final static Integer FTP_PORT = 22;
	private final  static String FTP_PWD = "wld72000";
	
	private final static String FTP_PRIVATE_KEY_PATH = "C:\\logs\\mws\\bpc_tc.pem";
	private final static String FTP_KNOW_HOSTS_PATH = "C:\\logs\\mws\\KnownHostKeys.txt";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			System.out.println("Inicia intento de envio de archivo");
			JSch jsch = new JSch();
			final String privateKey = FTP_PRIVATE_KEY_PATH;
			File file = new File(FTP_PRIVATE_KEY_PATH);
			// String privateKey =
			// "c://Users//pablocs//Desktop//MarioPrivate.ppk";
			jsch.setKnownHosts(new FileInputStream(FTP_KNOW_HOSTS_PATH));

			// jsch.addIdentity(privateKey,"wld72000");
			jsch.addIdentity(privateKey, FTP_PWD);

			System.out.println("identity added ");
			Session session = jsch.getSession(FTP_USER, FTP_HOST, FTP_PORT);
			System.out.println("session created.");

			session.connect();
			System.out.println("session connected.....");

			ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();

			 sftp.cd("/home/weblogic/iofiles/incoming/application/iss/");
//			 System.out.println("Subiendo ..." + origen + nombreArchivo);
			 sftp.put("C:\\logs\\mws\\" + "file.xml", "file.xml");

			System.out.println("Archivos subidos.");

			sftp.exit();
			sftp.disconnect();
			session.disconnect();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JSchException e) {
			e.printStackTrace();
		} catch (SftpException e) {
			e.printStackTrace();
		}

	}

}
