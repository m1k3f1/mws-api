/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.test.core.provider.clientws.bpc.apigate;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;

import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.AccountDataType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.ActivateCardRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.Apigate;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.Apigate_Service;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CardDataType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CardIdentificationType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CardStatusInquiryRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CardStatusInquiryResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CashDepositRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.ChangeCardStatusRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CheckCardRequestRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CheckCardRequestResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CreateVirtualCardRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.CreateVirtualCardResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.FinancialTransactionResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GenerateCVC2RequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GenerateCVC2ResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetAccountDataRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetAccountDataResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardBalanceRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardBalanceResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardDataRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardDataResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardLimitsRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetCardLimitsResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetTransactionsRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.GetTransactionsResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.LimitFullDataType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.P2PTransferRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.PurchaseRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.ServiceLevelException;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.SimpleResponseType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.SvfeProcessingException;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.TransactionDataType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.TransactionDateStrictPeriodType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.TransactionsType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.ValidateCardRequestType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate.VirtualCardIdentificationType;

/**
 * @author Vladimir Aguirre
 *
 */
public class ApigateClientTest {
	private final static int HOT_STATUS_TEMPORAL_BLOCK_BY_CLIENT = 20;

	public static void main(String[] args) {
		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
		System.out.println("props OK");

		// createVirtualCard();
//		getCardData();
		// blockCard();
		// purchase();
//		 cashDeposit();
		 getTransactions();
//		 getBalance();
		// getLimit();
		// activateCard();
		// generateCVC2();
//		 validateCard();
//		 cardStatusInquiry();
//		checkCard();
//		p2PTransfer();

	}

	public static void createVirtualCard() {
		try {

			System.out.println("createVirtualCard");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			CreateVirtualCardRequestType req = new CreateVirtualCardRequestType();

			// CardIdentificationType ci = new CardIdentificationType();
			// ci.setCustomerId("10022830");
			// req.setCardIdentification(ci);
			VirtualCardIdentificationType vci = new VirtualCardIdentificationType();
			vci.setCardNumber("5064430000010216");
			req.setVirtualCardIdentification(vci);

			CreateVirtualCardResponseType resp = endPoint.createVirtualCard(req);
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void getCardData() {
		try {
			System.out.println("getCardData");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GetCardDataRequestType req = new GetCardDataRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//mena
			ci.setCardNumber("5064430900000051");
			ci.setExpDate("202905");
			req.setCardIdentification(ci);

			GetCardDataResponseType resp = endPoint.getCardData(req);
			describe("resp", resp);
			describe("resp.getCardData()", resp.getCardData());
			describe("tAccounts", resp.getCardData().getAccounts());
			for (AccountDataType adt : resp.getCardData().getAccounts().getAccountData()) {
				describe("adt", adt);
				getAccountData(adt.getNumber());
			}

			System.out.println("[OK]");

		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void getAccountData(String cuenta) {
		try {
			System.out.println("getAccountData");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GetAccountDataRequestType req = new GetAccountDataRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			req.setAccountNumber(cuenta);

			GetAccountDataResponseType resp = endPoint.getAccountData(req);
			describe("resp", resp);
			describe("resp.getAccountData()", resp.getAccountData());
			for (CardDataType cdt : resp.getAccountData().getCards().getCardData()) {
				describe("cdt", cdt);

			}

			System.out.println("[OK]");

		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void getValidateCard() {
		try {
			System.out.println("getCardData");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GetCardDataRequestType req = new GetCardDataRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064431000290782");
			// ci.setExpDate("202906");
			ci.setCardNumber("5064430000010711");
			ci.setExpDate("202903");
			req.setCardIdentification(ci);

			GetCardDataResponseType resp = endPoint.getCardData(req);
			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void getTransactions() {
		try {
			System.out.println("getTransactions");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GetTransactionsRequestType req = new GetTransactionsRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//Mena
			ci.setCardNumber("5064431000290923");
			ci.setExpDate("202906");
			req.setCardIdentification(ci);
			TransactionDateStrictPeriodType per = new TransactionDateStrictPeriodType();
			Calendar aux = Calendar.getInstance();
			aux.set(Calendar.HOUR_OF_DAY, 5);
			per.setEnd(getFechaXML(aux.getTime()));
			aux.add(Calendar.DATE, -5);
			aux.set(Calendar.MONTH, Calendar.AUGUST);
			per.setStart(getFechaXML(aux.getTime()));
			req.setPeriod(per);

			GetTransactionsResponseType resp = endPoint.getTransactions(req);
			TransactionsType tt = resp.getTransactions();
			List<TransactionDataType> listaTDs = tt.getTransaction();
			if (CollectionUtils.isNotEmpty(listaTDs)) {
				for (TransactionDataType row : listaTDs) {
					describe(row);
				}
			}
			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void GetTransactionDetailsB() {
		try {
			System.out.println("GetTransactionDetailsB");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GetTransactionsRequestType req = new GetTransactionsRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//Mena
			ci.setCardNumber("5064431000290923");
			ci.setExpDate("202906");
			req.setCardIdentification(ci);
			TransactionDateStrictPeriodType per = new TransactionDateStrictPeriodType();
			Calendar aux = Calendar.getInstance();
			aux.set(Calendar.HOUR_OF_DAY, 5);
			per.setEnd(getFechaXML(aux.getTime()));
			aux.add(Calendar.DATE, -5);
			per.setStart(getFechaXML(aux.getTime()));
			req.setPeriod(per);

			GetTransactionsResponseType resp = endPoint.getTransactions(req);
			TransactionsType tt = resp.getTransactions();
			List<TransactionDataType> listaTDs = tt.getTransaction();
			if (CollectionUtils.isNotEmpty(listaTDs)) {
				for (TransactionDataType row : listaTDs) {
					describe(row);
				}
			}
			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void blockCard() {
		try {
			System.out.println("blockCard");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			ChangeCardStatusRequestType req = new ChangeCardStatusRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//mena
			ci.setCardNumber("5064431000290923");
			ci.setExpDate("202906");
			req.setCardIdentification(ci);
			req.setHotCardStatus(HOT_STATUS_TEMPORAL_BLOCK_BY_CLIENT);
			SimpleResponseType resp = endPoint.blockCard(req);
			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void activateCard() {
		try {
			System.out.println("activateCard");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			ActivateCardRequestType req = new ActivateCardRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber("5064431000290956");
			ci.setExpDate("202906");
			req.setCardIdentification(ci);
			// req.setHotCardStatus(value);
			SimpleResponseType resp = endPoint.activateCard(req);
			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void validateCard() {
		try {
			System.out.println("validateCard");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			ValidateCardRequestType req = new ValidateCardRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber("5064430000010711");// Mena
			ci.setExpDate("202903");// Mena
//			ci.setCardNumber("5064431000290923");
//			ci.setExpDate("202906");
			req.setCardIdentification(ci);
			// req.setHotCardStatus(value);
			SimpleResponseType resp = endPoint.validateCard(req);
			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void cardStatusInquiry() {
		try {
			System.out.println("cardStatusInquiry");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			CardStatusInquiryRequestType req = new CardStatusInquiryRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber("5064430000010711");// Mena
			ci.setExpDate("202903");// Mena
//			ci.setCardNumber("5064431000290808");
//			ci.setExpDate("202906");
			req.setCardIdentification(ci);
			// req.setHotCardStatus(value);
			CardStatusInquiryResponseType resp = endPoint.cardStatusInquiry(req);
			describe("resp", resp);
			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			System.out.println("ServiceLevelException");
			describe(e);
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			System.out.println("SvfeProcessingException");
			System.out.println("e.getFaultInfo().getResponseCode() :: " + e.getFaultInfo().getResponseCode());
			describe(e);
			e.printStackTrace();
		}
	}

	public static void checkCard() {
		try {
			System.out.println("checkCard");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			CheckCardRequestRequestType req = new CheckCardRequestRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//Mena
			ci.setCardNumber("5064431000291038");
			ci.setExpDate("202906");
			req.setCardIdentification(ci);
			// req.setHotCardStatus(value);
			CheckCardRequestResponseType resp = endPoint.checkCard(req);
			describe("resp", resp);
			describe("resp.getCardData()", resp.getCardData());

			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			describe(e);
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			describe(e);
			e.printStackTrace();
		}
	}

	public static void getLimit() {
		try {
			System.out.println("getLimit");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GetCardLimitsRequestType req = new GetCardLimitsRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//mena
			ci.setCardNumber("5064431000290923");
			ci.setExpDate("202906");
			req.setCardIdentification(ci);

			GetCardLimitsResponseType resp = endPoint.getCardLimits(req);
			List<LimitFullDataType> limites = resp.getLimits().getLimit();
			limites.get(0).getValue();
			describe(resp);
			describe(resp.getLimits());

			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void purchase() {
		try {
			System.out.println("purchase");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			PurchaseRequestType req = new PurchaseRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber("5064430000010711");// Mena
			ci.setExpDate("202903");// mena
			// ci.setCardNumber("5064431000290923");
			// ci.setExpDate("202906");
			req.setCurrency(484);
			req.setCardIdentification(ci);
			req.setAmount(new BigInteger("23"));

			FinancialTransactionResponseType resp = endPoint.purchase(req);

			// TODO VAP poner timeput, si no reversa le damos reversal

			describe(resp);

			System.out.println(" purchase is [OK] !!!");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void cashDeposit() {
		try {
			System.out.println("cashDeposit");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			CashDepositRequestType req = new CashDepositRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//mena
			ci.setCardNumber("5064431000293174");
			ci.setExpDate("202906");
			req.setCurrency(484);
			req.setCardIdentification(ci);
			req.setAmount(new BigInteger("21"));

			FinancialTransactionResponseType resp = endPoint.cashDeposit(req);

			// TODO VAP poner timeput, si no reversa le damos reversal

			describe(resp);

			System.out.println(" cashDeposit is [OK] !!!");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	private static void describe(Object obj) {
		try {
			System.out.println(BeanUtils.describe(obj).toString());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

	}

	private static void describe(String cad, Object obj) {
		try {
			System.out.println(cad + " -> " + BeanUtils.describe(obj).toString());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

	}

	public static void reversal() {
		try {
			System.out.println("purchase");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			PurchaseRequestType req = new PurchaseRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber("5064430000010711");
			ci.setExpDate("202903");
			req.setCurrency(484);
			req.setCardIdentification(ci);
			req.setAmount(new BigInteger("10"));

			FinancialTransactionResponseType resp = endPoint.purchase(req);

			System.out.println(BeanUtils.describe(null).toString());

			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public static void getBalance() {
		try {
			System.out.println("getBalance");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GetCardBalanceRequestType req = new GetCardBalanceRequestType();

			CardIdentificationType ci = new CardIdentificationType();
			// ci.setCardNumber("5064430000010711");//Mena
			// ci.setExpDate("202903");//mena
			ci.setCardNumber("5064431000293174");
			ci.setExpDate("202906");
			req.setCardIdentification(ci);

			GetCardBalanceResponseType resp = endPoint.getCardBalance(req);
			resp.getAvailableExceedLimit();

			describe("resp", resp);
			describe("cardIdentification", resp.getCardIdentification());
			System.out.println("resp.getAvailableExceedLimit() ::" + resp.getAvailableExceedLimit());
			System.out.println("resp.getOwnFunds() :: " + resp.getOwnFunds());
			System.out.println("resp.getBalance() :: " + resp.getBalance());

			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void generateCVC2() {
		try {
			System.out.println("generateCVC2");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			GenerateCVC2RequestType req = new GenerateCVC2RequestType();

			CardIdentificationType ci = new CardIdentificationType();
			ci.setCardNumber("5064430000010711");// Mena
			ci.setExpDate("202903");// mena
			// ci.setCardNumber("5064431000290923");
			// ci.setExpDate("202906");
			req.setCardIdentification(ci);

			GenerateCVC2ResponseType resp = endPoint.generateCVC2(req);

			describe(resp);

			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void p2PTransfer() {
		/*
		 * <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
		 * xmlns:p2p="http://www.bpc.ru/apigate/command/p2pTransfer/"
		 * xmlns:gen="http://www.bpc.ru/apigate/general/"> <soapenv:Header/>
		 * <soapenv:Body> <p2p:p2pTransferRequest> <!--Optional:-->
		 * <p2p:sourceCardIdentification> <!--You have a CHOICE of the next 4 items at
		 * this level--> <gen:cardNumber>5064430000010711</gen:cardNumber>
		 * <gen:expDate>202903</gen:expDate> </p2p:sourceCardIdentification>
		 * <p2p:destinationCardIdentification> <!--You have a CHOICE of the next 4 items
		 * at this level--> <gen:cardNumber>5064431000290923</gen:cardNumber>
		 * <gen:expDate>202906</gen:expDate> </p2p:destinationCardIdentification>
		 * <p2p:amount>1</p2p:amount> <p2p:currency>484</p2p:currency>
		 * </p2p:p2pTransferRequest> </soapenv:Body> </soapenv:Envelope>
		 */
		try {
			System.out.println("p2PTransfer");
			System.out.println("Inicia");

			Apigate_Service apiGate = new Apigate_Service();
			Apigate endPoint = apiGate.getApigate();
			P2PTransferRequestType req = new P2PTransferRequestType();

			CardIdentificationType sourceCardIdentification = new CardIdentificationType();
			sourceCardIdentification.setCardNumber("5064430000010711");
			sourceCardIdentification.setExpDate("202903");

			CardIdentificationType destinationCardIdentification = new CardIdentificationType();
			destinationCardIdentification.setCardNumber("5064431000290923");
			destinationCardIdentification.setExpDate("202906");

			BigInteger amount = new BigInteger("1");
			int currency = 484;

			req.setAmount(amount);
			req.setCurrency(currency);
			req.setSourceCardIdentification(sourceCardIdentification);
			req.setDestinationCardIdentification(destinationCardIdentification);

			FinancialTransactionResponseType resp = endPoint.p2PTransfer(req);

			describe(resp);

			System.out.println("[OK]");
		} catch (ServiceLevelException e) {
			e.printStackTrace();
		} catch (SvfeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static XMLGregorianCalendar getFechaXML(Date fec) {
		final GregorianCalendar c = new GregorianCalendar();
		c.setTime(fec);
		XMLGregorianCalendar date2;
		try {
			date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c.get(Calendar.YEAR),
					c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.HOUR_OF_DAY),
					c.get(Calendar.MINUTE), c.get(Calendar.SECOND), DatatypeConstants.FIELD_UNDEFINED,
					DatatypeConstants.FIELD_UNDEFINED);
			return date2;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}
}
