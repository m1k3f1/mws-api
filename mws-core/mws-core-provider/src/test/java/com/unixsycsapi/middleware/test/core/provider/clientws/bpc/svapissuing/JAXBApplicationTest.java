/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.test.core.provider.clientws.bpc.svapissuing;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.ApplicationType;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Applications;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Contract;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Customer;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.Person;
import com.unixsycsapi.middleware.core.provider.clientws.bpc.svapissuing.PersonName;

/**
 * @author Vladimir Aguirre
 *
 */
public class JAXBApplicationTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			final Applications objApp = new Applications();
			final ApplicationType at = new ApplicationType();
			at.setApplicationType("APTPISSA");
			at.setApplicationFlowId(1001);
			at.setApplicationStatus("APST0006");
			at.setInstitutionId(1001);
			at.setAgentNumber("70000001");
			Customer cus = new Customer();
			cus.setCommand("CMMDCREX");
			cus.setCustomerRelation("RSCBEXTR");
			cus.setNationality("484");
			Contract con = new Contract();
			con.setCommand("CMMDCREX");
			con.setContractType("CNTPINIC");
			con.setProductId(70000002);
			Person per = new Person();
			per.setCommand("CMMDCRUP");
			PersonName pName = new PersonName();
			pName.setFirstName("Vlad");
			pName.setSurname("Pax Tests");
			per.addPersonName(pName);

			cus.setPerson(per);
			cus.setContract(con);
			final File file = new File("C:\\logs\\mws\\file.xml");
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			at.setCustomer(cus);
			objApp.addApplication(at);
			JAXBContext jaxbContext = JAXBContext.newInstance(Applications.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(objApp, file);
			jaxbMarshaller.marshal(objApp, baos);
			System.out.println("File OK");
			System.out.println("baos.size() :: "+baos.size());
			
			InputStream isFromFirstData = new ByteArrayInputStream(baos.toByteArray()); 
			System.out.println("isFromFirstData :: " + isFromFirstData);
			jaxbMarshaller.marshal(objApp, System.out);

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
