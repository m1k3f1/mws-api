
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Параметры идентификации виртуальной карты.
 * 
 * <p>Java class for virtualCardIdentificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="virtualCardIdentificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardNumber" type="{http://www.bpc.ru/apigate/general/}cardNumberType" minOccurs="0"/>
 *         &lt;element name="cardNumberMask" type="{http://www.bpc.ru/apigate/general/}maskedCardNumberType" minOccurs="0"/>
 *         &lt;element name="cardId" type="{http://www.bpc.ru/apigate/general/}cardIdType" minOccurs="0"/>
 *         &lt;element name="expDate" type="{http://www.bpc.ru/apigate/general/}cardExpDateType"/>
 *         &lt;element name="productType" type="{http://www.bpc.ru/apigate/general/}productTypeType"/>
 *         &lt;element name="barCode" type="{http://www.bpc.ru/apigate/general/}barCodeType" minOccurs="0"/>
 *         &lt;element name="cvv2" type="{http://www.bpc.ru/apigate/general/}cvv2Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "virtualCardIdentificationType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "cardNumber",
    "cardNumberMask",
    "cardId",
    "expDate",
    "productType",
    "barCode",
    "cvv2"
})
public class VirtualCardIdentificationType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardNumberMask;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String expDate;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String productType;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String barCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cvv2;

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the cardNumberMask property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumberMask() {
        return cardNumberMask;
    }

    /**
     * Sets the value of the cardNumberMask property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumberMask(String value) {
        this.cardNumberMask = value;
    }

    /**
     * Gets the value of the cardId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * Sets the value of the cardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardId(String value) {
        this.cardId = value;
    }

    /**
     * Gets the value of the expDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpDate() {
        return expDate;
    }

    /**
     * Sets the value of the expDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpDate(String value) {
        this.expDate = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the barCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarCode() {
        return barCode;
    }

    /**
     * Sets the value of the barCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarCode(String value) {
        this.barCode = value;
    }

    /**
     * Gets the value of the cvv2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCvv2() {
        return cvv2;
    }

    /**
     * Sets the value of the cvv2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCvv2(String value) {
        this.cvv2 = value;
    }

}
