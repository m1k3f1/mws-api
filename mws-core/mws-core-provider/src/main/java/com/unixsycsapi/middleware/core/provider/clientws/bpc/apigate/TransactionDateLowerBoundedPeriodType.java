
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Диапазон дат со строго ограниченным началом.
 * 
 * <p>Java class for transactionDateLowerBoundedPeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionDateLowerBoundedPeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.bpc.ru/apigate/general/}transactionDatePeriodType">
 *       &lt;sequence>
 *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDateLowerBoundedPeriodType", namespace = "http://www.bpc.ru/apigate/general/")
public class TransactionDateLowerBoundedPeriodType
    extends TransactionDatePeriodType
{


}
