
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for p2pTransferRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="p2pTransferRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sourceCardIdentification" type="{http://www.bpc.ru/apigate/general/}cardIdentificationType" minOccurs="0"/>
 *         &lt;element name="destinationCardIdentification" type="{http://www.bpc.ru/apigate/general/}cardIdentificationType" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://www.bpc.ru/apigate/general/}amountType"/>
 *         &lt;element name="currency" type="{http://www.bpc.ru/apigate/general/}currencyN3CodeType"/>
 *         &lt;element name="sourceAccountNumber" type="{http://www.bpc.ru/apigate/general/}accountNumberType" minOccurs="0"/>
 *         &lt;element name="destinationAccountNumber" type="{http://www.bpc.ru/apigate/general/}accountNumberType" minOccurs="0"/>
 *         &lt;element name="tds" type="{http://www.bpc.ru/apigate/general/}tdsType" minOccurs="0"/>
 *         &lt;element name="senderReceiverInfo" type="{http://www.bpc.ru/apigate/general/}senderReceiverInfoType" minOccurs="0"/>
 *         &lt;element name="pointOfServiceDataCode" type="{http://www.bpc.ru/apigate/general/}pointOfServiceDataCodeType" minOccurs="0"/>
 *         &lt;element name="pointOfServiceConditionCode" type="{http://www.bpc.ru/apigate/general/}pointOfServiceConditionCodeType" minOccurs="0"/>
 *         &lt;element name="cardAcceptorParameters" type="{http://www.bpc.ru/apigate/general/}cardAcceptorParametersType" minOccurs="0"/>
 *         &lt;element name="securityLevelIndicator" type="{http://www.bpc.ru/apigate/general/}securityLevelIndicatorType" minOccurs="0"/>
 *         &lt;element name="originalTransactionParameters" type="{http://www.bpc.ru/apigate/general/}originalTransactionParametersType" minOccurs="0"/>
 *         &lt;element name="posCardholderPresence" type="{http://www.bpc.ru/apigate/general/}posCardholderPresenceType" minOccurs="0"/>
 *         &lt;element name="businessApplicationIdentifier" type="{http://www.bpc.ru/apigate/general/}businessApplicationIdentifierType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "p2pTransferRequestType", namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/", propOrder = {
    "sourceCardIdentification",
    "destinationCardIdentification",
    "amount",
    "currency",
    "sourceAccountNumber",
    "destinationAccountNumber",
    "tds",
    "senderReceiverInfo",
    "pointOfServiceDataCode",
    "pointOfServiceConditionCode",
    "cardAcceptorParameters",
    "securityLevelIndicator",
    "originalTransactionParameters",
    "posCardholderPresence",
    "businessApplicationIdentifier"
})
public class P2PTransferRequestType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected CardIdentificationType sourceCardIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected CardIdentificationType destinationCardIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/", required = true)
    protected BigInteger amount;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected int currency;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected String sourceAccountNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected String destinationAccountNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected TdsType tds;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected SenderReceiverInfoType senderReceiverInfo;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected String pointOfServiceDataCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected String pointOfServiceConditionCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected CardAcceptorParametersType cardAcceptorParameters;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected String securityLevelIndicator;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected OriginalTransactionParametersType originalTransactionParameters;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected Integer posCardholderPresence;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/")
    protected String businessApplicationIdentifier;

    /**
     * Gets the value of the sourceCardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link CardIdentificationType }
     *     
     */
    public CardIdentificationType getSourceCardIdentification() {
        return sourceCardIdentification;
    }

    /**
     * Sets the value of the sourceCardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardIdentificationType }
     *     
     */
    public void setSourceCardIdentification(CardIdentificationType value) {
        this.sourceCardIdentification = value;
    }

    /**
     * Gets the value of the destinationCardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link CardIdentificationType }
     *     
     */
    public CardIdentificationType getDestinationCardIdentification() {
        return destinationCardIdentification;
    }

    /**
     * Sets the value of the destinationCardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardIdentificationType }
     *     
     */
    public void setDestinationCardIdentification(CardIdentificationType value) {
        this.destinationCardIdentification = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAmount(BigInteger value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     */
    public int getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     */
    public void setCurrency(int value) {
        this.currency = value;
    }

    /**
     * Gets the value of the sourceAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    /**
     * Sets the value of the sourceAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceAccountNumber(String value) {
        this.sourceAccountNumber = value;
    }

    /**
     * Gets the value of the destinationAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    /**
     * Sets the value of the destinationAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationAccountNumber(String value) {
        this.destinationAccountNumber = value;
    }

    /**
     * Gets the value of the tds property.
     * 
     * @return
     *     possible object is
     *     {@link TdsType }
     *     
     */
    public TdsType getTds() {
        return tds;
    }

    /**
     * Sets the value of the tds property.
     * 
     * @param value
     *     allowed object is
     *     {@link TdsType }
     *     
     */
    public void setTds(TdsType value) {
        this.tds = value;
    }

    /**
     * Gets the value of the senderReceiverInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SenderReceiverInfoType }
     *     
     */
    public SenderReceiverInfoType getSenderReceiverInfo() {
        return senderReceiverInfo;
    }

    /**
     * Sets the value of the senderReceiverInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SenderReceiverInfoType }
     *     
     */
    public void setSenderReceiverInfo(SenderReceiverInfoType value) {
        this.senderReceiverInfo = value;
    }

    /**
     * Gets the value of the pointOfServiceDataCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointOfServiceDataCode() {
        return pointOfServiceDataCode;
    }

    /**
     * Sets the value of the pointOfServiceDataCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointOfServiceDataCode(String value) {
        this.pointOfServiceDataCode = value;
    }

    /**
     * Gets the value of the pointOfServiceConditionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointOfServiceConditionCode() {
        return pointOfServiceConditionCode;
    }

    /**
     * Sets the value of the pointOfServiceConditionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointOfServiceConditionCode(String value) {
        this.pointOfServiceConditionCode = value;
    }

    /**
     * Gets the value of the cardAcceptorParameters property.
     * 
     * @return
     *     possible object is
     *     {@link CardAcceptorParametersType }
     *     
     */
    public CardAcceptorParametersType getCardAcceptorParameters() {
        return cardAcceptorParameters;
    }

    /**
     * Sets the value of the cardAcceptorParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardAcceptorParametersType }
     *     
     */
    public void setCardAcceptorParameters(CardAcceptorParametersType value) {
        this.cardAcceptorParameters = value;
    }

    /**
     * Gets the value of the securityLevelIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityLevelIndicator() {
        return securityLevelIndicator;
    }

    /**
     * Sets the value of the securityLevelIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityLevelIndicator(String value) {
        this.securityLevelIndicator = value;
    }

    /**
     * Gets the value of the originalTransactionParameters property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalTransactionParametersType }
     *     
     */
    public OriginalTransactionParametersType getOriginalTransactionParameters() {
        return originalTransactionParameters;
    }

    /**
     * Sets the value of the originalTransactionParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalTransactionParametersType }
     *     
     */
    public void setOriginalTransactionParameters(OriginalTransactionParametersType value) {
        this.originalTransactionParameters = value;
    }

    /**
     * Gets the value of the posCardholderPresence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPosCardholderPresence() {
        return posCardholderPresence;
    }

    /**
     * Sets the value of the posCardholderPresence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPosCardholderPresence(Integer value) {
        this.posCardholderPresence = value;
    }

    /**
     * Gets the value of the businessApplicationIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessApplicationIdentifier() {
        return businessApplicationIdentifier;
    }

    /**
     * Sets the value of the businessApplicationIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessApplicationIdentifier(String value) {
        this.businessApplicationIdentifier = value;
    }

}
