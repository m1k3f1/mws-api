
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Параметры идентификации карты.
 * 
 * <p>Java class for cardIdentificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardIdentificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="cardNumber" type="{http://www.bpc.ru/apigate/general/}cardNumberType"/>
 *           &lt;element name="cardLastDigitMask" type="{http://www.bpc.ru/apigate/general/}cardLastDigitMaskType"/>
 *           &lt;element name="cardNumberMask" type="{http://www.bpc.ru/apigate/general/}string255"/>
 *           &lt;element name="encryptedCardNumber" type="{http://www.w3.org/2001/XMLSchema}hexBinary"/>
 *         &lt;/choice>
 *         &lt;element name="cardId" type="{http://www.bpc.ru/apigate/general/}cardIdType" minOccurs="0"/>
 *         &lt;element name="expDate" type="{http://www.bpc.ru/apigate/general/}cardExpDateType" minOccurs="0"/>
 *         &lt;element name="plasticNumber" type="{http://www.bpc.ru/apigate/general/}cardPlasticNumberType" minOccurs="0"/>
 *         &lt;element name="phoneNumber" type="{http://www.bpc.ru/apigate/general/}phoneNumberType" minOccurs="0"/>
 *         &lt;element name="cardholderId" type="{http://www.bpc.ru/apigate/general/}cardholderIdType" minOccurs="0"/>
 *         &lt;element name="customerId" type="{http://www.bpc.ru/apigate/general/}customerIdType" minOccurs="0"/>
 *         &lt;element name="barCode" type="{http://www.bpc.ru/apigate/general/}barCodeType" minOccurs="0"/>
 *         &lt;element name="cvv2" type="{http://www.bpc.ru/apigate/general/}cvv2Type" minOccurs="0"/>
 *         &lt;element name="externalCardId" type="{http://www.bpc.ru/apigate/general/}string255" minOccurs="0"/>
 *         &lt;element name="token" type="{http://www.bpc.ru/apigate/general/}string255" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardIdentificationType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "cardNumber",
    "cardLastDigitMask",
    "cardNumberMask",
    "encryptedCardNumber",
    "cardId",
    "expDate",
    "plasticNumber",
    "phoneNumber",
    "cardholderId",
    "customerId",
    "barCode",
    "cvv2",
    "externalCardId",
    "token"
})
public class CardIdentificationType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardLastDigitMask;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardNumberMask;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", type = String.class)
    @XmlJavaTypeAdapter(HexBinaryAdapter.class)
    @XmlSchemaType(name = "hexBinary")
    protected byte[] encryptedCardNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String expDate;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected Integer plasticNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String phoneNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardholderId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String customerId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String barCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cvv2;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String externalCardId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String token;

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the cardLastDigitMask property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardLastDigitMask() {
        return cardLastDigitMask;
    }

    /**
     * Sets the value of the cardLastDigitMask property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardLastDigitMask(String value) {
        this.cardLastDigitMask = value;
    }

    /**
     * Gets the value of the cardNumberMask property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumberMask() {
        return cardNumberMask;
    }

    /**
     * Sets the value of the cardNumberMask property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumberMask(String value) {
        this.cardNumberMask = value;
    }

    /**
     * Gets the value of the encryptedCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public byte[] getEncryptedCardNumber() {
        return encryptedCardNumber;
    }

    /**
     * Sets the value of the encryptedCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptedCardNumber(byte[] value) {
        this.encryptedCardNumber = value;
    }

    /**
     * Gets the value of the cardId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * Sets the value of the cardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardId(String value) {
        this.cardId = value;
    }

    /**
     * Gets the value of the expDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpDate() {
        return expDate;
    }

    /**
     * Sets the value of the expDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpDate(String value) {
        this.expDate = value;
    }

    /**
     * Gets the value of the plasticNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlasticNumber() {
        return plasticNumber;
    }

    /**
     * Sets the value of the plasticNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlasticNumber(Integer value) {
        this.plasticNumber = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the cardholderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardholderId() {
        return cardholderId;
    }

    /**
     * Sets the value of the cardholderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardholderId(String value) {
        this.cardholderId = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the barCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarCode() {
        return barCode;
    }

    /**
     * Sets the value of the barCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarCode(String value) {
        this.barCode = value;
    }

    /**
     * Gets the value of the cvv2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCvv2() {
        return cvv2;
    }

    /**
     * Sets the value of the cvv2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCvv2(String value) {
        this.cvv2 = value;
    }

    /**
     * Gets the value of the externalCardId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalCardId() {
        return externalCardId;
    }

    /**
     * Sets the value of the externalCardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalCardId(String value) {
        this.externalCardId = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

}
