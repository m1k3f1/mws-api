
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createVirtualCardRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createVirtualCardRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardIdentification" type="{http://www.bpc.ru/apigate/general/}cardIdentificationType"/>
 *         &lt;element name="virtualCardIdentification" type="{http://www.bpc.ru/apigate/general/}virtualCardIdentificationType"/>
 *         &lt;element name="amount" type="{http://www.bpc.ru/apigate/general/}amountType"/>
 *         &lt;element name="currency" type="{http://www.bpc.ru/apigate/general/}currencyN3CodeType"/>
 *         &lt;element name="accountNumber" type="{http://www.bpc.ru/apigate/general/}accountNumberType" minOccurs="0"/>
 *         &lt;element name="deliveryMethod" type="{http://www.bpc.ru/apigate/general/}virtualCardDataDeliveryMethodType" minOccurs="0"/>
 *         &lt;element name="cardAcceptorParameters" type="{http://www.bpc.ru/apigate/general/}cardAcceptorParametersType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createVirtualCardRequestType", namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", propOrder = {
    "cardIdentification",
    "virtualCardIdentification",
    "amount",
    "currency",
    "accountNumber",
    "deliveryMethod",
    "cardAcceptorParameters"
})
public class CreateVirtualCardRequestType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", required = true)
    protected CardIdentificationType cardIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", required = true)
    protected VirtualCardIdentificationType virtualCardIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", required = true)
    protected BigInteger amount;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/")
    protected int currency;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/")
    protected String accountNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/")
    @XmlSchemaType(name = "string")
    protected VirtualCardDataDeliveryMethodType deliveryMethod;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/")
    protected CardAcceptorParametersType cardAcceptorParameters;

    /**
     * Gets the value of the cardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link CardIdentificationType }
     *     
     */
    public CardIdentificationType getCardIdentification() {
        return cardIdentification;
    }

    /**
     * Sets the value of the cardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardIdentificationType }
     *     
     */
    public void setCardIdentification(CardIdentificationType value) {
        this.cardIdentification = value;
    }

    /**
     * Gets the value of the virtualCardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link VirtualCardIdentificationType }
     *     
     */
    public VirtualCardIdentificationType getVirtualCardIdentification() {
        return virtualCardIdentification;
    }

    /**
     * Sets the value of the virtualCardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualCardIdentificationType }
     *     
     */
    public void setVirtualCardIdentification(VirtualCardIdentificationType value) {
        this.virtualCardIdentification = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAmount(BigInteger value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     */
    public int getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     */
    public void setCurrency(int value) {
        this.currency = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the deliveryMethod property.
     * 
     * @return
     *     possible object is
     *     {@link VirtualCardDataDeliveryMethodType }
     *     
     */
    public VirtualCardDataDeliveryMethodType getDeliveryMethod() {
        return deliveryMethod;
    }

    /**
     * Sets the value of the deliveryMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualCardDataDeliveryMethodType }
     *     
     */
    public void setDeliveryMethod(VirtualCardDataDeliveryMethodType value) {
        this.deliveryMethod = value;
    }

    /**
     * Gets the value of the cardAcceptorParameters property.
     * 
     * @return
     *     possible object is
     *     {@link CardAcceptorParametersType }
     *     
     */
    public CardAcceptorParametersType getCardAcceptorParameters() {
        return cardAcceptorParameters;
    }

    /**
     * Sets the value of the cardAcceptorParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardAcceptorParametersType }
     *     
     */
    public void setCardAcceptorParameters(CardAcceptorParametersType value) {
        this.cardAcceptorParameters = value;
    }

}
