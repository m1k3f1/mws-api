
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para pinDeliveryMethodType.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="pinDeliveryMethodType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SMS"/>
 *     &lt;enumeration value="WS"/>
 *     &lt;enumeration value="HALF_AND_HALF"/>
 *     &lt;enumeration value="WS_ENCRYPTED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "pinDeliveryMethodType", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum PinDeliveryMethodType {

    SMS,
    WS,
    HALF_AND_HALF,
    WS_ENCRYPTED;

    public String value() {
        return name();
    }

    public static PinDeliveryMethodType fromValue(String v) {
        return valueOf(v);
    }

}
