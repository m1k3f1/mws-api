
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAccountDataResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAccountDataResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountData" type="{http://www.bpc.ru/apigate/general/}accountDataDetailedType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAccountDataResponseType", namespace = "http://www.bpc.ru/apigate/command/getAccountData/", propOrder = {
    "accountData"
})
public class GetAccountDataResponseType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getAccountData/", required = true)
    protected AccountDataDetailedType accountData;

    /**
     * Gets the value of the accountData property.
     * 
     * @return
     *     possible object is
     *     {@link AccountDataDetailedType }
     *     
     */
    public AccountDataDetailedType getAccountData() {
        return accountData;
    }

    /**
     * Sets the value of the accountData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountDataDetailedType }
     *     
     */
    public void setAccountData(AccountDataDetailedType value) {
        this.accountData = value;
    }

}
