
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTokenListResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTokenListResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.bpc.ru/apigate/command/getTokenList/}tokenList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTokenListResponseType", namespace = "http://www.bpc.ru/apigate/command/getTokenList/", propOrder = {
    "tokenList"
})
public class GetTokenListResponseType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTokenList/", required = true)
    protected TokenList tokenList;

    /**
     * Gets the value of the tokenList property.
     * 
     * @return
     *     possible object is
     *     {@link TokenList }
     *     
     */
    public TokenList getTokenList() {
        return tokenList;
    }

    /**
     * Sets the value of the tokenList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TokenList }
     *     
     */
    public void setTokenList(TokenList value) {
        this.tokenList = value;
    }

}
