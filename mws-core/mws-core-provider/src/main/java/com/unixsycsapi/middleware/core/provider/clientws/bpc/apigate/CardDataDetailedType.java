
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Детальные параметры карты.
 * 
 * <p>Java class for cardDataDetailedType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardDataDetailedType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.bpc.ru/apigate/general/}cardDataType">
 *       &lt;sequence>
 *         &lt;element name="accounts" type="{http://www.bpc.ru/apigate/general/}accountsType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardDataDetailedType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "accounts"
})
public class CardDataDetailedType
    extends CardDataType
{

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected AccountsType accounts;

    /**
     * Gets the value of the accounts property.
     * 
     * @return
     *     possible object is
     *     {@link AccountsType }
     *     
     */
    public AccountsType getAccounts() {
        return accounts;
    }

    /**
     * Sets the value of the accounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountsType }
     *     
     */
    public void setAccounts(AccountsType value) {
        this.accounts = value;
    }

}
