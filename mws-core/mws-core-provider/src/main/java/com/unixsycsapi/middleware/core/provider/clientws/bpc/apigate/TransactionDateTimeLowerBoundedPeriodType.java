
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Временной диапазон со строго ограниченным началом.
 * 
 * <p>Java class for transactionDateTimeLowerBoundedPeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionDateTimeLowerBoundedPeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.bpc.ru/apigate/general/}transactionDateTimePeriodType">
 *       &lt;sequence>
 *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDateTimeLowerBoundedPeriodType", namespace = "http://www.bpc.ru/apigate/general/")
public class TransactionDateTimeLowerBoundedPeriodType
    extends TransactionDateTimePeriodType
{


}
