
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Параметры счета.
 * 
 * <p>Java class for accountDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accountDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="number" type="{http://www.bpc.ru/apigate/general/}accountNumberType"/>
 *         &lt;element name="currency" type="{http://www.bpc.ru/apigate/general/}currencyN3CodeType"/>
 *         &lt;element name="balance" type="{http://www.bpc.ru/apigate/general/}amountType"/>
 *         &lt;element name="customerId" type="{http://www.bpc.ru/apigate/general/}customerIdType"/>
 *         &lt;element name="defaultAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accountDataType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "number",
    "currency",
    "balance",
    "customerId",
    "defaultAccount"
})
public class AccountDataType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String number;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected int currency;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected BigInteger balance;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String customerId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected Boolean defaultAccount;

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     */
    public int getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     */
    public void setCurrency(int value) {
        this.currency = value;
    }

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBalance(BigInteger value) {
        this.balance = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the defaultAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultAccount() {
        return defaultAccount;
    }

    /**
     * Sets the value of the defaultAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultAccount(Boolean value) {
        this.defaultAccount = value;
    }

}
