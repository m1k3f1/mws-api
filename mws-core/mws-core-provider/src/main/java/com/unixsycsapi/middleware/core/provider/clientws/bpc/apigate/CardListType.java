
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Перечень карт.
 * 
 * <p>Java class for cardListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardList" type="{http://www.bpc.ru/apigate/general/}cardListElement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardListType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "cardList"
})
public class CardListType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected List<CardListElement> cardList;

    /**
     * Gets the value of the cardList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cardList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCardList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CardListElement }
     * 
     * 
     */
    public List<CardListElement> getCardList() {
        if (cardList == null) {
            cardList = new ArrayList<CardListElement>();
        }
        return this.cardList;
    }

}
