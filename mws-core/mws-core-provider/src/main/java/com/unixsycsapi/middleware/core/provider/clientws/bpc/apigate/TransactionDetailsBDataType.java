
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Детальные параметры транзакции.
 * 
 * <p>Java class for transactionDetailsBDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionDetailsBDataType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.bpc.ru/apigate/general/}transactionBDataType">
 *       &lt;sequence>
 *         &lt;element name="cardAcceptorTerminalId" type="{http://www.bpc.ru/apigate/general/}string255" minOccurs="0"/>
 *         &lt;element name="cardAcceptorCode" type="{http://www.bpc.ru/apigate/general/}string255" minOccurs="0"/>
 *         &lt;element name="cardAcceptorNameAndCode" type="{http://www.bpc.ru/apigate/general/}string255" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDetailsBDataType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "cardAcceptorTerminalId",
    "cardAcceptorCode",
    "cardAcceptorNameAndCode"
})
public class TransactionDetailsBDataType
    extends TransactionBDataType
{

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardAcceptorTerminalId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardAcceptorCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardAcceptorNameAndCode;

    /**
     * Gets the value of the cardAcceptorTerminalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorTerminalId() {
        return cardAcceptorTerminalId;
    }

    /**
     * Sets the value of the cardAcceptorTerminalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorTerminalId(String value) {
        this.cardAcceptorTerminalId = value;
    }

    /**
     * Gets the value of the cardAcceptorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorCode() {
        return cardAcceptorCode;
    }

    /**
     * Sets the value of the cardAcceptorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorCode(String value) {
        this.cardAcceptorCode = value;
    }

    /**
     * Gets the value of the cardAcceptorNameAndCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorNameAndCode() {
        return cardAcceptorNameAndCode;
    }

    /**
     * Sets the value of the cardAcceptorNameAndCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorNameAndCode(String value) {
        this.cardAcceptorNameAndCode = value;
    }

}
