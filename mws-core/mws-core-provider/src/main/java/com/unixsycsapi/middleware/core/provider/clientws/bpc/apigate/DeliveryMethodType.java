
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deliveryMethodType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="deliveryMethodType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SMS"/>
 *     &lt;enumeration value="WS"/>
 *     &lt;enumeration value="HALF_AND_HALF"/>
 *     &lt;enumeration value="WS_ENCRYPTED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "deliveryMethodType", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum DeliveryMethodType {

    SMS,
    WS,
    HALF_AND_HALF,
    WS_ENCRYPTED;

    public String value() {
        return name();
    }

    public static DeliveryMethodType fromValue(String v) {
        return valueOf(v);
    }

}
