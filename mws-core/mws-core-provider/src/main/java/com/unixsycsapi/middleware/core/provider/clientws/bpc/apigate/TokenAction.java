
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tokenAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tokenAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="TOKEN_UPDATE"/>
 *     &lt;enumeration value="TOKEN_SUSPEND"/>
 *     &lt;enumeration value="TOKEN_RESUME"/>
 *     &lt;enumeration value="TOKEN_DEACTIVATE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tokenAction", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum TokenAction {

    TOKEN_UPDATE,
    TOKEN_SUSPEND,
    TOKEN_RESUME,
    TOKEN_DEACTIVATE;

    public String value() {
        return name();
    }

    public static TokenAction fromValue(String v) {
        return valueOf(v);
    }

}
