
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Перечень исключений лимита.
 * 
 * <p>Java class for limitExceptionsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="limitExceptionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exception" type="{http://www.bpc.ru/apigate/general/}limitExceptionType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "limitExceptionsType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "exception"
})
public class LimitExceptionsType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected List<LimitExceptionType> exception;

    /**
     * Gets the value of the exception property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exception property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getException().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LimitExceptionType }
     * 
     * 
     */
    public List<LimitExceptionType> getException() {
        if (exception == null) {
            exception = new ArrayList<LimitExceptionType>();
        }
        return this.exception;
    }

}
