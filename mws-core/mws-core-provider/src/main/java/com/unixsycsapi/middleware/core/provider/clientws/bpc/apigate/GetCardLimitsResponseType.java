
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardLimitsResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardLimitsResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="limits" type="{http://www.bpc.ru/apigate/general/}limitsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardLimitsResponseType", namespace = "http://www.bpc.ru/apigate/command/getCardLimits/", propOrder = {
    "limits"
})
public class GetCardLimitsResponseType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getCardLimits/", required = true)
    protected LimitsType limits;

    /**
     * Gets the value of the limits property.
     * 
     * @return
     *     possible object is
     *     {@link LimitsType }
     *     
     */
    public LimitsType getLimits() {
        return limits;
    }

    /**
     * Sets the value of the limits property.
     * 
     * @param value
     *     allowed object is
     *     {@link LimitsType }
     *     
     */
    public void setLimits(LimitsType value) {
        this.limits = value;
    }

}
