
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for actionCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="actionCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACTION_CODE_ADD"/>
 *     &lt;enumeration value="ACTION_CODE_UPDATE"/>
 *     &lt;enumeration value="ACTION_CODE_DELETE"/>
 *     &lt;enumeration value="ACTION_CODE_CHECK"/>
 *     &lt;enumeration value="ACTION_CODE_CHANGE_ACCESS"/>
 *     &lt;enumeration value="ACTION_CODE_CHECK_PARAMETERS"/>
 *     &lt;enumeration value="ACTION_CODE_RESTORE_PARAMETERS"/>
 *     &lt;enumeration value="ACTION_CODE_INQUIRE"/>
 *     &lt;enumeration value="ACTION_CODE_ACTION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "actionCodeType", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum ActionCodeType {

    ACTION_CODE_ADD,
    ACTION_CODE_UPDATE,
    ACTION_CODE_DELETE,
    ACTION_CODE_CHECK,
    ACTION_CODE_CHANGE_ACCESS,
    ACTION_CODE_CHECK_PARAMETERS,
    ACTION_CODE_RESTORE_PARAMETERS,
    ACTION_CODE_INQUIRE,
    ACTION_CODE_ACTION;

    public String value() {
        return name();
    }

    public static ActionCodeType fromValue(String v) {
        return valueOf(v);
    }

}
