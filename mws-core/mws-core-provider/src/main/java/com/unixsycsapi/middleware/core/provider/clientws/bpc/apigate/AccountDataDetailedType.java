
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Детальные параметры счета.
 * 
 * <p>Java class for accountDataDetailedType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accountDataDetailedType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.bpc.ru/apigate/general/}accountDataType">
 *       &lt;sequence>
 *         &lt;element name="cards" type="{http://www.bpc.ru/apigate/general/}cardsType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accountDataDetailedType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "cards"
})
public class AccountDataDetailedType
    extends AccountDataType
{

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected CardsType cards;

    /**
     * Gets the value of the cards property.
     * 
     * @return
     *     possible object is
     *     {@link CardsType }
     *     
     */
    public CardsType getCards() {
        return cards;
    }

    /**
     * Sets the value of the cards property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardsType }
     *     
     */
    public void setCards(CardsType value) {
        this.cards = value;
    }

}
