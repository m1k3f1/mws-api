
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Параметры идентификации сервиса.
 * 
 * <p>Java class for serviceIdentificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="serviceIdentificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceType" type="{http://www.bpc.ru/apigate/general/}serviceTypeType"/>
 *         &lt;element name="serviceId" type="{http://www.bpc.ru/apigate/general/}serviceIdType"/>
 *         &lt;element name="serviceObjectType" type="{http://www.bpc.ru/apigate/general/}serviceObjectTypeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "serviceIdentificationType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "serviceType",
    "serviceId",
    "serviceObjectType"
})
public class ServiceIdentificationType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String serviceType;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String serviceId;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    @XmlSchemaType(name = "string")
    protected ServiceObjectTypeType serviceObjectType;

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the serviceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceId(String value) {
        this.serviceId = value;
    }

    /**
     * Gets the value of the serviceObjectType property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceObjectTypeType }
     *     
     */
    public ServiceObjectTypeType getServiceObjectType() {
        return serviceObjectType;
    }

    /**
     * Sets the value of the serviceObjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceObjectTypeType }
     *     
     */
    public void setServiceObjectType(ServiceObjectTypeType value) {
        this.serviceObjectType = value;
    }

}
