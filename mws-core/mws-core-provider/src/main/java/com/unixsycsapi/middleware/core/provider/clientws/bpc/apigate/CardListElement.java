
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Элемент списка карт.
 * 
 * <p>Java class for cardListElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardListElement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="cardNumber" type="{http://www.bpc.ru/apigate/general/}cardNumberType"/>
 *           &lt;element name="cardNumberMask" type="{http://www.bpc.ru/apigate/general/}maskedCardNumberType"/>
 *         &lt;/choice>
 *         &lt;element name="expiryDate" type="{http://www.bpc.ru/apigate/general/}cardExpDateType" minOccurs="0"/>
 *         &lt;element name="authenticationFlag" type="{http://www.bpc.ru/apigate/general/}string255"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardListElement", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "cardNumber",
    "cardNumberMask",
    "expiryDate",
    "authenticationFlag"
})
public class CardListElement {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String cardNumberMask;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String expiryDate;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String authenticationFlag;

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the cardNumberMask property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumberMask() {
        return cardNumberMask;
    }

    /**
     * Sets the value of the cardNumberMask property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumberMask(String value) {
        this.cardNumberMask = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the authenticationFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthenticationFlag() {
        return authenticationFlag;
    }

    /**
     * Sets the value of the authenticationFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthenticationFlag(String value) {
        this.authenticationFlag = value;
    }

}
