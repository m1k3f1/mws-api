
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for serviceObjectTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="serviceObjectTypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SERVICE_OBJECT_CARD"/>
 *     &lt;enumeration value="SERVICE_OBJECT_ACCOUNT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "serviceObjectTypeType", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum ServiceObjectTypeType {

    SERVICE_OBJECT_CARD,
    SERVICE_OBJECT_ACCOUNT;

    public String value() {
        return name();
    }

    public static ServiceObjectTypeType fromValue(String v) {
        return valueOf(v);
    }

}
