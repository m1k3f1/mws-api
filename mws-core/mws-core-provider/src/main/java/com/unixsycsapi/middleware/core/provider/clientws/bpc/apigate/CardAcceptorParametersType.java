
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Параметры терминала.
 * 
 * <p>Java class for cardAcceptorParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardAcceptorParametersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="terminalIdentification" type="{http://www.bpc.ru/apigate/general/}cardAcceptorTerminalIdentificationType"/>
 *         &lt;element name="merchantIdentification" type="{http://www.bpc.ru/apigate/general/}merchantIdType"/>
 *         &lt;element name="merchantType" type="{http://www.bpc.ru/apigate/general/}merchantTypeType"/>
 *         &lt;element name="nameAndLocation" type="{http://www.bpc.ru/apigate/general/}cardAcceptorNameAndLocationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardAcceptorParametersType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "terminalIdentification",
    "merchantIdentification",
    "merchantType",
    "nameAndLocation"
})
public class CardAcceptorParametersType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String terminalIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String merchantIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String merchantType;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected String nameAndLocation;

    /**
     * Gets the value of the terminalIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalIdentification() {
        return terminalIdentification;
    }

    /**
     * Sets the value of the terminalIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalIdentification(String value) {
        this.terminalIdentification = value;
    }

    /**
     * Gets the value of the merchantIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantIdentification() {
        return merchantIdentification;
    }

    /**
     * Sets the value of the merchantIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantIdentification(String value) {
        this.merchantIdentification = value;
    }

    /**
     * Gets the value of the merchantType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantType() {
        return merchantType;
    }

    /**
     * Sets the value of the merchantType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantType(String value) {
        this.merchantType = value;
    }

    /**
     * Gets the value of the nameAndLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameAndLocation() {
        return nameAndLocation;
    }

    /**
     * Sets the value of the nameAndLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameAndLocation(String value) {
        this.nameAndLocation = value;
    }

}
