
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCardDataResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCardDataResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardData" type="{http://www.bpc.ru/apigate/general/}cardDataDetailedType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCardDataResponseType", namespace = "http://www.bpc.ru/apigate/command/getCardData/", propOrder = {
    "cardData"
})
public class GetCardDataResponseType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getCardData/", required = true)
    protected CardDataDetailedType cardData;

    /**
     * Gets the value of the cardData property.
     * 
     * @return
     *     possible object is
     *     {@link CardDataDetailedType }
     *     
     */
    public CardDataDetailedType getCardData() {
        return cardData;
    }

    /**
     * Sets the value of the cardData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardDataDetailedType }
     *     
     */
    public void setCardData(CardDataDetailedType value) {
        this.cardData = value;
    }

}
