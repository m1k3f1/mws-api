
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for accountTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="accountTypeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACCOUNT_TYPE_DEFAULT"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_SAVING"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_CHECKING"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_CREDIT"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_FIRST_OTHERS"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_LOANS"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_ENVELOPE"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_ESCROW"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_MONEY_MARKET"/>
 *     &lt;enumeration value="ACCOUNT_TYPE_LOYALTY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "accountTypeType", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum AccountTypeType {

    ACCOUNT_TYPE_DEFAULT,
    ACCOUNT_TYPE_SAVING,
    ACCOUNT_TYPE_CHECKING,
    ACCOUNT_TYPE_CREDIT,
    ACCOUNT_TYPE_FIRST_OTHERS,
    ACCOUNT_TYPE_LOANS,
    ACCOUNT_TYPE_ENVELOPE,
    ACCOUNT_TYPE_ESCROW,
    ACCOUNT_TYPE_MONEY_MARKET,
    ACCOUNT_TYPE_LOYALTY;

    public String value() {
        return name();
    }

    public static AccountTypeType fromValue(String v) {
        return valueOf(v);
    }

}
