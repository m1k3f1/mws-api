
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecAuthenticationIndicator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ecAuthenticationIndicator">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NOT_PERFORMED"/>
 *     &lt;enumeration value="TDS_MERCHANT_ONLY"/>
 *     &lt;enumeration value="TDS_PERFORMED"/>
 *     &lt;enumeration value="ADDITIONAL_PROTOCOL_USED"/>
 *     &lt;enumeration value="RECURRENT_PAYMENT_AUTH"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ecAuthenticationIndicator", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum EcAuthenticationIndicator {

    NOT_PERFORMED,
    TDS_MERCHANT_ONLY,
    TDS_PERFORMED,
    ADDITIONAL_PROTOCOL_USED,
    RECURRENT_PAYMENT_AUTH;

    public String value() {
        return name();
    }

    public static EcAuthenticationIndicator fromValue(String v) {
        return valueOf(v);
    }

}
