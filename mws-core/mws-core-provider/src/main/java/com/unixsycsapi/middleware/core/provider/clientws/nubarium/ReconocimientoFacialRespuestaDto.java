package com.unixsycsapi.middleware.core.provider.clientws.nubarium;

/**
 * Created by Satellite on 19/10/2017.
 */
public class ReconocimientoFacialRespuestaDto {

    private String estatus;
    private String mensaje;
    private String similitud;
    private String codigoValidacion;
    private boolean validacionActiva;

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getSimilitud() {
        return similitud;
    }

    public void setSimilitud(String similitud) {
        this.similitud = similitud;
    }

    public String getCodigoValidacion() {
        return codigoValidacion;
    }

    public void setCodigoValidacion(String codigoValidacion) {
        this.codigoValidacion = codigoValidacion;
    }

	/**
	 * @return the validacionActiva
	 */
	public boolean isValidacionActiva() {
		return validacionActiva;
	}

	/**
	 * @param validacionActiva the validacionActiva to set
	 */
	public void setValidacionActiva(boolean validacionActiva) {
		this.validacionActiva = validacionActiva;
	}
}
