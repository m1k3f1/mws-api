
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTransactionsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransactionsRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardIdentification" type="{http://www.bpc.ru/apigate/general/}cardIdentificationType"/>
 *         &lt;element name="period" type="{http://www.bpc.ru/apigate/general/}transactionDateStrictPeriodType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransactionsRequestType", namespace = "http://www.bpc.ru/apigate/command/getTransactions/", propOrder = {
    "cardIdentification",
    "period"
})
public class GetTransactionsRequestType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTransactions/", required = true)
    protected CardIdentificationType cardIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTransactions/", required = true)
    protected TransactionDateStrictPeriodType period;

    /**
     * Gets the value of the cardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link CardIdentificationType }
     *     
     */
    public CardIdentificationType getCardIdentification() {
        return cardIdentification;
    }

    /**
     * Sets the value of the cardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardIdentificationType }
     *     
     */
    public void setCardIdentification(CardIdentificationType value) {
        this.cardIdentification = value;
    }

    /**
     * Gets the value of the period property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionDateStrictPeriodType }
     *     
     */
    public TransactionDateStrictPeriodType getPeriod() {
        return period;
    }

    /**
     * Sets the value of the period property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionDateStrictPeriodType }
     *     
     */
    public void setPeriod(TransactionDateStrictPeriodType value) {
        this.period = value;
    }

}
