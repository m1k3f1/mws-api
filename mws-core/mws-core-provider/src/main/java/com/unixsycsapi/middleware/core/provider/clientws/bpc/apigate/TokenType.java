
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tokenType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tokenType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tokenValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plasticNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="seid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tokenType", namespace = "http://www.bpc.ru/apigate/command/getTokenList/", propOrder = {
    "tokenValue",
    "plasticNumber",
    "expiryDate",
    "active",
    "seid"
})
public class TokenType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTokenList/", required = true)
    protected String tokenValue;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTokenList/")
    protected Integer plasticNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTokenList/")
    protected String expiryDate;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTokenList/")
    protected boolean active;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/getTokenList/")
    protected String seid;

    /**
     * Gets the value of the tokenValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenValue() {
        return tokenValue;
    }

    /**
     * Sets the value of the tokenValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenValue(String value) {
        this.tokenValue = value;
    }

    /**
     * Gets the value of the plasticNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlasticNumber() {
        return plasticNumber;
    }

    /**
     * Sets the value of the plasticNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlasticNumber(Integer value) {
        this.plasticNumber = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the seid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeid() {
        return seid;
    }

    /**
     * Sets the value of the seid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeid(String value) {
        this.seid = value;
    }

}
