
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for virtualCardDataDeliveryMethodType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="virtualCardDataDeliveryMethodType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="WS"/>
 *     &lt;enumeration value="HALF_AND_HALF"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "virtualCardDataDeliveryMethodType", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum VirtualCardDataDeliveryMethodType {

    WS,
    HALF_AND_HALF;

    public String value() {
        return name();
    }

    public static VirtualCardDataDeliveryMethodType fromValue(String v) {
        return valueOf(v);
    }

}
