
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tokenActionRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tokenActionRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardIdentification" type="{http://www.bpc.ru/apigate/general/}cardIdentificationType"/>
 *         &lt;element name="action" type="{http://www.bpc.ru/apigate/general/}tokenAction"/>
 *         &lt;element name="updatingParameters" type="{http://www.bpc.ru/apigate/command/tokenAction/}updatingParametersType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tokenActionRequestType", namespace = "http://www.bpc.ru/apigate/command/tokenAction/", propOrder = {
    "cardIdentification",
    "action",
    "updatingParameters"
})
public class TokenActionRequestType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/tokenAction/", required = true)
    protected CardIdentificationType cardIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/tokenAction/", required = true)
    @XmlSchemaType(name = "string")
    protected TokenAction action;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/tokenAction/")
    protected UpdatingParametersType updatingParameters;

    /**
     * Gets the value of the cardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link CardIdentificationType }
     *     
     */
    public CardIdentificationType getCardIdentification() {
        return cardIdentification;
    }

    /**
     * Sets the value of the cardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardIdentificationType }
     *     
     */
    public void setCardIdentification(CardIdentificationType value) {
        this.cardIdentification = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link TokenAction }
     *     
     */
    public TokenAction getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link TokenAction }
     *     
     */
    public void setAction(TokenAction value) {
        this.action = value;
    }

    /**
     * Gets the value of the updatingParameters property.
     * 
     * @return
     *     possible object is
     *     {@link UpdatingParametersType }
     *     
     */
    public UpdatingParametersType getUpdatingParameters() {
        return updatingParameters;
    }

    /**
     * Sets the value of the updatingParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdatingParametersType }
     *     
     */
    public void setUpdatingParameters(UpdatingParametersType value) {
        this.updatingParameters = value;
    }

}
