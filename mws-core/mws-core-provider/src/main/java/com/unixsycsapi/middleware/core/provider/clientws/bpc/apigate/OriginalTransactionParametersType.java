
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Параметры оригинальной транзакции. Должны присутствовать в запросах проверки статуса транзакции.
 * 			
 * 
 * <p>Java class for originalTransactionParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="originalTransactionParametersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="systemTraceAuditNumber" type="{http://www.bpc.ru/apigate/general/}systemTraceAuditNumberType"/>
 *         &lt;element name="localTransactionDate" type="{http://www.bpc.ru/apigate/general/}transactionDateType"/>
 *         &lt;element name="rrn" type="{http://www.bpc.ru/apigate/general/}rrnType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "originalTransactionParametersType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "systemTraceAuditNumber",
    "localTransactionDate",
    "rrn"
})
public class OriginalTransactionParametersType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected int systemTraceAuditNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar localTransactionDate;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/", required = true)
    protected String rrn;

    /**
     * Gets the value of the systemTraceAuditNumber property.
     * 
     */
    public int getSystemTraceAuditNumber() {
        return systemTraceAuditNumber;
    }

    /**
     * Sets the value of the systemTraceAuditNumber property.
     * 
     */
    public void setSystemTraceAuditNumber(int value) {
        this.systemTraceAuditNumber = value;
    }

    /**
     * Gets the value of the localTransactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLocalTransactionDate() {
        return localTransactionDate;
    }

    /**
     * Sets the value of the localTransactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLocalTransactionDate(XMLGregorianCalendar value) {
        this.localTransactionDate = value;
    }

    /**
     * Gets the value of the rrn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * Sets the value of the rrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRrn(String value) {
        this.rrn = value;
    }

}
