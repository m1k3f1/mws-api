
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for purchaseRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="purchaseRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardIdentification" type="{http://www.bpc.ru/apigate/general/}cardIdentificationType"/>
 *         &lt;element name="amount" type="{http://www.bpc.ru/apigate/general/}amountType"/>
 *         &lt;element name="currency" type="{http://www.bpc.ru/apigate/general/}currencyN3CodeType"/>
 *         &lt;element name="accountType" type="{http://www.bpc.ru/apigate/general/}accountTypeType" minOccurs="0"/>
 *         &lt;element name="accountNumber" type="{http://www.bpc.ru/apigate/general/}accountNumberType" minOccurs="0"/>
 *         &lt;element name="tds" type="{http://www.bpc.ru/apigate/general/}tdsType" minOccurs="0"/>
 *         &lt;element name="senderReceiverInfo" type="{http://www.bpc.ru/apigate/general/}senderReceiverInfoType" minOccurs="0"/>
 *         &lt;element name="pointOfServiceDataCode" type="{http://www.bpc.ru/apigate/general/}pointOfServiceDataCodeType" minOccurs="0"/>
 *         &lt;element name="pointOfServiceConditionCode" type="{http://www.bpc.ru/apigate/general/}pointOfServiceConditionCodeType" minOccurs="0"/>
 *         &lt;element name="cardAcceptorParameters" type="{http://www.bpc.ru/apigate/general/}cardAcceptorParametersType" minOccurs="0"/>
 *         &lt;element name="securityLevelIndicator" type="{http://www.bpc.ru/apigate/general/}securityLevelIndicatorType" minOccurs="0"/>
 *         &lt;element name="originalTransactionParameters" type="{http://www.bpc.ru/apigate/general/}originalTransactionParametersType" minOccurs="0"/>
 *         &lt;element name="posCardholderPresence" type="{http://www.bpc.ru/apigate/general/}posCardholderPresenceType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "purchaseRequestType", namespace = "http://www.bpc.ru/apigate/command/purchase/", propOrder = {
    "cardIdentification",
    "amount",
    "currency",
    "accountType",
    "accountNumber",
    "tds",
    "senderReceiverInfo",
    "pointOfServiceDataCode",
    "pointOfServiceConditionCode",
    "cardAcceptorParameters",
    "securityLevelIndicator",
    "originalTransactionParameters",
    "posCardholderPresence"
})
public class PurchaseRequestType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/", required = true)
    protected CardIdentificationType cardIdentification;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/", required = true)
    protected BigInteger amount;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected int currency;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    @XmlSchemaType(name = "string")
    protected AccountTypeType accountType;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected String accountNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected TdsType tds;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected SenderReceiverInfoType senderReceiverInfo;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected String pointOfServiceDataCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected String pointOfServiceConditionCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected CardAcceptorParametersType cardAcceptorParameters;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected String securityLevelIndicator;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected OriginalTransactionParametersType originalTransactionParameters;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/purchase/")
    protected Integer posCardholderPresence;

    /**
     * Gets the value of the cardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link CardIdentificationType }
     *     
     */
    public CardIdentificationType getCardIdentification() {
        return cardIdentification;
    }

    /**
     * Sets the value of the cardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardIdentificationType }
     *     
     */
    public void setCardIdentification(CardIdentificationType value) {
        this.cardIdentification = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAmount(BigInteger value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     */
    public int getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     */
    public void setCurrency(int value) {
        this.currency = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTypeType }
     *     
     */
    public AccountTypeType getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTypeType }
     *     
     */
    public void setAccountType(AccountTypeType value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the tds property.
     * 
     * @return
     *     possible object is
     *     {@link TdsType }
     *     
     */
    public TdsType getTds() {
        return tds;
    }

    /**
     * Sets the value of the tds property.
     * 
     * @param value
     *     allowed object is
     *     {@link TdsType }
     *     
     */
    public void setTds(TdsType value) {
        this.tds = value;
    }

    /**
     * Gets the value of the senderReceiverInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SenderReceiverInfoType }
     *     
     */
    public SenderReceiverInfoType getSenderReceiverInfo() {
        return senderReceiverInfo;
    }

    /**
     * Sets the value of the senderReceiverInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SenderReceiverInfoType }
     *     
     */
    public void setSenderReceiverInfo(SenderReceiverInfoType value) {
        this.senderReceiverInfo = value;
    }

    /**
     * Gets the value of the pointOfServiceDataCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointOfServiceDataCode() {
        return pointOfServiceDataCode;
    }

    /**
     * Sets the value of the pointOfServiceDataCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointOfServiceDataCode(String value) {
        this.pointOfServiceDataCode = value;
    }

    /**
     * Gets the value of the pointOfServiceConditionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPointOfServiceConditionCode() {
        return pointOfServiceConditionCode;
    }

    /**
     * Sets the value of the pointOfServiceConditionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPointOfServiceConditionCode(String value) {
        this.pointOfServiceConditionCode = value;
    }

    /**
     * Gets the value of the cardAcceptorParameters property.
     * 
     * @return
     *     possible object is
     *     {@link CardAcceptorParametersType }
     *     
     */
    public CardAcceptorParametersType getCardAcceptorParameters() {
        return cardAcceptorParameters;
    }

    /**
     * Sets the value of the cardAcceptorParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link CardAcceptorParametersType }
     *     
     */
    public void setCardAcceptorParameters(CardAcceptorParametersType value) {
        this.cardAcceptorParameters = value;
    }

    /**
     * Gets the value of the securityLevelIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityLevelIndicator() {
        return securityLevelIndicator;
    }

    /**
     * Sets the value of the securityLevelIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityLevelIndicator(String value) {
        this.securityLevelIndicator = value;
    }

    /**
     * Gets the value of the originalTransactionParameters property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalTransactionParametersType }
     *     
     */
    public OriginalTransactionParametersType getOriginalTransactionParameters() {
        return originalTransactionParameters;
    }

    /**
     * Sets the value of the originalTransactionParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalTransactionParametersType }
     *     
     */
    public void setOriginalTransactionParameters(OriginalTransactionParametersType value) {
        this.originalTransactionParameters = value;
    }

    /**
     * Gets the value of the posCardholderPresence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPosCardholderPresence() {
        return posCardholderPresence;
    }

    /**
     * Sets the value of the posCardholderPresence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPosCardholderPresence(Integer value) {
        this.posCardholderPresence = value;
    }

}
