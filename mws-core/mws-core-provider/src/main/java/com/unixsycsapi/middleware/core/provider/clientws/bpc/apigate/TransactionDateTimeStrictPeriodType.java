
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Временной диапазон со строгими границами.
 * 
 * <p>Java class for transactionDateTimeStrictPeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionDateTimeStrictPeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.bpc.ru/apigate/general/}transactionDateTimePeriodType">
 *       &lt;sequence>
 *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionDateTimeStrictPeriodType", namespace = "http://www.bpc.ru/apigate/general/")
public class TransactionDateTimeStrictPeriodType
    extends TransactionDateTimePeriodType
{


}
