
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CashDepositRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/cashDeposit/", "cashDepositRequest");
    private final static QName _BlockCardResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/blockCard/", "blockCardResponse");
    private final static QName _GetTransactionsRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/getTransactions/", "getTransactionsRequest");
    private final static QName _GetTokenListRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/getTokenList/", "getTokenListRequest");
    private final static QName _GetCardBalanceRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/getCardBalance/", "getCardBalanceRequest");
    private final static QName _P2PTransferResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/p2pTransfer/", "p2pTransferResponse");
    private final static QName _ReversalRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/reversal/", "reversalRequest");
    private final static QName _ChangeCardStatusResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/changeCardStatus/", "changeCardStatusResponse");
    private final static QName _BlockCardRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/blockCard/", "blockCardRequest");
    private final static QName _GetTokenListResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/getTokenList/", "getTokenListResponse");
    private final static QName _GetAccountDataResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/getAccountData/", "getAccountDataResponse");
    private final static QName _GetCardLimitsRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/getCardLimits/", "getCardLimitsRequest");
    private final static QName _CardStatusInquiryRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/cardStatusInquiry/", "cardStatusInquiryRequest");
    private final static QName _GetCardDataResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/getCardData/", "getCardDataResponse");
    private final static QName _ValidateCardResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/validateCard/", "validateCardResponse");
    private final static QName _CreateVirtualCardRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/createVirtualCard/", "createVirtualCardRequest");
    private final static QName _GetCardBalanceResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/getCardBalance/", "getCardBalanceResponse");
    private final static QName _ChangePinResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/changePin/", "changePinResponse");
    private final static QName _CreateVirtualCardResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/createVirtualCard/", "createVirtualCardResponse");
    private final static QName _GetAccountDataRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/getAccountData/", "getAccountDataRequest");
    private final static QName _GetCardLimitsResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/getCardLimits/", "getCardLimitsResponse");
    private final static QName _ChangeCardStatusRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/changeCardStatus/", "changeCardStatusRequest");
    private final static QName _CheckCardRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/checkCard/", "checkCardRequest");
    private final static QName _CheckCardResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/checkCard/", "checkCardResponse");
    private final static QName _TokenActionResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/tokenAction/", "tokenActionResponse");
    private final static QName _ValidateCardRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/validateCard/", "validateCardRequest");
    private final static QName _ServiceLevelFault_QNAME = new QName("http://www.bpc.ru/apigate/errors/", "serviceLevelFault");
    private final static QName _ActivateCardRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/activateCard/", "activateCardRequest");
    private final static QName _CashDepositResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/cashDeposit/", "cashDepositResponse");
    private final static QName _SvfeProcessingFault_QNAME = new QName("http://www.bpc.ru/apigate/errors/", "svfeProcessingFault");
    private final static QName _PurchaseRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/purchase/", "purchaseRequest");
    private final static QName _P2PTransferRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/p2pTransfer/", "p2pTransferRequest");
    private final static QName _ChangePinRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/changePin/", "changePinRequest");
    private final static QName _PurchaseResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/purchase/", "purchaseResponse");
    private final static QName _TokenActionRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/tokenAction/", "tokenActionRequest");
    private final static QName _ActivateCardResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/activateCard/", "activateCardResponse");
    private final static QName _ReversalResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/reversal/", "reversalResponse");
    private final static QName _GenerateCVC2Response_QNAME = new QName("http://www.bpc.ru/apigate/command/generateCVC2/", "generateCVC2Response");
    private final static QName _GenerateCVC2Request_QNAME = new QName("http://www.bpc.ru/apigate/command/generateCVC2/", "generateCVC2Request");
    private final static QName _GetTransactionsResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/getTransactions/", "getTransactionsResponse");
    private final static QName _CardStatusInquiryResponse_QNAME = new QName("http://www.bpc.ru/apigate/command/cardStatusInquiry/", "cardStatusInquiryResponse");
    private final static QName _GetCardDataRequest_QNAME = new QName("http://www.bpc.ru/apigate/command/getCardData/", "getCardDataRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceLevelFaultType }
     * 
     */
    public ServiceLevelFaultType createServiceLevelFaultType() {
        return new ServiceLevelFaultType();
    }

    /**
     * Create an instance of {@link SvfeProcessingFaultType }
     * 
     */
    public SvfeProcessingFaultType createSvfeProcessingFaultType() {
        return new SvfeProcessingFaultType();
    }

    /**
     * Create an instance of {@link LimitExceptionsType }
     * 
     */
    public LimitExceptionsType createLimitExceptionsType() {
        return new LimitExceptionsType();
    }

    /**
     * Create an instance of {@link PersonalDataType }
     * 
     */
    public PersonalDataType createPersonalDataType() {
        return new PersonalDataType();
    }

    /**
     * Create an instance of {@link OriginalTransactionParametersType }
     * 
     */
    public OriginalTransactionParametersType createOriginalTransactionParametersType() {
        return new OriginalTransactionParametersType();
    }

    /**
     * Create an instance of {@link FeeType }
     * 
     */
    public FeeType createFeeType() {
        return new FeeType();
    }

    /**
     * Create an instance of {@link FinancialTransactionResponseType }
     * 
     */
    public FinancialTransactionResponseType createFinancialTransactionResponseType() {
        return new FinancialTransactionResponseType();
    }

    /**
     * Create an instance of {@link TransactionDateTimeLowerBoundedPeriodType }
     * 
     */
    public TransactionDateTimeLowerBoundedPeriodType createTransactionDateTimeLowerBoundedPeriodType() {
        return new TransactionDateTimeLowerBoundedPeriodType();
    }

    /**
     * Create an instance of {@link CardsType }
     * 
     */
    public CardsType createCardsType() {
        return new CardsType();
    }

    /**
     * Create an instance of {@link TransactionsType }
     * 
     */
    public TransactionsType createTransactionsType() {
        return new TransactionsType();
    }

    /**
     * Create an instance of {@link RowRangeType }
     * 
     */
    public RowRangeType createRowRangeType() {
        return new RowRangeType();
    }

    /**
     * Create an instance of {@link AccountsType }
     * 
     */
    public AccountsType createAccountsType() {
        return new AccountsType();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link TransactionDateTimePeriodType }
     * 
     */
    public TransactionDateTimePeriodType createTransactionDateTimePeriodType() {
        return new TransactionDateTimePeriodType();
    }

    /**
     * Create an instance of {@link ServiceParametersType }
     * 
     */
    public ServiceParametersType createServiceParametersType() {
        return new ServiceParametersType();
    }

    /**
     * Create an instance of {@link CardDataType }
     * 
     */
    public CardDataType createCardDataType() {
        return new CardDataType();
    }

    /**
     * Create an instance of {@link TransactionDatePeriodType }
     * 
     */
    public TransactionDatePeriodType createTransactionDatePeriodType() {
        return new TransactionDatePeriodType();
    }

    /**
     * Create an instance of {@link AccountDataType }
     * 
     */
    public AccountDataType createAccountDataType() {
        return new AccountDataType();
    }

    /**
     * Create an instance of {@link LimitBaseDataType }
     * 
     */
    public LimitBaseDataType createLimitBaseDataType() {
        return new LimitBaseDataType();
    }

    /**
     * Create an instance of {@link TransactionDataType }
     * 
     */
    public TransactionDataType createTransactionDataType() {
        return new TransactionDataType();
    }

    /**
     * Create an instance of {@link TransactionDateStrictPeriodType }
     * 
     */
    public TransactionDateStrictPeriodType createTransactionDateStrictPeriodType() {
        return new TransactionDateStrictPeriodType();
    }

    /**
     * Create an instance of {@link TransactionsBType }
     * 
     */
    public TransactionsBType createTransactionsBType() {
        return new TransactionsBType();
    }

    /**
     * Create an instance of {@link CustomerType }
     * 
     */
    public CustomerType createCustomerType() {
        return new CustomerType();
    }

    /**
     * Create an instance of {@link CardDataDetailedType }
     * 
     */
    public CardDataDetailedType createCardDataDetailedType() {
        return new CardDataDetailedType();
    }

    /**
     * Create an instance of {@link ParameterListType }
     * 
     */
    public ParameterListType createParameterListType() {
        return new ParameterListType();
    }

    /**
     * Create an instance of {@link TransactionDetailsBDataType }
     * 
     */
    public TransactionDetailsBDataType createTransactionDetailsBDataType() {
        return new TransactionDetailsBDataType();
    }

    /**
     * Create an instance of {@link AccountDataDetailedType }
     * 
     */
    public AccountDataDetailedType createAccountDataDetailedType() {
        return new AccountDataDetailedType();
    }

    /**
     * Create an instance of {@link TdsType }
     * 
     */
    public TdsType createTdsType() {
        return new TdsType();
    }

    /**
     * Create an instance of {@link CardIdentificationType }
     * 
     */
    public CardIdentificationType createCardIdentificationType() {
        return new CardIdentificationType();
    }

    /**
     * Create an instance of {@link TransactionBDataType }
     * 
     */
    public TransactionBDataType createTransactionBDataType() {
        return new TransactionBDataType();
    }

    /**
     * Create an instance of {@link VirtualCardIdentificationType }
     * 
     */
    public VirtualCardIdentificationType createVirtualCardIdentificationType() {
        return new VirtualCardIdentificationType();
    }

    /**
     * Create an instance of {@link LimitsType }
     * 
     */
    public LimitsType createLimitsType() {
        return new LimitsType();
    }

    /**
     * Create an instance of {@link TransactionDateLowerBoundedPeriodType }
     * 
     */
    public TransactionDateLowerBoundedPeriodType createTransactionDateLowerBoundedPeriodType() {
        return new TransactionDateLowerBoundedPeriodType();
    }

    /**
     * Create an instance of {@link LimitFullDataType }
     * 
     */
    public LimitFullDataType createLimitFullDataType() {
        return new LimitFullDataType();
    }

    /**
     * Create an instance of {@link TransactionDateTimeStrictPeriodType }
     * 
     */
    public TransactionDateTimeStrictPeriodType createTransactionDateTimeStrictPeriodType() {
        return new TransactionDateTimeStrictPeriodType();
    }

    /**
     * Create an instance of {@link ServiceIdentificationType }
     * 
     */
    public ServiceIdentificationType createServiceIdentificationType() {
        return new ServiceIdentificationType();
    }

    /**
     * Create an instance of {@link SimpleResponseType }
     * 
     */
    public SimpleResponseType createSimpleResponseType() {
        return new SimpleResponseType();
    }

    /**
     * Create an instance of {@link CardListElement }
     * 
     */
    public CardListElement createCardListElement() {
        return new CardListElement();
    }

    /**
     * Create an instance of {@link SenderReceiverInfoType }
     * 
     */
    public SenderReceiverInfoType createSenderReceiverInfoType() {
        return new SenderReceiverInfoType();
    }

    /**
     * Create an instance of {@link CardAcceptorParametersType }
     * 
     */
    public CardAcceptorParametersType createCardAcceptorParametersType() {
        return new CardAcceptorParametersType();
    }

    /**
     * Create an instance of {@link ParameterType }
     * 
     */
    public ParameterType createParameterType() {
        return new ParameterType();
    }

    /**
     * Create an instance of {@link ChangeCardStatusRequestType }
     * 
     */
    public ChangeCardStatusRequestType createChangeCardStatusRequestType() {
        return new ChangeCardStatusRequestType();
    }

    /**
     * Create an instance of {@link CardListType }
     * 
     */
    public CardListType createCardListType() {
        return new CardListType();
    }

    /**
     * Create an instance of {@link LimitExceptionType }
     * 
     */
    public LimitExceptionType createLimitExceptionType() {
        return new LimitExceptionType();
    }

    /**
     * Create an instance of {@link ActivateCardRequestType }
     * 
     */
    public ActivateCardRequestType createActivateCardRequestType() {
        return new ActivateCardRequestType();
    }

    /**
     * Create an instance of {@link CardStatusInquiryResponseType }
     * 
     */
    public CardStatusInquiryResponseType createCardStatusInquiryResponseType() {
        return new CardStatusInquiryResponseType();
    }

    /**
     * Create an instance of {@link CardStatusInquiryRequestType }
     * 
     */
    public CardStatusInquiryRequestType createCardStatusInquiryRequestType() {
        return new CardStatusInquiryRequestType();
    }

    /**
     * Create an instance of {@link CashDepositRequestType }
     * 
     */
    public CashDepositRequestType createCashDepositRequestType() {
        return new CashDepositRequestType();
    }

    /**
     * Create an instance of {@link ChangePinRequestType }
     * 
     */
    public ChangePinRequestType createChangePinRequestType() {
        return new ChangePinRequestType();
    }

    /**
     * Create an instance of {@link CheckCardRequestRequestType }
     * 
     */
    public CheckCardRequestRequestType createCheckCardRequestRequestType() {
        return new CheckCardRequestRequestType();
    }

    /**
     * Create an instance of {@link CheckCardRequestResponseType }
     * 
     */
    public CheckCardRequestResponseType createCheckCardRequestResponseType() {
        return new CheckCardRequestResponseType();
    }

    /**
     * Create an instance of {@link CreateVirtualCardResponseType }
     * 
     */
    public CreateVirtualCardResponseType createCreateVirtualCardResponseType() {
        return new CreateVirtualCardResponseType();
    }

    /**
     * Create an instance of {@link CreateVirtualCardRequestType }
     * 
     */
    public CreateVirtualCardRequestType createCreateVirtualCardRequestType() {
        return new CreateVirtualCardRequestType();
    }

    /**
     * Create an instance of {@link GenerateCVC2RequestType }
     * 
     */
    public GenerateCVC2RequestType createGenerateCVC2RequestType() {
        return new GenerateCVC2RequestType();
    }

    /**
     * Create an instance of {@link GenerateCVC2ResponseType }
     * 
     */
    public GenerateCVC2ResponseType createGenerateCVC2ResponseType() {
        return new GenerateCVC2ResponseType();
    }

    /**
     * Create an instance of {@link GetAccountDataResponseType }
     * 
     */
    public GetAccountDataResponseType createGetAccountDataResponseType() {
        return new GetAccountDataResponseType();
    }

    /**
     * Create an instance of {@link GetAccountDataRequestType }
     * 
     */
    public GetAccountDataRequestType createGetAccountDataRequestType() {
        return new GetAccountDataRequestType();
    }

    /**
     * Create an instance of {@link GetCardBalanceResponseType }
     * 
     */
    public GetCardBalanceResponseType createGetCardBalanceResponseType() {
        return new GetCardBalanceResponseType();
    }

    /**
     * Create an instance of {@link GetCardBalanceRequestType }
     * 
     */
    public GetCardBalanceRequestType createGetCardBalanceRequestType() {
        return new GetCardBalanceRequestType();
    }

    /**
     * Create an instance of {@link GetCardDataResponseType }
     * 
     */
    public GetCardDataResponseType createGetCardDataResponseType() {
        return new GetCardDataResponseType();
    }

    /**
     * Create an instance of {@link GetCardDataRequestType }
     * 
     */
    public GetCardDataRequestType createGetCardDataRequestType() {
        return new GetCardDataRequestType();
    }

    /**
     * Create an instance of {@link GetCardLimitsRequestType }
     * 
     */
    public GetCardLimitsRequestType createGetCardLimitsRequestType() {
        return new GetCardLimitsRequestType();
    }

    /**
     * Create an instance of {@link GetCardLimitsResponseType }
     * 
     */
    public GetCardLimitsResponseType createGetCardLimitsResponseType() {
        return new GetCardLimitsResponseType();
    }

    /**
     * Create an instance of {@link GetTransactionsRequestType }
     * 
     */
    public GetTransactionsRequestType createGetTransactionsRequestType() {
        return new GetTransactionsRequestType();
    }

    /**
     * Create an instance of {@link GetTransactionsResponseType }
     * 
     */
    public GetTransactionsResponseType createGetTransactionsResponseType() {
        return new GetTransactionsResponseType();
    }

    /**
     * Create an instance of {@link GetTokenListResponseType }
     * 
     */
    public GetTokenListResponseType createGetTokenListResponseType() {
        return new GetTokenListResponseType();
    }

    /**
     * Create an instance of {@link TokenList }
     * 
     */
    public TokenList createTokenList() {
        return new TokenList();
    }

    /**
     * Create an instance of {@link TokenType }
     * 
     */
    public TokenType createTokenType() {
        return new TokenType();
    }

    /**
     * Create an instance of {@link GetTokenListRequestType }
     * 
     */
    public GetTokenListRequestType createGetTokenListRequestType() {
        return new GetTokenListRequestType();
    }

    /**
     * Create an instance of {@link TokenActionRequestType }
     * 
     */
    public TokenActionRequestType createTokenActionRequestType() {
        return new TokenActionRequestType();
    }

    /**
     * Create an instance of {@link UpdatingParametersType }
     * 
     */
    public UpdatingParametersType createUpdatingParametersType() {
        return new UpdatingParametersType();
    }

    /**
     * Create an instance of {@link P2PTransferRequestType }
     * 
     */
    public P2PTransferRequestType createP2PTransferRequestType() {
        return new P2PTransferRequestType();
    }

    /**
     * Create an instance of {@link PurchaseRequestType }
     * 
     */
    public PurchaseRequestType createPurchaseRequestType() {
        return new PurchaseRequestType();
    }

    /**
     * Create an instance of {@link ReversalResponseType }
     * 
     */
    public ReversalResponseType createReversalResponseType() {
        return new ReversalResponseType();
    }

    /**
     * Create an instance of {@link ReversalRequestType }
     * 
     */
    public ReversalRequestType createReversalRequestType() {
        return new ReversalRequestType();
    }

    /**
     * Create an instance of {@link ValidateCardRequestType }
     * 
     */
    public ValidateCardRequestType createValidateCardRequestType() {
        return new ValidateCardRequestType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CashDepositRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/cashDeposit/", name = "cashDepositRequest")
    public JAXBElement<CashDepositRequestType> createCashDepositRequest(CashDepositRequestType value) {
        return new JAXBElement<CashDepositRequestType>(_CashDepositRequest_QNAME, CashDepositRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/blockCard/", name = "blockCardResponse")
    public JAXBElement<SimpleResponseType> createBlockCardResponse(SimpleResponseType value) {
        return new JAXBElement<SimpleResponseType>(_BlockCardResponse_QNAME, SimpleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransactionsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getTransactions/", name = "getTransactionsRequest")
    public JAXBElement<GetTransactionsRequestType> createGetTransactionsRequest(GetTransactionsRequestType value) {
        return new JAXBElement<GetTransactionsRequestType>(_GetTransactionsRequest_QNAME, GetTransactionsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTokenListRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getTokenList/", name = "getTokenListRequest")
    public JAXBElement<GetTokenListRequestType> createGetTokenListRequest(GetTokenListRequestType value) {
        return new JAXBElement<GetTokenListRequestType>(_GetTokenListRequest_QNAME, GetTokenListRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardBalanceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getCardBalance/", name = "getCardBalanceRequest")
    public JAXBElement<GetCardBalanceRequestType> createGetCardBalanceRequest(GetCardBalanceRequestType value) {
        return new JAXBElement<GetCardBalanceRequestType>(_GetCardBalanceRequest_QNAME, GetCardBalanceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialTransactionResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/", name = "p2pTransferResponse")
    public JAXBElement<FinancialTransactionResponseType> createP2PTransferResponse(FinancialTransactionResponseType value) {
        return new JAXBElement<FinancialTransactionResponseType>(_P2PTransferResponse_QNAME, FinancialTransactionResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReversalRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/reversal/", name = "reversalRequest")
    public JAXBElement<ReversalRequestType> createReversalRequest(ReversalRequestType value) {
        return new JAXBElement<ReversalRequestType>(_ReversalRequest_QNAME, ReversalRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/changeCardStatus/", name = "changeCardStatusResponse")
    public JAXBElement<SimpleResponseType> createChangeCardStatusResponse(SimpleResponseType value) {
        return new JAXBElement<SimpleResponseType>(_ChangeCardStatusResponse_QNAME, SimpleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCardStatusRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/blockCard/", name = "blockCardRequest")
    public JAXBElement<ChangeCardStatusRequestType> createBlockCardRequest(ChangeCardStatusRequestType value) {
        return new JAXBElement<ChangeCardStatusRequestType>(_BlockCardRequest_QNAME, ChangeCardStatusRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTokenListResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getTokenList/", name = "getTokenListResponse")
    public JAXBElement<GetTokenListResponseType> createGetTokenListResponse(GetTokenListResponseType value) {
        return new JAXBElement<GetTokenListResponseType>(_GetTokenListResponse_QNAME, GetTokenListResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountDataResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getAccountData/", name = "getAccountDataResponse")
    public JAXBElement<GetAccountDataResponseType> createGetAccountDataResponse(GetAccountDataResponseType value) {
        return new JAXBElement<GetAccountDataResponseType>(_GetAccountDataResponse_QNAME, GetAccountDataResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardLimitsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getCardLimits/", name = "getCardLimitsRequest")
    public JAXBElement<GetCardLimitsRequestType> createGetCardLimitsRequest(GetCardLimitsRequestType value) {
        return new JAXBElement<GetCardLimitsRequestType>(_GetCardLimitsRequest_QNAME, GetCardLimitsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardStatusInquiryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/cardStatusInquiry/", name = "cardStatusInquiryRequest")
    public JAXBElement<CardStatusInquiryRequestType> createCardStatusInquiryRequest(CardStatusInquiryRequestType value) {
        return new JAXBElement<CardStatusInquiryRequestType>(_CardStatusInquiryRequest_QNAME, CardStatusInquiryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardDataResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getCardData/", name = "getCardDataResponse")
    public JAXBElement<GetCardDataResponseType> createGetCardDataResponse(GetCardDataResponseType value) {
        return new JAXBElement<GetCardDataResponseType>(_GetCardDataResponse_QNAME, GetCardDataResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/validateCard/", name = "validateCardResponse")
    public JAXBElement<SimpleResponseType> createValidateCardResponse(SimpleResponseType value) {
        return new JAXBElement<SimpleResponseType>(_ValidateCardResponse_QNAME, SimpleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateVirtualCardRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", name = "createVirtualCardRequest")
    public JAXBElement<CreateVirtualCardRequestType> createCreateVirtualCardRequest(CreateVirtualCardRequestType value) {
        return new JAXBElement<CreateVirtualCardRequestType>(_CreateVirtualCardRequest_QNAME, CreateVirtualCardRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardBalanceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getCardBalance/", name = "getCardBalanceResponse")
    public JAXBElement<GetCardBalanceResponseType> createGetCardBalanceResponse(GetCardBalanceResponseType value) {
        return new JAXBElement<GetCardBalanceResponseType>(_GetCardBalanceResponse_QNAME, GetCardBalanceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/changePin/", name = "changePinResponse")
    public JAXBElement<SimpleResponseType> createChangePinResponse(SimpleResponseType value) {
        return new JAXBElement<SimpleResponseType>(_ChangePinResponse_QNAME, SimpleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateVirtualCardResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", name = "createVirtualCardResponse")
    public JAXBElement<CreateVirtualCardResponseType> createCreateVirtualCardResponse(CreateVirtualCardResponseType value) {
        return new JAXBElement<CreateVirtualCardResponseType>(_CreateVirtualCardResponse_QNAME, CreateVirtualCardResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountDataRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getAccountData/", name = "getAccountDataRequest")
    public JAXBElement<GetAccountDataRequestType> createGetAccountDataRequest(GetAccountDataRequestType value) {
        return new JAXBElement<GetAccountDataRequestType>(_GetAccountDataRequest_QNAME, GetAccountDataRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardLimitsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getCardLimits/", name = "getCardLimitsResponse")
    public JAXBElement<GetCardLimitsResponseType> createGetCardLimitsResponse(GetCardLimitsResponseType value) {
        return new JAXBElement<GetCardLimitsResponseType>(_GetCardLimitsResponse_QNAME, GetCardLimitsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCardStatusRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/changeCardStatus/", name = "changeCardStatusRequest")
    public JAXBElement<ChangeCardStatusRequestType> createChangeCardStatusRequest(ChangeCardStatusRequestType value) {
        return new JAXBElement<ChangeCardStatusRequestType>(_ChangeCardStatusRequest_QNAME, ChangeCardStatusRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckCardRequestRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/checkCard/", name = "checkCardRequest")
    public JAXBElement<CheckCardRequestRequestType> createCheckCardRequest(CheckCardRequestRequestType value) {
        return new JAXBElement<CheckCardRequestRequestType>(_CheckCardRequest_QNAME, CheckCardRequestRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckCardRequestResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/checkCard/", name = "checkCardResponse")
    public JAXBElement<CheckCardRequestResponseType> createCheckCardResponse(CheckCardRequestResponseType value) {
        return new JAXBElement<CheckCardRequestResponseType>(_CheckCardResponse_QNAME, CheckCardRequestResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/tokenAction/", name = "tokenActionResponse")
    public JAXBElement<SimpleResponseType> createTokenActionResponse(SimpleResponseType value) {
        return new JAXBElement<SimpleResponseType>(_TokenActionResponse_QNAME, SimpleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCardRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/validateCard/", name = "validateCardRequest")
    public JAXBElement<ValidateCardRequestType> createValidateCardRequest(ValidateCardRequestType value) {
        return new JAXBElement<ValidateCardRequestType>(_ValidateCardRequest_QNAME, ValidateCardRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceLevelFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/errors/", name = "serviceLevelFault")
    public JAXBElement<ServiceLevelFaultType> createServiceLevelFault(ServiceLevelFaultType value) {
        return new JAXBElement<ServiceLevelFaultType>(_ServiceLevelFault_QNAME, ServiceLevelFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivateCardRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/activateCard/", name = "activateCardRequest")
    public JAXBElement<ActivateCardRequestType> createActivateCardRequest(ActivateCardRequestType value) {
        return new JAXBElement<ActivateCardRequestType>(_ActivateCardRequest_QNAME, ActivateCardRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialTransactionResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/cashDeposit/", name = "cashDepositResponse")
    public JAXBElement<FinancialTransactionResponseType> createCashDepositResponse(FinancialTransactionResponseType value) {
        return new JAXBElement<FinancialTransactionResponseType>(_CashDepositResponse_QNAME, FinancialTransactionResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SvfeProcessingFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/errors/", name = "svfeProcessingFault")
    public JAXBElement<SvfeProcessingFaultType> createSvfeProcessingFault(SvfeProcessingFaultType value) {
        return new JAXBElement<SvfeProcessingFaultType>(_SvfeProcessingFault_QNAME, SvfeProcessingFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/purchase/", name = "purchaseRequest")
    public JAXBElement<PurchaseRequestType> createPurchaseRequest(PurchaseRequestType value) {
        return new JAXBElement<PurchaseRequestType>(_PurchaseRequest_QNAME, PurchaseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link P2PTransferRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/p2pTransfer/", name = "p2pTransferRequest")
    public JAXBElement<P2PTransferRequestType> createP2PTransferRequest(P2PTransferRequestType value) {
        return new JAXBElement<P2PTransferRequestType>(_P2PTransferRequest_QNAME, P2PTransferRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePinRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/changePin/", name = "changePinRequest")
    public JAXBElement<ChangePinRequestType> createChangePinRequest(ChangePinRequestType value) {
        return new JAXBElement<ChangePinRequestType>(_ChangePinRequest_QNAME, ChangePinRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialTransactionResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/purchase/", name = "purchaseResponse")
    public JAXBElement<FinancialTransactionResponseType> createPurchaseResponse(FinancialTransactionResponseType value) {
        return new JAXBElement<FinancialTransactionResponseType>(_PurchaseResponse_QNAME, FinancialTransactionResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenActionRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/tokenAction/", name = "tokenActionRequest")
    public JAXBElement<TokenActionRequestType> createTokenActionRequest(TokenActionRequestType value) {
        return new JAXBElement<TokenActionRequestType>(_TokenActionRequest_QNAME, TokenActionRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/activateCard/", name = "activateCardResponse")
    public JAXBElement<SimpleResponseType> createActivateCardResponse(SimpleResponseType value) {
        return new JAXBElement<SimpleResponseType>(_ActivateCardResponse_QNAME, SimpleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReversalResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/reversal/", name = "reversalResponse")
    public JAXBElement<ReversalResponseType> createReversalResponse(ReversalResponseType value) {
        return new JAXBElement<ReversalResponseType>(_ReversalResponse_QNAME, ReversalResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCVC2ResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/generateCVC2/", name = "generateCVC2Response")
    public JAXBElement<GenerateCVC2ResponseType> createGenerateCVC2Response(GenerateCVC2ResponseType value) {
        return new JAXBElement<GenerateCVC2ResponseType>(_GenerateCVC2Response_QNAME, GenerateCVC2ResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCVC2RequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/generateCVC2/", name = "generateCVC2Request")
    public JAXBElement<GenerateCVC2RequestType> createGenerateCVC2Request(GenerateCVC2RequestType value) {
        return new JAXBElement<GenerateCVC2RequestType>(_GenerateCVC2Request_QNAME, GenerateCVC2RequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransactionsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getTransactions/", name = "getTransactionsResponse")
    public JAXBElement<GetTransactionsResponseType> createGetTransactionsResponse(GetTransactionsResponseType value) {
        return new JAXBElement<GetTransactionsResponseType>(_GetTransactionsResponse_QNAME, GetTransactionsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardStatusInquiryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/cardStatusInquiry/", name = "cardStatusInquiryResponse")
    public JAXBElement<CardStatusInquiryResponseType> createCardStatusInquiryResponse(CardStatusInquiryResponseType value) {
        return new JAXBElement<CardStatusInquiryResponseType>(_CardStatusInquiryResponse_QNAME, CardStatusInquiryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCardDataRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.bpc.ru/apigate/command/getCardData/", name = "getCardDataRequest")
    public JAXBElement<GetCardDataRequestType> createGetCardDataRequest(GetCardDataRequestType value) {
        return new JAXBElement<GetCardDataRequestType>(_GetCardDataRequest_QNAME, GetCardDataRequestType.class, null, value);
    }

}
