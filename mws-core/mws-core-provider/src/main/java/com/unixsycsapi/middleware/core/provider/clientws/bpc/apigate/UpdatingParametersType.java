
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Новые значения параметров токена.
 * 
 * <p>Java class for updatingParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updatingParametersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cardNumber" type="{http://www.bpc.ru/apigate/general/}cardNumberType" minOccurs="0"/>
 *         &lt;element name="plasticNumber" type="{http://www.bpc.ru/apigate/general/}cardPlasticNumberType" minOccurs="0"/>
 *         &lt;element name="expiryDate" type="{http://www.bpc.ru/apigate/general/}cardExpDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updatingParametersType", namespace = "http://www.bpc.ru/apigate/command/tokenAction/", propOrder = {
    "cardNumber",
    "plasticNumber",
    "expiryDate"
})
public class UpdatingParametersType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/tokenAction/")
    protected String cardNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/tokenAction/")
    protected Integer plasticNumber;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/tokenAction/")
    protected String expiryDate;

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the plasticNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlasticNumber() {
        return plasticNumber;
    }

    /**
     * Sets the value of the plasticNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlasticNumber(Integer value) {
        this.plasticNumber = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

}
