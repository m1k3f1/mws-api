
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Перечень счетов.
 * 
 * <p>Java class for accountsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accountsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountData" type="{http://www.bpc.ru/apigate/general/}accountDataType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accountsType", namespace = "http://www.bpc.ru/apigate/general/", propOrder = {
    "accountData"
})
public class AccountsType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/general/")
    protected List<AccountDataType> accountData;

    /**
     * Gets the value of the accountData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountDataType }
     * 
     * 
     */
    public List<AccountDataType> getAccountData() {
        if (accountData == null) {
            accountData = new ArrayList<AccountDataType>();
        }
        return this.accountData;
    }

}
