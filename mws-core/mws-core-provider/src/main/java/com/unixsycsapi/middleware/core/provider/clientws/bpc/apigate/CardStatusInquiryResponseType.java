
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cardStatusInquiryResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cardStatusInquiryResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hotCardStatus" type="{http://www.bpc.ru/apigate/general/}hotCardStatusType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardStatusInquiryResponseType", namespace = "http://www.bpc.ru/apigate/command/cardStatusInquiry/", propOrder = {
    "hotCardStatus"
})
public class CardStatusInquiryResponseType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/cardStatusInquiry/")
    protected int hotCardStatus;

    /**
     * Gets the value of the hotCardStatus property.
     * 
     */
    public int getHotCardStatus() {
        return hotCardStatus;
    }

    /**
     * Sets the value of the hotCardStatus property.
     * 
     */
    public void setHotCardStatus(int value) {
        this.hotCardStatus = value;
    }

}
