
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createVirtualCardResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createVirtualCardResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="virtualCardIdentification" type="{http://www.bpc.ru/apigate/general/}virtualCardIdentificationType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createVirtualCardResponseType", namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", propOrder = {
    "virtualCardIdentification"
})
public class CreateVirtualCardResponseType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/createVirtualCard/", required = true)
    protected VirtualCardIdentificationType virtualCardIdentification;

    /**
     * Gets the value of the virtualCardIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link VirtualCardIdentificationType }
     *     
     */
    public VirtualCardIdentificationType getVirtualCardIdentification() {
        return virtualCardIdentification;
    }

    /**
     * Sets the value of the virtualCardIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualCardIdentificationType }
     *     
     */
    public void setVirtualCardIdentification(VirtualCardIdentificationType value) {
        this.virtualCardIdentification = value;
    }

}
