
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for debitCreditIndicatorType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="debitCreditIndicatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="credit"/>
 *     &lt;enumeration value="debit"/>
 *     &lt;enumeration value="noop"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "debitCreditIndicatorType", namespace = "http://www.bpc.ru/apigate/general/")
@XmlEnum
public enum DebitCreditIndicatorType {

    @XmlEnumValue("credit")
    CREDIT("credit"),
    @XmlEnumValue("debit")
    DEBIT("debit"),
    @XmlEnumValue("noop")
    NOOP("noop");
    private final String value;

    DebitCreditIndicatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DebitCreditIndicatorType fromValue(String v) {
        for (DebitCreditIndicatorType c: DebitCreditIndicatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
