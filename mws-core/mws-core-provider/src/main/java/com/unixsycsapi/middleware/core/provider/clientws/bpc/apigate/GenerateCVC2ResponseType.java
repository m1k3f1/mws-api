
package com.unixsycsapi.middleware.core.provider.clientws.bpc.apigate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for generateCVC2ResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generateCVC2ResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseCode" type="{http://www.bpc.ru/apigate/general/}responseCodeType"/>
 *         &lt;element name="cvc2" type="{http://www.bpc.ru/apigate/general/}cvv2Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generateCVC2ResponseType", namespace = "http://www.bpc.ru/apigate/command/generateCVC2/", propOrder = {
    "responseCode",
    "cvc2"
})
public class GenerateCVC2ResponseType {

    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/generateCVC2/", required = true)
    protected String responseCode;
    @XmlElement(namespace = "http://www.bpc.ru/apigate/command/generateCVC2/")
    protected String cvc2;

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the cvc2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCvc2() {
        return cvc2;
    }

    /**
     * Sets the value of the cvc2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCvc2(String value) {
        this.cvc2 = value;
    }

}
