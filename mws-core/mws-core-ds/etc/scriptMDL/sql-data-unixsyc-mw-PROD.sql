CREATE SEQUENCE seq_enrollment;

INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_HOST', true, 'Host del SFTP', '172.19.223.3');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PORT', true, 'Port del SFTP', '22');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_USER', true, 'USR del SFTP', 'weblogic');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PWD', true, 'PWD del SFTP', 'wld72000');	
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_FOLDER_DESTINO', true, 'Folder del SFTP', '/home/weblogic/iofiles/incoming/application/iss/');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PATH_FILE_PRIVATE_KEY', true, 'Archivo .pem', '/home/wildfly/archivitos/bpc_tc.pem');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PATH_FILE_KNOW_HOSTS', true, 'Archivo KnownHostKeys', '/home/wildfly/archivitos/KnownHostKeys.txt');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_FOLDER_BACKUP_APP_FILE', true, 'Ruta donde se almacena el respaldo de app_flow generados', '/home/wildfly/archivitos/app_files');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_INT_URL_CM', true, 'URL del CM', 'http://localhost:8096/cm/api/archivos/upload');

INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_URL_BPC_APIGATE', true, 'URL del servicio ApiGate de SV', 'http://172.19.223.3:7005/apigate/ws/soap?wsdl');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_URL_NUBARIUM_INE', true, 'URL del servicio nubarium para procesar la INE', 'https://ine.nubarium.com:443/ocr/obtener_datos');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_URL_NUBARIUM_FACIAL', true, 'URL del servicio nubarium para reconocimiento facial', 'https://curp.nubarium.com:443/antifraude/reconocimiento_facial');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_NUBARIUM_FACIAL_ACTIVO', true, 'Activación del servicio de reconocimiento facil (INE vs selfie)', 'true');

INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'MONTO_BITCLUB', true, 'Monto descuento por membresia Bitclub', '7250');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'PAN_BITCLUB', true, 'Cuenta de notificacion Bitclub', '5064430900001448');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'URL_NOTIFICACION_EBITWARE', true, 'URL de servicio REST de ebitware', 'http://172.19.223.10:8096/mws/api/consultas/saldo');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'AUTORIZACION_KEY', true, 'Token de autorizacion de peticion REST', 'U2FsdGVkX18q9MsyrXLVmsFhQ+ZSnNrkWX29YqSgGy0=');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'CRYPTO_SALT', true, 'Llave de cifrado para datos de peticion REST', 'Un1xSup3rS3cr3t3NC1ripti0nke7');

    
INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Creado', 'Creado');
INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Guardado Intranet', 'Guardado');
INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Abortado (Persona existe)', 'Repetido');
INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Abortado (Cliente existe)', 'Repetido');

INSERT INTO public.ESTADO_CLIENTE(ID_ESTADO_CLIENTE, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_CLIENTE'),true,'Activo', 'Activo');
INSERT INTO public.ESTADO_CLIENTE(ID_ESTADO_CLIENTE, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_CLIENTE'),true,'Inactivo', 'Inactivo');
	
INSERT INTO public.estado_tarjeta(id_estado_tarjeta, ind_activo, cliente, negocio, sm_status_code, sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TARJETA'), true, 'Enrolada', 'Enrolada', 'TBD', 'TBD');	
INSERT INTO public.estado_tarjeta(id_estado_tarjeta, ind_activo, cliente, negocio, sm_status_code, sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TARJETA'), true, 'Valida', 'Valida', '0', 'VALID CARD');
INSERT INTO public.estado_tarjeta(id_estado_tarjeta, ind_activo, cliente, negocio, sm_status_code, sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TARJETA'), true, 'Bloqueo por cliente', 'Bloqueo temporal por cliente', '20', 'TEMPORAL_BLOCK_BY_CLIENT');
	    

INSERT INTO public.tipo_documento(
            id_tipo_documento, acronimo, ind_activo, descripcion, nombre, 
            orden)
    VALUES (nextval('SEQ_TIPO_DOCUMENTO'), 'IDEN', true, 'Identificación oficial', 'Identificación', 
            1);

INSERT INTO public.tipo_documento(
            id_tipo_documento, acronimo, ind_activo, descripcion, nombre, 
            orden)
    VALUES (nextval('SEQ_TIPO_DOCUMENTO'), 'SELF', true, 'Selfie', 'Selfie', 
            2);            


INSERT INTO public.empresa(
            id_empresa, ind_activo, cuenta, fecha_registro, nombre, ind_propietario, rfc)
    VALUES (nextval('SEQ_EMPRESA'), true, '000000000', current_timestamp, 'UNIXSYC', true, 
            'UUU010101FGH');
INSERT INTO public.apikey(
            id_apikey, ind_activo, apikey, fecha_fin, fecha_inicio, fecha_registro, id_empresa, ID_EQUIPO)
    VALUES (nextval('SEQ_APIKEY'), true, '16639c9f7212d71ed1959337509fed6b1a8cff25', null, current_date, current_timestamp, 1 , 'PUn1xycs$');			

INSERT INTO public.empresa(
            id_empresa, ind_activo, cuenta, fecha_registro, nombre, ind_propietario, rfc)
    VALUES (nextval('SEQ_EMPRESA'), true, '000000001', current_timestamp, 'Handy Global', false, 
            'HHH010101TYU');
INSERT INTO public.apikey(
            id_apikey, ind_activo, apikey, fecha_fin, fecha_inicio, fecha_registro, id_empresa, ID_EQUIPO)
    VALUES (nextval('SEQ_APIKEY'), true, 'a4eb3ebad85482301623a07f1e30b67ffda52613', null, current_date, current_timestamp, 2, '$H4is-GaL');			

	
	
INSERT INTO public.tiraje_tarjeta(
            id_tiraje_tarjeta, fecha_registro, nombre, id_archivo_cm, HASH_MD5)
    VALUES (nextval('SEQ_TIRAJE_TARJETA'), current_timestamp, 'Carga inicial F&F por BD', null, 'HASH_MD5');

INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064430900000010', 1, '2905');
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064430900000028', 1, '2905');	
	INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064430900000036', 1, '2905');	
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064430900000044', 1, '2905');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064430900000051', 1, '2905');		

    
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'FABP170101HASRRRA3', current_timestamp, 'AGUASCALIENTES', 
            'MEXICANA', 'FRANCISCO', 'BARERRA', 'XEX010101000', 'PRUEBA', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302132', '009302132', 
		current_timestamp, '5515356082', 'FRANCISCO BARRERA PRUEBA', 2, 1, 1);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 1, 1, 2);
	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'FABP170101HASRRRA3', current_timestamp, 'AGUASCALIENTES', 
            'MEXICANA', 'FRANCISCO', 'BARERRA', 'XEX010101000', 'PRUEBA 2', current_date);
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302133', '009302133', 
		current_timestamp, '5515356083', 'FRANCISCO BARRERA PRUEBA 2', 2, 1, 2);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 2, 2, 2);	
	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'JEGP170101HASSRRA8', current_timestamp, 'MORELOS', 
            'MEXICANA', 'JESUS ELIAS', 'GARCIA', 'JEGP170101H29', 'PRUEBA', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302134', '009302134', 
		current_timestamp, '5515356084', 'JESUS ELIAS GARCIA PRUEBA', 2, 1, 3);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 3, 3, 2);

INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AOMT170101HASLYPA5', current_timestamp, 'MORELOS', 
            'MEXICANA', 'ALFONSO', 'MAYORAL', 'AOMT170101GD1', 'TAPIA PRUE', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'ALFONSO MAYORAL TAPIA PRUE', 2, 1, 4);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 4, 4, 2);	
-- 16-oct
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'EUCP170101HASDRRA3', current_timestamp, 'MORELOS', 
            'MEXICANA', 'EDUARDO', 'CARBAJAL', 'EUCP1701013F7', 'PRUEBA', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'EDUARDO CARBAJAL PRUEBA', 2, 1, 5);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 5, 5, 2);	

	
