CREATE SEQUENCE seq_enrollment;

INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_HOST', true, 'Host del SFTP', '192.168.238.47');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PORT', true, 'Port del SFTP', '22');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_USER', true, 'USR del SFTP', 'weblogic');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PWD', true, 'PWD del SFTP', 'wld72000');	
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_FOLDER_DESTINO', true, 'Folder del SFTP', '/home/weblogic/iofiles/incoming/application/iss/');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PATH_FILE_PRIVATE_KEY', true, 'Archivo .pem', '/home/wildfly/archivitos/bpc_tc.pem');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_SFTP_PATH_FILE_KNOW_HOSTS', true, 'Archivo KnownHostKeys', '/home/wildfly/archivitos/KnownHostKeys.txt');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PBC_FOLDER_BACKUP_APP_FILE', true, 'Ruta donde se almacena el respaldo de app_flow generados', '/home/wildfly/archivitos/app_files');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_INT_URL_CM', true, 'URL del CM', 'http://localhost:8080/cm/api/archivos/upload');

INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_URL_BPC_APIGATE', true, 'URL del servicio ApiGate de SV', 'http://192.168.238.47:7005/apigate/ws/soap?wsdl');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_URL_NUBARIUM_INE', true, 'URL del servicio nubarium para procesar la INE', 'https://ine.nubarium.com:443/ocr/obtener_datos');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_URL_NUBARIUM_FACIAL', true, 'URL del servicio nubarium para reconocimiento facial', 'https://curp.nubarium.com:443/antifraude/reconocimiento_facial');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'SIS_PROV_NUBARIUM_FACIAL_ACTIVO', true, 'Activación del servicio de reconocimiento facil (INE vs selfie)', 'true');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'MONTO_BITCLUB', true, 'Monto descuento por membresia Bitclub', '7250');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'PAN_BITCLUB', true, 'Cuenta de notificacion Bitclub', '5064430900001448');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'URL_NOTIFICACION_EBITWARE', true, 'URL de servicio REST de ebitware', 'http://172.19.223.10:8096/mws/api/consultas/saldo');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'AUTORIZACION_KEY', true, 'Token de autorizacion de peticion REST', 'U2FsdGVkX18q9MsyrXLVmsFhQ+ZSnNrkWX29YqSgGy0=');
INSERT INTO public.parametro(id_parametro, clave, IND_CACHE, descripcion, valor)
    VALUES (nextval('SEQ_PARAMETRO'), 'CRYPTO_SALT', true, 'Llave de cifrado para datos de peticion REST', 'Un1xSup3rS3cr3t3NC1ripti0nke7');

INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Creado', 'Creado');
INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Guardado Intranet', 'Guardado');
INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Abortado (Persona existe)', 'Repetido');
INSERT INTO public.ESTADO_ENROLAMIENTO(ID_ESTADO_ENROLAMIENTO, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_ENROLAMIENTO'),true,'Abortado (Cliente existe)', 'Repetido');

INSERT INTO public.ESTADO_CLIENTE(ID_ESTADO_CLIENTE, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_CLIENTE'),true,'Activo', 'Activo');
INSERT INTO public.ESTADO_CLIENTE(ID_ESTADO_CLIENTE, IND_ACTIVO, NEGOCIO, CLIENTE)
	VALUES (nextval('SEQ_ESTADO_CLIENTE'),true,'Inactivo', 'Inactivo');
	
INSERT INTO public.estado_tarjeta(id_estado_tarjeta, ind_activo, cliente, negocio, sm_status_code, sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TARJETA'), true, 'Enrolada', 'Enrolada', 'TBD', 'TBD');	
INSERT INTO public.estado_tarjeta(id_estado_tarjeta, ind_activo, cliente, negocio, sm_status_code, sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TARJETA'), true, 'Valida', 'Valida', '0', 'VALID CARD');
INSERT INTO public.estado_tarjeta(id_estado_tarjeta, ind_activo, cliente, negocio, sm_status_code, sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TARJETA'), true, 'Bloqueo por cliente', 'Bloqueo temporal por cliente', '20', 'TEMPORAL_BLOCK_BY_CLIENT');
	    
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'pdf', 'application/pdf');
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'xml', 'text/xml');
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'jpg', 'image/jpeg');
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'png', 'image/png');
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'zip', 'application/x-compressed-zip');
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'txt', 'text/plain');
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'xls', 'application/vnd.ms-excel');
INSERT INTO tipo_multimedia VALUES (nextval('SEQ_ID_TIPO_MULTIMEDIA'), 'xlsx', 'application/vnd.ms-excel');

INSERT INTO public.tipo_documento(
            id_tipo_documento, acronimo, ind_activo, descripcion, nombre, 
            orden)
    VALUES (nextval('SEQ_TIPO_DOCUMENTO'), 'IDEN', true, 'Identificación oficial', 'Identificación', 
            1);

INSERT INTO public.tipo_documento(
            id_tipo_documento, acronimo, ind_activo, descripcion, nombre, 
            orden)
    VALUES (nextval('SEQ_TIPO_DOCUMENTO'), 'SELF', true, 'Selfie', 'Selfie', 
            2);            


INSERT INTO public.empresa(
            id_empresa, ind_activo, cuenta, fecha_registro, nombre, ind_propietario, rfc)
    VALUES (nextval('SEQ_EMPRESA'), true, '000000000', current_timestamp, 'UNIXSYC', true, 
            'UUU010101FGH');
INSERT INTO public.apikey(
            id_apikey, ind_activo, apikey, fecha_fin, fecha_inicio, fecha_registro, id_empresa, ID_EQUIPO)
    VALUES (nextval('SEQ_APIKEY'), true, '16639c9f7212d71ed1959337509fed6b1a8cff25', null, current_date, current_timestamp, 1 , 'qwerty09875');			

INSERT INTO public.empresa(
            id_empresa, ind_activo, cuenta, fecha_registro, nombre, ind_propietario, rfc)
    VALUES (nextval('SEQ_EMPRESA'), true, '000000001', current_timestamp, 'Handy Global', false, 
            'HHH010101TYU');
INSERT INTO public.apikey(
            id_apikey, ind_activo, apikey, fecha_fin, fecha_inicio, fecha_registro, id_empresa, ID_EQUIPO)
    VALUES (nextval('SEQ_APIKEY'), true, '83159b488d0a0f325c3cf4166bb3d73f0b7b59fe', null, current_date, current_timestamp, 2, 'qwerty09874');			

INSERT INTO public.empresa(
            id_empresa, ind_activo, cuenta, fecha_registro, nombre, ind_propietario, rfc)
    VALUES (nextval('SEQ_EMPRESA'), true, '000000002', current_timestamp, 'e-networkx', false, 
            'ENW010101FGH');
INSERT INTO public.apikey(
            id_apikey, ind_activo, apikey, fecha_fin, fecha_inicio, fecha_registro, id_empresa, ID_EQUIPO)
    VALUES (nextval('SEQ_APIKEY'), true, '19c4be619f18a6ec88122ae3d35c7c8d5553851c', null, current_date, current_timestamp, 3, 'qwerty09873');			
	
	
INSERT INTO public.tiraje_tarjeta(
            id_tiraje_tarjeta, fecha_registro, nombre, id_archivo_cm, HASH_MD5)
    VALUES (nextval('SEQ_TIRAJE_TARJETA'), current_timestamp, 'Carga inicial de prueba', null, 'HASH_MD5');

INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064430000010711', 1, '2903');
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000290923', 1, '2906');	
	INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000290980', 1, '2906');	
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000291038', 1, '2906');		
-- 16-oct
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000292960', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000292978', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000292986', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000292994', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293000', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293018', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293026', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293034', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293042', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293059', 1, '2906');		
-- 18-oct	
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293174', 1, '2906');		
INSERT INTO public.TARJETA_DISPONIBLE(
            id_tarjeta_disponible, ind_asignanda, fecha_asignacion, fecha_registro, numero, id_tiraje_tarjeta, vencimiento)
    VALUES (nextval('SEQ_TARJETA_DISPONIBLE'), true, current_timestamp, current_timestamp, '5064431000293182', 1, '2906');		


	
    
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL00', current_timestamp, 'MORELOS', 
            'MEXICANA', 'VLAD', 'PAX', 'AUPV820209', null, current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302132', '009302132', 
		current_timestamp, '5515356082', 'VLAD PAX', 1, 1, 1);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 1, 1, 2);
	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL01', current_timestamp, 'MORELOS', 
            'MEXICANA', 'VLADIMIR', 'AGUIRRE', 'AUPV820209', 'PIEDRA', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302133', '009302133', 
		current_timestamp, '5515356083', 'Vladimir Aguirre P', 1, 1, 2);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 2, 2, 2);	
	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL02', current_timestamp, 'MORELOS', 
            'MEXICANA', 'Vladimir Jose de Jesus', 'REVILLAGIGEDO', 'AUPV820209', 'Zempoatecatl', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302134', '009302134', 
		current_timestamp, '5515356084', 'Vladimir Jose de Jesus REVILLAGIGEDO Zempoatecatl', 1, 1, 3);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 3, 3, 2);

INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'Vlad', 'Birth', 'AUPV820209', 'Day', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'Vlad Birth Day', 1, 1, 4);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 4, 4, 2);	
-- 16-oct
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'ALEJANDRO', 'SANTAMARIA', 'AUPV820209', 'AGUILAR', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'ALEJANDRO SANTAMARIA AGUILAR', 1, 1, 5);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 5, 5, 2);	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'PAULO', 'BARRERA', 'AUPV820209', 'PALACIOS', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'PAULO BARRERA PALACIOS', 1, 1, 6);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 6, 6, 2);
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'CATALINA', 'CAMACHO', 'AUPV820209', 'HERNANDEZ', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'CATALINA CAMACHO HERNANDEZ', 1, 1, 7);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 7, 7, 2);
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'AXEL', 'GARCIA', 'AUPV820209', 'FERNANDEZ', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'AXEL GARCIA FERNANDEZ', 1, 1, 8);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 8, 8, 2);
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'DANIELA', 'CERVANTES', 'AUPV820209', 'LOPEZ', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'DANIELA CERVANTES LOPEZ', 1, 1, 9);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 9, 9, 2);	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'LILIAN', 'DOMINGUEZ', 'AUPV820209', 'LOPEZ', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'LILIAN DOMINGUEZ LOPEZ', 1, 1, 10);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 10, 10, 2);
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'KAREN', 'RODRIGUEZ', 'AUPV820209', 'MENDOZA', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'KAREN RODRIGUEZ MENDOZA', 1, 1, 11);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 11, 11, 2);
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'UBALDO', 'ACOSTA', 'AUPV820209', 'BARRERA', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'UBALDO ACOSTA BARRERA', 1, 1, 12);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 12, 12, 2);	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'KENDY', 'RODRIGUEZ', 'AUPV820209', 'ADALI', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'KENDY RODRIGUEZ ADALI', 1, 1, 13);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 13, 13, 2);	
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'ARMANDO', 'MEDINA', 'AUPV820209', 'SANCGEZ', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'ARMANDO MEDINA SANCGEZ', 1, 1, 14);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 14, 14, 2);	
-- 18-oct		
INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'ANTHONY EDWARD', 'STARK', 'AUPV820209', null, current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'THONY STARK', 1, 1, 15);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 15, 15, 2);	

INSERT INTO public.persona(
            id_persona, ind_activo, curp, fecha_registro, lugar_nacimientp, 
            nacionalidad, nombre, primer_apellido, rfc, segundo_apellido, FECHA_NACIMIENTO)
    VALUES (nextval('SEQ_PERSONA'), true, 'AUPV810209HMSGDL03', current_timestamp, 'MORELOS', 
            'MEXICANA', 'ALEJANDRO', 'SANTAMARIA', 'AUPV820209', 'AGUILAR', current_date);	
INSERT INTO public.cliente(
		id_cliente, customer_category, customer_id, customer_number, 
		fecha_registro, negocio, nombre_titular, id_empresa, id_estado, id_persona)
VALUES (nextval('SEQ_CLIENTE'), 'CCTGORDN', '009302135', '009302135', 
		current_timestamp, '5515356085', 'BRUNO DÍAZ', 1, 1, 5);
INSERT INTO public.tarjeta_virtual(
            id_tarjeta_virtual, card_id, fecha_registro, numero_enmascarado, id_cliente, id_tarjeta_disponible,id_estado)
    VALUES (nextval('SEQ_TARJETA_VIRTUAL'), '13243546', current_timestamp, '', 5, 5, 2);	
	
INSERT INTO public.banco_cuenta_ref(
            id_banco_cuenta_ref, ind_activo, descripcion, nombre_banco, numero, codigo)
    VALUES (nextval('SEQ_BANCO_CUENTA_REF'), true, 'BBVA Bancomer', 'Bancomer', '0', 'BBVA');    

INSERT INTO public.estado_transaccion(
            id_estado_transaccion, ind_activo, cliente, negocio, sm_status_code, 
            sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TRANSACCION'), true, 'Correcto', 'Correcto', 'OK', 'OK');        
INSERT INTO public.estado_transaccion(
            id_estado_transaccion, ind_activo, cliente, negocio, sm_status_code, 
            sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TRANSACCION'), true, 'Registrado', 'Registrado', 'NA', 'NA');
INSERT INTO public.estado_transaccion(
            id_estado_transaccion, ind_activo, cliente, negocio, sm_status_code, 
            sm_status_desc)
    VALUES (nextval('SEQ_ESTADO_TRANSACCION'), true, 'Confirmada', 'Confirmada', 'NA', 'NA');        
    
