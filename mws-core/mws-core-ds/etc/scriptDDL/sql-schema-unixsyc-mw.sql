
    alter table APIKEY 
        drop constraint FK_4eo8h3focawsl185krr2cpdf4;

    alter table ARCHIVO_CM 
        drop constraint FK_t8rxvfff9e85uiwf7evqkmh1u;

    alter table CELULAR 
        drop constraint FK_apj519siwthhiooqyv7d17ilq;

    alter table CLIENTE 
        drop constraint FK_cxgsap6yltmin71x2ors1c31q;

    alter table CLIENTE 
        drop constraint FK_dkd39hjidx4afunrg2a9bk7em;

    alter table CLIENTE 
        drop constraint FK_1oittlrc17gy1mvaxufxhxvwt;

    alter table CUENTA 
        drop constraint FK_gh1xydv2yd7e0vr40ja66fop0;

    alter table CUENTA 
        drop constraint FK_h7y1i4kx91tps1hgqem1dl58q;

    alter table DEPOSITO_BANCARIO 
        drop constraint FK_51fiputaikrr4mar0vkkxw942;

    alter table DEPOSITO_BANCARIO 
        drop constraint FK_ldbn31w6gm9kk5r0jfgiqbt6k;

    alter table DEPOSITO_BANCARIO 
        drop constraint FK_rd5g37g778ivmn43530qugd3f;

    alter table DIRECCION 
        drop constraint FK_muoy31779e5i39hyqv0oapg2a;

    alter table ENROLAMIENTO 
        drop constraint FK_arxiyw3ixunjc9jl5cqme0mu6;

    alter table ENROLAMIENTO 
        drop constraint FK_p0imrmwvurl5nhnrwynt3eg8k;

    alter table ENROLAMIENTO 
        drop constraint FK_mpwcrwswlxhl67dckaxvyoko7;

    alter table ENROLAMIENTO 
        drop constraint FK_i8hmfc908ydbx0s0fypx227b4;

    alter table ENROLAMIENTO 
        drop constraint FK_br4skqo1f2pq3es3mh7lon3be;

    alter table ENROLAMIENTO 
        drop constraint FK_lmwy2fdrxcjcs0ge06dpiebhd;

    alter table TARJETA_DISPONIBLE 
        drop constraint FK_ftni48b3x9ocptimy1eoym0l2;

    alter table TARJETA_VIRTUAL 
        drop constraint FK_r7repxemyfj4qdwtg4qynlgee;

    alter table TARJETA_VIRTUAL 
        drop constraint FK_o1vtkbv6229gorskhlb3ees3;

    alter table TARJETA_VIRTUAL 
        drop constraint FK_6ls4tbkgjlre8ln4ku47e2in0;

    alter table TARJETA_VIRTUAL 
        drop constraint FK_j8fo17jit46cn806xq8vbrnpm;

    alter table TIRAJE_TARJETA 
        drop constraint FK_jms7pnk6tvqt8mwu86y11nyku;

    drop table if exists APIKEY cascade;

    drop table if exists ARCHIVO_CM cascade;

    drop table if exists BANCO_CUENTA_REF cascade;

    drop table if exists CELULAR cascade;

    drop table if exists CLIENTE cascade;

    drop table if exists CUENTA cascade;

    drop table if exists DEPOSITO_BANCARIO cascade;

    drop table if exists DIRECCION cascade;

    drop table if exists EMPRESA cascade;

    drop table if exists ENROLAMIENTO cascade;

    drop table if exists ESTADO_CLIENTE cascade;

    drop table if exists ESTADO_CUENTA cascade;

    drop table if exists ESTADO_ENROLAMIENTO cascade;

    drop table if exists ESTADO_TARJETA cascade;

    drop table if exists ESTADO_TRANSACCION cascade;

    drop table if exists PARAMETRO cascade;

    drop table if exists PARAMETRO_BPC cascade;

    drop table if exists PERSONA cascade;

    drop table if exists TARJETA_DISPONIBLE cascade;

    drop table if exists TARJETA_VIRTUAL cascade;

    drop table if exists TIPO_DOCUMENTO cascade;

    drop table if exists TIRAJE_TARJETA cascade;

    drop sequence SEQ_APIKEY;

    drop sequence SEQ_ARCHIVO_CM;

    drop sequence SEQ_BANCO_CUENTA_REF;

    drop sequence SEQ_CELULAR;

    drop sequence SEQ_CLIENTE;

    drop sequence SEQ_CUENTA;

    drop sequence SEQ_DEPOSITO_BANCARIO;

    drop sequence SEQ_DIRECCION;

    drop sequence SEQ_EMPRESA;

    drop sequence SEQ_ENROLAMIENTO;

    drop sequence SEQ_ESTADO_CLIENTE;

    drop sequence SEQ_ESTADO_CUENTA;

    drop sequence SEQ_ESTADO_ENROLAMIENTO;

    drop sequence SEQ_ESTADO_TARJETA;

    drop sequence SEQ_ESTADO_TRANSACCION;

    drop sequence SEQ_PARAMETRO;

    drop sequence SEQ_PARAMETRO_BPC;

    drop sequence SEQ_PERSONA;

    drop sequence SEQ_TARJETA_DISPONIBLE;

    drop sequence SEQ_TARJETA_VIRTUAL;

    drop sequence SEQ_TIPO_DOCUMENTO;

    drop sequence SEQ_TIRAJE_TARJETA;

    create table APIKEY (
        ID_APIKEY int8 not null,
        IND_ACTIVO boolean not null,
        APIKEY varchar(40) not null,
        FECHA_FIN timestamp,
        FECHA_INICIO timestamp not null,
        FECHA_REGISTRO timestamp not null,
        ID_EQUIPO varchar(40) not null,
        ID_SESION varchar(40),
        ID_EMPRESA int8 not null,
        primary key (ID_APIKEY)
    );

    create table ARCHIVO_CM (
        ID_ARCHIVO_CM int8 not null,
        COMENTARIO text,
        FECHA_MODIFICACION timestamp,
        FECHA_REGISTRO timestamp not null,
        ID_CM varchar(20) not null,
        NOMBRE varchar(120) not null,
        NUMERO_VERSION int4,
        ID_TIPO_DOCUMENTO int8,
        primary key (ID_ARCHIVO_CM)
    );

    create table BANCO_CUENTA_REF (
        ID_BANCO_CUENTA_REF int8 not null,
        IND_ACTIVO boolean,
        CODIGO varchar(10) not null,
        DESCRIPCION varchar(240),
        NOMBRE_BANCO varchar(120) not null,
        NUMERO varchar(20) not null,
        primary key (ID_BANCO_CUENTA_REF)
    );

    create table CELULAR (
        ID_CELULAR int8 not null,
        IND_ACTIVO boolean not null,
        COMPANIA varchar(10),
        FECHA_FIN timestamp,
        FECHA_INICIO timestamp not null,
        FECHA_REGISTRO timestamp not null,
        IMEI varchar(17),
        NUMERO varchar(10) not null,
        ID_CLIENTE int8 not null,
        primary key (ID_CELULAR)
    );

    create table CLIENTE (
        ID_CLIENTE int8 not null,
        CUSTOMER_CATEGORY varchar(10),
        CUSTOMER_ID varchar(22),
        CUSTOMER_NUMBER varchar(11),
        FECHA_REGISTRO timestamp not null,
        NEGOCIO varchar(25),
        NOMBRE_TITULAR varchar(160),
        ID_EMPRESA int8 not null,
        ID_ESTADO int8 not null,
        ID_PERSONA int8 not null,
        primary key (ID_CLIENTE)
    );

    create table CUENTA (
        ID_CUENTA int8 not null,
        FECHA_REGISTRO timestamp not null,
        MONEDA int4 not null,
        NUMERO varchar(14) not null,
        ID_CLIENTE int8 not null,
        ID_ESTADO int8 not null,
        primary key (ID_CUENTA)
    );

    create table DEPOSITO_BANCARIO (
        ID_DEPOSITO_BANCARIO int8 not null,
        AUTORIZACION varchar(20),
        CUENTA varchar(20),
        FECHA_DEPOSITO timestamp not null,
        FECHA_REGISTRO timestamp not null,
        FOLIO varchar(20),
        MONTO BIGINT not null,
        MOTIVO varchar(150),
        ID_BANCO_CUENTA_REF int8 not null,
        ID_ESTADO_TRANSACCION int8 not null,
        ID_TARJETA_VIRTUAL int8 not null,
        primary key (ID_DEPOSITO_BANCARIO)
    );

    create table DIRECCION (
        ID_DIRECCION int8 not null,
        IND_ACTIVO boolean not null,
        CALLE varchar(120),
        CIUDAD varchar(50),
        COLONIA varchar(80),
        CODIGO_POSTAL varchar(5),
        FECHA_FIN timestamp,
        FECHA_INICIO timestamp not null,
        FECHA_REGISTRO timestamp not null,
        LATITUD varchar(15),
        LONGITUD varchar(15),
        NO_EXTERIOR varchar(50),
        NO_INTERIOR varchar(50),
        PAIS varchar(10) not null,
        REFERENCIA varchar(150),
        TIPO_DOMICILIO varchar(1) not null,
        ID_CLIENTE int8 not null,
        primary key (ID_DIRECCION)
    );

    create table EMPRESA (
        ID_EMPRESA int8 not null,
        IND_ACTIVO boolean not null,
        CUENTA varchar(255),
        FECHA_REGISTRO timestamp not null,
        NOMBRE varchar(150) not null,
        IND_PROPIETARIO boolean not null,
        RFC varchar(13) not null,
        primary key (ID_EMPRESA)
    );

    create table ENROLAMIENTO (
        ID_ENROLAMIENTO int8 not null,
        APLICACION varchar(15),
        CANAL varchar(15) not null,
        CLAVE_ELECTOR varchar(18),
        F_CODIGO_VALIDACION varchar(20),
        FECHA_REGISTRO timestamp not null,
        INTENTOS int4 not null,
        F_SIMILITUD varchar(18),
        ID_ARCHIVO_CM_INE int8,
        ID_ARCHIVO_CM_SELFIE int8,
        ID_CLIENTE int8,
        ID_ESTADO int8 not null,
        ID_PERSONA int8,
        ID_TARJETA_DISPONIBLE int8 not null,
        primary key (ID_ENROLAMIENTO)
    );

    create table ESTADO_CLIENTE (
        ID_ESTADO_CLIENTE int8 not null,
        IND_ACTIVO boolean,
        CLIENTE varchar(255) not null,
        NEGOCIO varchar(255) not null,
        primary key (ID_ESTADO_CLIENTE)
    );

    create table ESTADO_CUENTA (
        ID_ESTADO_CUENTA int8 not null,
        IND_ACTIVO boolean,
        CLIENTE varchar(255) not null,
        NEGOCIO varchar(255) not null,
        SM_STATUS_CODE varchar(255) not null,
        SM_STATUS_DESC varchar(255),
        primary key (ID_ESTADO_CUENTA)
    );

    create table ESTADO_ENROLAMIENTO (
        ID_ESTADO_ENROLAMIENTO int8 not null,
        IND_ACTIVO boolean,
        CLIENTE varchar(255) not null,
        NEGOCIO varchar(255) not null,
        primary key (ID_ESTADO_ENROLAMIENTO)
    );

    create table ESTADO_TARJETA (
        ID_ESTADO_TARJETA int8 not null,
        IND_ACTIVO boolean,
        CLIENTE varchar(255) not null,
        NEGOCIO varchar(255) not null,
        SM_STATUS_CODE varchar(255) not null,
        SM_STATUS_DESC varchar(255),
        primary key (ID_ESTADO_TARJETA)
    );

    create table ESTADO_TRANSACCION (
        ID_ESTADO_TRANSACCION int8 not null,
        IND_ACTIVO boolean,
        CLIENTE varchar(255) not null,
        NEGOCIO varchar(255) not null,
        SM_STATUS_CODE varchar(255) not null,
        SM_STATUS_DESC varchar(255),
        primary key (ID_ESTADO_TRANSACCION)
    );

    create table PARAMETRO (
        ID_PARAMETRO int8 not null,
        IND_CACHE boolean not null,
        CLAVE varchar(50) not null,
        DESCRIPCION varchar(160) not null,
        VALOR varchar(250) not null,
        primary key (ID_PARAMETRO)
    );

    create table PARAMETRO_BPC (
        ID_PARAMETRO_BPC int8 not null,
        IND_CACHE boolean not null,
        CLAVE varchar(150) not null,
        DESCRIPCION varchar(160) not null,
        VALOR varchar(150) not null,
        primary key (ID_PARAMETRO_BPC)
    );

    create table PERSONA (
        ID_PERSONA int8 not null,
        IND_ACTIVO boolean not null,
        CURP varchar(18),
        FECHA_NACIMIENTO date not null,
        FECHA_REGISTRO timestamp not null,
        LUGAR_NACIMIENTP varchar(45),
        NACIONALIDAD varchar(25),
        NOMBRE varchar(80) not null,
        PRIMER_APELLIDO varchar(80) not null,
        RFC varchar(13),
        SEGUNDO_APELLIDO varchar(80),
        primary key (ID_PERSONA)
    );

    create table TARJETA_DISPONIBLE (
        ID_TARJETA_DISPONIBLE int8 not null,
        IND_ASIGNANDA boolean not null,
        FECHA_ASIGNACION timestamp,
        FECHA_REGISTRO timestamp not null,
        NUMERO varchar(60) not null,
        VENCIMIENTO varchar(4) not null,
        ID_TIRAJE_TARJETA int8 not null,
        primary key (ID_TARJETA_DISPONIBLE)
    );

    create table TARJETA_VIRTUAL (
        ID_TARJETA_VIRTUAL int8 not null,
        CARD_ID varchar(11),
        FECHA_REGISTRO timestamp not null,
        NUMERO_ENMASCARADO varchar(19) not null,
        ID_CLIENTE int8 not null,
        ID_CUENTA int8,
        ID_ESTADO int8 not null,
        ID_TARJETA_DISPONIBLE int8 not null,
        primary key (ID_TARJETA_VIRTUAL)
    );

    create table TIPO_DOCUMENTO (
        ID_TIPO_DOCUMENTO int8 not null,
        ACRONIMO varchar(12) not null,
        IND_ACTIVO boolean not null,
        DESCRIPCION varchar(120) not null,
        NOMBRE varchar(80) not null,
        ORDEN  int4,
        primary key (ID_TIPO_DOCUMENTO)
    );

    create table TIRAJE_TARJETA (
        ID_TIRAJE_TARJETA int8 not null,
        FECHA_REGISTRO timestamp not null,
        HASH_MD5 varchar(40) not null,
        NOMBRE varchar(60) not null,
        ID_ARCHIVO_CM int8,
        primary key (ID_TIRAJE_TARJETA)
    );

    alter table PARAMETRO 
        add constraint UK_py81jnxj1214dlqrf0120euy1  unique (CLAVE);

    alter table PARAMETRO_BPC 
        add constraint UK_iqwhao1k5f2tsoujpk0u37msk  unique (CLAVE);

    alter table APIKEY 
        add constraint FK_4eo8h3focawsl185krr2cpdf4 
        foreign key (ID_EMPRESA) 
        references EMPRESA;

    alter table ARCHIVO_CM 
        add constraint FK_t8rxvfff9e85uiwf7evqkmh1u 
        foreign key (ID_TIPO_DOCUMENTO) 
        references TIPO_DOCUMENTO;

    alter table CELULAR 
        add constraint FK_apj519siwthhiooqyv7d17ilq 
        foreign key (ID_CLIENTE) 
        references CLIENTE;

    alter table CLIENTE 
        add constraint FK_cxgsap6yltmin71x2ors1c31q 
        foreign key (ID_EMPRESA) 
        references EMPRESA;

    alter table CLIENTE 
        add constraint FK_dkd39hjidx4afunrg2a9bk7em 
        foreign key (ID_ESTADO) 
        references ESTADO_CLIENTE;

    alter table CLIENTE 
        add constraint FK_1oittlrc17gy1mvaxufxhxvwt 
        foreign key (ID_PERSONA) 
        references PERSONA;

    alter table CUENTA 
        add constraint FK_gh1xydv2yd7e0vr40ja66fop0 
        foreign key (ID_CLIENTE) 
        references CLIENTE;

    alter table CUENTA 
        add constraint FK_h7y1i4kx91tps1hgqem1dl58q 
        foreign key (ID_ESTADO) 
        references ESTADO_CUENTA;

    alter table DEPOSITO_BANCARIO 
        add constraint FK_51fiputaikrr4mar0vkkxw942 
        foreign key (ID_BANCO_CUENTA_REF) 
        references BANCO_CUENTA_REF;

    alter table DEPOSITO_BANCARIO 
        add constraint FK_ldbn31w6gm9kk5r0jfgiqbt6k 
        foreign key (ID_ESTADO_TRANSACCION) 
        references ESTADO_TRANSACCION;

    alter table DEPOSITO_BANCARIO 
        add constraint FK_rd5g37g778ivmn43530qugd3f 
        foreign key (ID_TARJETA_VIRTUAL) 
        references TARJETA_VIRTUAL;

    alter table DIRECCION 
        add constraint FK_muoy31779e5i39hyqv0oapg2a 
        foreign key (ID_CLIENTE) 
        references CLIENTE;

    alter table ENROLAMIENTO 
        add constraint FK_arxiyw3ixunjc9jl5cqme0mu6 
        foreign key (ID_ARCHIVO_CM_INE) 
        references ARCHIVO_CM;

    alter table ENROLAMIENTO 
        add constraint FK_p0imrmwvurl5nhnrwynt3eg8k 
        foreign key (ID_ARCHIVO_CM_SELFIE) 
        references ARCHIVO_CM;

    alter table ENROLAMIENTO 
        add constraint FK_mpwcrwswlxhl67dckaxvyoko7 
        foreign key (ID_CLIENTE) 
        references CLIENTE;

    alter table ENROLAMIENTO 
        add constraint FK_i8hmfc908ydbx0s0fypx227b4 
        foreign key (ID_ESTADO) 
        references ESTADO_ENROLAMIENTO;

    alter table ENROLAMIENTO 
        add constraint FK_br4skqo1f2pq3es3mh7lon3be 
        foreign key (ID_PERSONA) 
        references PERSONA;

    alter table ENROLAMIENTO 
        add constraint FK_lmwy2fdrxcjcs0ge06dpiebhd 
        foreign key (ID_TARJETA_DISPONIBLE) 
        references TARJETA_DISPONIBLE;

    alter table TARJETA_DISPONIBLE 
        add constraint FK_ftni48b3x9ocptimy1eoym0l2 
        foreign key (ID_TIRAJE_TARJETA) 
        references TIRAJE_TARJETA;

    alter table TARJETA_VIRTUAL 
        add constraint FK_r7repxemyfj4qdwtg4qynlgee 
        foreign key (ID_CLIENTE) 
        references CLIENTE;

    alter table TARJETA_VIRTUAL 
        add constraint FK_o1vtkbv6229gorskhlb3ees3 
        foreign key (ID_CUENTA) 
        references CUENTA;

    alter table TARJETA_VIRTUAL 
        add constraint FK_6ls4tbkgjlre8ln4ku47e2in0 
        foreign key (ID_ESTADO) 
        references ESTADO_TARJETA;

    alter table TARJETA_VIRTUAL 
        add constraint FK_j8fo17jit46cn806xq8vbrnpm 
        foreign key (ID_TARJETA_DISPONIBLE) 
        references TARJETA_DISPONIBLE;

    alter table TIRAJE_TARJETA 
        add constraint FK_jms7pnk6tvqt8mwu86y11nyku 
        foreign key (ID_ARCHIVO_CM) 
        references ARCHIVO_CM;

    create sequence SEQ_APIKEY;

    create sequence SEQ_ARCHIVO_CM;

    create sequence SEQ_BANCO_CUENTA_REF;

    create sequence SEQ_CELULAR;

    create sequence SEQ_CLIENTE;

    create sequence SEQ_CUENTA;

    create sequence SEQ_DEPOSITO_BANCARIO;

    create sequence SEQ_DIRECCION;

    create sequence SEQ_EMPRESA;

    create sequence SEQ_ENROLAMIENTO;

    create sequence SEQ_ESTADO_CLIENTE;

    create sequence SEQ_ESTADO_CUENTA;

    create sequence SEQ_ESTADO_ENROLAMIENTO;

    create sequence SEQ_ESTADO_TARJETA;

    create sequence SEQ_ESTADO_TRANSACCION;

    create sequence SEQ_PARAMETRO;

    create sequence SEQ_PARAMETRO_BPC;

    create sequence SEQ_PERSONA;

    create sequence SEQ_TARJETA_DISPONIBLE;

    create sequence SEQ_TARJETA_VIRTUAL;

    create sequence SEQ_TIPO_DOCUMENTO;

    create sequence SEQ_TIRAJE_TARJETA;
