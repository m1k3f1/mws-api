/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.ds.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.middleware.core.ds.TirajeTarjetaDAO;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.entity.TirajeTarjeta;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class TirajeTarjetaDAOBean  extends BaseGenericMWSDAOImpl<TirajeTarjeta, Long> implements TirajeTarjetaDAO {
	private final static Logger LOG = LogManager.getLogger(TirajeTarjetaDAOBean.class);

	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}

	@Override
	public Long consultarIdByHash(String md5Hash) {
		final StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT obj.id");
		queryStr.append(" FROM ");
		queryStr.append(TirajeTarjeta.class.getSimpleName()).append(" as obj");

		queryStr.append(" WHERE ");
		queryStr.append(" hashMd5 = :hashMd5");


		final Query query = super.em.createQuery(queryStr.toString());
		query.setParameter("hashMd5", md5Hash);
		final List<Long> data = query.getResultList();
		if (CollectionUtils.isNotEmpty(data)) {
			return data.get(PRIMERA_POSICION);
		}
	
		return null;
	}
}
