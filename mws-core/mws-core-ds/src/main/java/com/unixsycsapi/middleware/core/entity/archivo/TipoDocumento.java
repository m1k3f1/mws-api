/**
 * 
 */
package com.unixsycsapi.middleware.core.entity.archivo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * <ul>
 * <li>INE</li>
 * <li>Selfie</li>
 * <li>Tiraje tarejtas</li>
 * <li><i>etc.</i></li>
 * </ul>
 * 
 * @author Vladimir Aguirre
 *
 */
@Entity
@Table(name = "TIPO_DOCUMENTO")
public class TipoDocumento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2209755454025190804L;
	@Id
	@SequenceGenerator(name = "TIPO_DOCUMENTO_ID_GENERATOR", sequenceName = "SEQ_TIPO_DOCUMENTO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TIPO_DOCUMENTO_ID_GENERATOR")
	@Column(name = "ID_TIPO_DOCUMENTO")
	private Long id;
	@Column(name = "NOMBRE", length = 80, nullable = false)
	private String nombre;
	@Column(name = "ACRONIMO", length = 12, nullable = false)
	private String acronimo;
	@Column(name = "DESCRIPCION", length = 120, nullable = false)
	private String descripcion;
	@Column(name = "IND_ACTIVO", nullable = false)
	private Boolean activo;

	@Column(name = "ORDEN ")
	private Integer orden;


	/**
	 * 
	 */
	public TipoDocumento() {
		super();
	}

	public TipoDocumento(Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo
	 *            the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the acronimo
	 */
	public String getAcronimo() {
		return acronimo;
	}

	/**
	 * @param acronimo
	 *            the acronimo to set
	 */
	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}

	/**
	 * @return the orden
	 */
	public Integer getOrden() {
		return orden;
	}

	/**
	 * @param orden
	 *            the orden to set
	 */
	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	
}
