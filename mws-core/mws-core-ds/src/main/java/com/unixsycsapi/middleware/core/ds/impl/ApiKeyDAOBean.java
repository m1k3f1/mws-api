/**
 * 
 */
package com.unixsycsapi.middleware.core.ds.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.InfraestructuraException;
import com.unixsycsapi.middleware.core.ds.ApiKeyDAO;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.entity.ApiKey;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class ApiKeyDAOBean extends BaseGenericMWSDAOImpl<ApiKey, Long> implements ApiKeyDAO {
	private final static Logger LOG = LogManager.getLogger(ApiKeyDAOBean.class);

	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}

	@Override
	public ApiKey findByKey(String key) {
		List<ApiKey> data = em.createQuery("SELECT c FROM ApiKey c WHERE c.apikey = :custName and c.activo=true")
				.setParameter("custName", key).getResultList();
		return data.get(PRIMERA_POSICION);
	}

	@Override
	public Long getIdEmpresa(String key) throws InfraestructuraException {
		final StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT obj.empresa.id");
		queryStr.append(" FROM ");
		queryStr.append(ApiKey.class.getSimpleName()).append(" as obj");

		queryStr.append(" WHERE ");
		queryStr.append(" obj.apikey = :key");
		queryStr.append(" AND obj.activo = true");

		if (LOG.isDebugEnabled()) {
			LOG.debug(key);
		}

		final Query query = super.em.createQuery(queryStr.toString());
		query.setParameter("key", key);
		final List<Long> data = query.getResultList();
		if (CollectionUtils.isNotEmpty(data) && data.size() == SIZE_UNA_POSICION) {
			return data.get(PRIMERA_POSICION);
		}
		if (data.size() > 1) {
			LOG.warn("Existe un problema de configuracion con apikeys");
			throw new InfraestructuraException(CodigosError.INFRA_CONFIGURACION);
		}
		return null;
	}

	@Override
	public ApiKey loginEmpresa(String key, String equipo) throws InfraestructuraException {
		final StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT obj");
		queryStr.append(" FROM ");
		queryStr.append(ApiKey.class.getSimpleName()).append(" as obj");

		queryStr.append(" WHERE ");
		queryStr.append(" obj.apikey = :key");
		queryStr.append(" AND obj.activo = true");
		queryStr.append(" AND obj.idEquipo = :idEquipo");

		if (LOG.isDebugEnabled()) {
			LOG.debug(key);
			LOG.debug(equipo);

		}

		final Query query = super.em.createQuery(queryStr.toString());
		query.setParameter("key", key);
		query.setParameter("idEquipo", equipo);
		final List<ApiKey> data = query.getResultList();
		if (CollectionUtils.isNotEmpty(data) && data.size() == SIZE_UNA_POSICION) {
			return data.get(PRIMERA_POSICION);
		}
		if (data.size() > 1) {
			LOG.warn("Existe un problema de configuracion con apikeys");
			throw new InfraestructuraException(CodigosError.INFRA_CONFIGURACION);
		}
		return null;
	}

}
