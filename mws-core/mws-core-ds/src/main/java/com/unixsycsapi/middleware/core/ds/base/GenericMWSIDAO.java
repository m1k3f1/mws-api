/**
 * 
 */
package com.unixsycsapi.middleware.core.ds.base;

import java.io.Serializable;
import java.util.List;

/**
 * Interfaz que deben implementar todas las interfaces que definana un DAO. <br>
 * Sa soporte al CRUD.
 * 
 * @author Vladimir Aguirre Piedragil
 *
 */

public interface GenericMWSIDAO<T, ID extends Serializable> {
	final static String CAMPO_NEGOCIO[] = { "negocio" };
	/**
	 * persistence-unit name="mwsPU"
	 */
	final public String MWS_PERSISTENCE_UNIT = "mwsPU";

	/**
	 * Permite hacer un <code>INSERT</code> en la BD:
	 * <ul>
	 * <li>Acto</li>
	 * <li>Rol</li>
	 * </ul>
	 * 
	 * @param entity
	 */
	void guardar(T entity);



	/**
	 * Permite hacer un <code>UPDATE</code> en la BD.
	 * <ul>
	 * <li>Entidad</li>
	 * <li>Rol</li>
	 * <li>Participacion</li>
	 * <li>Acto</li>
	 * <li>Cat�logo</li>
	 * </ul>
	 * 
	 * @param entity
	 */
	void modificar(T entity);

	/**
	 * Permimte consultar por un <code>id</code>.
	 * <ul>
	 * <li>Entidad</li>
	 * <li>Rol</li>
	 * <li>Participacion</li>
	 * <li>Acto</li>
	 * <li>Cat�logo</li>
	 * </ul>
	 * 
	 * @param filtro
	 * @return
	 */
	T consultar(ID id);

	/**
	 * Busca en base a la propiedad.<br>
	 * Si value
	 * <ul>
	 * <li>es <code>null</code> se genera <code>propertyName = value</code></li>
	 * <li>en caso contrario se genera <code>propertyName is null</li>
	 * </ul>
	 * 
	 * @param propNames
	 * @param filtro
	 * @return
	 */
	List<T> consultarByProperties(String[] propNames, T filtro);

	/**
	 * Busca en base a la propiedad.<br>
	 * Si value
	 * <ul>
	 * <li>es <code>null</code> se genera <code>propertyName = value</code></li>
	 * <li>en caso contrario se genera <code>propertyName is null</li>
	 * </ul>
	 *
	 * @param propNames
	 * @param filtro
	 * @param campoOrd
	 * @param asc
	 * @return
	 * @throws JivaException
	 */
	List<T> consultarByProperties(String[] propNames, T filtro,
			String campoOrd, boolean asc);
	
	void eliminar(T entity);

}
