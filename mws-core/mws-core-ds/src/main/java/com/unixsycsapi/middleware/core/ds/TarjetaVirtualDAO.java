/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.ds;

import java.util.List;

import javax.ejb.Local;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.entity.TarjetaVirtual;

/**
 * @author Vladimir Aguirre
 *
 */
@Local
public interface TarjetaVirtualDAO extends GenericMWSIDAO<TarjetaVirtual, Long> {
	List<TarjetaVirtual> consultarTarjetasByCliente(Long idCliente);

}
