/**
 * 
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.unixsycsapi.middleware.core.entity.archivo.ArchivoCM;

/**
 * Entidad para respaldar el registro de un <code>Cliente</code>. Un
 * <code>Cliente</code> y/o una <code>Persona</code> pueden tener varios
 * procesos de enrolamiento, pero solo debe ser efectivo, los demás son intentos
 * fallidos
 * 
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "TIRAJE_TARJETA")
public class TirajeTarjeta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8000496857600699497L;

	@Id
	@SequenceGenerator(name = "TIRAJE_TARJETA_ID_GENERATOR", sequenceName = "SEQ_TIRAJE_TARJETA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TIRAJE_TARJETA_ID_GENERATOR")
	@Column(name = "ID_TIRAJE_TARJETA")
	private Long id;

	@Column(name = "NOMBRE", length = 60, nullable = false)
	private String nombre;
	
//	@Column(name = "ID_PRODUCTO", length = 20, nullable = false)
	@Transient
	private String idProducto;
	
	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ARCHIVO_CM", referencedColumnName = "ID_ARCHIVO_CM", nullable = true)
	private ArchivoCM archivoTiraje;

	@Column(name = "HASH_MD5", length = 40, nullable = false)
	private String hashMd5;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the archivoTiraje
	 */
	public ArchivoCM getArchivoTiraje() {
		return archivoTiraje;
	}

	/**
	 * @param archivoTiraje
	 *            the archivoTiraje to set
	 */
	public void setArchivoTiraje(ArchivoCM archivoTiraje) {
		this.archivoTiraje = archivoTiraje;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the hashMd5
	 */
	public String getHashMd5() {
		return hashMd5;
	}

	/**
	 * @param hashMd5
	 *            the hashMd5 to set
	 */
	public void setHashMd5(String hashMd5) {
		this.hashMd5 = hashMd5;
	}

	
}
