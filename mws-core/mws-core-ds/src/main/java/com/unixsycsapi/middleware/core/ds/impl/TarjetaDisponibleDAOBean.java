/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.ds.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.middleware.core.ds.TarjetaDisponibleDAO;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.entity.TarjetaDisponible;


/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class TarjetaDisponibleDAOBean  extends BaseGenericMWSDAOImpl<TarjetaDisponible, Long>  implements TarjetaDisponibleDAO {
	private final static Logger LOG = LogManager.getLogger(TarjetaDisponibleDAOBean.class);

	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}

	@Override
	public TarjetaDisponible getNextTarjeta() {
		
		final StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT obj");
		queryStr.append(" FROM ");
		queryStr.append(TarjetaDisponible.class.getSimpleName()).append(" as obj");

		queryStr.append(" WHERE ");
		queryStr.append(" obj.id = (select MIN(obji.id) from ");
		queryStr.append(TarjetaDisponible.class.getSimpleName()).append(" as obji");
		queryStr.append(" WHERE obji.asignada = false)");

		final Query query = super.em.createQuery(queryStr.toString());
	    List<TarjetaDisponible> data=	query.getResultList();
	    if (data==null || data.isEmpty()){
	    	return null;
	    	
	    }
	    return data.get(PRIMERA_POSICION);
	}
	
	@Override
	public void desvincular(TarjetaDisponible obj) {
		super.desvincular(obj);
	}
}
