/**
 * 
 */
package com.unixsycsapi.middleware.core.ds.infra.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.framework.commons.enums.CodigosError;
import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.ds.infra.ParametroDAO;
import com.unixsycsapi.middleware.core.entity.infra.Parametro;

/**
 * @author Vladimir Aguirre Piedragil
 *
 */
@Stateless
public class ParametroDAOBean extends BaseGenericMWSDAOImpl<Parametro, Long> implements ParametroDAO {
	/**
	 * Logger.
	 */
	private final static Logger LOG = LogManager.getLogger(ParametroDAOBean.class);

	/**
	 * M�todo que permite agregar el Entity Manager al DAO
	 */
	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}

	@Override
	public Parametro consultarPorClave(String claveParam) throws USException {
		try {
			final StringBuilder cad = armarSelectFrom();
			cad.append(" WHERE o.clave = :claveParam");

			final Query qry = this.em.createQuery(cad.toString());
			qry.setParameter("claveParam", claveParam);
			return (Parametro) qry.getSingleResult();
		} catch (NoResultException nre) {
			LOG.error("Parametro no existe " + claveParam);
			return null;
		} catch (NonUniqueResultException nu) {
			LOG.error("Parametro repetido " + claveParam);
			return null;
		} catch (Exception e) {
			LOG.error("Error al leer el parametro: " + e.getMessage(), e);
			throw new USException(CodigosError.GENERICO_INTERNO);
		}
	}

	@Override
	public List<Parametro> consultarCacheables() {
		final StringBuilder cad = armarSelectFrom();
		cad.append(" WHERE o.cache = true");

		return super.ejecutarJPAQL(cad);
	}

	@Override
	public void desvincular(Parametro obj) {
		super.desvincular(obj);

	}

	@Override
	public List<Parametro> consultarTodos() {
		final StringBuilder cad = armarSelectFrom();
		cad.append(" order by o.clave");
		return super.ejecutarJPAQL(cad);
	}

	/**
	 * 
	 * @return
	 */
	private StringBuilder armarSelectFrom() {
		final StringBuilder cad = new StringBuilder();
		cad.append("SELECT o from ").append(Parametro.class.getSimpleName());
		cad.append(" as o ");
		return cad;
	}

	@Override
	public void modificar(Parametro par) {
		super.modificar(par);
	}

}
