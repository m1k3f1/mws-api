/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.ds.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.middleware.core.ds.DepositoBancarioDAO;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.entity.DepositoBancario;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class DepositoBancarioDAOBean extends BaseGenericMWSDAOImpl<DepositoBancario, Long> implements DepositoBancarioDAO {
	private final static Logger LOG = LogManager.getLogger(DepositoBancarioDAOBean.class);

	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}
}
