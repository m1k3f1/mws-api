package com.unixsycsapi.middleware.core.ds;

import javax.ejb.Local;

import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.entity.archivo.ArchivoCM;

@Local
public interface ArchivoCMDAO extends GenericMWSIDAO<ArchivoCM, Long> {

}