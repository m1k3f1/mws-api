/**
 * 
 */
package com.unixsycsapi.middleware.core.entity.infra;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Parametros para el archivo application que se manda en el enrolamiento de PBC
 * 
 * @author Vladimir Aguirre
 *
 */
@Entity
@Table(name = "PARAMETRO_BPC")
public class ParametroBPC implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6359186864203961882L;

	@Id
	@SequenceGenerator(name = "PARAMETRO_BPC_ID_GENERATOR", sequenceName = "SEQ_PARAMETRO_BPC", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PARAMETRO_BPC_ID_GENERATOR")
	@Column(name = "ID_PARAMETRO_BPC")
	private Long id;

	@Column(name = "CLAVE", length = 150, nullable = false, unique = true)
	private String clave;
	@Column(name = "VALOR", length = 150, nullable = false)
	private String valor;
	@Column(name = "DESCRIPCION", length = 160, nullable = false)
	private String descripcion;

	@Column(name = "IND_CACHE", nullable = false)
	private Boolean cache;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave
	 *            the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the cache
	 */
	public Boolean getCache() {
		return cache;
	}

	/**
	 * @param cache
	 *            the cache to set
	 */
	public void setCache(Boolean cache) {
		this.cache = cache;
	}

}
