/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.ds.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.middleware.core.ds.TarjetaVirtualDAO;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.entity.TarjetaVirtual;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class TarjetaVirtualDAOBean extends BaseGenericMWSDAOImpl<TarjetaVirtual, Long> implements TarjetaVirtualDAO {
	private final static Logger LOG = LogManager.getLogger(TarjetaVirtualDAOBean.class);

	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}

	@Override
	public List<TarjetaVirtual> consultarTarjetasByCliente(Long idCliente) {
		final StringBuilder queryStr = new StringBuilder();
		queryStr.append("SELECT obj");
		queryStr.append(" FROM ");
		queryStr.append(TarjetaVirtual.class.getSimpleName()).append(" as obj");

		queryStr.append(" WHERE ");
		queryStr.append(" obj.cliente.id = ").append(idCliente);
		return super.ejecutarJPAQL(queryStr);
	}

}
