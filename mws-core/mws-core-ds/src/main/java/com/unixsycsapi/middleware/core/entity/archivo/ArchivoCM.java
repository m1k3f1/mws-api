/**
 * 
 */
package com.unixsycsapi.middleware.core.entity.archivo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "ARCHIVO_CM")
public class ArchivoCM implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4071299237406877064L;
	@Id
	@SequenceGenerator(name = "ARCHIVO_CM_ID_GENERATOR", sequenceName = "SEQ_ARCHIVO_CM", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARCHIVO_CM_ID_GENERATOR")
	@Column(name = "ID_ARCHIVO_CM")
	private Long id;
	@Column(name = "NOMBRE", length = 120, nullable = false)
	private String nombre;

	/**
	 * <code>Identificador</code> del <code>Content Manager</code>.<br>
	 * Con este identificador se recupera la instancia del archivo.
	 */
	@Column(name = "ID_CM", length = 20, nullable = false)
	private String idCM;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_DOCUMENTO", referencedColumnName = "ID_TIPO_DOCUMENTO", nullable = true)
	private TipoDocumento tipoDocumento;
	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	@Column(name = "FECHA_MODIFICACION", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaModificacion;

	@Column(name = "NUMERO_VERSION", nullable = true)
	private Integer numeroVersion;

	@Column(name = "COMENTARIO", nullable = true, columnDefinition = "text")
	private String comentario;

	/**
	 * 
	 */
	public ArchivoCM() {
		super();
	}

	/**
	 * @param id
	 */
	public ArchivoCM(Long id) {
		super();
		this.id = id;
	}

	/**
	 * @param id
	 * @param nombre
	 */
	public ArchivoCM(Long id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public ArchivoCM(Long id, String nombre, Date fec) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.fechaRegistro = fec;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the numeroVersion
	 */
	public Integer getNumeroVersion() {
		return numeroVersion;
	}

	/**
	 * @param numeroVersion
	 *            the numeroVersion to set
	 */
	public void setNumeroVersion(Integer numeroVersion) {
		this.numeroVersion = numeroVersion;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the idCM
	 */
	public String getIdCM() {
		return idCM;
	}

	/**
	 * @param idCM
	 *            the idCM to set
	 */
	public void setIdCM(String idCM) {
		this.idCM = idCM;
	}

	/**
	 * @return the tipoDocumento
	 */
	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
}
