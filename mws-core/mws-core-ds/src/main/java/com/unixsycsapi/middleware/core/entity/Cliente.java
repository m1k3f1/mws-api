/**
 * 
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unixsycsapi.middleware.core.entity.estado.EstadoCliente;

/**
 * Entidad que representa al cliente. <br>
 * Es el catalogo de clientes para una <code>Empresa</code>. <br>
 * Para el negocio de las tarjetas esta entidad es mas importante que
 * <code>Persona</code>.
 * 
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "CLIENTE")
public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5060477136677346020L;

	@Id
	@SequenceGenerator(name = "CLIENTE_ID_GENERATOR", sequenceName = "SEQ_CLIENTE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLIENTE_ID_GENERATOR")
	@Column(name = "ID_CLIENTE")
	private Long id;

	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID_EMPRESA", nullable = false)
	private Empresa empresa;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID_PERSONA", nullable = false)
	private Persona persona;

	/**
	 * Proporcionado en el enrolamiento.
	 */
	@Column(name = "NOMBRE_TITULAR", length = 160, nullable = true)
	private String nombreTitular;

	
	/**
	 * Campo de negocio definido y conocido por la <code>Empresa</code> que nos
	 * consume
	 */
	@Column(name = "NEGOCIO", length = 25, nullable = true)
	private String negocio;
	/**
	 * Referencia de BPC
	 */
	@Column(name = "CUSTOMER_ID", length = 22, nullable = true)
	private String customerId;
	/**
	 * Referencia de BPC
	 */
	@Column(name = "CUSTOMER_NUMBER", length = 11, nullable = true)
	private String customerNumber;
	/**
	 * Referencia de BPC
	 */
	@Column(name = "CUSTOMER_CATEGORY", length = 10, nullable = true)
	private String customerCategory;
	@ManyToOne
	@JoinColumn(name = "ID_ESTADO", nullable = false)
	private EstadoCliente estado;

	public Cliente() {
		super();
	}

	public Cliente(Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * @param empresa
	 *            the empresa to set
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * @return the persona
	 */
	public Persona getPersona() {
		return persona;
	}

	/**
	 * @param persona
	 *            the persona to set
	 */
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	/**
	 * @return the nombreTitular
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}

	/**
	 * @param nombreTitular
	 *            the nombreTitular to set
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber() {
		return customerNumber;
	}

	/**
	 * @param customerNumber
	 *            the customerNumber to set
	 */
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	/**
	 * @return the customerCategory
	 */
	public String getCustomerCategory() {
		return customerCategory;
	}

	/**
	 * @param customerCategory
	 *            the customerCategory to set
	 */
	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	/**
	 * @return the estado
	 */
	public EstadoCliente getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(EstadoCliente estado) {
		this.estado = estado;
	}

	/**
	 * @return the negocio
	 */
	public String getNegocio() {
		return negocio;
	}

	/**
	 * @param negocio the negocio to set
	 */
	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
}
