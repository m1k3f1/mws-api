/**
 * 
 */
package com.unixsycsapi.middleware.core.ds.base.impl;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;

/**
 * Implementación de los métodos CRUD de un DAO basado en JPA.
 * 
 * @author vladimir.aguirre.c
 *
 */
public class BaseGenericMWSDAOImpl<T, ID extends Serializable> implements GenericMWSIDAO<T, ID> {
	/**
	 * Para obtener la posici�n <b>cero</b> del arreglo
	 */
	protected final static int PRIMERA_POSICION = 0;

	/**
	 * Para comparar cuando el arreglo tiene un solo registro.
	 */
	protected final static int SIZE_UNA_POSICION = 1;

	/**
	 * Logger.
	 */
	private final static Logger LOG = LogManager.getLogger(BaseGenericMWSDAOImpl.class);
	/**
	 * Contexto de persistencia
	 */
	protected EntityManager em;

	/**
	 * Para trabajar con la clase parametrizada.
	 */
	private Class<T> type = null;

	@Override
	public void guardar(T entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Guardando ... " + entity);
		}
		this.em.persist(entity);
		this.em.flush();
	}

	@Override
	public void modificar(T entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Modificando ..." + entity);
		}
		this.em.merge(entity);

	}

	@Override
	public T consultar(ID id) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Consultando " + this.getType() + " con id " + id);
		}
		return this.em.find(this.getType(), id);
	}

	/**
	 * Obtiene por reflexion el tipo del DomainObject que usa este DAO
	 * 
	 * @return the type
	 */
	@SuppressWarnings("rawtypes")
	public Class<T> getType() {
		if (type == null) {
			Class clazz = getClass();

			while (!(clazz.getGenericSuperclass() instanceof ParameterizedType)) {
				clazz = clazz.getSuperclass();
			}

			type = (Class<T>) ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
		}

		return type;
	}

	@Override
	public List<T> consultarByProperties(String[] propNames, T filtro) {
		return ejecutarConsultaByProperties(propNames, filtro, null, false);
	}

	@Override
	public List<T> consultarByProperties(String[] propNames, T filtro, String campoOrd, boolean asc) {
		return this.ejecutarConsultaByProperties(propNames, filtro, campoOrd, asc);
	}

	/**
	 * 
	 * @param propName
	 * @param filtro
	 * @return
	 */
	protected List<T> consultarByPropertyLike(String propName, Object filtro) {
		final StringBuilder qry = new StringBuilder();
		qry.append("select obj");
		qry.append(" from ").append(getType().getSimpleName()).append(" as obj ");
		qry.append(" where ");
		qry.append("obj.");
		qry.append(propName);
		qry.append(" like :nombreParam");

		final Query query = this.em.createQuery(qry.toString());
		query.setParameter("nombreParam", filtro);
		final List<T> resp = query.getResultList();
		if (LOG.isDebugEnabled()) {
			LOG.debug("consultarByPropertyLike.resp.size() :: " + resp.size());
		}

		return resp;
	}

	/**
	 * 
	 * @param propName
	 * @param filtro
	 * @return
	 */
	protected T consultarByProperty(String propName, Object filtro) {

		List<T> resp = new ArrayList<T>();
		final StringBuilder qry = new StringBuilder();
		qry.append("select obj");
		qry.append(" from ").append(getType().getSimpleName()).append(" as obj ");
		qry.append(" where ");
		qry.append("obj.");
		qry.append(propName);
		qry.append(" = :nombreParam");

		final Query query = this.em.createQuery(qry.toString());
		query.setParameter("nombreParam", filtro);
		resp = query.getResultList();
		if (LOG.isDebugEnabled()) {
			LOG.debug("consultarByProperty.resp.size() :: " + resp.size());
		}
		if (resp.isEmpty()) {
			return null;
		}

		return resp.get(PRIMERA_POSICION);
	}

	/**
	 * Arma y ejecuta el query.
	 * 
	 * @param propNames
	 * @param filtro
	 * @param campoOrd
	 * @param asc
	 * @return
	 */
	private List<T> ejecutarConsultaByProperties(String[] propNames, T filtro, String campoOrd, boolean asc) {

		List<T> resp = new ArrayList<T>();

		try {
			final PropertyUtilsBean pubean = new PropertyUtilsBean();
			int pos = 0;
			String name = "";
			Object val = null;
			final StringBuilder qry = new StringBuilder();
			qry.append("select obj");
			qry.append(" from ").append(filtro.getClass().getSimpleName()).append(" as obj ");
			boolean first = true;
			if (propNames != null) {
				while (pos < propNames.length) {
					if (first) {
						qry.append(" where ");
						first = false;
					} else {
						qry.append(" and ");
					}
					name = propNames[pos];
					if (name.contains(".")) {
						val = pubean.getNestedProperty(filtro, name);
					} else {
						val = pubean.getProperty(filtro, name);

					}
					if (val != null) {
						qry.append(" obj.").append(name).append(" = :").append(name.replace(".", ""));
					} else {
						qry.append(" obj.").append(name).append(" is null");
					}
					name = propNames[0];
					pos++;
				}
			}
			addOrderBy(qry, campoOrd, asc);

			if (LOG.isDebugEnabled()) {
				LOG.debug("qry :: " + qry);
			}
			final Query query = this.em.createQuery(qry.toString());
			setParameters(propNames, filtro, pubean, query);

			resp = query.getResultList();
			if (LOG.isDebugEnabled()) {
				LOG.debug("findByProperties.resp.size() :: " + resp.size());
			}

		} catch (IllegalAccessException e) {
			LOG.error(e.getMessage(), e);
		} catch (SecurityException e) {
			LOG.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(), e);
		} catch (NoSuchMethodException e) {
			LOG.error(e.getMessage(), e);
		}

		return resp;
	}

	/**
	 * @param propNames
	 * @param filtro
	 * @param pub
	 * @param query
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	private void setParameters(String[] propNames, T filtro, final PropertyUtilsBean pub, final Query query)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		int pos;
		String name;
		Object val;
		pos = 0;
		if (propNames != null) {
			while (pos < propNames.length) {
				name = propNames[pos];
				if (name.contains(".")) {
					val = pub.getNestedProperty(filtro, name);
				} else {
					val = pub.getProperty(filtro, name);

				}
				if (val != null) {
					query.setParameter(name.replace(".", ""), val);
				}
				pos++;
			}
		}
	}

	/**
	 * Agrega la sentencia <code>order by</code> si <code>campoOrd</code> tiene
	 * valor.
	 * 
	 * @param qry
	 *            Query armando, el alias debe ser <code> <b>obj</b> </code>
	 * @param campoOrd
	 *            Campo para ordenar
	 * @param asc
	 *            true|false
	 */
	protected void addOrderBy(final StringBuilder qry, String campoOrd, boolean asc) {
		if (StringUtils.isNotBlank(campoOrd)) {
			qry.append(" order by obj.").append(campoOrd);
			if (asc) {
				qry.append(" ASC");
			} else {
				qry.append(" DESC");
			}
		}
	}

	/**
	 * 
	 * @param hql
	 * @return
	 */
	protected T ejecutarJPAQLUnique(String hql) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("hql :: " + hql);
		}
		final Query qry = this.em.createQuery(hql);
		return (T) qry.getSingleResult();
	}

	/**
	 * 
	 * @param hql
	 * @return
	 */
	protected List<T> ejecutarJPAQL(String hql) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("hql :: " + hql);
		}
		final Query qry = this.em.createQuery(hql);
		final List<T> result = (List<T>) qry.getResultList();
		if (LOG.isDebugEnabled()) {
			LOG.debug("result.size() :: " + result.size());
		}
		return result;
	}

	/**
	 * Para ejecutar una sentencia <code>COUNT</code>
	 * 
	 * @param hqlCount
	 * @return
	 */
	protected Long ejecutarJPAQLCount(StringBuilder hqlCount) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("hqlCount :: " + hqlCount);
		}
		final Query qry = this.em.createQuery(hqlCount.toString());
		return (Long) qry.getSingleResult();
	}

	/**
	 * 
	 * @param hql
	 * @return
	 */
	protected T ejecutarJPAQLUnique(StringBuilder hql) {
		return ejecutarJPAQLUnique(hql.toString());
	}

	/**
	 * 
	 * @param hql
	 * @return
	 */
	protected List<T> ejecutarJPAQL(StringBuilder hql) {
		return ejecutarJPAQL(hql.toString());
	}

	/**
	 * Este método se tiene que sobreescribir en cada <b>
	 * <code>extends</code> </b>
	 * 
	 * @param em
	 */
	public void setEm(EntityManager em) {
		this.em = em;
	}

	protected void flush() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Desalojando ...");
		}
		this.em.flush();
	}

	protected void desvincular(T obj) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Desvinculando ... " + obj);
		}
		this.em.detach(obj);

	}

	@Override
	public void eliminar(T entity) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Eliminando ... " + entity);
		}
		this.em.remove(entity);
	}
	
	protected void addLikeCondicion(StringBuilder qCad, String campo, String valor) {
		qCad.append("upper(obj.");
		qCad.append(campo);
		qCad.append(") like '%").append(valor.toUpperCase()).append("%'");
	}
	/**
	 * 
	 * @param qCad
	 * @param whereOK Bandera que indica que el <code>where</code> ha sido agregado.
	 * @return
	 */
	protected boolean addWhereAnd(final StringBuilder qCad, boolean whereOK) {
		if (whereOK) {
			qCad.append(" OR ");
		} else {
			qCad.append(" WHERE ");
			whereOK = true;
		}
		return whereOK;
	}
	
	public Long getSequenceNextVal(String secuence) {
		try {
			System.out.println("Entity manager: " + this.em);
			final Query query = this.em.createNativeQuery("select nextval('" + secuence + "')");
			BigInteger key = (BigInteger) query.getSingleResult();
			return key.longValue();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int resetSequence(String secuence) {
		final Query query = this.em.createNativeQuery("ALTER SEQUENCE " + secuence + " RESTART WITH 1");
		int reset = query.executeUpdate();
		return reset;
	}

}