/**
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase que contiene los datos para un estado que puede tener un
 * <code>Cliente</code>.
 * 
 * @see EstadosCliente
 * 
 * @author Vladimir Aguirre
 * 
 */
@Entity
@Table(name = "BANCO_CUENTA_REF")
public class BancoCuenta implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6359896617306339281L;

	/**
	 * Identificador unico en la base de datos para el estado de una tabla de
	 * negocio.
	 */
	@Id
	@SequenceGenerator(name = "BANCO_CUENTA_REF_ID_GENERATOR", sequenceName = "SEQ_BANCO_CUENTA_REF", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BANCO_CUENTA_REF_ID_GENERATOR")
	@Column(name = "ID_BANCO_CUENTA_REF")
	private Long id;

	/**
	 * Estado de negocio para la entidad. Descripción para el UNIXSYC y sus
	 * empresas que tienen un apikey
	 */
	@Column(name = "NOMBRE_BANCO", nullable = false, length = 120)
	private String nombre;
	/** Especifica si el estado de la entidad es activo o inactivo. */
	@Column(name = "IND_ACTIVO")
	private Boolean activo;

	@Column(name = "NUMERO", length = 20, nullable = false)
	private String numero;
	
	@Column(name = "CODIGO", length = 10, nullable = false)
	private String codigo;

	@Column(name = "DESCRIPCION", length = 240, nullable = true)
	private String descripcion;

	/** Constructor por defecto. */
	public BancoCuenta() {
		super();
	}

	/**
	 * 
	 * @param id
	 */
	public BancoCuenta(Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


}
