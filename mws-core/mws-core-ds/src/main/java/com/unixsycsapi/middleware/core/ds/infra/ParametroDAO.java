/**
 * 
 */
package com.unixsycsapi.middleware.core.ds.infra;

import java.util.List;

import javax.ejb.Local;

import com.unixsycsapi.framework.commons.exception.USException;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.entity.infra.Parametro;

/**
 * @author Vladimir Aguirre Piedragil
 *
 */
@Local
public interface ParametroDAO extends GenericMWSIDAO<Parametro, Long> {
	/**
	 * 
	 * @param claveParam
	 * @return
	 * @throws com.tattvait.vf.base.excepcion.VFException
	 * @throws TalentosException
	 */
	Parametro consultarPorClave(String claveParam) throws USException;

	List<Parametro> consultarCacheables();

	List<Parametro> consultarTodos();

	void desvincular(Parametro obj);

	@Override
	void modificar(Parametro aseg);

}
