/**
 * 
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Tarjeta creada en un <code>TirajeTarjeta</code>
 * 
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "TARJETA_DISPONIBLE")
public class TarjetaDisponible implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3933817120905961770L;

	@Id
	@SequenceGenerator(name = "TARJETA_DISPONIBLE_ID_GENERATOR", sequenceName = "SEQ_TARJETA_DISPONIBLE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TARJETA_DISPONIBLE_ID_GENERATOR")
	@Column(name = "ID_TARJETA_DISPONIBLE")
	private Long id;

	@Column(name = "NUMERO", length = 60, nullable = false)
	private String numero;
	
	@Column(name = "VENCIMIENTO", length = 4, nullable = false)
	private String vencimiento;

	@Column(name = "IND_ASIGNANDA", nullable = false)
	private Boolean asignada;

	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	@Column(name = "FECHA_ASIGNACION", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAsignacion;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIRAJE_TARJETA", referencedColumnName = "ID_TIRAJE_TARJETA", nullable = false)
	private TirajeTarjeta tiraje;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the fechaAsignacion
	 */
	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	/**
	 * @param fechaAsignacion
	 *            the fechaAsignacion to set
	 */
	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the asignada
	 */
	public Boolean getAsignada() {
		return asignada;
	}

	/**
	 * @param asignada the asignada to set
	 */
	public void setAsignada(Boolean asignada) {
		this.asignada = asignada;
	}

	/**
	 * @return the tiraje
	 */
	public TirajeTarjeta getTiraje() {
		return tiraje;
	}

	/**
	 * @param tiraje the tiraje to set
	 */
	public void setTiraje(TirajeTarjeta tiraje) {
		this.tiraje = tiraje;
	}

	/**
	 * @return the vencimiento
	 */
	public String getVencimiento() {
		return vencimiento;
	}

	/**
	 * @param vencimiento the vencimiento to set
	 */
	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}

}
