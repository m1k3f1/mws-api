/**
 * 
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unixsycsapi.middleware.core.entity.archivo.ArchivoCM;
import com.unixsycsapi.middleware.core.entity.estado.EstadoEnrolamiento;

/**
 * Entidad para respaldar el registro de un <code>Cliente</code>. Un
 * <code>Cliente</code> y/o una <code>Persona</code> pueden tener varios
 * procesos de enrolamiento, pero solo debe ser efectivo, los demás son intentos
 * fallidos
 * 
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "ENROLAMIENTO")
public class Enrolamiento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3803611899733672312L;

	@Id
	@SequenceGenerator(name = "ENROLAMIENTO_ID_GENERATOR", sequenceName = "SEQ_ENROLAMIENTO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ENROLAMIENTO_ID_GENERATOR")
	@Column(name = "ID_ENROLAMIENTO")
	private Long id;

	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	/**
	 * <code>Persona</code> que fue registrada en un proceso de enrolamiento
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID_PERSONA", nullable = true)
	private Persona persona;

	/**
	 * <code>Cliente</code> que fue registrada en un proceso de enrolamiento
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID_CLIENTE", nullable = true)
	private Cliente cliente;

	@Column(name = "F_SIMILITUD", length = 18, nullable = true)
	private String similitud;

	@Column(name = "F_CODIGO_VALIDACION", length = 20, nullable = true)
	private String codigoValidacion;

	@Column(name = "CANAL", length = 15, nullable = false)
	private String canal;

	@Column(name = "APLICACION", length = 15, nullable = true)
	private String aplicacion;

	@ManyToOne
	@JoinColumn(name = "ID_ESTADO", nullable = false)
	private EstadoEnrolamiento estado;

	@Column(name = "INTENTOS", nullable = false)
	private Integer intentos = new Integer(0);
	@Column(name = "CLAVE_ELECTOR", length = 18, nullable = true)
	private String claveElector;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ARCHIVO_CM_INE", referencedColumnName = "ID_ARCHIVO_CM", nullable = true)
	private ArchivoCM archivoINE;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ARCHIVO_CM_SELFIE", referencedColumnName = "ID_ARCHIVO_CM", nullable = true)
	private ArchivoCM archivoSelfie;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TARJETA_DISPONIBLE", referencedColumnName = "ID_TARJETA_DISPONIBLE", nullable = false)
	private TarjetaDisponible tarjeta;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the similitud
	 */
	public String getSimilitud() {
		return similitud;
	}

	/**
	 * @param similitud
	 *            the similitud to set
	 */
	public void setSimilitud(String similitud) {
		this.similitud = similitud;
	}

	/**
	 * @return the codigoValidacion
	 */
	public String getCodigoValidacion() {
		return codigoValidacion;
	}

	/**
	 * @param codigoValidacion
	 *            the codigoValidacion to set
	 */
	public void setCodigoValidacion(String codigoValidacion) {
		this.codigoValidacion = codigoValidacion;
	}

	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * @param canal
	 *            the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * @return the aplicacion
	 */
	public String getAplicacion() {
		return aplicacion;
	}

	/**
	 * @param aplicacion
	 *            the aplicacion to set
	 */
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}

	/**
	 * @return the estado
	 */
	public EstadoEnrolamiento getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(EstadoEnrolamiento estado) {
		this.estado = estado;
	}

	/**
	 * @return the intentos
	 */
	public Integer getIntentos() {
		return intentos;
	}

	/**
	 * @param intentos
	 *            the intentos to set
	 */
	public void setIntentos(Integer intentos) {
		this.intentos = intentos;
	}

	/**
	 * @return the persona
	 */
	public Persona getPersona() {
		return persona;
	}

	/**
	 * @param persona
	 *            the persona to set
	 */
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the claveElector
	 */
	public String getClaveElector() {
		return claveElector;
	}

	/**
	 * @param claveElector the claveElector to set
	 */
	public void setClaveElector(String claveElector) {
		this.claveElector = claveElector;
	}

	/**
	 * @return the archivoINE
	 */
	public ArchivoCM getArchivoINE() {
		return archivoINE;
	}

	/**
	 * @param archivoINE the archivoINE to set
	 */
	public void setArchivoINE(ArchivoCM archivoINE) {
		this.archivoINE = archivoINE;
	}

	/**
	 * @return the archivoSelfie
	 */
	public ArchivoCM getArchivoSelfie() {
		return archivoSelfie;
	}

	/**
	 * @param archivoSelfie the archivoSelfie to set
	 */
	public void setArchivoSelfie(ArchivoCM archivoSelfie) {
		this.archivoSelfie = archivoSelfie;
	}

	/**
	 * @return the tarjeta
	 */
	public TarjetaDisponible getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(TarjetaDisponible tarjeta) {
		this.tarjeta = tarjeta;
	}

}
