/**
 *
 * UNIX. SYC. S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX. SYC. S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.ds.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.unixsycsapi.middleware.core.ds.EnrolamientoDAO;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.entity.Enrolamiento;

/**
 * @author Vladimir Aguirre
 *
 */
@Stateless
public class EnrolamientoDAOBean extends BaseGenericMWSDAOImpl<Enrolamiento, Long> implements EnrolamientoDAO {

	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}
}
