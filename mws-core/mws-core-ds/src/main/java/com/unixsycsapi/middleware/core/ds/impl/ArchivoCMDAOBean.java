/**
 * 
 */
package com.unixsycsapi.middleware.core.ds.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.unixsycsapi.middleware.core.ds.ArchivoCMDAO;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.ds.base.impl.BaseGenericMWSDAOImpl;
import com.unixsycsapi.middleware.core.entity.archivo.ArchivoCM;

/**
 * @author jesus
 *
 */
@Stateless
public class ArchivoCMDAOBean extends BaseGenericMWSDAOImpl<ArchivoCM, Long> implements ArchivoCMDAO{

	private final static Logger LOG = LogManager.getLogger(ArchivoCMDAOBean.class);

	@PersistenceContext(name = GenericMWSIDAO.MWS_PERSISTENCE_UNIT)
	public void setEm(EntityManager em) {
		super.setEm(em);
	}
}
