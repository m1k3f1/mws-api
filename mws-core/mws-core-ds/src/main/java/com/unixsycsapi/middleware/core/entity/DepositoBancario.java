/**
 * 
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unixsycsapi.middleware.core.entity.estado.EstadoTransaccion;

/**
 * Entidad que representa un deposito bancario realizado en ventanilla/cajero
 * 
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "DEPOSITO_BANCARIO")
public class DepositoBancario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -914626975454662754L;

	@Id
	@SequenceGenerator(name = "DEPOSITO_BANCARIO_ID_GENERATOR", sequenceName = "SEQ_DEPOSITO_BANCARIO", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPOSITO_BANCARIO_ID_GENERATOR")
	@Column(name = "ID_DEPOSITO_BANCARIO")
	private Long id;

	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	/**
	 * Punto en el tiempo en que se realiza el deposito en el cajero.
	 */
	@Column(name = "FECHA_DEPOSITO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaDeposito;

	@ManyToOne
	@JoinColumn(name = "ID_ESTADO_TRANSACCION", nullable = false)
	private EstadoTransaccion estado;

	@ManyToOne
	@JoinColumn(name = "ID_TARJETA_VIRTUAL", nullable = false)
	private TarjetaVirtual tarjeta;

	@Column(name = "MONTO", nullable = false,  columnDefinition="BIGINT")
	private BigInteger monto;

	/**
	 * Cuenta o tarjeta
	 */
	@Column(name = "CUENTA", length = 20, nullable = true)
	private String cuenta;
	/**
	 * Motivo o concepto
	 */
	@Column(name = "MOTIVO", length = 150, nullable = true)
	private String motivo;
	/**
	 * Autorizacion/referencia
	 */
	@Column(name = "AUTORIZACION", length = 20, nullable = true)
	private String autorizacion;
	/**
	 * Si existe el folio/movimiento se guarda
	 */
	@Column(name = "FOLIO", length = 20, nullable = true)
	private String folio;
	@ManyToOne
	@JoinColumn(name = "ID_BANCO_CUENTA_REF", nullable = false)
	private BancoCuenta banco;

	public DepositoBancario() {
		super();
	}

	public DepositoBancario(Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the fechaDeposito
	 */
	public Date getFechaDeposito() {
		return fechaDeposito;
	}

	/**
	 * @param fechaDeposito
	 *            the fechaDeposito to set
	 */
	public void setFechaDeposito(Date fechaDeposito) {
		this.fechaDeposito = fechaDeposito;
	}

	/**
	 * @return the estado
	 */
	public EstadoTransaccion getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(EstadoTransaccion estado) {
		this.estado = estado;
	}

	/**
	 * @return the tarjeta
	 */
	public TarjetaVirtual getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta
	 *            the tarjeta to set
	 */
	public void setTarjeta(TarjetaVirtual tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the monto
	 */
	public BigInteger getMonto() {
		return monto;
	}

	/**
	 * @param monto
	 *            the monto to set
	 */
	public void setMonto(BigInteger monto) {
		this.monto = monto;
	}

	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta
	 *            the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * @return the motivo
	 */
	public String getMotivo() {
		return motivo;
	}

	/**
	 * @param motivo
	 *            the motivo to set
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	/**
	 * @return the autorizacion
	 */
	public String getAutorizacion() {
		return autorizacion;
	}

	/**
	 * @param autorizacion
	 *            the autorizacion to set
	 */
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio
	 *            the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * @return the banco
	 */
	public BancoCuenta getBanco() {
		return banco;
	}

	/**
	 * @param banco
	 *            the banco to set
	 */
	public void setBanco(BancoCuenta banco) {
		this.banco = banco;
	}

}
