/**
 * 
 */
package com.unixsycsapi.middleware.core.ds;

import javax.ejb.Local;

import com.unixsycsapi.framework.commons.exception.InfraestructuraException;
import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.entity.ApiKey;

/**
 * @author Vladimir Aguirre
 *
 */
@Local
public interface ApiKeyDAO  extends GenericMWSIDAO<ApiKey, Long> {

	ApiKey findByKey(String key);
	
	Long getIdEmpresa(String key) throws InfraestructuraException;
	ApiKey loginEmpresa(String key, String equipo) throws InfraestructuraException;

}
