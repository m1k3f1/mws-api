/**
 *
 * UNIX SYC S.A.P.I. DE C.V.
 *
 * http://unixsycsapi.com.mx/
 * __________________
 * 
 *  [2018] UNIX SYC S.A.P.I. DE C.V.  
 *  All Rights Reserved.
 * 
 */
package com.unixsycsapi.middleware.core.ds;

import javax.ejb.Local;

import com.unixsycsapi.middleware.core.ds.base.GenericMWSIDAO;
import com.unixsycsapi.middleware.core.entity.Celular;

/**
 * @author Vladimir Aguirre
 *
 */
@Local
public interface CelularDAO extends GenericMWSIDAO<Celular, Long> {

}
