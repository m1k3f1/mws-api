/**
 * 
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.unixsycsapi.middleware.core.entity.estado.EstadoTarjeta;

/**
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "TARJETA_VIRTUAL")
public class TarjetaVirtual implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7002183457518238740L;

	@Id
	@SequenceGenerator(name = "TARJETA_VIRTUAL_ID_GENERATOR", sequenceName = "SEQ_TARJETA_VIRTUAL", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TARJETA_VIRTUAL_ID_GENERATOR")
	@Column(name = "ID_TARJETA_VIRTUAL")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID_CLIENTE", nullable = false)
	private Cliente cliente;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID_ESTADO_TARJETA", nullable = false)
	private EstadoTarjeta estado;
	/**
	 * Referencia de BPC
	 */
	@Column(name = "NUMERO_ENMASCARADO", length = 19, nullable = false)
	private String numeroEnmascarado;
	/**
	 * Referencia de BPC
	 */
	@Column(name = "CARD_ID", length = 11, nullable = true)
	private String cardId;

	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TARJETA_DISPONIBLE", referencedColumnName = "ID_TARJETA_DISPONIBLE", nullable = false)
	private TarjetaDisponible origen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CUENTA", referencedColumnName = "ID_CUENTA", nullable = true)
	private Cuenta cuenta;

	
	
	
	
	public TarjetaVirtual() {
		super();
	}

	public TarjetaVirtual(Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the numeroEnmascarado
	 */
	public String getNumeroEnmascarado() {
		return numeroEnmascarado;
	}

	/**
	 * @param numeroEnmascarado
	 *            the numeroEnmascarado to set
	 */
	public void setNumeroEnmascarado(String numeroEnmascarado) {
		this.numeroEnmascarado = numeroEnmascarado;
	}

	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}

	/**
	 * @param cardId
	 *            the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the origen
	 */
	public TarjetaDisponible getOrigen() {
		return origen;
	}

	/**
	 * @param origen
	 *            the origen to set
	 */
	public void setOrigen(TarjetaDisponible origen) {
		this.origen = origen;
	}

	/**
	 * @return the estado
	 */
	public EstadoTarjeta getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(EstadoTarjeta estado) {
		this.estado = estado;
	}

	/**
	 * @return the cuenta
	 */
	public Cuenta getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}
}
