/**
 * 
 */
package com.unixsycsapi.middleware.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author vladimir.aguirre
 *
 */
@Entity
@Table(name = "EMPRESA")
public class Empresa implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6187553499557161333L;

	@Id
	@SequenceGenerator(name = "EMPRESA_ID_GENERATOR", sequenceName = "SEQ_EMPRESA", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPRESA_ID_GENERATOR")
	@Column(name = "ID_EMPRESA")
	private Long id;

	/**
	 * Punto en el tiempo en que se regitra.
	 */
	@Column(name = "FECHA_REGISTRO", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	@Column(name = "NOMBRE", length = 150, nullable = false)
	private String nombre;

	@Column(name = "RFC", length = 13, nullable = false)
	private String rfc;

	/** Especifica si el estado de la entidad es activo o inactivo. */
	@Column(name = "IND_ACTIVO", nullable = false)
	private Boolean activo;

	@Column(name = "CUENTA", nullable = true)
	private String cuenta;

	/** Especifica si el estado de la entidad es activo o inactivo. */
	@Column(name = "IND_PROPIETARIO", nullable = false)
	private Boolean propietario;

	/**
	 * Para las aclaraciones
	 */
	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	private List<ApiKey> apiKeys = new ArrayList<ApiKey>();

	public Empresa() {
		super();
	}

	public Empresa(Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fechaRegistro
	 */
	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	/**
	 * @param fechaRegistro
	 *            the fechaRegistro to set
	 */
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc
	 *            the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo
	 *            the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta
	 *            the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * @return the apiKeys
	 */
	public List<ApiKey> getApiKeys() {
		return apiKeys;
	}

	/**
	 * @param apiKeys
	 *            the apiKeys to set
	 */
	public void setApiKeys(List<ApiKey> apiKeys) {
		this.apiKeys = apiKeys;
	}

	/**
	 * @return the propietario
	 */
	public Boolean getPropietario() {
		return propietario;
	}

	/**
	 * @param propietario the propietario to set
	 */
	public void setPropietario(Boolean propietario) {
		this.propietario = propietario;
	}
}
